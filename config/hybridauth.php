<?php
use Cake\Core\Configure;

return [
    'HybridAuth' => [
        'providers' => [
            'Google' => [
                'enabled' => true,
                'keys' => [
                    'id' => GOOGLE_CLIENT_ID,
                    'secret' => GOOGLE_SECRET_KEY
                ]
            ],
            'Facebook' => [
                'enabled' => true,
                'keys' => [
                    'id' => FACEBOOK_APPID,
                    'secret' => FACEBOOK_APP_SECRET
                ],
                'scope' => 'email, user_about_me, user_birthday, user_hometown', 'phone'
            ]
        ],
        'debug_mode' => Configure::read('debug'),
        'debug_file' => LOGS . 'hybridauth.log',
    ]
];