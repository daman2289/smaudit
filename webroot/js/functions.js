function hasClass(el, className) {
    if (el.classList)
        return el.classList.contains(className)
    else
    return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
}

function addClass(el, className) {
    $(ele).removeClass(className);
}

function removeClass(el, className) {
    $(ele).removeClass(className);
}

function disableHidden() {
    $('.childs').each(function(i, element){
        if($(element).hasClass('hide')) {
            $(element).find('input, select').each(function(j, e) {
                $(e).attr('disabled', true);
            });
        }else {
            $(element).find('input, select').each(function(j, e) {
                $(e).attr('disabled', false);
            });
        }
    });
}

function manageEditVisibility() {
    $type = $("select[name='new_placeholder_type_id']").val();
    if($type) {
        showInputForPlaceholder($type);
    }
}

function combination (arr) {
    let i, j, temp
    let result = []
    let arrLen = arr.length
    let power = Math.pow
    let combinations = power(2, arrLen)  
  // Time & Space Complexity O (n * 2^n)  
    for (i = 0; i < combinations;  i++) {
        temp = '';    
        for (j = 0; j < arrLen; j++) {
            // & is bitwise AND
            if ((i & power(2, j))) {
                temp += arr[j]
            }
        }
        result.push(temp)
    }
    return result
}

function radioChild(ele) {
    selectedOptions = $(ele).val();
    // console.log(ele);
    questionBlock = $(ele).closest('div.question-block');
    // hidde all the child question
    $(questionBlock).find('.childs').addClass('hide');
    if(selectedOptions) {
    console.log(selectedOptions)
        // show the child question for selected option
        removeHideFromChild('.child'+selectedOptions);
        // find the value of child question selected radio
        // find all the inputs in child queston and if selected option find call the corresponding 
        // function for further dispaly/hide processing
        // var cele = ($('#child'+selectedOptions).find('input[type="radio"]:checked'));
       
        var cele = ($('.child'+selectedOptions).find('input')).each(function(index, element) {
            var type = $(element).attr('type');
            if(type == 'radio') {           
                if($(element).prop('checked') && !($('#child'+selectedOptions).attr('data-last'))) {
                    radioChild(element);
                }
            }else if(type == 'checkbox') {
                // checkboxChild(element);
            }
        });

        var cele = ($('.child'+selectedOptions).find('select')).each(function(index, element) {
            var type = $(element).attr('multiple');
            if(type) {           
                if(!($('.child'+selectedOptions).attr('data-last'))) {
                    multiSelectChild(element);
                }
            }else {
                selectChid(element);
            }
        });
       
        // console.log(cele)
        // if seleted option found check it further child exit if yes than call again this function for further processing
        // if(cele.length != 0 && !($('#child'+selectedOptions).attr('data-last'))) {
        //     radioChild(cele);
        // }
        $('.child'+selectedOptions).parent('.childs').removeClass('hide');
    }
    disableHidden();
}

function removeHideFromChild(ele_class) {
    $(ele_class).each(function(index, element) {
        $(element).removeClass('hide')
    });
}

function checkboxChild(ele) {
    selectedOptions = $(ele).val();
    questionBlock = $(ele).closest('div.question-block');
    $(questionBlock).find('.childs').addClass('hide');
    if($(ele).is(':checked')) {
        // $('#child'+selectedOptions).removeClass('hide');

        removeHideFromChild('.child'+selectedOptions);
    }
    disableHidden();
}

function selectChid(ele) {
    selectedOptions = $(ele).val();
    questionBlock = $(ele).closest('div.question-block');
    $(questionBlock).find('.childs').addClass('hide');
    console.log(selectedOptions)
    if(selectedOptions) {
        // $('#child'+selectedOptions).removeClass('hide');
        removeHideFromChild('.child'+selectedOptions);

        var cele = ($('.child'+selectedOptions).find('input')).each(function(index, element) {
            var type = $(element).attr('type');
            if(type == 'radio') {           
                if($(element).prop('checked') && !($('#child'+selectedOptions).attr('data-last'))) {
                    radioChild(element);
                }
            }else if(type == 'checkbox') {
                // checkboxChild(element);
            }
        });

        var cele = ($('.child'+selectedOptions).find('select')).each(function(index, element) {
            var type = $(element).attr('multiple');
            if(type) {           
                if( !($('#child'+selectedOptions).attr('data-last'))) {
                    multiSelectChild(element);
                }
            }else {
                selectChid(element);
            }
        });
    }
    disableHidden();
}

function multiSelectChild(ele) {
    selectedOptions = $(ele).val();
    questionBlock = $(ele).closest('div.question-block');
    $(questionBlock).find('.childs').addClass('hide');
    if(selectedOptions) {
        allPossibleCombinations = combination(selectedOptions);
        for (var option of allPossibleCombinations) {
            // $('#child'+option).removeClass('hide');
            removeHideFromChild('.child'+option);
        }
    }
    disableHidden();
}

function removeOptionAndButtonBlock(){
    var optionBlock = document.getElementById('option-block-body');
    var buttonBlock = document.getElementById('option-add-new-button');
    buttonBlock.innerHTML = ''; 
    optionBlock.innerHTML = '';  
}

function removeOption(ele) {
    // $(ele).closest('.row').addClass('hide');
    $(ele).closest('.row').remove();
} 

function showInputForPlaceholder(type) {
    $('#system-block').addClass('hide');
    $('#normal-block').addClass('hide');
    $('#question-block').addClass('hide');
    $('#text-block').addClass('hide');
    $('#legal-form-block').addClass('hide');
    $('#signature-gender-form-block').addClass('hide');
    $('#system-block, #normal-block, #question-block, #text-block, #legal-form-block, #signature-gender-form-block').find('input, select, textarea').each(function(){
        $(this).attr('disabled', true)
    });
    removeOptionAndButtonBlock();
    if(type == 1){
        $('#system-block').removeClass('hide');
        $('#system-block').find('input, select, textarea').each(function(){
            $(this).attr('disabled', false)
        });
    } else if(type == 2) {
        $('#normal-block').removeClass('hide');
        $('#normal-block').find('input, select, textarea').each(function(){
            $(this).attr('disabled', false)
        })
    } else if (type == 5) {
        $('#text-block').removeClass('hide');
        $('#text-block').find('input, select, textarea').each(function(){
            $(this).attr('disabled', false)
        })
    } else if (type == 6) {
        $('#legal-form-block').removeClass('hide');
        $('#legal-form-block').find('input, select, textarea').each(function(){
            $(this).attr('disabled', false)
        })
    } else if (type == 7) {
        $('#signature-gender-form-block').removeClass('hide');
        $('#signature-gender-form-block').find('input, select, textarea').each(function(){
            $(this).attr('disabled', false)
        })
    }else {
        $('#question-block').removeClass('hide');
        $('#question-block').find('input, select, textarea').each(function(){
            $(this).attr('disabled', false)
        })
    }
}

function radioTemplate(label=null, q = 'new_questions', is_removeble = '') {
    if ('content' in document.createElement('template')) {
        var count = document.getElementById('option-block-body').childElementCount;
        var optionBlock = document.getElementById('option-block-body');    
        // Radio template  
        var tmpl = document.getElementById('option-radio').content.cloneNode(true);
        if(is_removeble) {
            $(tmpl).find('.remove').removeClass('hide');
        }

        tmpl.querySelector('.radio-option-label').value = label;
        tmpl.querySelector('.radio-option-label').name = q+'[0][new_question_options]['+count+'][name_en]';
        tmpl.querySelector('.radio-option-content-en').name = q+'[0][new_question_options]['+count+'][content_en]';
        tmpl.querySelector('.radio-option-content-er').name = q+'[0][new_question_options]['+count+'][content_er]';
        tmpl.querySelector('.radio-option-content-fr').name = q+'[0][new_question_options]['+count+'][content_fr]';
        optionBlock.appendChild(tmpl);
    } else {
      // Templates are not supported.
    }
}

function selectTemplate(label=null, q = 'new_questions', is_removeble = '') {
    if ('content' in document.createElement('template')) {
        var count = document.getElementById('option-block-body').childElementCount;
        var optionBlock = document.getElementById('option-block-body'); 
        //get the select template     
        var tmpl = document.getElementById('option-select').content.cloneNode(true);
        if(is_removeble) {
            $(tmpl).find('.remove').removeClass('hide');
        }
        // tmpl.querySelector('.remove').innerHTML = 'Remove It';
        tmpl.querySelector('.select-option-label').value = label;
        tmpl.querySelector('.select-option-label').name = q+'[0][new_question_options]['+count+'][name_en]';
        tmpl.querySelector('.select-option-content-en').name = q+'[0][new_question_options]['+count+'][content_en]';
        tmpl.querySelector('.select-option-content-er').name = q+'[0][new_question_options]['+count+'][content_er]';
        tmpl.querySelector('.select-option-content-fr').name = q+'[0][new_question_options]['+count+'][content_fr]';
        optionBlock.appendChild(tmpl);
    } else {
      // Templates are not supported.
    }
}

function multiSelectTemplate(label=null, q = 'new_questions', is_removeble = '') {
    if ('content' in document.createElement('template')) {
        var count = document.getElementById('option-block-body').childElementCount;
        var optionBlock = document.getElementById('option-block-body'); 
        //get the select template     
        var tmpl = document.getElementById('option-multiselect').content.cloneNode(true);
        if(is_removeble) {
            $(tmpl).find('.remove').removeClass('hide');
        }
        tmpl.querySelector('.multiselect-option-label').value = label;
        tmpl.querySelector('.multiselect-option-label').name = q+'[0][new_question_options]['+count+'][name_en]';
        tmpl.querySelector('.multiselect-option-content-en').name = q+'[0][new_question_options]['+count+'][content_en]';
        tmpl.querySelector('.multiselect-option-content-er').name = q+'[0][new_question_options]['+count+'][content_er]';
        tmpl.querySelector('.multiselect-option-content-fr').name = q+'[0][new_question_options]['+count+'][content_fr]';
        optionBlock.appendChild(tmpl);
    } else {
      // Templates are not supported.
    }
}

function checkboxTemplate(q= 'new_questions') {
    if ('content' in document.createElement('template')) {
        var count = document.getElementById('option-block-body').childElementCount;
        var optionBlock = document.getElementById('option-block-body'); 
        //get the checkbox template     
        var tmpl = document.getElementById('option-checkbox').content.cloneNode(true);
        $(tmpl).find('input').each(function(){
            var ename = $(this).prop('name');
            $(this).prop('name', q+ename);
        });
        console.log(tmpl)
        optionBlock.appendChild(tmpl);
    } else {
      // Templates are not supported.
    }
}

function signatureTemplate(label= null) {
    if ('content' in document.createElement('template')) {
        var count = document.getElementById('option-block-body').childElementCount;
        var optionBlock = document.getElementById('option-block-body'); 
        //get the checkbox template     
        var tmpl = document.getElementById('signature-subquestion').content.cloneNode(true);
        optionBlock.appendChild(tmpl);
    } else {
      // Templates are not supported.
    }
}

function createButton(callback) {
    var optionBlock = document.getElementById('option-add-new-button');
    optionBlock.innerHTML = '';
    var tmpl = document.getElementById('add-button').content.cloneNode(true);
    tmpl.querySelector('button').setAttribute('onclick', callback);
    optionBlock.appendChild(tmpl);
}

function isPlacholderNameNeeded(type) {
    if(type == 9 || type == 10 || type == 11){
        $('#placeholder-input').addClass('hide');
        $('#placeholder-input').find('input, select').each(function(){
            $(this).attr('disabled', true);
        });
    } else {
        $('#placeholder-input').removeClass('hide');
        $('#placeholder-input').find('input, select').each(function(){
            $(this).attr('disabled', false);
        });
    }
}

function isQuestionNeedLabel(type) {
    if(type == 11 || type == 10){
        $('#question-input').addClass('hide');
        $('#question-input').find('input, select').each(function(){
            $(this).attr('disabled', true);
        });
    } else {
        $('#question-input').removeClass('hide');
        $('#question-input').find('input, select').each(function(){
            $(this).attr('disabled', false);
        });
    }
}

function placeOptionForQuestion (type, q = 'new_questions') {    
    removeOptionAndButtonBlock();
    if( type == 1) { // text type question

    } else if(type == 2) { // radio type question
        radioTemplate('Yes', q);
        radioTemplate('No', q);
        createButton('radioTemplate(null,"'+q+'", 1)');
    } else if(type == 3) { // multiselect type question
        multiSelectTemplate(null, q);
        createButton('multiSelectTemplate(null,"'+q+'", 1)');
    } else if(type == 4) { // checkbox type question
        checkboxTemplate(q);
    } else if (type == 5) { // select type question
        selectTemplate(null, q);
        createButton('selectTemplate(null,"'+q+'", 1)');
    } else if(type == 11) {

    } else if(type == 10){
        signatureTemplate(null)
    }else { // date type question

    }
    isPlacholderNameNeeded(type);
    isQuestionNeedLabel(type);
}

$( ".tag-tagname" ).on( "keydown", function( event ) {
    if ( event.keyCode === $.ui.keyCode.TAB &&
       $( this ).autocomplete( "instance" ).menu.active ) {
       event.preventDefault();
    }
}).autocomplete({
    source: function( request, response ) {
        $.getJSON("/Reports/getCompanies", {
           term:  request.term 
        }, response );
    },
    search: function() {
       // custom minLength
        var term = this.value ;
        if ( term.length < 2 ) {
           return false;
        }
    },
    focus: function() {
       // prevent value inserted on focus
       return false;
    },
    select: function( event, ui ) {
       this.value = ui.item.value;
       $('#company-id').val(ui.item.id);
       return false;
    }
});

$('.multiple-select').multiselect({
     selectAllValue: 'multiselect-all',
     enableCaseInsensitiveFiltering: true,
     enableFiltering: true,
     maxHeight: '300',
     buttonWidth: '235',
     onChange: function(element, checked) {
         var brands = $('#emphasis option:selected');
         var selected = [];
         var concern = false;
         var concern2 = false;
        $(brands).each(function(index, brand){
            selected.push($(this).val());
        });
        
        if(selected.includes('1'))
        {
            $('.Subordination').removeClass('hide');
        }
        else{
             $('.Subordination').addClass('hide');
        }
        if( selected.includes('7')  )
        {
            $('.Shareholders').removeClass('hide');
        }
        else{
            $('.Shareholders').addClass('hide');
        }

        if( selected.includes('2') )
        {
            concern = true;
        }
        if( selected.includes('3') )
        {
            concern2 = true;
        }                
        if(concern || concern2)
        {
            $('.Concern').removeClass('hide');
        }else{
            $('.Concern').addClass('hide');                   
        }
    }
    
});

$(document).ready(function(){
    disableHidden();
});