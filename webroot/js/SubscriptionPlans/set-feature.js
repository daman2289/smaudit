(function(){
	$(document).on('click', '.prmission_switch', function() {    	  	
    	var url = $(this).attr('data-url');
    	$.ajax({
            type:"get",
            url:url, 
            success: function(data){
            	
            }
        })
    });
    $(".switch_custom").on("change",function(){
      if ($(this).is(':checked')) {
        var url = $(this).attr('data-url')+"/1";
      } else {
        var url = $(this).attr('data-url')+"/0";
      }
      console.log(url);
    $.ajax({
               type:"GET",
               url : url,   
               success: function(data){
                   if(data == 0){
                    var msg = 'deactivated'
                }else {
                    var msg = "Activated"
                }
                    $('.main-success-box').remove();
                   $('.content-wrapper').prepend('<div class="message success alert alert-success main-success-box" onclick="this.classList.add('+"hidden"+')">Service has been '+msg+'</div>');
                  
               },
               
      });
  });
  $('.edit-plan').on('click',function() {
        var url = $(this).attr('data-url');
        $.ajax({
                type:"GET",
                url:url,        
                success: function(data){
                    console.log(data);
                    $('#edit-plan').find('.modal-body').html(data);
                    $('#edit-plan').modal('show');
                    $('#edit-plan-form').validate({
                       rules: {
                            'title': {
                                required: true,  
                            },
                            'trial_days': {
                                required: true, 
                                min: 1,
                                max:30 
                            }
                        }, 
                        messages: {
                            
                        }
                    });
                }
            });
    });    
})();