(function () {
    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-zA-ZüöäÜÖÄß\s]+$/i.test(value);
    }, "Enter Letters only please");
    
    $('.edit-profile').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
                type:"GET",
                url:url,        
                success: function(data){
                    console.log(data);
                    $('#organization').find('.modal-body').html(data);
                    $('#organization').modal('show');
                    $("#user-phone").mask("+41 (0) 99 999 99 99");
                    $("#user-fax").mask("+41 (0) 99 999 99 99");
                    $('#organization-profile').validate({
                        rules: {
                            'organization[name]': {
                                'required': true
                            },
                            'first_name': {
                                'required': true,
                                'lettersonly' : true
                            },
                            'last_name': {
                                'required': true,
                                'lettersonly' : true
                            },
                            'organization[fax]' :{
                                required : true
                            },
                            'organization[phone]' : {
                                required : true
                            },
                            'organization[street]' :{
                                required : true
                            },
                            'organization[street_number]' : {
                                required : true
                            },
                            'organization[zip]' : {
                                required : true
                            },
                            'organization[place]' :{
                                required : true
                            },
                            'organization[logo]' : {
                                extension : 'png|jpg|jpeg|gif'
                            },
                            'profile_pic' : {
                                extension : 'png|jpg|jpeg|gif'
                            }
                        },
                        messages: {
                            'organization[name]': {
                                'required': "Enter the organization name"
                            },
                            'first_name': {
                                'required': "Enter the first name",
                                'lettersonly' : "Enter letters only"
                            },
                            'last_name': {
                                'required': "Enter the last  name",
                                'lettersonly' : "Enters letters only"
                            },
                            'organization[fax]' :{
                                required : "Enter the fax number"
                            },
                            'organization[phone]' : {
                                required : "Phone number must not be empty"
                            },
                            'organization[street]' :{
                                required : "Organization street must not be empty"
                            },
                            'organization[street_number]' : {
                                required : "Organization street numbr must not be empty"
                            },
                            'organization[zip]' : {
                                required : "Organization zipcode must not be empty"
                            },
                            'organization[place]' :{
                                required : "Organization place must not be empty"
                            },
                            'organization[logo]' : {
                                required : "Select the organization logo",
                                extension : "Please Select valid image format"
                            },
                            'profile_pic' : {
                                extension : "Please Select valid image format"
                            }
                        }
                    });
                },
               
            });
    });

})();