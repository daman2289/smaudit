(function () {
    $('#signup').validate({
        rules: {
            'email': {
                'required': true,
                'email': true
            },
            'password': {
                'required': true,
                'minlength': 6
            },
            'confirm_password': {
                'required': true,
                'equalTo': '#password'
            },
            'organization[name]' :{
                required : true,
            },
            'organization[phone]' : {
                required : true
            },
            'language' : {
                required : true
            },
            'terms' : {
                required : true
            }
        },
        messages: {
            'email': {
                'required': "Enter the Email",
                'email': "Enter the correct format of the Email"
            },
            'password': {
                'required': "Enter the password",
                'minlength': "Password must be of atleast 6 characters"
            },
            'confirm_password': {
                'required': "Enter the confirm password",
                'equalTo': 'Password and confirm password do not match'
            },
            'organization[name]' :{
                required : "Enter the organization name",
            },
            'organization[phone]' : {
                required : "Enter the phone number of organization"
            },
            'language' : {
                required : "Select the language"
            },
            'terms' : {
                required : "Accept the terms and conditions"
            }
        },
        errorPlacement: function(error, element) {
              if (element.attr("name") == "terms" )
                   error.insertAfter($(element).closest('label'));                            
               
               else
                   error.insertAfter(element);
           }
    });
    $('#login').validate({
        rules: {
            'email': {
                'required': true,
                'email': true
            },
            'password': {
                'required': true,
            }
        },
        messages: {
            'email': {
                'required': "Enter the Email",
                'email': "Enter the correct format of the Email"
            },
            'password': {
                'required': "Enter the password"
            }
        }
    });
    $('#forgot-password').validate({
        rules: {
            'email': {
                'required': true,
                'email': true
            }
        },
        messages: {
            'email': {
                'required': "Enter the Email",
                'email': "Enter the correct format of the Email"
            }
        }
    });
    $('#forgot-password').validate({
        rules: {
            'email': {
                'required': true,
                'email': true
            }
        },
        messages: {
            'email': {
                'required': "Enter the Email",
                'email': "Enter the correct format of the Email"
            }
        }

    });
    $('#recover-password').validate({
        rules: {
            'password': {
                'required': true
            },
            'new_password' : {
                required : true,
                'equalTo': '#password'
            }
        },
        messages: {
            'password': {
                'required': "Enter the Password"
            },
            'new_password' : {
                required : "Enter the new password",
                equalTo : "Password and new Password do no match"
            }
        }
        
    });

    $("#register-phone").mask("+41 (9) 99 999 99 99");

    $('#upload-organization-user').validate({
        rules: {
            'user': {
                'required': true,
                'extension' : 'csv'
            },
        },
        messages: {
            'user': {
                'required': 'Select the file',
                'extension': 'upload only csv files'
            }
        }
    });
})();