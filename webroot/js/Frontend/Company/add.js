(function () {
     jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-zA-ZüöäÜÖÄß\s]+$/i.test(value);
    }, "Enter Letters only please");

    $('#add-company-form').validate({
        rules: {
            'company_name': {
                required: true,
                lettersonly:true
            },
            'street' :{
                required : true
            },
            'street_number' : {
                required : true
            },
            'zip' : {
                required : true,
                maxlength : 5
            },
            'place' :{
                required : true
            },
            'place_of_incorporation' :{
                required : true
            }
        },
        messages: {
            'name': {
                'required': "Enter the organization company name"
            },
            'street' :{
                required : "Organization company street must not be empty"
            },
            'street_number' : {
                required : "Organization street company number must not be empty"
            },
            'zip' : {
                required : "Organization company zipcode must not be empty",
                maxlength : "Zipcode must not be greater than 5 digits"
            },
            'place' :{
                required : "Organization company place must not be empty"
            },
            'place_of_incorporation' : {
                required : "Place of Incorporation must not  be empty"
            }
        }
    });
  $('.edit-company').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
                type:"GET",
                url:url,        
                success: function(data){
                    console.log(data);
                    $('#editCompany').find('.modal-body').html(data);
                    $('#editCompany').modal('show');
                    $('#edit-company-form').validate({
                        rules: {
                            'company_name': {
                                'required': true
                            },
                            'street' :{
                                required : true
                            },
                            'street_number' : {
                                required : true
                            },
                            'zip' : {
                                required : true,
                                maxlength : 5
                            },
                            'place' :{
                                required : true
                            },
                            'place_of_incorporation' :{
                                required : true
                            }
                        },
                        messages: {
                            'name': {
                                'required': "Enter the organization company name"
                            },
                            'street' :{
                                required : "Organization company street must not be empty"
                            },
                            'street_number' : {
                                required : "Organization street company number must not be empty"
                            },
                            'zip' : {
                                required : "Organization company zipcode must not be empty",
                                maxlength : "Zipcode must not be greater than 5 digits"
                            },
                            'place' :{
                                required : "Organization company place must not be empty"
                            },
                            'place_of_incorporation' : {
                                required : "Place of Incorporation must not  be empty"
                            }
                        }
                    });
                 },
                               
            });
    });
  $(".switch_custom").on("change",function(){
    if ($(this).is(':checked')) {
      var url = $(this).attr('data-url')+"/1";
      $(this).closest('tr').removeClass('active-tr');
    } else {
      var url = $(this).attr('data-url')+"/0";
      $(this).closest('tr').addClass('active-tr');
    }
    $.ajax({
               type:"GET",
               url : url,   
               success: function(data){
                if(data == 0){
                    var msg = 'deactivated'
                }else {
                    var msg = "Activated"
                }
                    $('.main-success-box').remove();
                   $('.content-wrapper').prepend('<div class="message success alert alert-success main-success-box" onclick="this.classList.add('+"hidden"+')">organization company has been '+msg+'</div>');
               },
               
      });
  });

    $('input[name="hide_active_company"]').on('click', function(){
        if($(this).is(':checked')){
            $('.active-tr').addClass('hide');
        }else{

            $('.active-tr').removeClass('hide');
        }
    });

})();