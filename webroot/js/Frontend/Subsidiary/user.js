(function () {
    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-zA-ZüöäÜÖÄß\s]+$/i.test(value);
    }, "Enter Letters only please");

    $('#add-organization-user').validate({
        rules: {
            'first_name': {
                required: true,
                lettersonly:true
            },
            'last_name' :{
                required : true,
                lettersonly:true
            },
            'email' :{
                required : true,
                email:true,
                remote : '/homes/unique-email'
            },
            'license' : {
                required : true
            },
            'organization_user[organization_subsidiary_id]' : {
                required : true
            }
        },
        messages: {
            'first_name': {
                'required': "First name must not be empty",
                
            },
            'last_name' :{
                required : "Last name must not be empty"
                
            },
            'license' : {
                required : "License number must not be empty"
            },
            'email' : {
                remote : 'Email already exists.'
            }

        }
    });
  $('.edit-org-user').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
                type:"GET",
                url:url,        
                success: function(data){
                    console.log(data);
                    $('#editUser').find('.modal-body').html(data);
                    $('#editUser').modal('show');
                    $('#edit-organization-user').validate({
                        rules: {
                            'first_name': {
                                required: true,
                                lettersonly:true                                
                            },
                            'last_name' :{
                                required : true,
                                lettersonly:true                                
                            },
                            'license' : {
                                required : true,
                            },
                            'organization_user[organization_subsidiary_id]' : {
                                required : true,
                            }
                        },
                        messages: {
                            'first_name': {
                                'required': "First name must not be empty"
                                
                            },
                            'last_name' :{
                                required : "Last name must not be empty"
                            },
                            'license' : {
                                required : "License number must not be empty"
                            }
                        }
                    });
                 },
                               
            });
    });
  
})();