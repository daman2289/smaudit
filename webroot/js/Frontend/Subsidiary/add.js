(function () {
     jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-zA-ZüöäÜÖÄß\s]+$/i.test(value);
    }, "Enter Letters only please");

    $('#add-subsidiary-form').validate({
        rules: {
            'name': {
                required: true,
                lettersonly: true
            },
            'fax' :{
                required : true
            },
            'phone' : {
                required : true
            },
            'street' :{
                required : true
            },
            'street_number' : {
                required : true
            },
            'zip' : {
                required : true
            },
            'place' :{
                required : true
            }
        },
        messages: {
            'name': {
                required: "Enter the organization name"
            },
            'fax' :{
                required : "Enter the fax number"
            },
            'phone' : {
                required : "Phone number must not be empty"
            },
            'street' :{
                required : "Organization street must not be empty"
            },
            'street_number' : {
                required : "Organization street numbr must not be empty"
            },
            'zip' : {
                required : "Organization zipcode must not be empty"
            },
            'place' :{
                required : "Organization place must not be empty"
            }
        }
    });
  $('.edit-subsidiary').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
                type:"GET",
                url:url,        
                success: function(data){
                    console.log(data);
                    $('#edit-subsidiary').find('.modal-body').html(data);
                    $('#edit-subsidiary').modal('show');
                    $("#fax-no").mask("+41 (0) 99 999 99 99");
                    $("#phone-no").mask("+41 (0) 99 999 99 99");
                    $('#edit-subsidiary-form').validate({
                        rules: {
                            'name': {
                                'required': true,
                                lettersonly: true
                            },
                            'fax' :{
                                required : true
                            },
                            'phone' : {
                                required : true
                            },
                            'street' :{
                                required : true
                            },
                            'street_number' : {
                                required : true
                            },
                            'zip' : {
                                required : true
                            },
                            'place' :{
                                required : true
                            }
                        },
                        messages: {
                            'name': {
                                'required': "Enter the organization name"
                            },
                            'fax' :{
                                required : "Enter the fax number"
                            },
                            'phone' : {
                                required : "Phone number must not be empty"
                            },
                            'street' :{
                                required : "Organization street must not be empty"
                            },
                            'street_number' : {
                                required : "Organization street number must not be empty"
                            },
                            'zip' : {
                                required : "Organization zipcode must not be empty"
                            },
                            'place' :{
                                required : "Organization place must not be empty"
                            }
                        }
                    });
                 },
                               
            });
    });
  $(".switch_custom").on("change",function(){
    if ($(this).is(':checked')) {
      var url = $(this).attr('data-url')+"/1";
    } else {
      var url = $(this).attr('data-url')+"/0";
    }
    $.ajax({
               type:"GET",
               url : url,   
               success: function(data){
                   console.log(data);
                  
               },
               
      });
  });


    $("#phone").mask("+41 (0) 99 999 99 99");

    $("#fax").mask("+41 (0) 99 999 99 99");

})();