(function(){
	$('.set-permission').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
            type:"GET",
            url:url,        
            success: function(data){
            	$('#emptyModal').find('.modal-body').html(data);
            	$('#emptyModal').find('.modal-title').html('Set User Permission');
            	$('#emptyModal').modal('show');
            }
        })
    });
    $(document).on('click', '.prmission_switch', function() {
    	var allowed = 0;
    	var id = $(this).val()
    	if($(this).is(':checked')) {
    		allowed = 1;
    	}
    	
    	var url = $(this).attr('data-url');
    	$.ajax({
            type:"Post",
            url:url, 
            data: { id: id, is_allowed: allowed } ,      
            success: function(data){
            	
            }
        })
    });
})();