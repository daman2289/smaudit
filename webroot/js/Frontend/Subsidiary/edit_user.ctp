<div class="col-xs-12">
    <?php
        echo $this->Form->create($user,
                [
                        'type' => 'post',
                        'id' => 'edit-organization-user',
                        'class' => 'form-horizontal'
                ]
            );
    ?>
        <div class="form-group">
            <label><?php echo __('First Name'); ?></label>
            <?php echo $this->Form->hidden('id');?>
            <?php
                echo $this->Form->input('first_name',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('First Name'),
                                'div' => false,
                                'label' => false
                            )
                    );
            ?>
        </div>
        <div class="form-group">
            <label><?php echo __('Last Name'); ?></label>
            <?php
                echo $this->Form->input('last_name',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('Last Name'),
                                'div' => false,
                                'label' => false
                            )
                    );
            ?>
        </div>
        <div class="form-group">
            <label><?php echo __('Licence No'); ?></label>
            <?php
                echo $this->Form->input('license',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('Licence No'),
                                'div' => false,
                                'label' => false
                            )
                    );
            ?>
        </div>
       <div class="form-group">
            <label><?php echo __('Diploma'); ?></label>
            <?php
                echo $this->Form->input('diploma_id',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('Diploma'),
                                'div' => false,
                                'label' => false,
                                'options' =>$diploma
                            )
                    );
            ?>
        </div>
        <div class="form-group">
            <label><?php echo __('Subsidiary'); ?></label>
            <?php
                echo $this->Form->input('organization_subsidiary_id',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('Subsidiary'),
                                'div' => false,
                                'label' => false,
                                'options' => $subsidiary
                            )
                    );
            ?>
        </div>
        <div class="form-group text-center">
            <div>
            <?php
                echo $this->Form->button('Save', array(
                            'class' => 'btn btn-blue',
                            'type' => 'submit'
                            ));
            ?>
            </div>
        </div>
    </form>
</div>