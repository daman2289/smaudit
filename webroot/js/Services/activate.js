(function () {
  $('#add-service-form').validate({
        rules: {
            'title_eng': {
                'required': true
            },
            'description_eng': {
                'required': true
            },
            'title_german': {
                'required': true
            },
            'description_german': {
                'required': true
            }
        },
        messages: {
            'title_eng': {
                'required': "Service Title in english must not ne empty"
            },
            'description_eng': {
                'required': "Service Description in english must not be empty"
            },
            'title_german': {
                'required': "Service title in german must not be empty"
            },
            'description_german': {
                'required': "Service Description in german must not be empty"
            }
        }
    });
  $('.edit-service').on('click',function() {
        var url = $(this).attr('data-url');
        $.ajax({
                type:"GET",
                url:url,        
                success: function(data){
                    console.log(data);
                    $('#edit-service').find('.modal-body').html(data);
                    $('#edit-service').modal('show');
                    $('#edit-service-form').validate({
                       rules: {
                            'title_eng': {
                                'required': true
                            },
                            'description_eng': {
                                'required': true
                            },
                            'title_german': {
                                'required': true
                            },
                            'description_german': {
                                'required': true
                            }
                        },
                        messages: {
                            'title_eng': {
                                'required': "Service Title in english must not ne empty"
                            },
                            'description_eng': {
                                'required': "Service Description in english must not be empty"
                            },
                            'title_german': {
                                'required': "Service title in german must not be empty"
                            },
                            'description_german': {
                                'required': "Service Description in german must not be empty"
                            }
                        }
                    });
                }
            });
    });
$(".switch_custom").on("change",function(){
    if ($(this).is(':checked')) {
      var url = $(this).attr('data-url')+"/1";
    } else {
      var url = $(this).attr('data-url')+"/0";
    }
    $.ajax({
               type:"GET",
               url : url,   
               success: function(data){
                   if(data == 0){
                    var msg = 'deactivated'
                }else {
                    var msg = "Activated"
                }
                    $('.main-success-box').remove();
                   $('.content-wrapper').prepend('<div class="message success alert alert-success main-success-box" onclick="this.classList.add('+"hidden"+')">Service has been '+msg+'</div>');
                  
               },
               
      });
  });

})();