(function () {
	$(".update-status-switch").on("change",function(){
		var status = 0;
		var element = $(this);
        if (element.is(':checked')) {
	        status = 1;
        }

        $.ajax({
            url: $(this).data('url') + '/' + status + '.json',
            beforeSend: function() {
                element.parents('td').find('.status-update-in-process').removeClass('hide');
            },
            success: function(resp) {
            	if (resp.responseData.type) {
                    show_stack_bar_top('success',resp.responseData.message, true,'pnotify-custom');
                } else {
                    show_stack_bar_top('error',resp.responseData.message, true,'pnotify-custom');
                }
            },
            complete: function(jqXHR, textStatus) {
                if (textStatus == 'success') {
                    var statusText = (status == 1) ? 'Activated' : 'Deactivated';
                    element.parents('tr').find('.show-status').text(statusText);
                }
                element.parents('td').find('.status-update-in-process').addClass('hide');
            },
            error: function(xhr) {
                show_stack_bar_top('error',xhr.responseText, true,'pnotify-custom');
            }
        });
    });
    // function checkSpecialKey(evt) {
    //     var keycode = evt.charCode || evt.keyCode;
    //     if (keycode  == 13) { //Enter key's keycode
    //         return false;
    //     }
    // } 
    $(document).on('keypress', '.scriptTagCheck', function(evt){
        var keycode = evt.charCode || evt.keyCode;
        if (keycode  == 13) { //Enter key's keycode
            return false;
        }
    })   
})();