(function () {
    $('#add-diploma-form').validate({
        rules: {
            'name': {
                'required': true,
                
            }
        },
        messages: {
            'name': {
                'required': 'Diploma name must be required',
               
            }
        }
    });
    $('.edit-diploma').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
                type:"GET",
                url:url,        
                success: function(data){
                    console.log(data);
                    $('#edit-diploma').find('.modal-body').html(data);
                    $('#edit-diploma').modal('show');
                    $('#edit-diploma-form').validate({
                       rules: {
                        'name': {
                        required : true,

                        }
                     }
                    });
                },
               
            });
    });
})();