(function(){
      $('#contact-form').validate({
        rules: {
            'name': {
                'required': true
            },
            'email': {
                'required': true,
                email : true
            },
            'question': {
                'required': true
            }    
        },
        messages: {
           'name': {
                'required': "Name must not be empty"
            },
            'email': {
                'required': "Email must not be empty",
                email : "Please enter the correct format"
            },
            'question': {
                'required': "Question must not be empty"
            }
        }
    });
})();