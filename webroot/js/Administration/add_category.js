(function () {
    $('#add-category-form').validate({
        rules: {
            'name': {
                'required': true,
                
            }
        },
        messages: {
            'name': {
                'required': 'Report category must be required',
               
            }
        }
    });
    $('.edit-category').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
                type:"GET",
                url:url,        
                success: function(data){
                    console.log(data);
                    $('#edit-category').find('.modal-body').html(data);
                    $('#edit-category').modal('show');
                    $('#edit-category-form').validate({
                       rules: {
                        'name': {
                        required : true,

                        }
                     }
                    });
                },
               
            });
    });
})();