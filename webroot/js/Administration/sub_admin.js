(function () {
    $('#add-sub-admin-form').validate({
        rules: {
            'first_name': {
                'required': true,
                'minlength': 3
            },
            'last_name': {
                'required': true,
                'minlength': 3
            },
            'user_profile[phone]': {
                'required': false,
                'validphoneno': true
            },
            'email': {
                'required': true,
                'email': true
            },
            'password': {
                'required': true,
                'minlength': 9,
                'pwcheckallowedchars': true,
                'pwcheckalphabets': true,
                'pwchecknumerics': true
            },
            'confirm_password': {
                'required': true,
                'equalTo': '#sub-admin-password'
            }
        },
        messages: {
            'first_name': {
                'required': 'First name must be provided.',
                'minlength': 'First name must be three characters long atleast'
            },
            'last_name': {
                'required': 'Last name must be provided.',
                'minlength': 'Last name must be three characters long atleast'
            },
            'email': {
                'required': 'Email must be provided.',
                'email': 'Valid email address must be provided.'
            },
            'password': {
                'required': 'password must be entered.',
                'minlength': 'Password must be nine characters long.'
            },
            'confirm_password': {
                'required': 'Confirm password must be entered.',
                'equalTo': 'Passowords do not match.'
            }
        }
    });

    $('.edit-admin').on('click', function(){
        var url = $(this).attr('data-url');
        $.ajax({
            type:"GET",
            url:url,        
            success: function(data){
                console.log(data);
                $('#edit-admin').find('.modal-body').html(data);
                $('#edit-admin').modal('show');
                $('#edit-sub-admin-form').validate({
                   'first_name': {
                        'required': true,
                        'minlength': 3
                    },
                    'last_name': {
                        'required': true,
                        'minlength': 3
                    },
                    'email': {
                        'required': true,
                        'email': true
                    },
                    'password': {
                        'minlength': 9,
                        'pwcheckallowedchars': true,
                        'pwcheckalphabets': true,
                        'pwchecknumerics': true
                    },
                    'confirm_password': {
                        'equalTo': '#admin-password'
                    }
                });
            }
        });
    });
})();