(function () {
	$('#admin-profile-update').validate({
		rules: {
			'first_name': {
				'required': true,
				'minlength': 3
			},
			'last_name': {
				'required': true,
				'minlength': 3
			},
			'user_profile[phone]': {
				'required': false,
				'validphoneno': true
			},
			'user_profile[address]': {
				'required': true
			},
			'email': {
				'required': true,
				'email': true
			}
		},
		messages: {
			'first_name': {
				'required': 'First name must be provided.',
				'minlength': 'First name must be three characters long atleast'
			},
			'last_name': {
				'required': 'Last name must be provided.',
				'minlength': 'Last name must be three characters long atleast'
			},
			'user_profile[address]': {
				'required': 'Address must be provided.'
			},
			'email': {
				'required': 'Email must be provided.',
				'email': 'Valid email address must be provided.'
			}
		}
	});
	// $('body').addClass('sidebar-collapse');
})();