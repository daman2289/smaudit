(function () {
	$('.edit-modified').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
            type:"GET",
            url:url,        
            success: function(data){
                console.log(data);
                $('#edit-modified').find('.modal-body').html(data);
                $('#edit-modified').modal('show');
                $('#edit-modified-form').validate({
                   rules: {
                    'name': {
                    required : true,
                    },
                    'name_ger' : {
                    	required : true
                    },
                    'content_eng' : {
                    	required : true
                    },
                    'content_ger' : {
                    	required : true
                    }
                 }
                });
            },
           
        });
    });
    $('.edit-ref').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
            type:"GET",
            url:url,        
            success: function(data){
                console.log(data);
                $('#edit-ref').find('.modal-body').html(data);
                $('#edit-ref').modal('show');
                $('#edit-ref-form').validate({
                   rules: {
                    'name': {
                    required : true,
                    },
                    'name_ger' : {
                    	required : true
                    },
                    'content_eng' : {
                    	required : true
                    },
                    'content_ger' : {
                    	required : true
                    }
                 }
                });
            },
           
        });
    });
    $('.edit-emphasis').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
            type:"GET",
            url:url,        
            success: function(data){
                console.log(data);
                $('#edit-emphasis').find('.modal-body').html(data);
                $('#edit-emphasis').modal('show');
                $('#edit-emphasis-form').validate({
                   rules: {
                    'name': {
                    required : true,
                    },
                    'name_ger' : {
                    	required : true
                    },
                    'content_eng' : {
                    	required : true
                    },
                    'content_ger' : {
                    	required : true
                    }
                 }
                });
            },
           
        });
    });
    $('.edit-opinion').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
            type:"GET",
            url:url,        
            success: function(data){
                console.log(data);
                $('#edit-opinion').find('.modal-body').html(data);
                $('#edit-opinion').modal('show');
                $('#edit-opinion-form').validate({
                   rules: {
                    'name': {
                    required : true,
                    },
                    'name_ger' : {
                    	required : true
                    },
                    'content_eng' : {
                    	required : true
                    },
                    'content_ger' : {
                    	required : true
                    }
                 }
                });
            },
           
        });
    });
    $('.edit-legal').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
            type:"GET",
            url:url,        
            success: function(data){
                console.log(data);
                $('#edit-legal').find('.modal-body').html(data);
                $('#edit-legal').modal('show');
                $('#edit-legal-form').validate({
                   rules: {
                    'name': {
                    required : true,
                    },
                    'name_ger' : {
                    	required : true
                    },
                    'content_eng' : {
                    	required : true
                    },
                    'content_ger' : {
                    	required : true
                    }
                 }
                });
            },
           
        });
    });
    $('.edit-accounting').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
            type:"GET",
            url:url,        
            success: function(data){
                console.log(data);
                $('#edit-accounting').find('.modal-body').html(data);
                $('#edit-accounting').modal('show');
                $('#edit-accounting-form').validate({
                   rules: {
                    'name': {
                    required : true,
                    },
                    'name_ger' : {
                    	required : true
                    },
                    'content_eng' : {
                    	required : true
                    },
                    'content_ger' : {
                    	required : true
                    }
                 }
                });
            },
           
        });
    });
    $('.edit-other').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
            type:"GET",
            url:url,        
            success: function(data){
                console.log(data);
                $('#edit-other').find('.modal-body').html(data);
                $('#edit-other').modal('show');
                $('#edit-other-form').validate({
                   rules: {
                    'name': {
                    required : true,
                    },
                    'name_ger' : {
                    	required : true
                    },
                    'content_eng' : {
                    	required : true
                    },
                    'content_ger' : {
                    	required : true
                    }
                 }
                });
            },
           
        });
    });
})();