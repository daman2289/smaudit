(function () {
    $('#support-form').validate({
        rules: {
            'message': {
                'required': true
            }
        },
        messages: {
            'message': {
                'required': "Enter the message"
            }
        }
    });
})();