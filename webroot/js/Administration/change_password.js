(function () {
    $('#change-password').validate({
        rules: {
            'old_password': {
                'required': true
            },
            'password': {
                'required': true,
               	'minlength' : 6
            },
            'confirm_password': {
                'required': true,
                'minlength' : 6,
                'equalTo': '#new-password'
            }
        },
        messages: {
            'old_password': {
                'required': "Enter the old password"
            },
            'password': {
                'required': "Enter the new password",
               	'minlength' : "Password must be of atleast 6 characters"
            },
            'confirm_password': {
                'required': "Enter the confirm Password",
                'equalTo': "Password and confirm password do not match"
            }
        }
    });
    $('#admin-change-password').validate({
        rules: {
            'old_password': {
                'required': true
            },
            'password': {
                'required': true,
               	'minlength' : 6
            },
            'confirm_password': {
                'required': true,
                'minlength' : 6,
                'equalTo': '#new-admin-password'
            }
        },
        messages: {
            'old_password': {
                'required': "Enter the old password"
            },
            'password': {
                'required': "Enter the new password",
               	'minlength' : "Password must be of atleast 6 characters"
            },
            'confirm_password': {
                'required': "Enter the confirm Password",
                'equalTo': "Password and confirm password do not match"
            }
        }
    });
})();