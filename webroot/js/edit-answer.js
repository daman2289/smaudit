$('#answer').validate({
	ignore: [],
	errorPlacement: function(error, element) {
        if(element.closest('.dateInput').length) {
            error.insertAfter(element.closest('.dateInput'));
        } else if (element.attr("type") == "radio")
           $(element).closest('.form-group').append(error); 
        else if (element.attr("id") == "company-id")
           error.insertAfter($(element).closest('.form-group').find('#text'));            
        else
           error.insertAfter(element);
    },
});

$(document).ready(function() {
    $('#answer').find('input').each(function(index, element) {
        var type = $(element).attr('type');
        var isChild = $(element).closest('.childs').length;
        if(type == 'radio' && !isChild) { 
        console.log(element);         
            if($(element).prop('checked')) {
                radioChild(element);
            }
        }
        if(type == 'checkbox') {
            checkboxChild(element);
        }
    });

    $('#answer').find('select').each(function(index, element) {
        var multiple = $(element).attr('multiple');
        (multiple) ? multiSelectChild(element) : selectChid(element);
    });
});