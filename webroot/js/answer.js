$('#answer').validate({
	ignore: [],
	errorPlacement: function(error, element) {
		console.log()
        if(element.closest('.dateInput').length) {
            error.insertAfter(element.closest('.dateInput'));
        } else if (element.attr("type") == "radio")
           $(element).closest('.form-group').append(error); 
        else if (element.attr("id") == "company-id")
           error.insertAfter($(element).closest('.form-group').find('#text'));            
        else
           error.insertAfter(element);
    },
});