//example below on change country request

/*$(document).on('change', '.country-input-class-name', function () {
    country_change_request($(this),$(".state-input-class-name"),$(".city-input-class-name"));
});*/

//example below on change state request
/*$(document).on('change', '.state-input-class-name', function () {
    state_change_request($(this),$(".city-input-class-name"));
});*/

var states = null;
var cities = null;
var another_states = null;
var another_cities = null;

function getStatesList(country_id) {
    $.ajax({
        url: '/api/Locations/states/' + country_id + '.json',
        async: false,
        success: function(resp) {
            states = resp;
        }
    });
}

function getCitiesList(state_id) {
    $.ajax({
        url: '/api/Locations/cities/' + state_id + '.json',
        async: false,
        success: function(resp) {
            cities = resp;
        }
    });
}

function country_change_request(element, state_class, city_class) {
    var cId = element.find(":selected").val();
    getStatesList(cId);

    state_class.html('');
    city_class.html("<option value='0'>Select City</option>");
    state_class.append("<option value='0'>Select State</option>");
    $.each(states.states, function (key, val) {
        state_class.append("<option value='" + key + "'>" + val + "</option>");
    });
}

function state_change_request(element,city_class) {
    var sId = element.find(":selected").val();
    getCitiesList(sId);

    city_class.html('');
    city_class.append("<option value='0'>Select City</option>");
    $.each(cities.cities, function (key, val) {
        city_class.append("<option value='" + key + "'>" + val + "</option>");
    });
}

$(function () {

    if (!localStorage.countries || JSON.parse(localStorage.getItem('expiretime')).expiretimestamp <= new Date().getTime()) {

        $.ajax({
            url: '/api/Locations/index.json',
            success: function(resp) {

                var object = {expiretimestamp: new Date(Date.now()).getTime() + 60 * 60 * 24 * 1000}
                localStorage.setItem("expiretime", JSON.stringify(object));
                localStorage.setItem("countries", JSON.stringify(resp));
            }
        });
    }
});