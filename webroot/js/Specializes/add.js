(function(){
      $('#add-spec-form').validate({
        rules: {
            'title_eng': {
                'required': true
            },
            'description_eng': {
                'required': true
            },
            'title_german': {
                'required': true
            },
            'description_german': {
                'required': true
            },
            'icon' : {
                extension : 'png|jpg|jpeg|gif'
            }
        },
        messages: {
            'title_eng': {
                'required': "Service Title in english must not ne empty"
            },
            'description_eng': {
                'required': "Service Description in english must not be empty"
            },
            'title_german': {
                'required': "Service title in german must not be empty"
            },
            'description_german': {
                'required': "Service Description in german must not be empty"
            },
            'icon' : {
               extension : 'Specialized icon must be of png,jpg,jpeg or gif format' 
            }
        }
    });
	$('.edit-specialize').on('click',function() {
        var url = $(this).attr('data-url');
        $.ajax({
            type:"GET",
            url:url,        
            success: function(data){
                console.log(data);
                $('#edit-specialize').find('.modal-body').html(data);
                $('#edit-specialize').modal('show');
               $('#edit-spec-form').validate({
                    rules: {
                        'title_eng': {
                            'required': true
                        },
                        'description_eng': {
                            'required': true
                        },
                        'title_german': {
                            'required': true
                        },
                        'description_german': {
                            'required': true
                        },
                        'icon' : {
                            extension : 'png|jpg|jpeg|gif'
                        }
                    },
                    messages: {
                        'title_eng': {
                            'required': "Service Title in english must not ne empty"
                        },
                        'description_eng': {
                            'required': "Service Description in english must not be empty"
                        },
                        'title_german': {
                            'required': "Service title in german must not be empty"
                        },
                        'description_german': {
                            'required': "Service Description in german must not be empty"
                        },
                        'icon' : {
                           extension : 'Specialized icon must be of png,jpg,jpeg or gif format' 
                        }
                    }
                });
            }
        });
    });
})();