(function () {
	$(document).on('click','.add',function() {
            $('.append-item:last').after($('.append-item:first').clone());
            if($('.append-item').length == 2){
                $('.plus').after('<div class="col-sm-1 minus"><span class="glyphicon glyphicon-minus remove">Remove</span></div>');
            }
            if($('.append-item').length < 2){
                $('.minus').remove();
            }
            $('.datetimepicker').datetimepicker({
                format : 'YYYY-MM-DD'
            });

      });
	$(document).on('click','.remove',function() {
                $('.append-item:last').remove();
                if($('.append-item').length < 2){
                    $('.minus').remove();
                }
            
          });
	var addScheduleValidate = function(ele){
            $(ele).validate({
              rules: {
                "description_eng": {
                    required: true,
                },
                "description_ger" : {
                    required: true,
                },
                'work_in_progress_features[date][]': {
                    required : true
                },
                "work_in_progress_features[release_number][]]" : {
                    required: true,
                },
                "work_in_progress_features[features][]" : {
                    required: true
                }
            
              },
               messages: { 
                "description_eng": {
                    required: "Description in english must not be empty",
                },
                "description_ger" : {
                    required: "Description in German must not be empty",
                },
                'work_in_progress_features[date][]': {
                    required : "Date must not be empty"
                },
                "work_in_progress_features[release_number][]" : {
                    required: "Release number must not be empty",
                },
                "work_in_progress_features[features][]" : {
                    required: "Features must not ne empty"
                }
            }
        });
    };
    addScheduleValidate('#add-placeholder-form');
    $('#selection_type').on('change',function(){
        if($(this).val() == 1){
            $('#legal').removeClass('hide');
            $('#account').addClass('hide');
            $('#accounting-framework-type-id').prop('disabled',true);
            $('#legal-form-type-id').prop('disabled',false);
        }else{
            $('#account').removeClass('hide');
            $('#legal').addClass('hide');
            $('#legal-form-type-id').prop('disabled',true);
            $('#accounting-framework-type-id').prop('disabled',false);
        }
    });
    $(document).find('.datetimepicker').datetimepicker({
            format : 'YYYY-MM-DD'
        });
})();