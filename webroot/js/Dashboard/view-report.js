(function(){
	var validateSignUp = function(element) {
        $.validator.addMethod("passwordRegex", function (value, element) {
            return this.optional(element) || /^((?=.*\d)|(?=.*["!#$%&'()*+,\-:;.<=>?@{|}~^_`\[\]\\]))(?=.*[a-zA-Z]).+$/i.test(value);
        }, "Use at least 1 letter & number or symbol.");

        function toDate(dateStr) {
            var parts = dateStr.split("/");
            return new Date(parts[2], parts[1] - 1, parts[0]);
        }

        $.validator.addMethod("greaterStart", function (value, element, params) {
               // console.log(element);
               
               return this.optional(element) || toDate(value) >= toDate($(params).val());
           },
           'Must be greater than from date.'
        );

        $.validator.addMethod("validCompany", function (value, element, params) {
               return this.optional(element) || $(params).val();
            },
           'Please provide a valid company.'
        );

        $.validator.addMethod("service", function(value, elem, param) {
            console.log($(".row-checkbox:checkbox:checked").length)
            if($(".row-checkbox:checkbox:checked").length > 0){
               return true;
           }else {
               return false;
           }
        },"You must select one of Auditor Incharge");
    
      $(element).validate({
        rules: {
            'organization_company': {
                required: true,
                validCompany : '#company-id'
            },
            'organization_company_id': {
                required: true,
            },
            'report_date': {
                required: true,
            },
            'financial_year_from': {
                required: true,
            },
            'financial_year_to': {
                required: true,
                greaterStart : '#fromdate'
            },
            'auditor_incharge': {
                service: true,
            },
            'datepriorinput': {
              required: true,
            },
        },
        messages: {
            'organization_company': {
              required: 'Company name is required.',
            },
            'organization_company_id': {
                required: 'Please provide a valid company.',
            },
            'report_date': {
              required: "Report date is required.",
            },
            'financial_year_from': {
              required: 'From date is required',
            },
            'financial_year_to': {
              required: 'To date is required',
              greaterStart : 'To date must be grater than From date'
            },
            'datepriorinput': {
              required: true,
            }
        },

        errorElement: 'span',
        errorClass: 'error',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.attr("name") == "financial_year_to" || element.attr("name") == "financial_year_from" )
               error.insertAfter($(element).closest('.date'));      
            else if (element.attr("name") == "report_date" || element.attr("name") == "financial_year_from" )
               error.insertAfter($(element).closest('.date'));            
            else
               error.insertAfter(element);
        },
      });
    };

	function setFinancialyear() {
        var from = $(document).find('#fromdate').val();
        var to = $(document).find('#todate').val();
        if(from && to ) {
            var fromParts = from.split("/");
            var toParts = to.split("/");
            if(fromParts[2] != toParts[2]) {
                $(document).find('#financeYear').val(fromParts[2]+ '-'+ toParts[2])
            }else {
                $(document).find('#financeYear').val(fromParts[2])
            }
        }else if(from) {
            var fromParts = from.split("/");
            $(document).find('#financeYear').val(fromParts[2])
        }else if(to) {
           var toParts = to.split("/");
           $(document).find('#financeYear').val(toParts[2])
        }
    }

	$(document).on('click', '.form-button-first',function(){
		$(document).find('.first-form').toggle()
		$(document).find('.first-form-view').toggle()
		validateSignUp("#first-form");
		 // Bootstrap DateTimePicker v4
	    $(document).find('#reportDate').datetimepicker({
	        format: 'DD/MM/YYYY'
	    });

	    $(document).find('#financialFrom').datetimepicker({
	        format: 'DD/MM/YYYY'
	    });

	    $(document).find('#financialTo').datetimepicker({
	        format: 'DD/MM/YYYY'
	    });

	    $(document).find('#auditReport').datetimepicker({
	         format: 'DD/MM/YYYY'
	    });
	    $(document).find("#financialFrom").on("dp.change",function (e) {
	        setFinancialyear();
	    });
	    $(document).find("#financialTo").on("dp.change",function (e) {
	        setFinancialyear();
	    });
	    $(document).find('#auditReports').datetimepicker({
	        format: 'DD/MM/YYYY'
	    });

	    $(document).find('#financialYear').datetimepicker({
	        format: 'DD/MM/YYYY'
	    });
	});

	$(document).on('click', '.form-button-second',function(){
		$(document).find('.second-form').toggle();
		$(document).find('.second-form-view').toggle();
		$(document).find('#auditReport').datetimepicker({
	         format: 'DD/MM/YYYY'
	    });
		function optingOutCheck(ele) {
            console.log($(ele).val());
            if ($(ele).val() == 0) {
                $(document).find(".dateprior").show();
                $(document).find('#date-prior-audit-report').attr('disabled', false);
                $(document).find(".opiniontypes").find("select").each(function(){
                    $(this).attr("disabled", false);
                })
            } else {
                $(document).find(".dateprior").hide();
                $(document).find('#date-prior-audit-report').attr('disabled', true);
                $(document).find(".opiniontypes").find("select").each(function(){
                    $(this).attr("disabled", true);
                })
            }
        }
        
        $(document).find('input:checkbox[name=first_year_audit]').change(function() {  
        	console.log($(this));         
            if(!$(this).is(':checked')){
                $(document).find(".optingout").toggle();
                $(document).find(".optingout").find("input").each(function(){
                    $(this).attr("disabled", true);
                });
                $(document).find('input:radio[name=opting_out][value=1]').prop('checked',true);
                optingOutCheck($(document).find('input:radio[name=opting_out]:checked'));
            } else {
                $(document).find(".optingout").toggle();
                $(document).find(".optingout").find("input").each(function(){
                    $(this).attr("disabled", false);
                }) 
                optingOutCheck($('input:radio[name=opting_out]:checked'));
            }
        });

        $(document).find('input:radio[name=opting_out]').change(function() {
            optingOutCheck($(this));
        });
	});

	$(document).on('click', '.form-button-third',function(){
		$(document).find('.third-form').toggle();
		$(document).find('.third-form-view').toggle();
        if($(document).find(".modifaction").hasClass( "hide" )) {
            $(document).find(".modifaction").find('textarea').attr("disabled", true);
        }
		// $(document).find('#auditReport').datetimepicker({
	 //         format: 'DD/MM/YYYY'
	 //    });
        $('#audit-terminated-date').datetimepicker({
                     format: 'DD/MM/YYYY'
                });
		$(document).find('input:radio[name=audit_terminated_later_than_report]').change(function() {
            $(document).find(".terminated").hide();
            $(document).find(".terminated").find("input").each(function(){
                $(this).attr("disabled", true);
            }) 
            if (this.value == 1) {
                $(document).find(".terminated").show();
                $(document).find(".terminated").find("input").each(function(){
                    $(this).attr("disabled", false);
                }) 
            }
        });
        $(document).find('#modified-opinion-id').on('change', function() {
            if (this.value == 3) {
                $(document).find(".modifaction").addClass('hide');  
                $(document).find(".modifaction").find('textarea').attr("disabled", true);   
            }
            else{
                $(document).find(".modifaction").removeClass('hide');   
                $(document).find(".modifaction").find('textarea').attr("disabled", false);                 
            }
        })
	    $(document).find('#emphasis').multiselect({
             selectAllValue: 'multiselect-all',
             enableCaseInsensitiveFiltering: true,
             enableFiltering: true,
             maxHeight: '300',
             buttonWidth: '235',
             onChange: function(element, checked) {
                 var brands = $('#emphasis option:selected');
                 var selected = [];
                 var concern = false;
                 var concern2 = false;
                $(brands).each(function(index, brand){
                    selected.push($(this).val());
                });
                
                if(selected.includes('1'))
                {
                    $('.Subordination').removeClass('hide');
                    $('.Subordination').find('input').attr("disabled", false)
                }
                else{
                    $('.Subordination').addClass('hide');
                    $('.Subordination').find('input').attr("disabled", true)

                }
                if( selected.includes('7')  )
                {
                    $('.Shareholders').removeClass('hide');
                }
                else{
                    $('.Shareholders').addClass('hide');
                }

                if( selected.includes('2') )
                {
                    concern = true;
                }
                if( selected.includes('3') )
                {
                    concern2 = true;
                }
                
                if(concern || concern2)
                {
                    $('.Concern').removeClass('hide');
                    $('.Concern').find('input').attr("disabled", false)

                }else{
                    $('.Concern').addClass('hide'); 
                    $('.Concern').find('input').attr("disabled", true)
                  
                }
            }
        });

        $('#references').multiselect({
            selectAllValue: 'multiselect-all',
            enableCaseInsensitiveFiltering: true,
            enableFiltering: true,
            maxHeight: '300',
            buttonWidth: '235',
            onChange: function(element, checked) {
                var brands = $('#references option:selected');
                var selected = [];
                var concern = false;
                var concern2 = false;
                $(brands).each(function(index, brand){
                    selected.push($(this).val());
                });
                if( selected.includes('2')  )
                {
                    $('.Shareholders').removeClass('hide');
                    $('.Shareholders').find('input').attr("disabled", false)

                }else {
                    $('.Shareholders').addClass('hide');
                    $('.Shareholders').find('input').attr("disabled", true)

                }

                if( selected.includes('4')  )
                {
                    $('.dividend').removeClass('hide');
                    $('.dividend').find('input').attr("disabled", false)

                }else {
                    $('.dividend').addClass('hide');
                    $('.dividend').find('input').attr("disabled", true)
                }
            }
        });
	});

	$(document).on('click', '.form-button-fourth',function(){
		$(document).find('.fourth-form').toggle();
		$(document).find('.fourth-form-view').toggle();
		
	});

    //Only one checkbox is checked
    $(document).find('.auditor-chk input:checkbox').click(function() {
        $(document).find('.auditor-chk input:checkbox').not(this).prop('checked', false);
    });  

    $(document).ready(function () {             
        $('input:checkbox[name=short_long]').change(function() {
            $(".longshortfinancial").toggle(); 
        });         
    });         
   

       // don't navigate away from the field on tab when selecting an item
    $( "#tag-tagname" ).on( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
           $( this ).autocomplete( "instance" ).menu.active ) {
           event.preventDefault();
        }
    }).autocomplete({
        source: function( request, response ) {
            $.getJSON("/Reports/getCompanies", {
               term:  request.term 
            }, response );
        },
        search: function() {
           // custom minLength
            var term = this.value ;
            if ( term.length < 2 ) {
               return false;
            }
        },
        focus: function() {
           // prevent value inserted on focus
           return false;
        },
        select: function( event, ui ) {
           this.value = ui.item.value;
           $('#company-id').val(ui.item.id);
           return false;
        }
    });
    
    $('#searchDate').datetimepicker({
        format: 'DD/MM/YYYY'
    });

    $(document).ready(function() {
        validateSignUp("#first-form");
    });
})();