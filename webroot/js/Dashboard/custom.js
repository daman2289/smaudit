$(function () {
   
  // validate signup form on keyup and submit
    var validateSignUp = function(element) {

        $.validator.addMethod("passwordRegex", function (value, element) {
            return this.optional(element) || /^((?=.*\d)|(?=.*["!#$%&'()*+,\-:;.<=>?@{|}~^_`\[\]\\]))(?=.*[a-zA-Z]).+$/i.test(value);
        }, "Use at least 1 letter & number or symbol.");

        function toDate(dateStr) {
            var parts = dateStr.split("/");
            return new Date(parts[2], parts[1] - 1, parts[0]);
        }

        $.validator.addMethod("greaterStart", function (value, element, params) {
               // console.log(element);
               
               return this.optional(element) || toDate(value) >= toDate($(params).val());
           },
           'Must be greater than from date.'
        );

        $.validator.addMethod("validCompany", function (value, element, params) {
               return this.optional(element) || $(params).val();
            },
           'Please provide a valid company.'
        );

        $.validator.addMethod("service", function(value, elem, param) {
            console.log($(".row-checkbox:checkbox:checked").length)
            if($(".row-checkbox:checkbox:checked").length > 0){
               return true;
           }else {
               return false;
           }
        },"You must select one of Auditor Incharge");
    
      $(element).validate({
        rules: {
            'organization_company': {
                required: true,
                validCompany : '#company-id'
            },
            'organization_company_id': {
                required: true,
            },
            'report_date': {
                required: true,
            },
            'financial_year_from': {
                required: true,
            },
            'financial_year_to': {
                required: true,
                greaterStart : '#fromdate'
            },
            'auditor_incharge': {
                service: true,
            },
            'datepriorinput': {
              required: true,
            },
        },
        messages: {
            'organization_company': {
              required: 'Company name is required.',
            },
            'organization_company_id': {
                required: 'Please provide a valid company.',
            },
            'report_date': {
              required: "Report date is required.",
            },
            'financial_year_from': {
              required: 'From date is required',
            },
            'financial_year_to': {
              required: 'To date is required',
              greaterStart : 'To date must be grater than From date'
            },
            'datepriorinput': {
              required: true,
            }
        },

        errorElement: 'span',
        errorClass: 'error',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.attr("name") == "financial_year_to" || element.attr("name") == "financial_year_from" )
               error.insertAfter($(element).closest('.date'));      
            else if (element.attr("name") == "report_date" || element.attr("name") == "financial_year_from" )
               error.insertAfter($(element).closest('.date'));            
            else
               error.insertAfter(element);
        },
      });
    };

    $(document).ready(function() {
        validateSignUp(".report-form");
    });

   $(document).on('keypress', '.scriptTagCheck', function(e){
        e = e || event;
            return !/[<>]/i.test(String.fromCharCode(e.charCode || e.keyCode)) 
                    || !!(!e.charCode && ~[8,37,39,46].indexOf(e.keyCode));
    }) 

});


