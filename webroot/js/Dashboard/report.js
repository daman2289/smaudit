(function() {    
    function setFinancialyear() {
       var from = $('#fromdate').val();
       var to = $('#todate').val();
       if(from && to ) {
            var fromParts = from.split("/");
            var toParts = to.split("/");
            if(fromParts[2] != toParts[2]) {
                $('#financeYear').val(fromParts[2]+ '-'+ toParts[2])
            }else {
                $('#financeYear').val(fromParts[2])
            }
        }else if(from) {
            var fromParts = from.split("/");
            $('#financeYear').val(fromParts[2])
        }else if(to) {
           var toParts = to.split("/");
           $('#financeYear').val(toParts[2])
        }
    }         
   // Bootstrap DateTimePicker v4
    $('#reportDate').datetimepicker({
        format: 'DD/MM/YYYY'
    });

    $('#financialFrom').datetimepicker({
        format: 'DD/MM/YYYY',
        // minDate: moment().startOf('year').format('YYYY-MM-DD'),
        // maxDate:  moment().endOf('year').format('YYYY-MM-DD'),        
    });

    $('#financialTo').datetimepicker({
        format: 'DD/MM/YYYY',
        // minDate: moment().startOf('year').format('YYYY-MM-DD'),
    });

    $('#auditReport').datetimepicker({
         format: 'DD/MM/YYYY'
    });
    $("#financialFrom").on("dp.change",function (e) {
        setFinancialyear();
    });
    $("#financialTo").on("dp.change",function (e) {
        setFinancialyear();
    });
    $('#auditReports').datetimepicker({
        format: 'DD/MM/YYYY'
    });

    $('#financialYear').datetimepicker({
        format: 'DD/MM/YYYY'
    });

    //Only one checkbox is checked
    $('.auditor-chk input:checkbox').click(function() {
        $('.auditor-chk input:checkbox').not(this).prop('checked', false);
    });  

    $(document).ready(function () {
        //Initialize tooltips
        $('.nav-tabs > li a[title]').tooltip();
        //Wizard
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        
         var $target = $(e.target);
        
         if ($target.parent().hasClass('disabled')) {
             return false;
         }
        });
        
        $(".next-step").click(function (e) {
            if ($(".report-form").valid() === true) {

                 var $active = $('.wizard .nav-tabs li.active');
                 $active.next().removeClass('disabled');
                 nextTab($active);
            }
        
        });
        $(".prev-step").click(function (e) {        
            var $active = $('.wizard .nav-tabs li.active');
            prevTab($active);
        
        });
            
        $('#modified-opinion-id').on('change', function() {
            if (this.value == 3) {
                $(".modifaction").addClass('hide');     
            }
            else{
                $(".modifaction").removeClass('hide');                 
            }
        })    

        function optingOutCheck(ele) {
            console.log($(ele).val());
            if ($(ele).val() == 0) {
                $(".dateprior").show();
                $('#date-prior-audit-report').attr('disabled', false);
                $(".opiniontypes").find("select").each(function(){
                    $(this).attr("disabled", false);
                })
            } else {
                $(".dateprior").hide();
                $('#date-prior-audit-report').attr('disabled', true);
                $(".opiniontypes").find("select").each(function(){
                    $(this).attr("disabled", true);
                })
            }
        }
        
        $('input:checkbox[name=first_year_audit]').change(function() {           
            if(!$(this).is(':checked')){
                $(".optingout").toggle();
                $(".optingout").find("input").each(function(){
                    $(this).attr("disabled", true);
                });
                $('input:radio[name=opting_out][value=1]').prop('checked',true);
                optingOutCheck($('input:radio[name=opting_out]:checked'));
            } else {
                $(".optingout").toggle();
                $(".optingout").find("input").each(function(){
                    $(this).attr("disabled", false);
                }) 
                optingOutCheck($('input:radio[name=opting_out]:checked'));
            }
        });
            
        $('input:checkbox[name=short_long]').change(function() {
            $(".longshortfinancial").toggle(); 
        });            
            
        $('input:radio[name=opting_out]').change(function() {
            optingOutCheck($(this));
        });
            
        $('input:radio[name=audit_terminated_later_than_report]').change(function() {
            $(".terminated").hide();
            $(".terminated").find("input").each(function(){
                $(this).attr("disabled", true);
            }) 
            if (this.value == 1) {
                $(".terminated").show();
                $(".terminated").find("input").each(function(){
                    $(this).attr("disabled", false);
                }) 
            }
        });

        $('#emphasis').multiselect({
             selectAllValue: 'multiselect-all',
             enableCaseInsensitiveFiltering: true,
             enableFiltering: true,
             maxHeight: '300',
             buttonWidth: '235',
             onChange: function(element, checked) {
                 var brands = $('#emphasis option:selected');
                 var selected = [];
                 var concern = false;
                 var concern2 = false;
                $(brands).each(function(index, brand){
                    selected.push($(this).val());
                });
                
                if(selected.includes('1'))
                {
                    $('.Subordination').removeClass('hide');
                }
                else{
                     $('.Subordination').addClass('hide');
                }
                if( selected.includes('7')  )
                {
                    $('.Shareholders').removeClass('hide');
                }
                else{
                    $('.Shareholders').addClass('hide');
                }

                if( selected.includes('2') )
                {
                    concern = true;
                }
                if( selected.includes('3') )
                {
                    concern2 = true;
                }                
                if(concern || concern2)
                {
                    $('.Concern').removeClass('hide');
                }else{
                    $('.Concern').addClass('hide');                   
                }
            }
        });

        $('#references').multiselect({
            selectAllValue: 'multiselect-all',
            enableCaseInsensitiveFiltering: true,
            enableFiltering: true,
            maxHeight: '300',
            buttonWidth: '235',
            onChange: function(element, checked) {
                var brands = $('#references option:selected');
                var selected = [];
                var concern = false;
                var concern2 = false;
                $(brands).each(function(index, brand){
                    selected.push($(this).val());
                });

                if( selected.includes('2')  )
                {
                    $('.Shareholders').removeClass('hide');
                }else {
                    $('.Shareholders').addClass('hide');
                }

                if( selected.includes('4')  )
                {
                    $('.dividend').removeClass('hide');
                }else {
                    $('.dividend').addClass('hide');
                }
            }
        });
    });
            
    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
            
    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
     // elem.prev().find('a[data-toggle="tab"]').click();
     // var curhref = elem.find('a[data-toggle="tab"]').attr('href');
     // $(curhref).hide();

     // var prevhref = elem.prev().find('a[data-toggle="tab"]').attr('href');
     // $(prevhref).show();
    }

       // don't navigate away from the field on tab when selecting an item
    $( "#tag-tagname" ).on( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
           $( this ).autocomplete( "instance" ).menu.active ) {
           event.preventDefault();
        }
    }).autocomplete({
        source: function( request, response ) {
            $.getJSON("/Reports/getCompanies", {
               term:  request.term 
            }, response );
        },
        search: function() {
           // custom minLength
            var term = this.value ;
            if ( term.length < 2 ) {
               return false;
            }
        },
        focus: function() {
           // prevent value inserted on focus
           return false;
        },
        select: function( event, ui ) {
           this.value = ui.item.value;
           $('#company-id').val(ui.item.id);
           return false;
        }
    });
    // Listing page script stats here
    $('.view-report').on('click', function(){
        var url = $(this).attr('data-url');
        $.ajax({
            type:"GET",
            url:url,         
            // dataType: "json",
            contentType: "application/json; charset=utf-8",   
            success: function(data){               
                $('#emptyModal').find('.modal-header').find('.modal-title').html('Report Detail');
                $('#emptyModal').find('.modal-body').find('.col-div').html(data);
                $('#emptyModal').find('.modal-dialog').addClass('modal-lg');
                $('#emphasis').multiselect('select', JSON.parse(emphasis));
                $('#references').multiselect('select', JSON.parse(references)); 
                $('#emphasis').multiselect('setOptions',{
                    selectAllValue: 'multiselect-all',
                    enableCaseInsensitiveFiltering: true,
                    enableFiltering: true,
                    maxHeight: '300',
                    buttonWidth: '235',
                    onChange: function(element, checked) {
                        var brands = $('#emphasis option:selected');
                        var selected = [];
                        var concern = false;
                        var concern2 = false;
                        $(brands).each(function(index, brand){
                            selected.push($(this).val());
                        });
                        
                        if(selected.includes('1'))
                        {
                            $('.Subordination').removeClass('hide');
                            $('.Subordination').find('input').attr("disabled", false)
                        }
                        else{
                            $('.Subordination').addClass('hide');
                            $('.Subordination').find('input').attr("disabled", true)

                        }
                        if( selected.includes('7')  )
                        {
                            $('.Shareholders').removeClass('hide');
                        }
                        else{
                            $('.Shareholders').addClass('hide');
                        }

                        if( selected.includes('2') )
                        {
                            concern = true;
                        }
                        if( selected.includes('3') )
                        {
                            concern2 = true;
                        }
                        
                        if(concern || concern2)
                        {
                            $('.Concern').removeClass('hide');
                            $('.Concern').find('input').attr("disabled", false)

                        }else{
                            $('.Concern').addClass('hide'); 
                            $('.Concern').find('input').attr("disabled", true)
                          
                        }
                    }
                });
                $('#references').multiselect('setOptions',{
                    selectAllValue: 'multiselect-all',
                    enableCaseInsensitiveFiltering: true,
                    enableFiltering: true,
                    maxHeight: '300',
                    buttonWidth: '235',
                    onChange: function(element, checked) {
                        var brands = $('#references option:selected');
                        var selected = [];
                        var concern = false;
                        var concern2 = false;
                        $(brands).each(function(index, brand){
                            selected.push($(this).val());
                        });
                        if( selected.includes('2')  )
                        {
                            $('.Shareholders').removeClass('hide');
                            $('.Shareholders').find('input').attr("disabled", false)

                        }else {
                            $('.Shareholders').addClass('hide');
                            $('.Shareholders').find('input').attr("disabled", true)

                        }

                        if( selected.includes('4')  )
                        {
                            $('.dividend').removeClass('hide');
                            $('.dividend').find('input').attr("disabled", false)

                        }else {
                            $('.dividend').addClass('hide');
                            $('.dividend').find('input').attr("disabled", true)
                        }
                    }
                });
 
                $('#emptyModal').modal('show');
                $('#audit-terminated-date').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $(document).find('#auditReport').datetimepicker({
                     format: 'DD/MM/YYYY'
                });

            },
            error: function (data) {
                alert('error');
            }
        });
    });
    $('#searchDate').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    
})();   