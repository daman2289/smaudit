$(function () {
        graphKey = JSON.parse(graphKey);
        var a = JSON.parse(graphData);
        graphData = a.map(function (x) { 
                    return parseInt(x, 10); 
                });
    $('#container').highcharts({
        colors: ['#ECECEC'],
        chart: {
            type: 'areaspline',
             backgroundColor: '#fff',
      style: {
         fontFamily: "Raleway', sans-serif"
      }
        },
        title: {
            text: 'Weekly Registered Users'
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 150,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ''
        },
        xAxis: {
             visible: false,
            categories: graphKey,
          
        },
        yAxis: {
            title: {
                text: '',

            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ' Users'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 1
            }
        },
        series: [{
            name: '',
            data: graphData,
            borderWidth: 0
        }]
    });
});
