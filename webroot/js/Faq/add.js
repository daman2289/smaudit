(function () {
    $('#add-faq-form').validate({
        rules: {
            'ques_eng': {
                'required': true,  
            },
            'ques_german' : {
                required : true
            },
            'ans_eng' : {
                required : true
            },
            'ans_german' : {
                required : true
            }
        }, 
        messages: {
            'ques_eng': {
                required: 'Question in english must not be empty',  
            },
            'ques_german' : {
                required : 'Question in German must not be empty'
            },
            'ans_eng' : {
                required : 'Answer in English must not be empty'
            },
            'ans_german' : {
                required : 'Answer in German must not be empty'
            }
        }
    });
    $('.edit-faq').on('click',function() {
        var url = $(this).attr('data-url');
        $.ajax({
                type:"GET",
                url:url,        
                success: function(data){
                    console.log(data);
                    $('#edit-faq').find('.modal-body').html(data);
                    $('#edit-faq').modal('show');
                    $('#edit-faq-form').validate({
                       rules: {
                            'ques_eng': {
                                'required': true,  
                            },
                            'ques_german' : {
                                required : true
                            },
                            'ans_eng' : {
                                required : true
                            },
                            'ans_german' : {
                                required : true
                            }
                        }, 
                        messages: {
                            'ques_eng': {
                                required: 'Question in english must not be empty',  
                            },
                            'ques_german' : {
                                required : 'Question in German must not be empty'
                            },
                            'ans_eng' : {
                                required : 'Answer in English must not be empty'
                            },
                            'ans_german' : {
                                required : 'Answer in German must not be empty'
                            }
                        }
                    });
                }
            });
    });
})();