<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<!-- 'echo "api_key: onBtc5AIgZJ4S1WdLLuFMHuFEyOCfBYU" > ~/.ultrahook' -->
<!-- Content Wrapper. Contains page content -->
<div class="top-header col-sm-12">
    <div class="col-sm-6">
        <div class="top-title"> Pricing
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div>
      	<div class="row">
      	<?php
			if (!empty($packages)) {				
				foreach ($packages as $key => $value) {
		?>
          	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="blocks active-block">
                    <div class="block-header">
                        <h4><?php echo __($value['title']);?></h4>
                        <div>
                        <?php 
                        	echo strtoupper($value['currency']).' '.
                        	number_format($value['price']/100,2);
                        ?>
						For 
						<?= $value['frequency'] ?>
	
						</div>
                    </div>
                    <?php 
		            	echo $this->element(
		            		'SubscriptionPlans/plans_features', 
		            		[
		            			'features' => $value['plan_features']
		            		]
		            	); 
		            ?>
                    <div class="block-footer">
                        <!-- <a class="order-now" href="#">Order Now</a> -->
                        <?php 					  		
				  			echo $this->Form->create(
								'Payments',
								[	
									'url' => (!empty($organizationPlan) && $organizationPlan->subscription_plan_id == $value['id'])?
									'javascript::void(0)':
									[
										'controller' => 'SubscriptionPlans',
										'action' => 'stripe-callback'
									],
									'class' => 'payment-btn'
								]
							);
							echo $this->Form->input('plan_id', 
									['type' => 'hidden', 'value' => $value['stripe_plan_id']]
								);
						?>			
							<button class="btn btn-block single-listing price-btn">
								<?php 
									if (!empty($organizationPlan) &&  $organizationPlan->subscription_plan_id == $value['id']) {
										echo '<span class="btn btn-success stripe-button">'.
										__('Already Enrolled').'</span>';
									} else {
								?>
									<script
										src="https://checkout.stripe.com/checkout.js" class="stripe-button"
										data-key="pk_test_cwchXot35zr41kn0slEu5VMR"
										data-name="Smaudit"
										data-description="Sumaudit subscription"
										data-image="https://smaudit.ch/img/logo.png"
										data-label=<?=__('Subscribe Now') ?>
										data-locale="auto">
									</script>	
								<?php		
									} 
								?>
															
							</button>	
						<?php echo $this->Form->end(); ?>
                    </div>
                </div>
            </div>
        <?php 
				}				
			}
		?>
	    </div>
	    <?php
	    	if (!empty($packages) && !empty($additionalUserSubscriptionPlans)) {	
	    ?>
	    <div class="row">	    	
	    	<div class="col-md-6">	    		
			    <h2> 
			    	<?= __('Additional User Pricing'); ?>
			    </h2>
	    	</div>
	    	<div class="col-md-6">
	    		<h2>	    			
		    		<?php
		    			if(!empty($organizationAdditinalPlan)){
		    				$left = ($organizationAdditinalPlan->additional_users - $users);
		    				echo __("You can add $left more users with you current plan");
		    			} 
		    		?>
	    		</h2>
	    	</div>
	    </div>
	    <div class="row">
        
      	<?php						
			foreach ($additionalUserSubscriptionPlans as $key => $value) {
		?>
          	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="blocks active-block">
                    <div class="block-header">
                        <h4><?php echo __($value['title']);?></h4>
                        <div>
                        <?php echo strtoupper($value['currency']).' '.number_format($value['price']/100,2);?>
						For <?= $value['frequency'] ?>
	
						</div>
                    </div>                    
                    <div class="block-footer">
                        <!-- <a class="order-now" href="#">Order Now</a> -->
                    <?php
                    	if(!empty($organizationAdditinalPlan) && $organizationAdditinalPlan->stripe_plan_id == $value['stripe_plan_id']) 
                    	{
                    		echo '<span class="btn btn-success stripe-button">'.__('Already Enrolled').'</span>';
                    	}elseif (!empty($organizationAdditinalPlan)){
                        	echo $this->Html->link(__('Subscribe Now'),[
								'controller' => 'additional-user-subscription-plans',
								'action'	=> 'upgrade-subscription',
								base64_encode($value->id) ],
								[
									'confirm' => 'Are you sure want to upgrade your plan.',
									'class' => "btn btn-success stripe-button"
								]
							);
                        }else {
                        	echo $this->Form->create(
								'Payments',
								[	
									'url' => (!empty($organizationAdditinalPlan) && $organizationAdditinalPlan->stripe_plan_id == $value['stripe_plan_id'])?
									'javascript::void(0)':
									[
										'controller' => 'AdditionalUserSubscriptionPlans',
										'action' => 'subscribe-organization'
									],
									'class' => 'payment-btn'
								]
							);
							echo $this->Form->input('plan_id', ['type' => 'hidden', 'value' => $value['stripe_plan_id']]);
					?>
							<button class="btn btn-block single-listing price-btn">
								<script
									src="https://checkout.stripe.com/checkout.js" class="stripe-button"
									data-key="pk_test_cwchXot35zr41kn0slEu5VMR"
									data-name="Smaudit"
									data-description="Sumaudit subscription"
									data-image="https://smaudit.ch/img/logo.png"
									data-label=<?= __('Subscribe Now') ?>
									data-locale="auto">
								</script>				
							</button>	

                    <?php
                    		echo $this->Form->end();
                        }
                    ?>	
						
                    </div>
                </div>
            </div>
        <?php 
				}
		?>
	    </div>
	    <?php
	     	}
	    ?>
    </div>
</div>
<style type="text/css">
.blocks{ border:1px solid #EEEEEE;}
.blocks:hover{box-shadow:0px 0px 10px #D9E0DB;}
.block-header{  text-align: center; width: 100%; padding: 2%; background: #F8F8F8; color: #333333;}
.block-container p:hover{background:#F8F8F8;}
.block-header h4{ font-weight: bold; vertical-align: center;}
.block-container{ text-align: left;}
.block-container p{ border-bottom:1px solid #F4F7F8; margin: 0; padding: 2%; }
.block-container p:last-child{ border-bottom:none;}
.price{font-size: 50px; font-family: PT Serif; color: #FF592D; font-weight: bold;}
.renew-price{font-size: 12px; color: #333333; font-style: italic; font-weight: normal;}
.block-footer{text-align: center; padding: 10%;}
.order-now{border: 1px solid #FF592D; padding: 15px; border-radius: 4%; color:#333333; font-weight: bold; }
.order-now:hover{text-decoration: none; background: #FF592D; color: #fff;}
.active-block { box-shadow:0px 0px 10px #D9E0DB;}
.active-block .block-header{ background: #263F92; color: #fff;}
.active-block .price{ color:#45BA76;}
.active-block .block-footer a{ background: #ff592d; color: #fff; border:none;}
.active-block .block-footer a:hover{ background: #ff592d; color: #fff; border:1px solid #45BA76;}
</style>