<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit New Question'), ['action' => 'edit', $newQuestion->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete New Question'), ['action' => 'delete', $newQuestion->id], ['confirm' => __('Are you sure you want to delete # {0}?', $newQuestion->id)]) ?> </li>
        <li><?= $this->Html->link(__('List New Questions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New New Question'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List New Placeholders'), ['controller' => 'NewPlaceholders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New New Placeholder'), ['controller' => 'NewPlaceholders', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List New Question Types'), ['controller' => 'NewQuestionTypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New New Question Type'), ['controller' => 'NewQuestionTypes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List New Question Options'), ['controller' => 'NewQuestionOptions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New New Question Option'), ['controller' => 'NewQuestionOptions', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="newQuestions view large-9 medium-8 columns content">
    <h3><?= h($newQuestion->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('New Placeholder') ?></th>
            <td><?= $newQuestion->has('new_placeholder') ? $this->Html->link($newQuestion->new_placeholder->name, ['controller' => 'NewPlaceholders', 'action' => 'view', $newQuestion->new_placeholder->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('New Question Type') ?></th>
            <td><?= $newQuestion->has('new_question_type') ? $this->Html->link($newQuestion->new_question_type->name, ['controller' => 'NewQuestionTypes', 'action' => 'view', $newQuestion->new_question_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Question En') ?></th>
            <td><?= h($newQuestion->question_en) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Question Er') ?></th>
            <td><?= h($newQuestion->question_er) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Question Fr') ?></th>
            <td><?= h($newQuestion->question_fr) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($newQuestion->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Parent') ?></th>
            <td><?= $this->Number->format($newQuestion->is_parent) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($newQuestion->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($newQuestion->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related New Question Options') ?></h4>
        <?php if (!empty($newQuestion->new_question_options)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('New Question Id') ?></th>
                <th scope="col"><?= __('Name En') ?></th>
                <th scope="col"><?= __('Name Er') ?></th>
                <th scope="col"><?= __('Name Fr') ?></th>
                <th scope="col"><?= __('Content En') ?></th>
                <th scope="col"><?= __('Content Er') ?></th>
                <th scope="col"><?= __('Question Fr') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($newQuestion->new_question_options as $newQuestionOptions): ?>
            <tr>
                <td><?= h($newQuestionOptions->id) ?></td>
                <td><?= h($newQuestionOptions->new_question_id) ?></td>
                <td><?= h($newQuestionOptions->name_en) ?></td>
                <td><?= h($newQuestionOptions->name_er) ?></td>
                <td><?= h($newQuestionOptions->name_fr) ?></td>
                <td><?= h($newQuestionOptions->content_en) ?></td>
                <td><?= h($newQuestionOptions->content_er) ?></td>
                <td><?= h($newQuestionOptions->question_fr) ?></td>
                <td><?= h($newQuestionOptions->created) ?></td>
                <td><?= h($newQuestionOptions->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'NewQuestionOptions', 'action' => 'view', $newQuestionOptions->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'NewQuestionOptions', 'action' => 'edit', $newQuestionOptions->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'NewQuestionOptions', 'action' => 'delete', $newQuestionOptions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $newQuestionOptions->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
