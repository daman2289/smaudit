<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List New Questions'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List New Placeholders'), ['controller' => 'NewPlaceholders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New New Placeholder'), ['controller' => 'NewPlaceholders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List New Question Types'), ['controller' => 'NewQuestionTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New New Question Type'), ['controller' => 'NewQuestionTypes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List New Question Options'), ['controller' => 'NewQuestionOptions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New New Question Option'), ['controller' => 'NewQuestionOptions', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="newQuestions form large-9 medium-8 columns content">
    <?= $this->Form->create($newQuestion) ?>
    <fieldset>
        <legend><?= __('Add New Question') ?></legend>
        <?php
            echo $this->Form->control('new_placeholder_id', ['options' => $newPlaceholders]);
            echo $this->Form->control('new_question_type_id', ['options' => $newQuestionTypes]);
            echo $this->Form->control('question_en');
            echo $this->Form->control('question_er');
            echo $this->Form->control('question_fr');
            echo $this->Form->control('is_parent');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
