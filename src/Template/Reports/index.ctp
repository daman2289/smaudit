<?php
    use Cake\Core\Configure;
    $this->Html->addCrumb(__('Dashboard'), ['controller' => 'Users', 'action' => 'dashboard']);
    $this->Html->addCrumb(__('Reports'), ['controller' => 'Reports', 'action' => 'report-listing']);
    $this->Html->addCrumb(__('Add Report'), ['controller' => 'Reports', 'action' => 'index']);
?>
<div class="col-sm-12 breadcrumb-no-bg">
    <div class="row">
       <?php
            echo $this->Html->getCrumbList(
                [
                    'firstClass' => false,
                    'lastClass' => 'active',
                    'class' => 'breadcrumb'
                ]
            );
        ?>
    </div>
</div>
<div class="top-header col-sm-6 col-sm-offset-1">
    <div class="col-sm-6">
        <div class="top-title"> <?= __('Reports') ?>
        </div>
    </div>
</div>
<div class=" clearfix col-lg-12">
    <div class="row">
        <section>
            <div class="wizard">
                <div class="wizard-inner">
                    <div class="connecting-line"></div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active prv-first">
                            <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                            <span class="round-tab">
                            1
                            </span>
                            </a>
                        </li>
                        <li role="presentation" class="disabled">
                            <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                            <span class="round-tab">
                            2
                            </span>
                            </a>
                        </li>
                        <li role="presentation" class="disabled">
                            <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                            <span class="round-tab">
                            3
                            </span>
                            </a>
                        </li>
                        <li role="presentation" class="disabled">
                            <a href="#step4" data-toggle="tab" aria-controls="step4" role="tab" title="Step 4">
                            <span class="round-tab">
                            4
                            </span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- <form data-toggle="validator" role="form" class="form-report report-form"> -->
                <?php
                    echo $this->Form->create(null,
                            array(
                                    'type' => 'post',
                                    'class'=> 'form-report report-form',
                                    'url' => array(
                                            'controller' => 'Reports',
                                            'action' => 'index'
                                        ),
                                    'id' => 'forgot-password',
                                    'data-toggle' => "validator"
                                    
                                )
                        );
                ?>
                    <div class="tab-content">
                        <?= $this->element('Reports/first_step') ?>
                        <?= $this->element('Reports/second_step') ?>
                        <?= $this->element('Reports/third_step') ?>
                        
                        <div class="tab-pane auditor-chk" role="tabpanel" id="step4">
                            <div class="form-group clearfix">
                                <div class="col-sm-3">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label><?php echo __('First Signature');?></label>
                                            </div>
                                            <div class="col-sm-12">
                                                <?php
                                                    echo $this->Form->input('first_signature_user_id',
                                                        array(
                                                            'class' => 'form-control',
                                                            'placeholder' => __(''),
                                                            'div' => false,
                                                            'label' => false,
                                                            'required' => true,
                                                            'options' => $usersList,
                                                            'empty' => 'Select'
                                                        )
                                                    );
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label><?php echo __('Auditor Incharge');?></label>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="works-in-ie-login">
                                                    <label>
                                                    <input type="checkbox" value="0" name='auditor_incharge' checked class="row-checkbox">
                                                    <i class="faa fa-unchecked unchecked"></i>
                                                    <i class="faa fa-checked  checked"></i>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <div class="col-sm-3">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label><?php echo __('Second Signature');?></label>
                                            </div>
                                            <div class="col-sm-12">
                                               <?php
                                                    echo $this->Form->input('second_signature_user_id',
                                                            array(
                                                                    'class' => 'form-control',
                                                                    'placeholder' => __(''),
                                                                    'div' => false,
                                                                    'label' => false,
                                                                    'required' => true,
                                                                    'options' => $usersList,
                                                                    'empty' => 'Select'
                                                                )
                                                        );
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label><?php echo __('Auditor Incharge');?></label>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="works-in-ie-login">
                                                    <label>
                                                    <input type="checkbox" value="1" name='auditor_incharge' class="row-checkbox">
                                                    <i class="faa fa-unchecked unchecked"></i>
                                                    <i class="faa fa-checked  checked"></i>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group clearfix text-right">
                                <div class="col-lg-8">
                                    <button type="button" class="btn btn-default-step prev-step"><?php echo __('Back');?></button>
                                    <button type="submit" class="btn btn-blue btn-info-full next-step"><?php echo __('Save');?></button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>
<?php echo $this->Html->script('Dashboard/report', ['block' => 'scriptBottom']); ?>
