<?php
    use Cake\Core\Configure;
    $this->Html->addCrumb(__('Dashboard'), ['controller' => 'Users', 'action' => 'dashboard']);
    $this->Html->addCrumb(__('Reports'));
?>

<div class="col-sm-12 breadcrumb-no-bg">
    <div class="row">
       <?php
            echo $this->Html->getCrumbList(
                [
                    'firstClass' => false,
                    'lastClass' => 'active',
                    'class' => 'breadcrumb'
                ]
            );
        ?>
    </div>
</div>
<div class="top-header col-sm-12">
    <div class="col-sm-2">
        <div class="top-title"> <?= __('Reports') ?> </div>
    </div>
   <!--  <div class="col-md-6"> 
        <?php
            if(empty($subsidiary)){ 
                echo "Please ensure you have managed the report settings for header and footer ";
                echo $this->Html->link( 
                    'Click to manage', 
                    ['controller' => 'subsidiaries', 'action' => 'index'],
                    ['class' => 'btn btn-xs btn-primary']
                );
            }
        ?>

    </div> -->
    <div class="col-sm-10 text-right">
         <?=
            $this->Html->link('<i class="fa fa-plus-circle"></i>'.__('Add Report'), 
                [
                    'controller' => 'Reports',
                    'action'    => 'index'
                ],
                [
                    'class' => 'btn btn-blue',
                    'escape' => false
                ]
            ) 
        ?>
    </div>
</div>

<div class="col-sm-12 form-group ">
    <div class="row">
    <?=
        $this->Form->create('',array('type' => 'get'));
    ?> 
        <div class="col-sm-3">
            <div class="input-group ">
                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
                <?= 
                    $this->Form->input(
                        'search',
                        [
                            'class' =>'form-control input-lg', 
                            'placeholder'   =>'Search by company', 
                            'aria-describedby'  => "basic-addon1",
                            'label' => false,
                            'div'   => false
                        ]
                    ) 
                ?>
            </div> 
        </div>
        <div class="col-sm-3">
            <div class="input-group date" id='searchDate'>
                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                <?= 
                    $this->Form->input(
                        'date',
                        [
                            'class'=>'form-control input-lg', 
                            'placeholder'=>'Search by report date', 
                            'aria-describedby'  => "basic-addon1", 
                            'label' => false,
                            'div'   => false
                        ]
                    ) 
                ?>
            </div> 
        </div>
        <div class="col-sm-2">
            <button class="btn btn-search"><?= __('Search') ?></button>
        </div>

        <?= $this->Form->end() ?>
    </div>
</div>
<div class="col-lg-12">
    <div class="table-responsive">
        <table class="table col-lg-12">
            <thead>
                <tr>
                    <th><?php echo __($this->Paginator->sort('organization_company_id',__('Company')))?></th>
                    <th><?php echo __($this->Paginator->sort('financial_year',__('Financial Year')))?></th>
                    <th><?php echo __($this->Paginator->sort('report_date',__('Report Date')))?></th>
                    <th><?php echo __($this->Paginator->sort('accounting_framework_type_id',__('Accounting Framework Type')))?></th>
                    <th><?php echo __($this->Paginator->sort('emphasis_of_matter_ids',__('Emphasis of Matter')))?></th>
                    <th><?php echo __($this->Paginator->sort('report_type_id',__('Report Type')))?></th>
                    <th><?php echo __($this->Paginator->sort('',__('Actions')))?></th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    if(!empty($reports->toArray())) {
                        foreach($reports as $key => $report) {
                ?>
                <tr>
                    <td>
                        <?php 
                            echo    ($report->has('organization_company')) ? 
                                    htmlspecialchars(__($report->organization_company->company_name) ): 
                                    'N/A' ;
                        ?>
                    </td>
                    <td><?= htmlspecialchars($report->financial_year) ?></td>
                    <td><?php echo date('d/m/Y',strtotime($report->report_date) ) ?></td>
                    <td>
                        <?php 
                            echo    ($report->has('accounting_framework_type')) ? 
                                    htmlspecialchars(__($report->accounting_framework_type->name)) : 
                                    'N/A' ;
                        ?>
                    </td>
                    <td>
                        <?php 
                            array_map(
                                function ($n){
                                    if(!empty($n)) {
                                        echo htmlspecialchars(__(Configure::read('Emphasis.'.$n))). "<br>";
                                    }else { echo 'N/A';}
                                }, 
                                explode(',', $report->emphasis_of_matter_ids)
                            );
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo ($report->has('report_type')) ? htmlspecialchars(__($report->report_type->name)) : 'N/A' ;
                        ?>
                    </td>
                    <td>
                        <?= $this->Html->Link('<i class="fa fa-eye" aria-hidden="true"></i>',
                               'javascript:void(0)',
                               [
                                   'escape'   => false,
                                   'class'    => 'view-report',
                                   'data-url' =>   $this->Url->build(
                                        [  
                                           "controller" => "Reports",
                                           "action" => "view-report",
                                           base64_encode($report->id)
                                        ],true
                                    ),
                                    'title'  => 'View Details'                           
                               ]
                           )
                        ?> 
                        <?= $this->Html->Link('<i class="fa fa-newspaper-o" aria-hidden="true"></i>',
                                [  
                                   "controller" => "Reports",
                                   "action"     => "report-editor",
                                   base64_encode($report->id)
                                ],
                                [
                                    'escape'    => false,
                                    'class'     => '',
                                    'title'     => 'Preview editor'                           
                                ]
                           )
                        ?> 
                        <?= 
                            $this->Html->Link('<i class="fa fa-file-pdf-o" aria-hidden="true"></i>',
                                [  
                                   "controller" => "Reports",
                                   "action"     => "get-report",
                                   base64_encode($report->id),
                                   '_ext' => 'pdf'
                                ],
                                [
                                   'escape'   => false,
                                   'data-url' =>   $this->Url->build(
                                        [  
                                           "controller" => "Subsidiaries",
                                           "action" => "edit",
                                        ],true
                                    ),
                                    'title'  => 'PDF'                           
                                ]
                           )
                        ?> 
                        <?= 
                            $this->Form->postLink('<i class="fa fa-trash" aria-hidden="true"></i>',
                                [  
                                   "controller" => "reports",
                                   "action" => "delete",
                                   base64_encode($report->id)
                                ],
                                [
                                   'escape'   => false,
                                    'title'  => 'Delete',
                                    'confirm' => __('Are you sure to delete report?')                          
                                ]
                           )
                        ?> 
                    </td>
                </tr>
                <?php } } else { ?>
                        <tr>
                            <td colspan="6" class="text-center">
                                <?php echo __('No Record Found'); ?>
                            </td>
                        </tr>
                 <?php } ?>
            </tbody>
        </table>
        <div class="text-right">
            
            <?php echo $this->element('pagination'); ?>
        </div>
    </div>
</div>
<?php
    echo $this->element('empty_modal'); 
?>
<?php 
    echo $this->Html->script([
            'Frontend/Users/profile',
            'Dashboard/report',
             'Dashboard/view-report'
        ], 
        [
            'block' => 'scriptBottom'
        ]
    ); 
?>
