<?php 
	$this->Html->addCrumb(__('Dashboard'), ['controller' => 'Users', 'action' => 'dashboard']);
    $this->Html->addCrumb(__('Reports'), ['controller' => 'Reports', 'action' => 'report-listing']);
    $this->Html->addCrumb(__('Preview'));
?>
<div class="col-sm-12 breadcrumb-no-bg">
    <div class="row">
       <?php
            echo $this->Html->getCrumbList(
                [
                    'firstClass' => false,
                    'lastClass' => 'active',
                    'class' => 'breadcrumb'
                ]
            );
        ?>
    </div>
</div>
<section class="content">

    <?=
        $this->Form->create('',['type' => 'get']);
    ?> 
        <div class="col-sm-3 pull-right">
            <div class="input-group date pull-right" id="searchDate">
                <div class="input text">
                    <?= 
                        $this->Form->input('lang',[
                            'default' => $lang,
                            'class' => 'form-control, input-lg',
                            'id'    => 'select-lang',
                            'aria-describedby'  => 'basic-addon1',
                            'options' => array(
                                'template_eng'  =>  'English',
                                'template_german'   =>'German',
                                'template_french'   => 'French',
                                'template_italian'  =>'Italian'
                            ),
                            'label' => false
                        ]);
                    ?>
                </div>        
            </div> 
        </div>
    <?= $this->Form->end() ?>
    <div class="col-lg-12">                     
        <div id="emails">
        <?= $this->Form->create('', [
                'type'  => 'post',
                'url'   => ['controller' => 'Reports', 'action' => 'report-pdf',"_ext" => "pdf"],
                'form-horizontal', 'novalidate' => true
            ]);
        ?>
        <?= $this->Form->hidden('lang',['value' => $lang]) ?>
        <?= $this->Form->hidden('report_id',['value' => $this->request->params['pass'][0]]); ?>
            <div class="form-group">
                <?php
                    echo $this->CKEditor->loadJs();
                    echo $this->Form->textarea('mail_body', [
                    	'class'	=>'form-control', 
                    	'label' => false,
                    	'value'	=> (!empty($reportEnglishTemplate))?$reportEnglishTemplate:'',
                    	'rows'	=>500,
                    ]);
                    echo $this->CKEditor->replace('mail_body');
                    if ($this->Form->isFieldError('mail_body')) {
                        echo $this->Form->error('mail_body');
                    }
                ?>
            </div>
            <div class="col-lg-8">
                <!-- <?= $this->Form->button(__('Save'), ['class' => "sbtn submit-info submit_black", 'type' => 'submit']); ?> -->
                    <input type="submit" class="sbtn submit-info submit_black" name="next" value="Save">
                    <input type="submit" class="sbtn submit-info submit_black" name="pdf" value="Save & Download">
                    <?= $this->Html->link('<button type="button" class="sbtn submit-info submit_black"> Back </button>', [
                            'controller' => 'Reports',
                            'action' => 'reportListing'
                        ],['escape' => false, 'style' => 'color:black']) 
                    ?>
            </div>
        <?= $this->Form->end() ?>
        </div>
    </div>
</section>
<?php 
    echo $this->Html->script([
            'Dashboard/report-editor'
        ], 
        [
            'block' => 'scriptBottom'
        ]
    ); 
?>