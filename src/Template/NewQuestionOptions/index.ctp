<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New New Question Option'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List New Questions'), ['controller' => 'NewQuestions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New New Question'), ['controller' => 'NewQuestions', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="newQuestionOptions index large-9 medium-8 columns content">
    <h3><?= __('New Question Options') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('new_question_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name_en') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name_er') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name_fr') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($newQuestionOptions as $newQuestionOption): ?>
            <tr>
                <td><?= $this->Number->format($newQuestionOption->id) ?></td>
                <td><?= $newQuestionOption->has('new_question') ? $this->Html->link($newQuestionOption->new_question->id, ['controller' => 'NewQuestions', 'action' => 'view', $newQuestionOption->new_question->id]) : '' ?></td>
                <td><?= h($newQuestionOption->name_en) ?></td>
                <td><?= h($newQuestionOption->name_er) ?></td>
                <td><?= h($newQuestionOption->name_fr) ?></td>
                <td><?= h($newQuestionOption->created) ?></td>
                <td><?= h($newQuestionOption->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $newQuestionOption->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $newQuestionOption->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $newQuestionOption->id], ['confirm' => __('Are you sure you want to delete # {0}?', $newQuestionOption->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
