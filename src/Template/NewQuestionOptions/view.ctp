<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit New Question Option'), ['action' => 'edit', $newQuestionOption->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete New Question Option'), ['action' => 'delete', $newQuestionOption->id], ['confirm' => __('Are you sure you want to delete # {0}?', $newQuestionOption->id)]) ?> </li>
        <li><?= $this->Html->link(__('List New Question Options'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New New Question Option'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List New Questions'), ['controller' => 'NewQuestions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New New Question'), ['controller' => 'NewQuestions', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="newQuestionOptions view large-9 medium-8 columns content">
    <h3><?= h($newQuestionOption->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('New Question') ?></th>
            <td><?= $newQuestionOption->has('new_question') ? $this->Html->link($newQuestionOption->new_question->id, ['controller' => 'NewQuestions', 'action' => 'view', $newQuestionOption->new_question->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name En') ?></th>
            <td><?= h($newQuestionOption->name_en) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name Er') ?></th>
            <td><?= h($newQuestionOption->name_er) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name Fr') ?></th>
            <td><?= h($newQuestionOption->name_fr) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($newQuestionOption->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($newQuestionOption->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($newQuestionOption->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Content En') ?></h4>
        <?= $this->Text->autoParagraph(h($newQuestionOption->content_en)); ?>
    </div>
    <div class="row">
        <h4><?= __('Content Er') ?></h4>
        <?= $this->Text->autoParagraph(h($newQuestionOption->content_er)); ?>
    </div>
    <div class="row">
        <h4><?= __('Question Fr') ?></h4>
        <?= $this->Text->autoParagraph(h($newQuestionOption->question_fr)); ?>
    </div>
</div>
