<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List New Question Options'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List New Questions'), ['controller' => 'NewQuestions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New New Question'), ['controller' => 'NewQuestions', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="newQuestionOptions form large-9 medium-8 columns content">
    <?= $this->Form->create($newQuestionOption) ?>
    <fieldset>
        <legend><?= __('Add New Question Option') ?></legend>
        <?php
            echo $this->Form->control('new_question_id', ['options' => $newQuestions]);
            echo $this->Form->control('name_en');
            echo $this->Form->control('name_er');
            echo $this->Form->control('name_fr');
            echo $this->Form->control('content_en');
            echo $this->Form->control('content_er');
            echo $this->Form->control('question_fr');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
