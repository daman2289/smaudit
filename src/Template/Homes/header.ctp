<?php 
    $this->Html->addCrumb(__('Dashboard'), ['controller' => 'Users', 'action' => 'dashboard']);
    // $this->Html->addCrumb(__('Reports'), ['controller' => 'new-reports']);
    $this->Html->addCrumb(__('Manage Report Header'));
?>
<div class="col-sm-12 breadcrumb-no-bg">
    <div class="row">
       <?php
            echo $this->Html->getCrumbList(
                [
                    'firstClass' => false,
                    'lastClass' => 'active',
                    'class' => 'breadcrumb'
                ]
            );
        ?>
    </div>
</div>
<section class="content">
    <!-- <div>
        <label>Preview</label>
        <div style="padding-bottom: 50px; border: 2px solid grey;">            
            <div class='col-md-4'>
                <?php 
                    $profile = $this->request->Session()->read('User.organization.logo');
                    $profilePicPath = 'auditA_front_logo.jpg';
                    if(!empty($profile)) {
                        $profilePic = new File(Configure::read('LogoPicPathToCheck') . $$this->request->Session()->read('User.organization.logo_dir') . DS . 'word_' .$profile, true, 0644);
                        if ($profilePic->exists()) {
                            $profilePicPath = Configure::read('LogoPicPath') . $$this->request->Session()->read('User.organization.logo_dir') . DS . 'word_' . $profile;
                        }
                    }
                    echo $this->Html->image($profilePicPath, [
                            'class' => 'user-image', 'alt' => 'User Image',
                            'height' => 100,
                            'width' => 150
                        ]);
                ?>
            </div>
            <div class='col-md-4'>
                <span id='info-1'>some text </span><br>
                <span id='info-3'>some text </span><br>
                <span id='info-5'>some text </span><br>
            </div>
            <div class='col-md-4'>
                <span id='info-2'>some text </span><br>
                <span id='info-4'>some text </span><br>
                <span id='info-6'>some text </span>
            </div>
        </div>
    </div> -->
    <div class="col-lg-12">  
        <h3>
         
        </h3>                   
        <div id="emails">
        <?= $this->Form->create('null', ['type' => 'post']) ?>
        <?php 
            $k = 0;
            foreach($headers as $key => $value): 
                $k = $key+1;
        ?>
            <div class="form-group col-md-6">
                <?php  
                    echo $this->Form->label('Select the information for header');
                    echo $this->Form->hidden($key.'.id', ['value' => $value->id]);
                    echo $this->Form->select($key.'.info_field_id',[
                            'Organization' => [
                                '1' => 'Name',
                                '2' => 'Website',
                                '3' => 'Phone',
                                '15' => 'Fax',
                                '4' => 'Street & Street no',
                                '5' => 'Zip & Place',
                            ],
                            'Subsidiary' => [
                                '11' => 'Name',
                                '12' => 'Phone',
                                '16' => 'Fax',
                                '13' => 'Street & Street no',
                                '14' => 'Zip & Place'
                            ]
                        ],
                        [
                            'class' =>'form-control',
                            'value' => $value->info_field_id,
                            'onchange'  => 'setTemplate(this)',
                            'empty' => 'Select'
                        ]
                    );
                ?>
            </div>
        <?php endforeach; ?>
        <?php 
            for($i=$k; $i < 6; $i++){
        ?>
            <div class="form-group col-md-6">
                <?php  
                    echo $this->Form->select($i.'.info_field_id', [
                            'Organization' => [
                                '1' => 'Name',
                                '2' => 'Website',
                                '3' => 'Phone',
                                '15' => 'Fax',
                                '4' => 'Street & Street no',
                                '5' => 'Zip & Place',
                            ],
                            'Subsidiary' => [
                                '11' => 'Name',
                                '12' => 'Phone',
                                '16' => 'Fax',
                                '13' => 'Street & Street no',
                                '14' => 'Zip & Place'
                            ]
                        ],
                        [
                            'class' =>'form-control',
                            'empty' => 'Select'
                        ]
                    );
                ?>
            </div>
        <?php        
            }
        ?>            
            <div class="col-lg-8">
                <?= $this->Form->button(__('Save'), ['class' => "btn btn-primary submit-info submit_black", 'type' => 'submit']); ?>
            </div>
        <?= $this->Form->end() ?>
        </div>
    </div>
</section>
<?php 
    echo $this->Html->script([
            'Dashboard/report-editor'
        ], 
        [
            'block' => 'scriptBottom'
        ]
    ); 
    
    $this->Html->scriptStart(['block' => 'scriptBottom']);
?>
        var details = <?php echo json_encode($data) ?>;
        console.log(details);
        setTemplate() {

        }
<?php
    $this->Html->scriptEnd();
?>