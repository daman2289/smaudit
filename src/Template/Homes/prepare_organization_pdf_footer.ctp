<?php if(!empty($footer)): ?>
<?php
	$size 	= (!empty($footer->footer_font_size)) ? $footer->footer_font_size + 2 : '8';
	$weight = (!empty($footer->footer_font_weight)) ? $footer->footer_font_weight : 'normal';
	$style 	= (!empty($footer->footer_font_style)) ? $footer->footer_font_style : 'normal';
	$family 	= (!empty($footer->footer_font_family)) ? $footer->footer_font_style : 'Tahoma';

	$text = "font-size:".$size."; font-family:". $family."; font-weight:". $weight. "; font-style:" . $style. ";";	
?>

<span style="<?php echo $text ?>"> <?php echo $footer->content; ?></span>

<?php endif; ?>