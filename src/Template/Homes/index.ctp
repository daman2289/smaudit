<div class="top-image">
    <div class="container">
        <div class="text-section clearfix">
            <div class="col-sm-4 left-top-banner">
                <img class="img-responsive" src="img/home-top-logo.png">
                <div class="banner-content">
                    <?= __('Online Report Management for Audit Experts') ?>
                </div>
                <!--button class="btn btn-blue hidden-xs" ><?= __('Read More') ?></button-->
            </div>
            <div class="col-sm-8 mac-img">
                <img  class="img-responsive" src="img/mac.png">
            </div>
            <div class="col-sm-12 text-center">
                <button class="btn btn-blue hidden-lg hidden-md hidden-sm">
                    <?= __('Read More') ?> 
                </button>
            </div>
        </div>
    </div>
</div>
<?= $this->element('Homes/specializes') ?>
<?= $this->element('Homes/services') ?>
<?= $this->element('Homes/plans') ?>
<?php
    if(!empty($work)) {
?>
<div class="work-in-progress" id='work-in-progress'>
    <div class="container">
        <div class="col-sm-5 left-work-progress">
            <h2>RoadMap</h2>
            <?php if($this->request->session()->read('Config.language') == 'en_GER') { 
                echo $work->description_ger;
            } else { 
                echo $work->description_eng;
            }?>
        </div>
        <div class="col-sm-7 ">
            <div class="release-work-section clearfix">
            <?php foreach($work['work_in_progress_features'] as $feature) {
                ?>
                <div class="col-sm-6">
                    <div class="col-sm-3">
                        <div class="calendar-circle active">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="release-date"><?php echo date('d-F-Y',strtotime($feature->date))?></div>
                        <div class="release-version">Release <?php echo $feature->release_number?></div>
                        <?php foreach (explode(',', $feature->features) as $key => $value) { ?>
                            <div class="text-work"><i class="fa fa-check" aria-hidden="true"></i> <?php echo $value;?></div>
                        <?php }?>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php
    }
?>
<?= $this->element('Homes/faq') ?>
<?= $this->element('Homes/contact_us') ?>