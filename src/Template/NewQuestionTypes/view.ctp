<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit New Question Type'), ['action' => 'edit', $newQuestionType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete New Question Type'), ['action' => 'delete', $newQuestionType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $newQuestionType->id)]) ?> </li>
        <li><?= $this->Html->link(__('List New Question Types'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New New Question Type'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List New Questions'), ['controller' => 'NewQuestions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New New Question'), ['controller' => 'NewQuestions', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="newQuestionTypes view large-9 medium-8 columns content">
    <h3><?= h($newQuestionType->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($newQuestionType->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($newQuestionType->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($newQuestionType->created) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related New Questions') ?></h4>
        <?php if (!empty($newQuestionType->new_questions)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('New Placeholder Id') ?></th>
                <th scope="col"><?= __('New Question Type Id') ?></th>
                <th scope="col"><?= __('Question En') ?></th>
                <th scope="col"><?= __('Question Er') ?></th>
                <th scope="col"><?= __('Question Fr') ?></th>
                <th scope="col"><?= __('Is Parent') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($newQuestionType->new_questions as $newQuestions): ?>
            <tr>
                <td><?= h($newQuestions->id) ?></td>
                <td><?= h($newQuestions->new_placeholder_id) ?></td>
                <td><?= h($newQuestions->new_question_type_id) ?></td>
                <td><?= h($newQuestions->question_en) ?></td>
                <td><?= h($newQuestions->question_er) ?></td>
                <td><?= h($newQuestions->question_fr) ?></td>
                <td><?= h($newQuestions->is_parent) ?></td>
                <td><?= h($newQuestions->created) ?></td>
                <td><?= h($newQuestions->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'NewQuestions', 'action' => 'view', $newQuestions->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'NewQuestions', 'action' => 'edit', $newQuestions->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'NewQuestions', 'action' => 'delete', $newQuestions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $newQuestions->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
