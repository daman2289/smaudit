<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $newQuestionType->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $newQuestionType->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List New Question Types'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List New Questions'), ['controller' => 'NewQuestions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New New Question'), ['controller' => 'NewQuestions', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="newQuestionTypes form large-9 medium-8 columns content">
    <?= $this->Form->create($newQuestionType) ?>
    <fieldset>
        <legend><?= __('Edit New Question Type') ?></legend>
        <?php
            echo $this->Form->control('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
