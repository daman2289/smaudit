<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New New Question Type'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List New Questions'), ['controller' => 'NewQuestions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New New Question'), ['controller' => 'NewQuestions', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="newQuestionTypes index large-9 medium-8 columns content">
    <h3><?= __('New Question Types') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($newQuestionTypes as $newQuestionType): ?>
            <tr>
                <td><?= $this->Number->format($newQuestionType->id) ?></td>
                <td><?= h($newQuestionType->name) ?></td>
                <td><?= h($newQuestionType->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $newQuestionType->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $newQuestionType->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $newQuestionType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $newQuestionType->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
