<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New New Questions Question'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="newQuestionsQuestions index large-9 medium-8 columns content">
    <h3><?= __('New Questions Questions') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('parent_question_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('child_question_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('condition_for_child') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($newQuestionsQuestions as $newQuestionsQuestion): ?>
            <tr>
                <td><?= $this->Number->format($newQuestionsQuestion->id) ?></td>
                <td><?= $this->Number->format($newQuestionsQuestion->parent_question_id) ?></td>
                <td><?= $this->Number->format($newQuestionsQuestion->child_question_id) ?></td>
                <td><?= h($newQuestionsQuestion->condition_for_child) ?></td>
                <td><?= h($newQuestionsQuestion->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $newQuestionsQuestion->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $newQuestionsQuestion->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $newQuestionsQuestion->id], ['confirm' => __('Are you sure you want to delete # {0}?', $newQuestionsQuestion->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
