<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit New Questions Question'), ['action' => 'edit', $newQuestionsQuestion->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete New Questions Question'), ['action' => 'delete', $newQuestionsQuestion->id], ['confirm' => __('Are you sure you want to delete # {0}?', $newQuestionsQuestion->id)]) ?> </li>
        <li><?= $this->Html->link(__('List New Questions Questions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New New Questions Question'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="newQuestionsQuestions view large-9 medium-8 columns content">
    <h3><?= h($newQuestionsQuestion->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Condition For Child') ?></th>
            <td><?= h($newQuestionsQuestion->condition_for_child) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($newQuestionsQuestion->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Parent Question Id') ?></th>
            <td><?= $this->Number->format($newQuestionsQuestion->parent_question_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Child Question Id') ?></th>
            <td><?= $this->Number->format($newQuestionsQuestion->child_question_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($newQuestionsQuestion->created) ?></td>
        </tr>
    </table>
</div>
