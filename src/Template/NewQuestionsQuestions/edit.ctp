<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $newQuestionsQuestion->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $newQuestionsQuestion->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List New Questions Questions'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="newQuestionsQuestions form large-9 medium-8 columns content">
    <?= $this->Form->create($newQuestionsQuestion) ?>
    <fieldset>
        <legend><?= __('Edit New Questions Question') ?></legend>
        <?php
            echo $this->Form->control('parent_question_id');
            echo $this->Form->control('child_question_id');
            echo $this->Form->control('condition_for_child');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
