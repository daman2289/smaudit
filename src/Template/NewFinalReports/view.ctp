<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit New Final Report'), ['action' => 'edit', $newFinalReport->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete New Final Report'), ['action' => 'delete', $newFinalReport->id], ['confirm' => __('Are you sure you want to delete # {0}?', $newFinalReport->id)]) ?> </li>
        <li><?= $this->Html->link(__('List New Final Reports'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New New Final Report'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List New Reports'), ['controller' => 'NewReports', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New New Report'), ['controller' => 'NewReports', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="newFinalReports view large-9 medium-8 columns content">
    <h3><?= h($newFinalReport->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('New Report') ?></th>
            <td><?= $newFinalReport->has('new_report') ? $this->Html->link($newFinalReport->new_report->name, ['controller' => 'NewReports', 'action' => 'view', $newFinalReport->new_report->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $newFinalReport->has('user') ? $this->Html->link($newFinalReport->user->id, ['controller' => 'Users', 'action' => 'view', $newFinalReport->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lang') ?></th>
            <td><?= h($newFinalReport->lang) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($newFinalReport->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($newFinalReport->created) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Body') ?></h4>
        <?= $this->Text->autoParagraph(h($newFinalReport->body)); ?>
    </div>
</div>
