<?php 
    $this->Html->addCrumb(__('Dashboard'), ['controller' => 'Users', 'action' => 'dashboard']);
    $this->Html->addCrumb(__('Reports'), ['controller' => 'new-reports']);
    $this->Html->addCrumb(__('Edit Report'));
?>
<div class="col-sm-12 breadcrumb-no-bg">
    <div class="row">
       <?php
            echo $this->Html->getCrumbList(
                [
                    'firstClass' => false,
                    'lastClass' => 'active',
                    'class' => 'breadcrumb'
                ]
            );
        ?>
    </div>
</div>
<section class="content">
    <?=
        $this->Form->create(null,['type' => 'get']);
    ?> 
        <div class="col-sm-3 pull-right">
            <div class="input-group date pull-right" id="searchDate">
                <div class="input text">
                    <?= 
                        $this->Form->input('lan',[
                            'default' => $language,
                            'class' => 'form-control, input-lg',
                            'id'    => 'select-lang',
                            'aria-describedby'  => 'basic-addon1',
                            'options' => array(
                                'en'  =>  'English',
                                'er'   =>'German',
                                'fr'   => 'French'
                            ),
                            'label' => false
                        ]);
                    ?>
                </div>        
            </div> 
        </div>
    <?= $this->Form->end() ?>
    <div class="col-lg-12">                     
        <div id="emails">
        <?= $this->Form->create($newFinalReport) ?>
            <?php if($language == 'en'): ?>
                <div class="form-group">
                    <?php
                        echo $this->CKEditor->loadJs();
                        echo $this->Form->textarea('body_en', [
                            'class' =>'form-control', 
                            'label' => 'Content English',
                            'rows'  => 400,
                        ]);
                        echo $this->CKEditor->replace('body_en');
                        if ($this->Form->isFieldError('body_en')) {
                            echo $this->Form->error('body_en');
                        }
                    ?>
                </div>
            <?php endif; ?>
            <?php if($language == 'er'): ?>
                <div class="form-group">
                    <?php
                        echo $this->CKEditor->loadJs();
                        echo $this->Form->textarea('body_er', [
                            'class' =>'form-control', 
                            'label' => 'Content English',
                            'rows'  => 400,
                        ]);
                        echo $this->CKEditor->replace('body_er');
                        if ($this->Form->isFieldError('body_er')) {
                            echo $this->Form->error('body_fr');
                        }
                    ?>
                </div>
            <?php endif; ?>
            <?php if($language == 'fr'): ?>
                <div class="form-group">
                    <?php
                        echo $this->CKEditor->loadJs();
                        echo $this->Form->textarea('body_fr', [
                            'class' =>'form-control', 
                            'label' => 'Content English',
                            'rows'  => 400,
                        ]);
                        echo $this->CKEditor->replace('body_fr');
                        if ($this->Form->isFieldError('body_fr')) {
                            echo $this->Form->error('body_fr');
                        }
                    ?>
                </div>
            <?php endif; ?>
            
            <div class="col-lg-8">
                <?= $this->Form->button(__('Save'), ['class' => "btn btn-primary submit-info submit_black", 'type' => 'submit']); ?>
            </div>
        <?= $this->Form->end() ?>
        </div>
    </div>
</section>
<?php 
    echo $this->Html->script([
            'Dashboard/report-editor'
        ], 
        [
            'block' => 'scriptBottom'
        ]
    ); 
?>
