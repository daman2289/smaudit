<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List New Final Reports'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List New Reports'), ['controller' => 'NewReports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New New Report'), ['controller' => 'NewReports', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="newFinalReports form large-9 medium-8 columns content">
    <?= $this->Form->create($newFinalReport) ?>
    <fieldset>
        <legend><?= __('Add New Final Report') ?></legend>
        <?php
            echo $this->Form->control('new_report_id', ['options' => $newReports]);
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('body');
            echo $this->Form->control('lang');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
