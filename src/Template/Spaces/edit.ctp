<?php
    use Cake\Core\Configure;
    use Cake\Utility\Hash;
    use Cake\I18n\Time;
?>
<div class="content-wrapper ">
    <?php echo $this->Form->create($space, [
                'class' => 'clearfix my-space',
                'novalidate' => true,
                'type' => 'file',
                'id' => 'user-space-upload'
            ]);
    ?>
        <section class="content-header clearfix myspace">
            <div class="col-lg-12 text-center border-bottom">
                <h1 class="heading-text-color"><?php echo __('Edit Your Parking Spot'); ?> </h1>
            </div>
        </section>
        <article class="clearfix margin-l-r article-first">
            <div class="location-outer">
                <div class="col-lg-8 col-lg-offset-2 right-height">
                    <div class="form-group clearfix">
                        <div class="col-lg-8">
                            <div id="imaginary_container">
                                <div class="input-group stylish-input-group">
                                    <?php
                                        echo $this->Form->hidden('lat');
                                        echo $this->Form->hidden('lng');
                                        echo $this->Form->input('address', [
                                                'placeholder' => __('Enter your keyword, it will Auto Populate'),
                                                'class' => 'form-control',
                                                'label' => false,
                                                'type' => 'text',
                                                'id' => 'geocomplete'
                                            ]);
                                    ?>
                                    <span class="input-group-addon">
                                        <?php
                                            echo $this->Form->button('<span class="glyphicon glyphicon-search"></span>', [
                                                'type' => 'button',
                                                'class' => 'find-address'
                                            ]);
                                        ?>
                                    </span>
                                </div>
                            </div>
                            <div class="help-block">
                                <small class="cant-find">
                                    <?php echo __('Can\'t find your address?'); ?> 
                                    <?php
                                        echo $this->Html->link(__('Click here'),
                                                'javascript:void(0)',
                                                [
                                                    'class' => 'find-address'
                                                ]
                                            );
                                    ?>
                                </small>
                            </div>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <div class="col-lg-11 border-map">
                            <div id="map_canvas"></div>
                            <div class="text-left adjust-pin"><?php echo __('adjust the pin over your space'); ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-10 text-right">
                <?php
                    echo $this->Html->link(__('Next'),
                            'javascript:void(0)',
                            [
                                'class' => 'btn btn-defaults map-next'
                            ]
                        );
                ?>
            </div>
        </article>
        <article class="clearfix margin-l-r" id="description">
            <div class="col-lg-8 col-lg-offset-2 right-height1">
                <div class="col-lg-12">
                    <h3><?php echo __('Type of Stash House'); ?></h3>
                </div>
                <div data-toggle="buttons" class="stash-house">
                    <?php
                        echo $this->Form->hidden('space_stash_house.id');
                        $this->Form->templates([
                                'nestingLabel' => '{{hidden}}<label class="btn {{selected}}">{{input}}<i class="{{helpicon}}" checked></i><div class="stash-text">{{text}}</div></label>'
                            ]);
                        echo $this->Form->radio(
                            'space_stash_house.space_stash_house_type',
                            [
                                [
                                    'value' => 1,
                                    'text' => __('Driveway'),
                                    'checked' => true,
                                    'templateVars' => [
                                        'helpicon' => 'driveway',
                                        'selected' => ($space->space_stash_house->space_stash_house_type == 1) ? 'active' : ''
                                    ]
                                ],
                                [
                                    'value' => 2,
                                    'text' => __('Garage'),
                                    'templateVars' => [
                                        'helpicon' => 'garage',
                                        'selected' => ($space->space_stash_house->space_stash_house_type == 2) ? 'active' : ''
                                    ]
                                ],
                                [
                                    'value' => 3,
                                    'text' => __('Parking Lot'),
                                    'templateVars' => [
                                        'helpicon' => 'parking-lot',
                                        'selected' => ($space->space_stash_house->space_stash_house_type == 3) ? 'active' : ''
                                    ]
                                ]
                            ]);
                    ?>
                </div>
                <div class="form-group clearfix">
                    <div class="col-lg-8 custom-select">
                        <?php
                            echo $this->Form->select('space_stash_house.no_of_stashes', Configure::read('StashHouseNumber'), [
                                    'empty' => __('Select number of stashes'),
                                    'class' => 'form-control stash-number'
                                ]);
                        ?>
                    </div>
                </div>
                <div class="form-group clearfix" id="spot-number">
                    <?php
                        if (isset($space->space_spot_letters)) {
                            $index = 1;
                            foreach ($space->space_spot_letters as $spotLetter) {
                    ?>
                                <div class="clearfix form-group">
                                    <div class="col-lg-8">
                                        <?php
                                            echo $this->Form->input('space_spot_letters.' . $index . '.name', [
                                                    'type' => 'text',
                                                    'class' => 'form-control',
                                                    'placeholder' => __('Spot1: space #,3rd floor,left side of driveway,corner of the lot closest to Kuhio'),
                                                    'label' => false,
                                                    'value' => $spotLetter['name']
                                                ]);
                                        ?>
                                    </div>
                                </div>
                    <?php
                                $index++;
                            }
                        }
                    ?>
                </div>
                <div class="form-group clearfix">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <label class="Amenties"><?php echo __('Amenities'); ?></label>
                            </div>
                            <?php $amenities = Hash::extract($space->space_amenities, '{n}.amenity_id'); ?>
                            <div class="col-lg-6">
                                <div class="works-in-ie-login">
                                    <label>
                                        <?php
                                            echo $this->Form->checkbox('space_amenities.1.amenity_id', [
                                                    'value' => 1,
                                                    'hiddenField' => false,
                                                    'checked' => (in_array(1, $amenities)) ? true : false
                                                ]);
                                        ?>
                                        <i class="faa fa-unchecked unchecked"></i>
                                        <i class="faa fa-checked  checked"></i>
                                        <span class="text-checkbox">
                                            <?php echo $this->Html->image('Amenities/wheelchair-access.png') . ' ' . __('Handicap'); ?>
                                        </span>
                                    </label>
                                </div>
                                <div class="works-in-ie-login">
                                    <label>
                                        <?php
                                            echo $this->Form->checkbox('space_amenities.2.amenity_id', [
                                                    'value' => 2,
                                                    'hiddenField' => false,
                                                    'checked' => (in_array(2, $amenities)) ? true : false
                                                ]);
                                        ?>
                                        <i class="faa fa-unchecked unchecked"></i>
                                        <i class="faa fa-checked  checked"></i>
                                        <span class="text-checkbox">
                                            <?php echo $this->Html->image('Amenities/garage-with-roof-and-parked-car.png') . ' ' . __('Covered'); ?>
                                        </span>
                                    </label>
                                </div>
                                <div class="works-in-ie-login">
                                    <label>
                                        <?php
                                            echo $this->Form->checkbox('space_amenities.3.amenity_id', [
                                                    'value' => 3,
                                                    'hiddenField' => false,
                                                    'checked' => (in_array(3, $amenities)) ? true : false
                                                ]);
                                        ?>
                                        <i class="faa fa-unchecked unchecked"></i>
                                        <i class="faa fa-checked  checked"></i>
                                        <span class="text-checkbox">
                                            <?php echo $this->Html->image('Amenities/gatedd.png') . ' ' . __('Gated'); ?>
                                        </span>
                                    </label>
                                </div>
                                <div class="works-in-ie-login">
                                    <label>
                                        <?php
                                            echo $this->Form->checkbox('space_amenities.4.amenity_id', [
                                                    'value' => 4,
                                                    'hiddenField' => false,
                                                    'checked' => (in_array(4, $amenities)) ? true : false
                                                ]);
                                        ?>
                                        <i class="faa fa-unchecked unchecked"></i>
                                        <i class="faa fa-checked  checked"></i>
                                        <span class="text-checkbox">
                                            <?php echo $this->Html->image('Amenities/car-battery-charging.png') . __('Electric Car Charging'); ?>
                                        </span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="works-in-ie-login">
                                    <label>
                                        <?php
                                            echo $this->Form->checkbox('space_amenities.5.amenity_id', [
                                                    'value' => 5,
                                                    'hiddenField' => false,
                                                    'checked' => (in_array(5, $amenities)) ? true : false
                                                ]);
                                        ?>
                                        <i class="faa fa-unchecked unchecked"></i>
                                        <i class="faa fa-checked  checked"></i>
                                        <span class="text-checkbox">
                                            <?php echo $this->Html->image('Amenities/lock.png') . ' ' . __('Security'); ?>
                                        </span>
                                    </label>
                                </div>
                                <div class="works-in-ie-login">
                                    <label>
                                        <?php
                                            echo $this->Form->checkbox('space_amenities.6.amenity_id', [
                                                    'value' => 6,
                                                    'hiddenField' => false,
                                                    'checked' => (in_array(6, $amenities)) ? true : false
                                                ]);
                                        ?>
                                        <i class="faa fa-unchecked unchecked"></i>
                                        <i class="faa fa-checked  checked"></i>
                                        <span class="text-checkbox">
                                            <?php echo $this->Html->image('Amenities/policeman-with-stick.png') . ' ' . __('On Site Staff'); ?>
                                        </span>
                                    </label>
                                </div>
                                <div class="works-in-ie-login">
                                    <label>
                                        <?php
                                            echo $this->Form->checkbox('space_amenities.7.amenity_id', [
                                                    'value' => 7,
                                                    'hiddenField' => false,
                                                    'checked' => (in_array(7, $amenities)) ? true : false
                                                ]);
                                        ?>
                                        <i class="faa fa-unchecked unchecked"></i>
                                        <i class="faa fa-checked  checked"></i>
                                        <span class="text-checkbox">
                                            <?php echo $this->Html->image('Amenities/chauffeur.png') . ' ' . __('Valet'); ?>
                                        </span>
                                    </label>
                                </div>
                            </div>
                            <div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="height-clearnce form-group clearfix">
                    <div class="col-lg-2 text-right">
                        <i class="fa fa-certificate" aria-hidden="true"></i> 
                        <?php echo $this->Html->image('height-clearnce.png'); ?>
                    </div>
                    <div class="col-lg-2 clearnce-text padding-right">
                        <?php echo __('Height Clearance'); ?>
                    </div>
                    <div class="col-lg-1">
                        <?php
                            echo $this->Form->input('feet', [
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'placeholder' => __('ft'),
                                    'label' => false
                                ]);
                        ?>
                    </div>
                    <div class="col-lg-1">
                        <?php
                            echo $this->Form->input('inches', [
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'placeholder' => __('in'),
                                    'label' => false
                                ]);
                        ?>
                    </div>
                    <div class="col-lg-1">
                        <div class="items">
                            <div class="info-block block-info clearfix">
                                <div data-toggle="buttons" class="btn-group itemcontent">
                                    <?php
                                        $heightNotApplicableActive = (isset($space->height_not_applicable) && $space->height_not_applicable) ? 'active' : false;
                                    ?>
                                    <label class="btn btn-default <?php echo $heightNotApplicableActive; ?>">
                                        <div class="itemcontent">
                                            <?php
                                                echo $this->Form->checkbox('height_not_applicable', [
                                                        'value' => 1,
                                                        'autocomplete' => 'off'
                                                    ]);
                                            ?>
                                            <h5><?php echo __('N/A'); ?></h5>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-10 text-right">
                <?php
                    echo $this->Html->link(__('Next'),
                            'javascript:void(0)',
                            [
                                'class' => 'btn btn-defaults description-next'
                            ]
                        );
                ?>
            </div>
        </article>
        <article class="clearfix margin-l-r" id="availability">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="spot-availabel col-lg-12 ">
                    <h3><?php echo __('When is your spot available'); ?></h3>
                    <div class="form-group clearfix">
                        <div class="row">
                            <div class="col-lg-12">
                                <label><?php echo __('Access Time'); ?></label>
                            </div>
                            <div class="col-lg-4">
                                <div class="works-in-ie-login">
                                    <label>
                                        <?php
                                            echo $this->Form->hidden('space_pricing.id');
                                            echo $this->Form->checkbox('space_pricing.is_hourly_available', [
                                                    'value' => 1,
                                                    'hiddenField' => false
                                                ]);
                                        ?>
                                        <i class="faa fa-unchecked unchecked"></i>
                                        <i class="faa fa-checked  checked"></i>
                                        <span class="text-checkbox"><?php echo __('Hourly'); ?></span>
                                    </label>
                                </div>
                                <div class="row access-time hourly" style="display:<?php echo ($space->space_pricing->is_hourly_available) ? 'inline' : 'none'; ?>">
                                    <div class="col-lg-7">
                                        <?php
                                            echo $this->Form->input('space_pricing.hourly_price', [
                                                    'type' => 'text',
                                                    'class' => 'form-control',
                                                    'label' => false,
                                                    'placeholder' => '10'
                                                ]);
                                        ?>
                                    </div>
                                    <div class="col-lg-2 hr-text"><?php echo __('$/hr'); ?></div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="works-in-ie-login">
                                    <label>
                                        <?php
                                            echo $this->Form->checkbox('space_pricing.is_daily_available', [
                                                    'value' => 1,
                                                    'hiddenField' => false
                                                ]);
                                        ?>
                                        <i class="faa fa-unchecked unchecked"></i>
                                        <i class="faa fa-checked  checked"></i>
                                        <span class="text-checkbox"><?php echo __('Daily'); ?></span>
                                    </label>
                                </div>
                                <div class="row access-time daily" style="display:<?php echo ($space->space_pricing->is_daily_available) ? 'inline' : 'none'; ?>">
                                    <div class="col-lg-7">
                                        <?php
                                            echo $this->Form->input('space_pricing.daily_price', [
                                                    'type' => 'text',
                                                    'class' => 'form-control',
                                                    'label' => false,
                                                    'placeholder' => '30'
                                                ]);
                                        ?>
                                    </div>
                                    <div class="col-lg-2 hr-text"><?php echo __('$/daily'); ?></div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="works-in-ie-login">
                                    <label>
                                        <?php
                                            echo $this->Form->checkbox('space_pricing.is_monthly_available', [
                                                    'value' => 1,
                                                    'hiddenField' => false
                                                ]);
                                        ?>
                                        <i class="faa fa-unchecked unchecked"></i>
                                        <i class="faa fa-checked  checked"></i>
                                        <span class="text-checkbox"><?php echo __('Monthly'); ?></span>
                                    </label>
                                </div>
                                <div class="row access-time monthly" style="display:<?php echo ($space->space_pricing->is_monthly_available) ? 'inline' : 'none'; ?>">
                                    <div class="col-lg-7">
                                        <?php
                                            echo $this->Form->input('space_pricing.monthly_price', [
                                                    'type' => 'text',
                                                    'class' => 'form-control',
                                                    'label' => false,
                                                    'placeholder' => '50'
                                                ]);
                                        ?>
                                    </div>
                                    <div class="col-lg-2 hr-text"><?php echo __('$/monthly'); ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 days-avail">
                    <div class="row">
                        <div class="form-group">
                            <div class="items-collection">
                                <div class="col-lg-12">
                                    <label><?php echo __('Days Availability'); ?></label>
                                </div>
                                <?php echo $this->Form->hidden('space_time_days_availability.id'); ?>
                                <div class="items col-lg-1">
                                    <div class="info-block block-info clearfix">
                                        <div data-toggle="buttons" class="btn-group bizmoduleselect">
                                            <?php $modayActive = ($space->space_time_days_availability->monday) ? 'active' : false; ?>
                                            <label class="btn btn-default <?php echo $modayActive; ?>">
                                                <div class="itemcontent">
                                                    <?php
                                                        echo $this->Form->checkbox('space_time_days_availability.monday', [
                                                            'value' => 1,
                                                            // 'hiddenField' => false,
                                                            'checked' => $modayActive ? true : false
                                                        ]);
                                                     ?>
                                                    <h5><?php echo __('Mon'); ?></h5>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="items  col-lg-1">
                                    <div class="info-block block-info clearfix">
                                        <div data-toggle="buttons" class="btn-group itemcontent">
                                            <?php $tuesdayActive = ($space->space_time_days_availability->tuesday) ? 'active' : false; ?>
                                            <label class="btn btn-default <?php echo $tuesdayActive; ?>">
                                                <div class="itemcontent">
                                                    <?php echo $this->Form->checkbox('space_time_days_availability.tuesday', [
                                                            'value' => 1,
                                                            // 'hiddenField' => false,
                                                            'checked' => $tuesdayActive ? true : false
                                                        ]);
                                                     ?>
                                                    <h5><?php echo __('Tus'); ?></h5>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="items  col-lg-1">
                                    <div class="info-block block-info clearfix">
                                        <div data-toggle="buttons" class="btn-group itemcontent">
                                            <?php $wednesdayActive = ($space->space_time_days_availability->wednesday) ? 'active' : false;
                                            ?>
                                            <label class="btn btn-default <?php echo $wednesdayActive; ?>">
                                                <div class="itemcontent">
                                                    <?php echo $this->Form->checkbox('space_time_days_availability.wednesday', [
                                                            'value' => 1,
                                                            // 'hiddenField' => false,
                                                            'checked' => $wednesdayActive ? true : false
                                                        ]);
                                                     ?>
                                                    <h5><?php echo __('Wed'); ?></h5>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="items  col-lg-1">
                                    <div class="info-block block-info clearfix">
                                        <div data-toggle="buttons" class="btn-group itemcontent">
                                            <?php $thursdayActive = ($space->space_time_days_availability->thursday) ? 'active' : false; ?>
                                            <label class="btn btn-default <?php echo $thursdayActive; ?>">
                                                <div class="itemcontent">
                                                    <?php echo $this->Form->checkbox('space_time_days_availability.thursday', [
                                                            'value' => 1,
                                                            // 'hiddenField' => false,
                                                            'checked' => $thursdayActive ? true : false
                                                        ]);
                                                     ?>
                                                    <h5><?php echo __('Thu'); ?></h5>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="items  col-lg-1">
                                    <div class="info-block block-info clearfix">
                                        <div data-toggle="buttons" class="btn-group itemcontent">
                                            <?php $fridayActive = ($space->space_time_days_availability->friday) ? 'active' : false; ?>
                                            <label class="btn btn-default <?php echo $fridayActive; ?>">
                                                <div class="itemcontent">
                                                    <?php echo $this->Form->checkbox('space_time_days_availability.friday', [
                                                            'value' => 1,
                                                            // 'hiddenField' => false,
                                                            'checked' => $fridayActive ? true : false
                                                        ]);
                                                     ?>
                                                    <h5><?php echo __('Fri'); ?></h5>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="items  col-lg-1">
                                    <div class="info-block block-info clearfix">
                                        <div data-toggle="buttons" class="btn-group itemcontent">
                                            <?php $saturdayActive = ($space->space_time_days_availability->saturday) ? 'active' : false; ?>
                                            <label class="btn btn-default <?php echo $saturdayActive; ?>">
                                                <div class="itemcontent">
                                                    <?php echo $this->Form->checkbox('space_time_days_availability.saturday', [
                                                            'value' => 1,
                                                            // 'hiddenField' => false,
                                                            'checked' => $saturdayActive ? true : false
                                                        ]);
                                                     ?>
                                                    <h5><?php echo __('Sat'); ?></h5>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="items  col-lg-1">
                                    <div class="info-block block-info clearfix">
                                        <div data-toggle="buttons" class="btn-group itemcontent">
                                            <?php $sundayActive = ($space->space_time_days_availability->sunday) ? 'active' : false; ?>
                                            <label class="btn btn-default <?php echo $sundayActive; ?>">
                                                <div class="itemcontent">
                                                    <?php echo $this->Form->checkbox('space_time_days_availability.sunday', [
                                                            'value' => 1,
                                                            // 'hiddenField' => false,
                                                            'checked' => $sundayActive ? true : false
                                                        ]);
                                                     ?>
                                                    <h5><?php echo __('Sun'); ?></h5>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group  col-lg-12 clearfix open-time-close">
                    <div class="row">
                        <div class="col-lg-12">
                            <label><?php echo __('Is your spot available for rent 24 hours?'); ?></label>
                            <div class="works-in-ie-signup">
                                <?php
                                    $this->Form->templates([
                                            'nestingLabel' => '{{hidden}}<label>{{input}}<i class="fa-radio fa-unchecked unchecked"></i><i class="fa-radio fa-checked  checked"></i><span class="text-checkbox">{{text}}</span></label>'
                                        ]);
                                    echo $this->Form->radio(
                                        'space_time_days_availability.is_24_hours_open',
                                        [
                                            ['value' => 1, 'text' => __('Yes'), 'checked' => true],
                                            ['value' => 0, 'text' => __('No')]
                                        ]);
                                ?>
                            </div>
                            <div class="spot-accessible" style="display:<?php echo (!$space->space_time_days_availability->is_24_hours_open) ? 'inline' : 'none'; ?>">
                                <div class="col-lg-4 padding-left custom-select">
                                    <label><?php echo __('Open Time'); ?></label>
                                    <?php
                                        $openTime = (!empty($space->space_time_days_availability->open_time)) ? Time::parse($space->space_time_days_availability->open_time)->i18nFormat('HH:mm') : '';
                                        echo $this->Form->input('space_time_days_availability.open_time', [
                                                'class' => 'form-control',
                                                'label' => false,
                                                'id' => 'space-open-time',
                                                'type' => 'text',
                                                'placeholder' => __('Open Time'),
                                                'value' => $openTime
                                            ]);
                                    ?>
                                </div>
                                <div class="col-lg-4 padding-right custom-select">
                                    <label><?php echo __('Close Time'); ?></label>
                                    <?php
                                        $closeTime = (!empty($space->space_time_days_availability->close_time)) ? Time::parse($space->space_time_days_availability->close_time)->i18nFormat('HH:mm') : '';
                                        echo $this->Form->input('space_time_days_availability.close_time', [
                                                'class' => 'form-control',
                                                'label' => false,
                                                'id' => 'space-close-time',
                                                'type' => 'text',
                                                'placeholder' => __('Close Time'),
                                                'value' => $closeTime
                                            ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group  col-lg-12 clearfix open-time-close">
                    <div class="row">
                        <div class="col-lg-12">
                            <label><?php echo __('Is the parking facility closed or off limits during certain hours?'); ?></label>
                            <div class="works-in-ie-signup">
                                <?php
                                    $this->Form->templates([
                                            'nestingLabel' => '{{hidden}}<label>{{input}}<i class="fa-radio fa-unchecked unchecked"></i><i class="fa-radio fa-checked  checked"></i><span class="text-checkbox">{{text}}</span></label>'
                                        ]);
                                    echo $this->Form->radio(
                                        'space_time_days_availability.is_parking_closed_during_certain_hours',
                                        [
                                            ['value' => 1, 'text' => __('Yes'), 'checked' => ($space->space_time_days_availability->is_parking_closed_during_certain_hours) ? true : false],
                                            ['value' => 0, 'text' => __('No'), 'checked' => (!$space->space_time_days_availability->is_parking_closed_during_certain_hours) ? true : false]
                                        ]);
                                ?>
                            </div>
                             <div class="certain-hours" style="display:<?php echo ($space->space_time_days_availability->is_parking_closed_during_certain_hours) ? 'inline' : 'none'; ?>">
                                 <div class="form-group clearfix">
                                    <div class="col-lg-4 padding-left custom-select">
                                        <label><?php echo __('Start Time'); ?></label>
                                        <?php
                                            $startTime = (!empty($space->space_time_days_availability->parking_closed_start_time)) ? Time::parse($space->space_time_days_availability->parking_closed_start_time)->i18nFormat('HH:mm') : '';
                                            echo $this->Form->input('space_time_days_availability.parking_closed_start_time', [
                                                    'class' => 'form-control',
                                                    'label' => false,
                                                    'id' => 'space-start-time',
                                                    'type' => 'text',
                                                    'placeholder' => __('Start Time'),
                                                    'value' => $startTime
                                                ]);
                                        ?>
                                        <div class="help-block">
                                            <div class="text-danger text-red-big"><?php echo __('(No access to the spot during these hours)'); ?></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 padding-right custom-select">
                                        <label><?php echo __('End Time'); ?></label>
                                        <?php
                                            $endTime = (!empty($space->space_time_days_availability->parking_closed_close_time)) ? Time::parse($space->space_time_days_availability->parking_closed_close_time)->i18nFormat('HH:mm') : '';
                                            echo $this->Form->input('space_time_days_availability.parking_closed_close_time', [
                                                    'class' => 'form-control',
                                                    'label' => false,
                                                    'id' => 'space-end-time',
                                                    'type' => 'text',
                                                    'placeholder' => __('End Time'),
                                                    'value' => $endTime
                                                ]);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group  col-lg-12 clearfix open-time-close">
                    <div class="row">
                        <div class="col-lg-12">
                            <label><?php echo __('Is your spot always available or only during a certain date range?'); ?></label>
                            <div class="works-in-ie-signup">
                                <?php
                                    $this->Form->templates([
                                            'nestingLabel' => '{{hidden}}<label>{{input}}<i class="fa-radio fa-unchecked unchecked"></i><i class="fa-radio fa-checked  checked"></i><span class="text-checkbox">{{text}}</span></label>'
                                        ]);
                                    echo $this->Form->radio(
                                        'space_time_days_availability.is_available_during_certain_date_range',
                                        [
                                            ['value' => 1, 'text' => __('Always'), 'checked' => true],
                                            ['value' => 0, 'text' => __('Date Range')]
                                        ]);
                                ?>
                            </div>
                            <div class="hide-range" style="display: <?php echo (!$space->space_time_days_availability->is_available_during_certain_date_range) ? 'inline' : 'none'; ?>;">
                                <div class="col-lg-4">
                                    <?php
                                        $startDate = Time::parse($space->space_time_days_availability->start_date)->i18nFormat('yyyy-MM-dd');
                                        $endDate = Time::parse($space->space_time_days_availability->end_date)->i18nFormat('yyyy-MM-dd');
                                        echo $this->Form->input('space_time_days_availability.certain_date_range', [
                                                'type' => 'text',
                                                'class' => 'pull-right form-control',
                                                'id' => 'daterange-edit',
                                                'label' => false,
                                                'data-start-date' => $startDate,
                                                'data-end-date' => $endDate
                                            ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-10 text-right">
                <?php
                    echo $this->Html->link(__('Next'),
                            'javascript:void(0)',
                            [
                                'class' => 'btn btn-defaults availability-next'
                            ]
                        );
                ?>
            </div>
        </article>
        <article class="clearfix margin-l-r" id="commission">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="form-group col-lg-12 clearfix open-time-close comision">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3><?php echo __('Commission Option'); ?></h3>
                            <div class="works-in-ie-signup">
                                <?php
                                    $this->Form->templates([
                                            'nestingLabel' => '{{hidden}}<label>{{input}}<i class="fa-radio fa-unchecked unchecked"></i><i class="fa-radio fa-checked  checked"></i><span class="text-checkbox">{{text}}</span></label>'
                                        ]);
                                    echo $this->Form->radio(
                                        'commission_option',
                                        [
                                            ['value' => 1, 'text' => __('Flat rate'), 'checked' => true],
                                            ['value' => 2, 'text' => __('Pass the fees')]
                                        ]);
                                ?>
                            </div>
                            <div class="help-block">
                                <small><b><?php echo __('Flat Rate'); ?> -</b> <?php echo __('if you charge $100/month, there will be a 10% commission and 5% tax deducted leaving you $85'); ?></small>
                                <div>
                                    <small><b><?php echo __('Pass the Fees'); ?> -</b> <?php echo __('if you charge $100/month, the 10% commission and 5% tax is passed on to the spot renter. With this option the spot will be listed at $115 rather than $100'); ?></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group  col-lg-12 clearfix open-time-close comision">
                    <div class="row">
                        <div class="col-lg-12">
                            <label><?php echo __('How do you want to be paid?'); ?></label>
                            <div class="works-in-ie-signup">
                                <?php
                                    $this->Form->templates([
                                            'nestingLabel' => '{{hidden}}<label>{{input}}<i class="fa-radio fa-unchecked unchecked"></i><i class="fa-radio fa-checked  checked"></i><span class="text-checkbox">{{text}}</span></label>'
                                        ]);
                                    echo $this->Form->radio(
                                        'pay_type',
                                        [
                                            ['value' => 1, 'text' => __('Mailed Check'), 'checked' => true],
                                            ['value' => 2, 'text' => __('Direct Deposit')]
                                        ]);
                                ?>
                            </div>
                            <div class="help-block">
                                <small>
                                    <b><?php echo __('Mailed Check'); ?> -</b> <?php echo __('Payments are processed in the first 7-10 business days of the month. Please allow an additional 5 business days to receive ACH payments, and 10 business days to receive all checks.'); ?>
                                </small>
                                <div>
                                    <small>
                                        <b><?php echo __('Direct Deposit'); ?> -</b> <?php echo __('Payments are processed in the first 7-10 business days of the month. Please allow an additional 5 business days to receive ACH payments, and 10 business days to receive all checks.'); ?>
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-10 text-right">
                <?php
                    echo $this->Html->link(__('Next'),
                            'javascript:void(0)',
                            [
                                'class' => 'btn btn-defaults commission-next'
                            ]
                        );
                ?>
            </div>
        </article>
        <article class="clearfix margin-l-r" id="gallery">
            <div class="col-lg-8 col-lg-offset-2 right-height2">
                <div class="col-lg-12 gallery-top-heading"><?php echo __('Photos of your spot will help drivers know they\'re renting form the right owner'); ?></div>
                <!-- <div class="row row-margin">
                    <div class="col-lg-3">
                        <div class="img-border">
                            <?php echo $this->Html->image('spot.jpg', ['class' => 'img-responsive']); ?>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="img-border">
                            <?php echo $this->Html->image('spot.jpg', ['class' => 'img-responsive']); ?>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="img-border">
                            <?php echo $this->Html->image('spot.jpg', ['class' => 'img-responsive']); ?>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="img-border">
                            <?php echo $this->Html->image('spot.jpg', ['class' => 'img-responsive']); ?>
                        </div>
                    </div>
                </div> -->
                <div class="form-group clearfix add-spot">
                    <div class="col-sm-4">
                        <label><?php echo __('Add Spot images'); ?></label>
                        <?php
                            echo $this->Form->file('space_galleries.file[]', [
                                    'class' => 'form-control',
                                    'label' => false,
                                    'multiple' => true,
                                    'id' => 'myFile'
                                ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-10 text-right">
                <?php
                    echo $this->Html->link(__('Next'),
                            'javascript:void(0)',
                            [
                                'class' => 'btn btn-defaults gallery-next'
                            ]
                        );
                ?>
            </div>
        </article>
        <article class="clearfix margin-l-r" id="contact">
            <div class="col-lg-8 col-lg-offset-2 right-height2">
                <div class="col-lg-12">
                    <h3 class="contact-info-heading"><?php echo __('YOUR CONTACT INFO'); ?></h3>
                </div>
                <?php echo $this->Form->hidden('space_contact_info.id'); ?>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label><?php echo __('First Name'); ?> <span class="red-star">*</span></label>
                        <?php
                            echo $this->Form->input('space_contact_info.first_name', [
                                    'class' => 'form-control',
                                    'placeholder' => __('First Name'),
                                    'label' => false
                                ]);
                        ?>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label><?php echo __('Email'); ?> <span class="red-star">*</span></label>
                        <?php
                            echo $this->Form->input('space_contact_info.email', [
                                    'class' => 'form-control',
                                    'placeholder' => __('Email'),
                                    'label' => false
                                ]);
                        ?>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label><?php echo __('Last Name'); ?> <span class="red-star">*</span></label>
                        <?php
                            echo $this->Form->input('space_contact_info.last_name', [
                                    'class' => 'form-control',
                                    'placeholder' => __('Last Name'),
                                    'label' => false
                                ]);
                        ?>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label><?php echo __('Phone'); ?></label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <?php
                                echo $this->Form->input('space_contact_info.phone', [
                                        'class' => 'form-control',
                                        'type' => 'text',
                                        'data-inputmask' => '"mask": "(999) 999-9999"',
                                        'data-mask' => '',
                                        'label' => false
                                    ]);
                            ?>
                        </div>
                        <!-- /.input group -->
                    </div>
                </div>
                <div class=" right-height2">
                    <div class="form-group clearfix sumbit-btn col-lg-12">
                        <?php
                            echo $this->Form->button(__('Submit your Stash House'), [
                                    'type' => 'submit',
                                    'class' => 'btn btn-blue'
                                ]);
                        ?>
                    </div>
                </div>
        </article>
    <?php echo $this->Form->end(); ?>
</div>
<div class="clearfix form-group hide" id="stash-spot-name">
    <div class="col-lg-8">
        <?php
            echo $this->Form->input('space_stash_house.name', [
                    'type' => 'text',
                    'class' => 'form-control',
                    'placeholder' => __('Spot1: space #,3rd floor,left side of driveway,corner of the lot closest to Kuhio'),
                    'label' => false
                ]);
        ?>
    </div>
</div>
<script src="http://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo GOOGLE_MAP_API_KEY; ?>"></script>
<?php
    echo $this->Html->css([
            'daterangepicker',
            'jquery.datetimepicker'
        ], [
            'block' => 'css'
        ]);
?>
    <script>
        var map = true;
        var lat = '<?php echo $space->lat; ?>';
        var lng = '<?php echo $space->lng; ?>';
    </script>
<?php
    echo $this->Html->script([
            'moment',
            'daterangepicker',
            'Frontend/jquery.inputmask',
            'jquery.datetimepicker.full.min',
            'Frontend/Space/add',
            'Frontend/jquery.geocomplete',
            'Frontend/Space/address'
        ], [
            'block' => 'scriptBottom'
        ]);
?>