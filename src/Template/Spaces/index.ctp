<?php
    use Cake\Core\Configure;
    use Cake\Filesystem\File;
?>
<div class="content-wrapper clearfix">
    <div class="top-parking-space-heading clearfix">
        <div class="col-xs-6">
            <h1><?php echo __('Parking spaces'); ?></h1>
        </div>
        <div class="col-xs-6 text-right">
            <?php
                echo $this->Html->link(
                        '<i class="fa fa-plus-circle" aria-hidden="true"></i> ' . __('Add another space'),
                        '/Spaces/add',
                        [
                            'escape' => false,
                            'class' => 'btn btn-blue-top'
                        ]
                    );
            ?>
        </div>
    </div>
    <div class="col-xs-12 parking-space-description clearfix">
        <p>
            <?php echo __('Below is a list of all your parking space listings.Under the title of each Listing are a number that you are able to make including editing the parking space details, photos, availability and showing or hiding your listing in our search engine'); ?>
        </p>
    </div>
    <?php
        if (!$spaces->isEmpty()) {
            $latLng = [];
            foreach ($spaces as $space) {
                $spaceForMap = [
                    'lat' => $space->lat,
                    'lng' => $space->lng,
                    'space_id' => $space->id,
                    'space_address' => $space->address,
                ];
                $latLng[] = $spaceForMap;
    ?>
                <div class="my-space-listing clearfix col-xs-12">
                    <div class="well well-sm no-bg clearfix">
                        <div class="col-sm-8">
                            <div class="col-xs-9">
                                <h3><?php echo __('Parking Space on') . ' ' . $space->address; ?></h3>
                                <div class="not-reviewed"><?php echo __('Not yet reviewed'); ?></div>
                                <div class="stash-house-space">
                                    <?php
                                        echo __('Private') . ' ' . Configure::read('SpaceSpotHouseTypesToShow.1') . ' - (' . $space->space_stash_house->no_of_stashes , ' ' . __('space(s)') . ')';
                                    ?>
                                </div>
                                <?php if (!empty($space->space_amenities)) { ?>
                                    <div class="amenties">
                                        <h4><?php echo __('Amenities'); ?></h4>
                                        <ul class="list-inline">
                                            <?php foreach ($space->space_amenities as $amenities) {  ?>
                                                <li>
                                                    <?php echo $this->Html->image('Amenities' . DS . $amenities->amenity->image); ?>
                                                    <div><?php echo $amenities->amenity->name; ?></div>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                <?php } if (!empty($space->space_galleries)) { ?>
                                        <div class="gallery-section">
                                            <h4><?php echo __('Stash Gallery'); ?></h4>
                                            <div class="row gallery">
                                                <?php foreach ($space->space_galleries as $galleryPic) {
                                                        $spaceGalleryFile = new File(Configure::read('SpaceGalleryPicToCheck') . $galleryPic->space_pic_dir . DS . $galleryPic->file, true, 0644);
                                                        if ($spaceGalleryFile->exists()) {
                                                            echo $this->Html->link(
                                                                    $this->Html->image(Configure::read('SpaceGalleryPic') . $galleryPic->space_pic_dir . DS . 'square_' . $galleryPic->file, [
                                                                        'class' => 'img-responsive img-demension',
                                                                        'alt' => $galleryPic->file
                                                                    ]),
                                                                    $this->Url->build('/', true) . Configure::read('SpaceGalleryPicPathForAnchor') . $galleryPic->space_pic_dir . DS . $galleryPic->file,
                                                                    [
                                                                        'escape' => false
                                                                    ]
                                                                );
                                                        }
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                <?php } ?>
                                <div class="action-btn">
                                    <?php
                                        $isActive = ($space->is_activated) ? "active" : "default";;
                                        echo $this->Html->link(__('Activate'),
                                                'javascript:void(0)',
                                                [
                                                    'class' => 'update-space-status active-space btn btn-' . $isActive,
                                                    'data-url' => $this->Url->build([
                                                                                    'controller' => 'Users',
                                                                                    'action' => 'updateUserStatus',
                                                                                    'prefix' => 'api',
                                                                                    $space->id,
                                                                                    'Spaces'
                                                                                ],true),
                                                    'data-status' => 1
                                                ]
                                            );
                                    ?>
                                    <?php
                                        $isDeactive = (!$space->is_activated) ? "active" : "default";;
                                        echo $this->Html->link(__('Deactivate'),
                                                'javascript:void(0)',
                                                [
                                                    'class' => 'update-space-status deactive-space btn btn-' . $isDeactive,
                                                    'data-url' => $this->Url->build([
                                                                                    'controller' => 'Users',
                                                                                    'action' => 'updateUserStatus',
                                                                                    'prefix' => 'api',
                                                                                    $space->id,
                                                                                    'Spaces'
                                                                                ],true),
                                                    'data-status' => 0
                                                ]
                                            );
                                    ?>
                                    <?php
                                        echo $this->Html->link(__('Edit'),
                                                '/Spaces/edit/' . base64_encode($space->id),
                                                [
                                                    'class' => 'btn btn-default'
                                                ]
                                            );
                                    ?>
                                    <?php echo $this->element('loader'); ?>
                                </div>
                            </div>
                            <div class="col-xs-3 price-parking-space">
                                <?php if ($space->space_pricing->is_hourly_available) {
                                        echo $this->Number->currency($space->space_pricing->hourly_price, Configure::read('DefaultCurrenyFormat')) . ' ' . __('per hour');
                                    }
                                ?>
                            </div>
                            <div class="col-xs-3 price-parking-space">
                                <?php if ($space->space_pricing->is_daily_available) {
                                        echo $this->Number->currency($space->space_pricing->daily_price, Configure::read('DefaultCurrenyFormat')) . ' ' . __('per day');
                                    }
                                ?>
                            </div>
                            <div class="col-xs-3 price-parking-space">
                                <?php if ($space->space_pricing->is_monthly_available) {
                                        echo $this->Number->currency($space->space_pricing->monthly_price, Configure::read('DefaultCurrenyFormat')) . ' ' . __('per month');
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-4 space-right-section">
                            <div style='overflow:hidden;height:350px;width:100%;'>
                                <div id='gmap_canvas_<?php echo $space->id; ?>' style='height:350px;width:100%;'></div>
                            </div>
                        </div>
                    </div>
                </div>
    <?php
            }
        } else {
    ?>
            <div class="my-space-listing clearfix col-xs-12">
                <div class="well well-sm no-bg clearfix">
                    <div class="col-sm-8">
                        <div class="col-xs-9">
                            <?php echo __('You have added no space yet.'); ?>
                        </div>
                    </div>
                </div>
            </div>
    <?php } ?>
    <?php echo $this->element('pagination'); ?>
</div>
<script src="http://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo GOOGLE_MAP_API_KEY; ?>"></script>
<script type="text/javascript">
    var latlng = <?php echo json_encode($latLng); ?>;
</script>
<?php
    echo $this->Html->css([
            'Frontend/lightbox'
        ], [
            'block' => 'css'
        ]);
    echo $this->Html->script([
            'Frontend/Space/lightbox',
            'Frontend/Space/scripts',
            'Frontend/Space/index'
        ], [
            'block' => 'scriptBottom'
        ]);
?>