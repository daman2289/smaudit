<?php
    $this->Html->addCrumb(__('Dashboard'), ['controller' => 'Users', 'action' => 'dashboard']);
    $this->Html->addCrumb(__('Subsidiaries'), ['controller' => 'Subsidiaries', 'action' => 'index']);
?>
<div class="col-sm-12 breadcrumb-no-bg">
    <div class="row">
       <?php
            echo $this->Html->getCrumbList(
                [
                    'firstClass' => false,
                    'lastClass' => 'active',
                    'class' => 'breadcrumb'
                ]
            );
        ?>
    </div>
</div>
    <div class="top-header col-sm-12">
        <div class="col-sm-6">
            <div class="top-title"> <?= __('My Subsidary') ?>
            </div>
        </div>
        <div class="col-sm-6 text-right">
            <a href="#"  data-toggle="modal" data-target="#addSubsidiary" class="btn btn-blue"><?= __('Add Subsidary') ?></a>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table col-lg-12">
                <thead>
                    <tr>
                        <th><?php echo __($this->Paginator->sort('name',__('Name')))    ?></th>
                        <th><?php echo __($this->Paginator->sort('phone',__('Phone')))  ?></th>
                        <th><?php echo __($this->Paginator->sort('fax',__('Fax')))      ?></th>
                        <th><?php echo __($this->Paginator->sort('street',__('Street'))) ?></th>
                        <th><?php echo __($this->Paginator->sort('street_number',__('Street No')))  ?></th>
                        <th><?php echo __($this->Paginator->sort('zip',__('Zip Code'))) ?></th>
                        <th><?php echo __($this->Paginator->sort('place',__('Place')))  ?></th>
                        <th><?php echo __($this->Paginator->sort('status',__('Status')))?></th>
                        <th><?php echo __($this->Paginator->sort('',__('Actions'))) ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        if(!empty($subsidiaries->toArray())) {
                            foreach($subsidiaries as $subsidiary) {
                            
                    ?>
                    <tr>
                        <td><?php echo __(htmlspecialchars($subsidiary->name)) ?></td>
                        <td><?php echo __(htmlspecialchars($subsidiary->phone)) ?></td>
                        <td><?php echo __(htmlspecialchars($subsidiary->fax)) ?></td>
                        <td><?php echo __(htmlspecialchars($subsidiary->street)) ?> 
                        </td>
                        <td>
                            <?php echo __(htmlspecialchars($subsidiary->street_number)) ?>
                        </td>
                        <td><?php echo __(htmlspecialchars($subsidiary->zip)) ?></td>
                        <td><?php echo __(htmlspecialchars($subsidiary->place)) ?></td>
                        <td>  
                            <span class="switch-toggle custom-switch">
                                <?php echo $this->Form->input('',array(
                                "data-url" => $this->Url->build(array('controller' => 'Subsidiaries','action' => 'deactivate',base64_encode($subsidiary->id)),true),
                                "id"=> "test".$subsidiary->id,
                                "class" =>  "switch_custom",
                                "type" => "checkbox",
                                "hidden" => "hidden",
                                "label" => false,
                                'templates' => [
                                   'inputContainer' => '{{content}}',
                                   'inputContainerError' => '{{content}}{{error}}'
                                 ],
                                "value"=>$subsidiary->id,
                                 ($subsidiary->status == 1) ? 'checked' : ''
                                ))
                              ?>
                                <label for="test<?php echo $subsidiary->id?>" class="switch"></label>
                            </span>
                        </td>
                        <td>
                            <?= $this->Html->Link("<span class='fa fa-pencil-square-o'></span>",
                                   'javascript:void(0)',
                                   [
                                       'escape'   => false,
                                       'class'    => 'edit-subsidiary',
                                       'data-url' =>   $this->Url->build(
                                                   [  
                                                       "controller" => "Subsidiaries",
                                                       "action" => "edit",
                                                       base64_encode($subsidiary->id)
                                                   ],true
                                               ),
                                        'title'  => 'edit'                           
                                   ]
                               )
                            ?> 
                            <?php 
                                if($subsidiary->is_primary) {
                                    echo 'Primary';
                                }else {                                    
                                    echo $this->Form->postLink(__("Set Primary"),
                                        [
                                            'controller' => 'Subsidiaries',
                                            'action' => 'set-primary',
                                            base64_encode($subsidiary->id)
                                        ],
                                        [
                                            'escape'   => false,
                                            'class'    => '',
                                            'title'  => 'Set as primary'  ,
                                            'confirm'   => __('There can be only one primary subsidiary.By clicking ok every other subsidiary will be set to secondary.')                         
                                        ]
                                    );
                                }
                            ?> 
                        </td>
                    </tr>
                    <?php } } else { ?>
                            <tr>
                                <td colspan="6" class="text-center">
                                    <?php echo __('No Record Found'); ?>
                                </td>
                            </tr>
                     <?php } ?>
                </tbody>
            </table>
            <?php echo $this->element('pagination'); ?>
        </div>
    </div>
<div class="modal Educational-info fade" id="edit-subsidiary" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Edit Organization Subsidiary'); ?></h4>
            </div>
            <div class="modal-body clearfix">
            </div>
        </div>
    </div>
</div>
<?php echo $this->element('Subsidiaries/add');?>
<?php echo $this->Html->script('Frontend/Subsidiary/add', ['block' => 'scriptBottom']); ?>

<?php echo $this->Html->script('Frontend/Users/profile', ['block' => 'scriptBottom']); ?>

