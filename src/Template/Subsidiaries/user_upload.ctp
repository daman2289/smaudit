<div class="col-xs-12 col-sm-offset-4 upload">
        <?php echo $this->Form->create(null, array('url' => array('controller' => 'Subsidiaries', 'action' => 'download'),'id' => 'user-download')); ?>
        <div class="form-group">
            <label><?php echo __('Download Sample File'); ?></label>
            <div>
             <button type="submit" class="btn btn-black">Download</button>
             </div>
        </div>
        </form>
</div>
<div class="col-xs-12 col-sm-offset-4">
        <?php
        echo $this->Form->create(null,
                [
                        'type' => 'file',
                        'url' => [
                            'controller' => 'Subsidiaries',
                            'action' => 'userUpload'
                        ],
                        'id' => 'upload-organization-user',
                        'class' => 'form-horizontal'
                ]
            );
        ?>
        <div>
            <label><?php echo __('Browse User Csv'); ?></label>
            <?php echo $this->Form->input('user',array('type' => 'file','label' => false));?>
            <h6>Note: Upload only csv files</h6>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
            <?php
                echo $this->Form->button('upload', array(
                            'class' => 'btn btn-blue',
                            'type' => 'submit'
                            ));
            ?>
            </div>
        </div>
    </form>
</div>
<style>
   .upload {
    margin-top: 30px;
}
</style>
<?php echo $this->Html->script('Frontend/Users/login_register', ['block' => 'scriptBottom']); ?>
          