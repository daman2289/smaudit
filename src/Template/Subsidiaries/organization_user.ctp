<?php
    $this->Html->addCrumb(__('Dashboard'), ['controller' => 'Users', 'action' => 'dashboard']);
    $this->Html->addCrumb(__('Users'), ['controller' => 'Subsidiaries', 'action' => 'organization-user']);
?>
<div class="col-sm-12 breadcrumb-no-bg">
    <div class="row">
       <?php
            echo $this->Html->getCrumbList(
                [
                    'firstClass' => false,
                    'lastClass' => 'active',
                    'class' => 'breadcrumb'
                ]
            );
        ?>
    </div>
</div>

<div class="top-header col-sm-12">
    <div class="col-sm-2">
        <div class="top-title"> 
            <?php echo __('Users');?>
        </div>
    </div>
    <div class="col-md-5"> 
        <?php
            if(empty($subsidiary)){ 
                echo __("Before adding user there should be atleast one existing subsidiary for the organization. Kindly add your subsidiary first. ");
                echo $this->Html->link( 
                    __('Add Subsidiary'), 
                    ['controller' => 'subsidiaries', 'action' => 'index'],
                    ['class' => 'btn btn-xs btn-primary']
                );
            }
        ?>

    </div>
    <div class="col-sm-5 text-right">
        <a href="#"  data-toggle="modal" data-target="#addUser" class="btn btn-blue"><?php echo __('Add User'); ?></a>
        <a href="/Subsidiaries/userUpload" class="btn btn-blue"><?php echo __('Upload User CSV'); ?></a>
    </div>
    <div class="col-md-12">
        <h3 class="text-center">
        <?php
            if(!empty($upgradeRequired)) {
            
                echo __('Upgrade your plan to add more user ');
                echo $this->Html->link(__('click here'),[
                    'controller' => 'additional-user-subscription-plans'
                ]);
            }
        ?>
        </h3>
    </div>
</div>
<div class="col-lg-12">
    <div class="table-responsive">
        <table class="table col-lg-12 user-table">
            <thead>
                <tr>
                    <th><?php echo __($this->Paginator->sort('user_id','User Id'))?></th>
                    <th><?php echo __($this->Paginator->sort('first_name','Name'))?></th>
                    <th><?php echo __($this->Paginator->sort('organization_subsidiary_id','Subsidary'))?></th>
                    <th><?php echo __($this->Paginator->sort('license_id','License'))?></th>
                    <th><?php echo __($this->Paginator->sort('created','Created Date'))?></th>
                    <th><?php echo __($this->Paginator->sort('modified','Modified Date'))?></th>
                    <th><?php echo __($this->Paginator->sort('','Actions'))?></th>
                </tr>
            </thead>
            <tbody>

                <?php if(!empty($users->toArray())){
                    foreach($users as $user) {
                        
                      ?>
              
                <tr>
                    <td><?php //echo $user['user']->id ;?>
                         <?php 
                            echo ($user->has('user')) ?
                            $user->user->id:
                            '' 
                        ;?>
                    </td>
                    <td>
                        <?php //echo $user['user']->first_name.' '.$user['user']->last_name ;?>
                        <?php 
                            echo htmlspecialchars(($user->has('user')) ?
                            $user->user->first_name.' '.$user->user->last_name :
                            '' );
                        ?>    
                    </td>
                    <td><?php echo htmlspecialchars($user->organization_subsidiary->name) ;?></td>
                    <td><?php echo htmlspecialchars($user->license->name) ;?></td>
                    <td><?php echo date('Y-m-d',strtotime($user->created));?></td>
                     <td><?php echo date('Y-m-d',strtotime($user->modified));?></td>
                    <td>
                    <?= $this->Html->Link("<i class='fa fa-pencil-square-o'></i>".__('Edit'),
                           'javascript:void(0)',
                           [
                               'escape'   => false,
                               'class'    => 'edit-org-user',
                               'data-url' =>   $this->Url->build(
                                           [  
                                               "controller" => "Subsidiaries",
                                               "action" => "editUser",
                                              // base64_encode($user['user']->id)
                                               ($user->has('user')) ? base64_encode($user->user->id) :''

                                           ],true
                                       ),
                                'title'  => 'edit'                           
                           ]
                       )
                    ?> 
                    <?= $this->Form->postLink("<i class='fa fa-trash-o'></i>".__('Delete'),
                           '/Subsidiaries/deleteUser/'.base64_encode($user->user_id),
                            [
                               'escape'   => false,
                               'class'    => 'delete-user',
                                'title'  => 'delete',
                                'confirm' => 'Are you sure you want to delete Organization User?'                           
                             ]
                       )
                    ?> 
                    <?= $this->Html->Link("<i class='fa fa-braille'></i>".__('Set Permissions'),
                            'javascript:void(0)',                          
                            [
                                'escape'    => false,
                                'class'     => 'set-permission',
                                'data-url'  =>   $this->Url->build(
                                    [  
                                        'controller' => 'subsidiaries',
                                        'action'    => 'userPermissions',
                                        base64_encode($user->user_id)

                                    ],true
                                ),
                                'title'     => 'Set User Permissions',                       
                            ]
                       )
                    ?> 
                    </td>
                </tr>
                <?php } } else { ?>
                <tr>
                    <td colspan="6" class="text-center">
                        <?php echo __('No Record Found'); ?>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<div class="modal addSubsidiary fade" id="editUser" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times-circle" aria-hidden="true"></i></span></button>
                <h4 class="modal-title text-center"><?php echo __('Edit User'); ?></h4>
            </div>
            <div class="modal-body clearfix">
            </div>
        </div>
    </div>
</div>
<?php echo $this->element('Subsidiaries/add_user');?>
<?php echo $this->element('empty_modal');?>
<?php 
    echo $this->Html->script(
            [
                'Frontend/Subsidiary/user',
                'Frontend/Users/profile',
                'Frontend/Subsidiary/permission'
            ], 
            ['block' => 'scriptBottom']
        );

?>
<?php echo $this->Html->script('Frontend/Users/profile', ['block' => 'scriptBottom']); ?>
