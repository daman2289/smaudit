<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Smaudit</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!--link rel="shortcut icon" href="favicon.ico"/-->
        <!-- Bootstrap 3.3.5 -->
        <?php echo $this->Html->css([
                    'bootstrap.min',
                    'Administration/login'
                ]);
            echo $this->fetch('meta');
            echo $this->fetch('css');
        ?>
    </head>
    <body>
	    <div class="site-wrapper">
	    	<div class="site-wrapper-inner">
	    		<div class="container">
                    <div class="col-md-8 col-lg-offset-1 col-md-offset-1 login-inner-wraper padding-zero">
                        <div class="col-md-12 padding-zero">
                            <?php echo $this->Flash->render(); ?>
                        </div>
                        <div class="background-strip-top"></div>
	                    <?php echo $this->fetch('content'); ?>
	                </div>
				</div>
			</div>
		</div>
    </body>
</html>