<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo __('Smaudit'); ?></title>
        <?php
            echo $this->Html->css([
                'bootstrap.min',
                'Frontend/style',
                'Frontend/sticky-footer',
                'Frontend/font-awesome'
            ]);
            echo $this->fetch('meta');
            echo $this->fetch('css');
        ?>
        <link href="https://fonts.googleapis.com/css?family=Overpass:100,100i,200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    </head>

    <body>
        <?php
            echo $this->element('Frontend/header');
            echo $this->Flash->render();
            echo $this->fetch('content');
            echo $this->element('Frontend/footer');
            echo $this->Html->script([
                'jQuery-2.1.4.min',
                'bootstrap.min',
                'Dashboard/jquery.maskedinput',
                'Dashboard/jquery.validate',
                'additional-methods'
            ]);
            echo $this->fetch('scriptBottom');
        ?>
        <script type="text/javascript">
            $(function() {
              $('a[href*=#]:not([href=#])').click(function() {
                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                  var target = $(this.hash);
                  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                  if (target.length) {
                    $('html,body').animate({
                      scrollTop: target.offset().top
                    }, 1000);
                    return false;
                  }
                }
              });
              
            });
        </script>
    </body>           
</html>