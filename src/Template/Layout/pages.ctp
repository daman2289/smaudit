<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo __('Stash My Ride'); ?></title>
        <?php
            echo $this->Html->css([
                'bootstrap.min',
                'Frontend/home',
                'Frontend/font-awesome',
                'Frontend/sticky-footer'
            ]);
            echo $this->fetch('meta');
            echo $this->fetch('css');
        ?>
        <link href="https://fonts.googleapis.com/css?family=Overpass:100,100i,200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    </head>

    <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
        <?php
            echo $this->element('Frontend/header');
            echo $this->Flash->render();
            echo $this->fetch('content');
            echo $this->element('Frontend/footer');
            echo $this->Html->script([
                'jQuery-2.1.4.min',
                'bootstrap.min',
                'Frontend/jquery.easing.min',
                'Frontend/scrolling-nav'
            ]);
            echo $this->fetch('scriptBottom');
        ?>
        <script>
            $(document).ready(function(){  
                var scroll_pos = 0;
                $(document).scroll(function() { 
                    scroll_pos = $(this).scrollTop();
                    if(scroll_pos > 100) {
                        $(".navbar-default").addClass('change-scroll');
                    } else {
                        $(".navbar-default").removeClass('change-scroll');
                    }
                });
            });
        </script>
    </body>           
</html>