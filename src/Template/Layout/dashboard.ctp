<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <?= $this->Html->meta('icon') ?>

        <?php
            echo $this->Html->css([
                'bootstrap.min',
                'Dashboard/jquery-ui',
                'Dashboard/style',
                'Dashboard/font-awesome',
                'Dashboard/sticky-footer',
                'Dashboard/custom-checkbox',
                'Dashboard/custom-font',
                'Dashboard/custom-switch',
                'pnotify/pnotify.custom.min',
                'Dashboard/skin-blue',
                'Dashboard/AdminLTE',
                'Frontend/bootstrap-multiselect',
                'Frontend/bootstrap-datetimepicker',

            ]);
            echo $this->fetch('meta');
            echo $this->fetch('css');
        ?>
        <!-- Font Awesome -->
        <link href="https://fonts.googleapis.com/css?family=Overpass:100,100i,200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i" rel="stylesheet">
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper clearfix">
            <?php
                echo $this->element('Frontend/Dashboard/header');
                echo $this->element('Frontend/Dashboard/left_navigation'); ?>
                <div class="content-wrapper clearfix ">
                    <?php  
                        echo $this->Flash->render(); 
                        echo $this->element('Organization/notification');                      
                        echo $this->fetch('content'); 
                    ?>
                </div>
                <?php echo $this->element('Frontend/Dashboard/footer');
            ?>
        </div>
        <?php
            echo $this->Html->script([
                'jQuery-2.1.4.min',
                'bootstrap.min',
                'Dashboard/jquery-ui.min',
                'Dashboard/app',
                'Dashboard/jquery.maskedinput',
                'Dashboard/jquery.validate',
                'Dashboard/custom',
                'additional-methods',
                'Dashboard/jquery.inputmask',
                'Dashboard/highcharts',
                'Dashboard/exporting',
                'Frontend/moment',
                'Frontend/bootstrap-datetimepicker',
                'Frontend/bootstrap-multiselect'

            ]);
            echo $this->fetch('scriptBottom');
        ?>
        <script type="text/javascript">
            $("[data-mask]").inputmask();
            $('.dateInput').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        </script>
    </body>
</html>