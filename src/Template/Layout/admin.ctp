<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>
            Smaudit
        </title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!--link rel="shortcut icon" href="favicon.ico"/-->
        
        <!-- Bootstrap 3.3.5 -->
        <?php
            echo $this->Html->css([
                    'bootstrap.min',
                    'Administration/style',
                    'Administration/AdminLTE',
                    'Administration/custom-switch',
                    'pnotify/pnotify.custom.min',
                    'jquery-ui.css'

                ]);
            echo $this->fetch('meta');
            echo $this->fetch('css');
        ?>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper clearfix">
            <?php
                echo $this->element('Administration/header');
                echo $this->element('Administration/left_navigation');
            ?>
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper clearfix">
                    <?php
                        echo $this->Flash->render();
                        echo $this->fetch('content');
                    ?>
                </div>
        </div>
        <?php
            echo $this->element('Administration/change_password');
            echo $this->element('Administration/profile');
        ?>
        <?php
            echo $this->Html->script([
                    'jQuery-2.1.4.min',
                    'bootstrap.min',
                    'jquery.validate',
                    'additional-methods',
                    'app',
                    'underscore-min',
                    'pnotify/jquery.pnotify',
                    'pnotify/index',
                    'jquery-ui.js',
                    'common_validation_methods',
                    'location',
                    'Frontend/moment',
                    'Frontend/bootstrap-datetimepicker'
                    
                ]
            );
            echo $this->fetch('scriptBottom');
        ?>
        <?php 
            $bodyClass = '';
            if($this->request->Session()->read('collapse')) {
                if($this->request->Session()->read('collapse') == 1) {
                    $bodyClass = 'sidebar-collapse'; 
                }else {
                    $bodyClass = 'apse';
                }
            }
            echo "<script>";
                echo "var bodyClass; bodyClass ='". $bodyClass."';";
            echo "</script>";
        ?>

        <script>
            console.log(bodyClass);
            $(function() {
                if(bodyClass) {
                    $("body").addClass(bodyClass);
                }else {                    
                    if ($(window).width() < 1024) {
                        $("body").addClass('sidebar-collapse');
                    }
                }
                // var equalto = $(".content-wrapper").innerHeight();
                // console.log(equalto);
                // $(".main-sidebar").innerHeight(equalto);
                $('.sidebar-toggle').on('click', function(){
                    console.log();
                    var url = '/admin/admins/manu_collapse/0';
                    if($('body').hasClass('sidebar-collapse')) {
                        url = '/admin/admins/manu_collapse/1';
                    }
                    $.ajax({
                        type :   "GET",
                        url  :    url,        
                        success : function(data){
                        },
                       
                    });
                });
            });
        </script>
   </body>
</html>