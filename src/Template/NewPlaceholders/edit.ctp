<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $newPlaceholder->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $newPlaceholder->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List New Placeholders'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List New Reports'), ['controller' => 'NewReports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New New Report'), ['controller' => 'NewReports', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List New Placeholder Types'), ['controller' => 'NewPlaceholderTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New New Placeholder Type'), ['controller' => 'NewPlaceholderTypes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List New Questions'), ['controller' => 'NewQuestions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New New Question'), ['controller' => 'NewQuestions', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="newPlaceholders form large-9 medium-8 columns content">
    <?= $this->Form->create($newPlaceholder) ?>
    <fieldset>
        <legend><?= __('Edit New Placeholder') ?></legend>
        <?php
            echo $this->Form->control('new_report_id', ['options' => $newReports]);
            echo $this->Form->control('new_placeholder_type_id', ['options' => $newPlaceholderTypes, 'empty' => true]);
            echo $this->Form->control('uid');
            echo $this->Form->control('lft');
            echo $this->Form->control('rght');
            echo $this->Form->control('name');
            echo $this->Form->control('content_en');
            echo $this->Form->control('content_er');
            echo $this->Form->control('content_fr');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
