<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New New Placeholder'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List New Reports'), ['controller' => 'NewReports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New New Report'), ['controller' => 'NewReports', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List New Placeholder Types'), ['controller' => 'NewPlaceholderTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New New Placeholder Type'), ['controller' => 'NewPlaceholderTypes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List New Questions'), ['controller' => 'NewQuestions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New New Question'), ['controller' => 'NewQuestions', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="newPlaceholders index large-9 medium-8 columns content">
    <h3><?= __('New Placeholders') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('new_report_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('new_placeholder_type_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('uid') ?></th>
                <th scope="col"><?= $this->Paginator->sort('lft') ?></th>
                <th scope="col"><?= $this->Paginator->sort('rght') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('content_en') ?></th>
                <th scope="col"><?= $this->Paginator->sort('content_er') ?></th>
                <th scope="col"><?= $this->Paginator->sort('content_fr') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($newPlaceholders as $newPlaceholder): ?>
            <tr>
                <td><?= $this->Number->format($newPlaceholder->id) ?></td>
                <td><?= $newPlaceholder->has('new_report') ? $this->Html->link($newPlaceholder->new_report->name, ['controller' => 'NewReports', 'action' => 'view', $newPlaceholder->new_report->id]) : '' ?></td>
                <td><?= $newPlaceholder->has('new_placeholder_type') ? $this->Html->link($newPlaceholder->new_placeholder_type->name, ['controller' => 'NewPlaceholderTypes', 'action' => 'view', $newPlaceholder->new_placeholder_type->id]) : '' ?></td>
                <td><?= h($newPlaceholder->uid) ?></td>
                <td><?= $this->Number->format($newPlaceholder->lft) ?></td>
                <td><?= $this->Number->format($newPlaceholder->rght) ?></td>
                <td><?= h($newPlaceholder->name) ?></td>
                <td><?= h($newPlaceholder->content_en) ?></td>
                <td><?= h($newPlaceholder->content_er) ?></td>
                <td><?= h($newPlaceholder->content_fr) ?></td>
                <td><?= h($newPlaceholder->created) ?></td>
                <td><?= h($newPlaceholder->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $newPlaceholder->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $newPlaceholder->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $newPlaceholder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $newPlaceholder->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
