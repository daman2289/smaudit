<?php
/**
  * @var \App\View\AppView $this
  */
?>

<?php 
    $this->Html->addCrumb(__('Dashboard'), ['controller' => 'Users', 'action' => 'dashboard']);
    $this->Html->addCrumb(__('Reports'), ['controller' => 'Reports', 'action' => 'report-listing']);
    $this->Html->addCrumb(__('Preview'));
?>
<div class="col-sm-12 breadcrumb-no-bg">
    <div class="row">
       <?php
            echo $this->Html->getCrumbList(
                [
                    'firstClass' => false,
                    'lastClass' => 'active',
                    'class' => 'breadcrumb'
                ]
            );
        ?>
    </div>
</div>
<section class="content">
    <div class="col-lg-12">                     
        <div id="emails">
            <h3><?= h($placeholder->name) ?></h3>
            <div class="form-group">
                <?= $this->Form->hidden('name', ['value' => $placeholder->name]) ?>
            </div>
            <div class="form-group">
                <h4>
                    Placeholder Type :
                <?php
                    echo ($placeholder->has('new_placeholder_type'))?$placeholder->new_placeholder_type->name : "" ;
                    $type =  $placeholder->new_placeholder_type_id;
                ?>
                </h4>   
                <h4>
                    <?php
                        if(!empty($placeholder->content_en)) {
                            echo "Replaced By : ". ucwords(str_replace('_', ' ', $placeholder->content_en));
                        }
                    ?>
                </h4>             
            </div>
            <div id='question-block' class='<?= $type == 3 ? "" :"hide"?>'>
                <?php if(!empty($placeholder->new_questions)): ?>
                    <h3>
                        Placeholder Question                     
                    </h3>
                    <h4><?php 
                            $types = $questionTypes->toArray();
                            echo 'Question Type : '.  $types[$placeholder->new_questions[0]->new_question_type_id];
                        ?>                    
                    </h4>
                                   
                    <?php                         
                        echo '<h4> Question in English : '. $placeholder->new_questions[0]->question_en. '</h4>';
                        echo '<h4> Question in German : '. $placeholder->new_questions[0]->question_er. '</h4>';
                        echo '<h4> Question in French : '. $placeholder->new_questions[0]->question_fr. '</h4>';
                    ?>
                    <?php 
                        $question = (!empty($placeholder->new_questions)) ?$placeholder->new_questions[0]  : '';
                        if($placeholder->new_placeholder_type_id ==3 && !empty($question) && !empty($question->new_question_options)):
                            echo '<h3> Question Options </h3>';
                    ?>
                            <div class="row">
                                <div class="col-md-2 text-center"><h4>Type</h4></div>
                                <div class="col-md-5"><h4>Option Label</h4></div>
                                <div class="col-md-5"><h4>Replacement text</h4></div>
                            </div>
                    <?php
                            foreach ($question->new_question_options as $index => $option) :
                    ?>
                            <div class="row">  
                                <div class="col-md-2">
                                    <div class="form-group">        
                                        <input type="radio" name="" class="form-control" checked>
                                    </div>              
                                </div>      
                                <div class="col-md-5">              
                                    <div class="form-group">
                                        <?= $option->name_en ?>
                                    </div>
                                </div>      
                                <div class="col-md-5">      
                                    <div class="form-group"> 
                                        <?= $option->content_en ?>
                                    </div>  
                                </div>
                            </div>
                    <?php
                            endforeach;
                        endif;
                    ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php 
    echo $this->Html->script([
            'functions'
        ], 
        [
            'block' => 'scriptBottom'
        ]
    ); 
?>