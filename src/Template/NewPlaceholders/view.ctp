<?php
/**
  * @var \App\View\AppView $this
  */
?>

<?php 
    $this->Html->addCrumb(__('Dashboard'), ['controller' => 'Users', 'action' => 'dashboard']);
    $this->Html->addCrumb(__('Reports'), ['controller' => 'Reports', 'action' => 'report-listing']);
    $this->Html->addCrumb(__('Preview'));
?>
<div class="col-sm-12 breadcrumb-no-bg">
    <div class="row">
       <?php
            echo $this->Html->getCrumbList(
                [
                    'firstClass' => false,
                    'lastClass' => 'active',
                    'class' => 'breadcrumb'
                ]
            );
        ?>
    </div>
</div>
<section class="content">
    <div class="col-lg-12">                     
        <div id="emails">
        <?= $this->Form->create('', 
            [
                'type' => 'post', 
                'url' => ['controller'=> 'new-placeholders', 'action' => 'edit',$placeholder->id] , 
                'class' => 'form-group'
            ]) 
        ?>
            <h3><?= h($placeholder->name) ?></h3>
            <div class="form-group">
                <?= $this->Form->hidden('name', ['value' => $placeholder->name]) ?>
            </div>
            <div class="form-group">                
                <?= 
                    $this->Form->select('new_placeholder_type_id', 
                        $types, 
                        [
                            'empty' => 'Choose placeholder type', 
                            'class' => 'placeholder-type form-control', 
                            'onChange' => 'showInputForPlaceholder(this.value)'
                        ]
                    ) 
                ?>
            </div>

            <?= $this->element('Placeholders/system_placeholder') ?>
            <?= $this->element('Placeholders/normal_placeholder') ?>
            <?= $this->element('Placeholders/question_placeholder') ?>

            <div class="related">
                <?= $this->element('OptionTemplates/options') ?>
                <?= $this->element('OptionTemplates/new') ?>
            </div>
            <div id='option-block'>
                <div id='option-block-body'>
                    
                </div> 
                <div id= 'option-add-new-button'>
                    
                </div>           
            </div>
            <div class="col-lg-8 form-group">
                <?= $this->Form->button(__('Save'), ['class' => "btn btn-primary submit-info submit_black", 'type' => 'submit']); ?>
            </div>
        <?= $this->Form->end() ?>
        </div>
    </div>
</section>
<?php 
    echo $this->Html->script([
            'functions'
        ], 
        [
            'block' => 'scriptBottom'
        ]
    ); 
?>