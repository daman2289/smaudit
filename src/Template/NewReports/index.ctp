<?php
/**
  * @var \App\View\AppView $this
  */
?>
<?php
    use Cake\Core\Configure;
    $this->Html->addCrumb(__('Dashboard'), ['controller' => 'Users', 'action' => 'dashboard']);
    $this->Html->addCrumb(__('Reports'));
?>

<div class="col-sm-12 breadcrumb-no-bg">
    <div class="row">
       <?php
            echo $this->Html->getCrumbList(
                [
                    'firstClass' => false,
                    'lastClass' => 'active',
                    'class' => 'breadcrumb'
                ]
            );
        ?>
    </div>
</div>
<div class="top-header col-sm-12">
    <div class="col-sm-2">
        <div class="top-title"> <?= __('Reports') ?> </div>
    </div>
    <div class="col-sm-10 text-right">
        <div class="dropdown text-right">
            <a id="dLabel" data-target="#" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="btn btn-blue">
                <i class="fa fa-plus-circle"></i>Add Report
                <span class="caret"></span>
            </a>

            <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                <?php foreach ($reports as $key => $value): ?>
                    <li>
                        <?= $this->Html->link(__($value),[
                                'controller' => 'new-questions',
                                'action' => 'index' ,
                                base64_encode($key)
                            ],[
                                'escape' => false
                            ]) 
                        ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
<div class="col-sm-12 form-group ">
    <div class="row">
    <?=
        $this->Form->create('',array('type' => 'get'));
    ?> 
        <div class="col-sm-3">
            <div class="input-group ">
                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
                <?= 
                    $this->Form->input(
                        'search',
                        [
                            'class' =>'form-control input-lg', 
                            'placeholder'   =>'Search by company', 
                            'aria-describedby'  => "basic-addon1",
                            'label' => false,
                            'div'   => false
                        ]
                    ) 
                ?>
            </div> 
        </div>
        <div class="col-sm-2">
            <button class="btn btn-search"><?= __('Search') ?></button>
        </div>

        <?= $this->Form->end() ?>
    </div>
</div>
<div class="col-lg-12">
    <div class="table-responsive">
        <table class="table col-lg-12" style="margin-bottom: 240px;">
            <thead>
                <tr class="bg-primary">
                    <th><?php echo __($this->Paginator->sort('created',__('Report Date')))?></th>
                    <th><?php echo __($this->Paginator->sort('company',__('Company')))?></th>
                    <th><?php echo __($this->Paginator->sort('name',__('Report Type')))?></th>
                    <th><?php echo __($this->Paginator->sort('',__('Action')))?></th>
                    <th></th>
                    
                </tr>
            </thead>
            <tbody>
            <?php 
                if(!empty($savedReports)) :
                    foreach($savedReports as $key => $report) :
            ?>
                <tr class="bg-success">
                    <td>                         
                        <?php echo htmlspecialchars($report->created); ?>
                    </td>
                    <td>                         
                        <?php echo htmlspecialchars($report->company); ?>
                    </td>
                    <td><?= htmlspecialchars($report->new_report->name) ?></td>
                    <td>
                        <?= 
                            $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i>',
                                [  
                                   "controller" => "new-final-reports",
                                   "action" => "edit",
                                   base64_encode($report->id),
                                   '?' => ['lan' => 'en']
                                ],
                                [
                                   'escape'   => false,
                                    'title'  => 'edit'  ,
                                    'class' => 'text-primary'                       
                                ]
                           )
                        ?>
                        <?= 
                            $this->Form->postLink('<i class="fa fa-trash" aria-hidden="true"></i>',
                                [  
                                   "controller" => "new-final-reports",
                                   "action" => "delete",
                                   base64_encode($report->id)
                                ],
                                [
                                   'escape'   => false,
                                    'title'  => 'Delete',
                                    'confirm' => __('Are you sure to delete report?'),
                                    'class' => 'text-danger'                          
                                ]
                           )
                        ?>
                        <?php
                            if(!empty($report->uuid)) {

                                echo $this->Html->link('Edit answer',
                                    [  
                                       "controller" => "new-reports",
                                       "action" => "edit-answer",
                                       base64_encode($report->new_report->id),
                                       $report->uuid
                                    ],
                                    [
                                        'escape'   => false,
                                        'title'  => 'Delete',
                                        'class' => 'text-warning'                          
                                    ]
                               );
                            }
                        ?>
                    </td>
                    <td>
                        <div class="dropdown text-right">
                            <a id="dLabel" data-target="#" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="btn btn-primary btn-sm">Download 
                                <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                                <?php $lang = [ 
                                        ['en', '<i class="fa fa-file-pdf-o" aria-hidden="true"></i> English', 'pdf' ], 
                                        ['er', '<i class="fa fa-file-pdf-o" aria-hidden="true"></i> German', 'pdf'], 
                                        ['fr', '<i class="fa fa-file-pdf-o" aria-hidden="true"></i> French', 'pdf'], 
                                        ['en', '<i class="fa fa-file-word-o" aria-hidden="true"></i> English', 'doc' ], 
                                        ['er', '<i class="fa fa-file-word-o" aria-hidden="true"></i> German', 'doc'], 
                                        ['fr', '<i class="fa fa-file-word-o" aria-hidden="true"></i> French', 'doc'],
                                    ] 
                                ?>
                                <?php foreach ($lang as $key => $value): ?>
                                    <li>
                                        <?= $this->Html->Link($value[1],
                                                [  
                                                   "controller" => "new-final-reports",
                                                   base64_encode($report->id),
                                                   '_ext' => $value[2],
                                                   '?'  => ['language' => $value[0]]
                                                ],
                                                [
                                                    'escape'    => false,
                                                    'class'     => '',                           
                                                ]
                                           )
                                        ?> 
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </td>
                </tr>                
            <?php 
                    endforeach;
                else:
            ?>
                    <tr>
                        <td colspan="7" class="text-center">
                            <?php echo __('No Record Found'); ?>
                        </td>
                    </tr>
             <?php endif; ?>
            </tbody>
        </table>
        <div class="text-right">
            
            <?php //echo $this->element('pagination'); ?>
        </div>
    </div>
</div>
<?php
    echo $this->element('empty_modal'); 
?>
<?php 
    echo $this->Html->script([
            'Frontend/Users/profile',
            'Dashboard/report',
            'Dashboard/view-report',
            'function'
        ], 
        [
            'block' => 'scriptBottom'
        ]
    ); 
?>
