<?php
/**
  * @var \App\View\AppView $this
  */
?>

<?php 
    $this->Html->addCrumb(__('Dashboard'), ['controller' => 'Users', 'action' => 'dashboard']);
    $this->Html->addCrumb(__('Reports'), ['controller' => 'Reports', 'action' => 'report-listing']);
    $this->Html->addCrumb(__('Preview'));
?>
<div class="col-sm-12 breadcrumb-no-bg">
    <div class="row">
       <?php
            echo $this->Html->getCrumbList(
                [
                    'firstClass' => false,
                    'lastClass' => 'active',
                    'class' => 'breadcrumb'
                ]
            );
        ?>
    </div>
</div>
<section class="content">
    <div class="col-lg-12">                     
        <div id="emails">
        <?= $this->Form->create($newReport) ?>
            <div class="form-group">
                <?= $this->Form->input('name', ['class' => 'form-control', 'label' => 'Report Name']) ?>
            </div>
            <div class="form-group">
                <?php
                    echo $this->Form->label('English');
                    echo $this->CKEditor->loadJs();
                    echo $this->Form->textarea('content_en', [
                        'class' =>'form-control', 
                        'label' => 'Content English',
                        'rows'  => 400,
                    ]);
                    echo $this->CKEditor->replace('content_en');
                    if ($this->Form->isFieldError('content_en')) {
                        echo $this->Form->error('content_en');
                    }
                ?>
            </div>
            <div class="form-group">
                <?php
                    echo $this->Form->label('German');
                    echo $this->CKEditor->loadJs();
                    echo $this->Form->textarea('content_er', [
                        'class' =>'form-control', 
                        'label' => 'Content English',
                        'rows'  => 400,
                    ]);
                    echo $this->CKEditor->replace('content_er');
                    if ($this->Form->isFieldError('content_er')) {
                        echo $this->Form->error('content_er');
                    }
                ?>
            </div>
            <div class="form-group">
                <?php
                    echo $this->Form->label('French');
                    echo $this->CKEditor->loadJs();
                    echo $this->Form->textarea('content_fr', [
                        'class' =>'form-control', 
                        'label' => 'Content English',
                        'rows'  => 400,
                    ]);
                    echo $this->CKEditor->replace('content_fr');
                    if ($this->Form->isFieldError('content_fr')) {
                        echo $this->Form->error('content_fr');
                    }
                ?>
            </div>
            <div class="col-lg-8">
                <?= $this->Form->button(__('Save'), ['class' => "btn btn-primary submit-info submit_black", 'type' => 'submit']); ?>
            </div>
        <?= $this->Form->end() ?>
        </div>
    </div>
</section>
<?php 
    echo $this->Html->script([
            'Dashboard/report-editor'
        ], 
        [
            'block' => 'scriptBottom'
        ]
    ); 
?>