<?php
/**
  * @var \App\View\AppView $this
  */
    use Cake\Core\Configure;

?>

<?php 
    $this->Html->addCrumb(__('Dashboard'), ['controller' => 'Users', 'action' => 'dashboard']);
    $this->Html->addCrumb(__('Reports'), ['controller' => 'NewReports', 'action' => 'report-listing']);
    $this->Html->addCrumb(__('Report Questions'));
?>
<div class="col-sm-12 breadcrumb-no-bg">
    <div class="row">
       <?php
            echo $this->Html->getCrumbList(
                [
                    'firstClass' => false,
                    'lastClass' => 'active',
                    'class' => 'breadcrumb'
                ]
            );
        ?>
    </div>
</div>
<section class="content">
    <div class="col-lg-12">
    <?php
        echo $this->Form->create(null, [
            
            'id' => 'answer'
        ])
    ?>
        <?php foreach ($answers as $key => $answer): ?>
            <div class='question-block'> 
                <?php
                    
                    echo $this->element(
                        Configure::read('FORMELEMENTFOREDIT.'.$answer->new_question->new_question_type->name), 
                        [
                            'question' => $answer->new_question->toArray(), 
                            'is_parent' => (!empty($answer->new_question->child_questions)) ? true : false,
                            'key' => $answer->new_question->id,
                            'answer'    => $answer
                        ]
                    );
                    if($answer->new_question->has('child_questions') && ($answer->new_question->new_question_type_id != 10) ) {
                        foreach ($answer->new_question->child_questions as $index => $val) {
                            // $childId = str_replace(",","",$val->_joinData->condition_for_child);
                            if($val->_joinData->condition_type == 'AND') {
                                $childId = 'child'.str_replace(",","",$val->_joinData->condition_for_child);
                            }else {
                                $childId = explode(',', $val->_joinData->condition_for_child);
                                foreach ($childId as $position => $ids) {
                                    $childId[$position] = 'child'.$ids;
                                }
                                $childId = implode(' ', $childId);
                            }
                            $secChild = (!empty($val->second_child_questions))? true : false;
                ?>
                            <div class='childs hide question-block <?= $childId ?>' id='<?= $childId ?>' data-last='<?= !($secChild) ?>'>                    
                                <?php 
                                    echo $this->element(
                                        Configure::read('FORMELEMENTFOREDIT.'.$val->new_question_type->name), 
                                        [
                                            'question' => $val->toArray(), 
                                            'is_parent' => $secChild,                                    
                                            'key' => $val->id,
                                            'answer'    => $val->child_answer
                                        ]
                                    );
                                ?>
                                <?php 
                                    if(!empty($val->second_child_questions) ):
                                        $secondQuestions = $val->second_child_questions;
                                        foreach($secondQuestions as $cindex => $cvalue): 
                                            // $secChildId = str_replace(",","",$cvalue->_joinData->condition_for_child);
                                            if($cvalue->_joinData->condition_type == 'AND') {
                                                $secChildId = 'child'.str_replace(",","",$cvalue->_joinData->condition_for_child);
                                            }else {
                                                $secChildId = explode(',', $cvalue->_joinData->condition_for_child);
                                                foreach ($secChildId as $cposition => $cids) {
                                                    $secChildId[$cposition] = 'child'.$cids;
                                                }
                                                $secChildId = implode(' ', $secChildId);
                                            }
                                ?>
                                        <div class='childs hide question-block <?= $secChildId ?>' id='<?= $secChildId ?>' data-last='<?= true ?>'>
                                            <?php 
                                                echo $this->element(
                                                    Configure::read('FORMELEMENTFOREDIT.'.$cvalue->new_question_type->name), 
                                                    [
                                                        'question' => $cvalue->toArray(), 
                                                        'is_parent' => false,    
                                                        'key' => $cvalue->id,
                                                        'answer'    => $cvalue->second_child_answer
                                                    ]
                                                );
                                            ?> 
                                        </div>
                                <?php 
                                        endforeach;
                                    endif; 
                                ?>
                            </div>
                <?php
                        }
                    }
                ?>
            </div>
        <?php endforeach; ?>
        <?= $this->Form->submit('submit', ['class' => 'btn btn-success']) ?>
    <?= $this->Form->end() ?>
    </div>
</section>
<?php 
    echo $this->Html->script([
            'functions',
            'edit-answer'
        ], 
        [
            'block' => 'scriptBottom'
        ]
    ); 
?>
<style type="text/css">
    input.error, select.error {
        border-color: red;
    }
</style>