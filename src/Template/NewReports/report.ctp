<?php
/**
  * @var \App\View\AppView $this
  */
    use Cake\Core\Configure;

?>

<?php 
    $this->Html->addCrumb(__('Dashboard'), ['controller' => 'Users', 'action' => 'dashboard']);
    $this->Html->addCrumb(__('Reports'), ['controller' => 'Reports', 'action' => 'report-listing']);
    $this->Html->addCrumb(__('Report Preview'));
?>
<div class="col-sm-12 breadcrumb-no-bg">
    <div class="row">
       <?php
            echo $this->Html->getCrumbList(
                [
                    'firstClass' => false,
                    'lastClass' => 'active',
                    'class' => 'breadcrumb'
                ]
            );
        ?>
    </div>
</div>
<section class="content">
    <div class="col-lg-12">
        <!-- <?php $report = preg_replace("/&nbsp;/",'',$report); ?> -->
        <h3>
            
            <?= $report ?>
        </h4>
    </div>
</section>
<?php 
    echo $this->Html->script([
            'functions'
        ], 
        [
            'block' => 'scriptBottom'
        ]
    ); 
?>