<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit New Report'), ['action' => 'edit', $newReport->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete New Report'), ['action' => 'delete', $newReport->id], ['confirm' => __('Are you sure you want to delete # {0}?', $newReport->id)]) ?> </li>
        <li><?= $this->Html->link(__('List New Reports'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New New Report'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List New Placeholders'), ['controller' => 'NewPlaceholders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New New Placeholder'), ['controller' => 'NewPlaceholders', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List New Report Placeholders'), ['controller' => 'NewReportPlaceholders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New New Report Placeholder'), ['controller' => 'NewReportPlaceholders', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="newReports view large-9 medium-8 columns content">
    <h3><?= h($newReport->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Uid') ?></th>
            <td><?= h($newReport->uid) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($newReport->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($newReport->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Activated') ?></th>
            <td><?= $this->Number->format($newReport->is_activated) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($newReport->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($newReport->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Content En') ?></h4>
        <?= $this->Text->autoParagraph(h($newReport->content_en)); ?>
    </div>
    <div class="row">
        <h4><?= __('Content Er') ?></h4>
        <?= $this->Text->autoParagraph(h($newReport->content_er)); ?>
    </div>
    <div class="row">
        <h4><?= __('Content Fr') ?></h4>
        <?= $this->Text->autoParagraph(h($newReport->content_fr)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related New Placeholders') ?></h4>
        <?php if (!empty($newReport->new_placeholders)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('New Report Id') ?></th>
                <th scope="col"><?= __('New Placeholder Type Id') ?></th>
                <th scope="col"><?= __('Uid') ?></th>
                <th scope="col"><?= __('Lft') ?></th>
                <th scope="col"><?= __('Rght') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Content En') ?></th>
                <th scope="col"><?= __('Content Er') ?></th>
                <th scope="col"><?= __('Content Fr') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($newReport->new_placeholders as $newPlaceholders): ?>
            <tr>
                <td><?= h($newPlaceholders->id) ?></td>
                <td><?= h($newPlaceholders->new_report_id) ?></td>
                <td><?= h($newPlaceholders->new_placeholder_type_id) ?></td>
                <td><?= h($newPlaceholders->uid) ?></td>
                <td><?= h($newPlaceholders->lft) ?></td>
                <td><?= h($newPlaceholders->rght) ?></td>
                <td><?= h($newPlaceholders->name) ?></td>
                <td><?= h($newPlaceholders->content_en) ?></td>
                <td><?= h($newPlaceholders->content_er) ?></td>
                <td><?= h($newPlaceholders->content_fr) ?></td>
                <td><?= h($newPlaceholders->created) ?></td>
                <td><?= h($newPlaceholders->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'NewPlaceholders', 'action' => 'view', $newPlaceholders->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'NewPlaceholders', 'action' => 'edit', $newPlaceholders->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'NewPlaceholders', 'action' => 'delete', $newPlaceholders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $newPlaceholders->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related New Report Placeholders') ?></h4>
        <?php if (!empty($newReport->new_report_placeholders)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('New Report Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($newReport->new_report_placeholders as $newReportPlaceholders): ?>
            <tr>
                <td><?= h($newReportPlaceholders->id) ?></td>
                <td><?= h($newReportPlaceholders->new_report_id) ?></td>
                <td><?= h($newReportPlaceholders->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'NewReportPlaceholders', 'action' => 'view', $newReportPlaceholders->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'NewReportPlaceholders', 'action' => 'edit', $newReportPlaceholders->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'NewReportPlaceholders', 'action' => 'delete', $newReportPlaceholders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $newReportPlaceholders->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
