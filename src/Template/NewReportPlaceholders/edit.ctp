<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $newReportPlaceholder->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $newReportPlaceholder->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List New Report Placeholders'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List New Reports'), ['controller' => 'NewReports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New New Report'), ['controller' => 'NewReports', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="newReportPlaceholders form large-9 medium-8 columns content">
    <?= $this->Form->create($newReportPlaceholder) ?>
    <fieldset>
        <legend><?= __('Edit New Report Placeholder') ?></legend>
        <?php
            echo $this->Form->control('new_report_id', ['options' => $newReports]);
            echo $this->Form->control('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
