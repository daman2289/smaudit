<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New New Report Placeholder'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List New Reports'), ['controller' => 'NewReports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New New Report'), ['controller' => 'NewReports', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="newReportPlaceholders index large-9 medium-8 columns content">
    <h3><?= __('New Report Placeholders') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('new_report_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($newReportPlaceholders as $newReportPlaceholder): ?>
            <tr>
                <td><?= $this->Number->format($newReportPlaceholder->id) ?></td>
                <td><?= $newReportPlaceholder->has('new_report') ? $this->Html->link($newReportPlaceholder->new_report->name, ['controller' => 'NewReports', 'action' => 'view', $newReportPlaceholder->new_report->id]) : '' ?></td>
                <td><?= h($newReportPlaceholder->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $newReportPlaceholder->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $newReportPlaceholder->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $newReportPlaceholder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $newReportPlaceholder->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
