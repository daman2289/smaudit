<?php
    use Cake\Filesystem\File;
    use Cake\Core\Configure;
    $this->Html->addCrumb(__('Dashboard'), ['controller' => 'Users', 'action' => 'dashboard']);
    $this->Html->addCrumb(__('Company Info'));
?>
<div class="col-sm-12 breadcrumb-no-bg">
    <div class="row">
       <?php
            echo $this->Html->getCrumbList(
                [
                    'firstClass' => false,
                    'lastClass' => 'active',
                    'class' => 'breadcrumb'
                ]
            );
        ?>
    </div>
</div>
<div class="organization-detail col-sm-12">
    <div class="col-sm-12">
        <div class="top-title"> 
            <?= __('Organization Detail') ?>
        </div>
    </div>
    <!-- <div class="col-sm-6 text-right">
        <a href="#" data-toggle="modal" data-target="#addSubsidiary" class="btn btn-blue">Add Subsidiary</a>
        </div> -->
    <?php //pr($queryToGetusers);?>
    <div class="col-lg-2">
        <div class="logo-organization">
            <?php 
            	$profile = $queryToGetusers['organization']->logo;
                $profilePicPath = 'auditA_front_logo.jpg';
                if(!empty($profile)) {
                    $profilePic = new File(Configure::read('LogoPicPathToCheck') . $queryToGetusers['organization']->logo_dir . DS . 'square_' .$profile, true, 0644);
                    if ($profilePic->exists()) {
                        $profilePicPath = Configure::read('LogoPicPath') . $queryToGetusers['organization']->logo_dir . DS . 'square_' . $profile;
                    }
                }
                echo $this->Html->image($profilePicPath, [
                            'class' => 'user-image', 'alt' => 'User Image'
                        ]);
                ?>
        </div>
    </div>
    <div class="col-lg-4 col-lg-offset-1">
        <h2><?php echo htmlspecialchars($queryToGetusers['organization']->name) ?></h2>
        <div class="text-organization">
            <i class="fa fa-phone-square" aria-hidden="true"></i> 
            <?php echo htmlspecialchars($queryToGetusers['organization']->phone) ?>
        </div>
        <div class="text-organization">
            <i class="fa fa-fax" aria-hidden="true"></i> 
            <?php 
                echo (!empty($queryToGetusers['organization']->fax)) ? 
                    htmlspecialchars($queryToGetusers['organization']->fax) : 
                    htmlspecialchars("no number") ;
            ?>
        </div>
        <div class="text-organization">
            <i class="fa fa-map-marker" aria-hidden="true"></i> 
            <?php 
                echo (!empty($queryToGetusers['organization']->street_number)) ? 
                    htmlspecialchars(($queryToGetusers['organization']->street.' '.$queryToGetusers['organization']->street_number.', '.$queryToGetusers['organization']->place)) : 
                        htmlspecialchars("No address");
            ?>
        </div>
        <div class="text-organization"><?php echo htmlspecialchars($queryToGetusers->email); ?></div>
    </div>
    <div class="col-lg-3">
        <div class="current-plan"> <?= __('Current Plan') ?>:
        </div>
        <div class="text-organization">
            <?php 
                if(!empty($organizationPlan)){
                    echo htmlspecialchars(ucwords($organizationPlan->subscription_plan->title));
                }
            ?>            
        </div>
    </div>
</div>