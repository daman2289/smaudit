<?php
    $this->Html->addCrumb(__('Dashboard'), ['controller' => 'Users', 'action' => 'dashboard']);
    $this->Html->addCrumb(__('Companies'), ['controller' => 'Companies', 'action' => 'index']);
?>
<div class="col-sm-12 breadcrumb-no-bg">
    <div class="row">
       <?php
            echo $this->Html->getCrumbList(
                [
                    'firstClass' => false,
                    'lastClass' => 'active',
                    'class' => 'breadcrumb'
                ]
            );
        ?>
    </div>
</div>
<div class="top-header col-sm-12">
    <div class="col-sm-6">
        <div class="top-title"> <?php echo __('Companies');?>
        </div>
    </div>
    <div class="col-sm-6 text-right">
        <a href="#"  data-toggle="modal" data-target="#addCompany" class="btn btn-sm btn-blue"><?php echo __('Add Company'); ?></a>
        <?php 
            echo $this->Html->link( __('Upload Companies'), [
                'controller' => 'companies', 
                'action'    => 'upload-companies'
            ], [
                'class' => "btn btn-sm btn-blue"
            ]);
        ?>
    </div>
</div>
<div class="col-lg-12">
    <div class="table-responsive">
        <div>
            <div class="col-lg-4">
                <div class="works-in-ie-login">
                    <label>
                    <input name="hide_active_company" value="0" type="hidden">
                    <input name="hide_active_company" value="1" type="checkbox">                            
                    <i class="faa fa-unchecked unchecked"></i>
                    <i class="faa fa-checked  checked"></i>
                    </label>
                    Hide Inactive Companies
                </div>
            </div>
        </div>
        <table class="table col-lg-12 user-table">
            <thead>
                <tr>
                    <th><?php echo __($this->Paginator->sort('company_name','Company Name'))?></th>
                    <th><?php echo __($this->Paginator->sort('street','Street'))?></th>
                    <th><?php echo __($this->Paginator->sort('street_number','Street No.'))?></th>
                    <th><?php echo __($this->Paginator->sort('place','Place'))?></th>
                    <th><?php echo __($this->Paginator->sort('zip','Zip'))?></th>
                    <th><?php echo __($this->Paginator->sort('place_of_incorporation','Place of Incorporation'))?></th>
                    <th><?php echo __($this->Paginator->sort('legal_form_type_id','Legal Form'))?></th>
                    <!-- <th><?php echo __($this->Paginator->sort('accounting_framework_type_id','Accounting Framework'))?></th> -->
                    <th><?php echo __($this->Paginator->sort('','Actions'))?></th>
                </tr>
            </thead>
            <tbody>

                <?php if(!empty($companies->toArray())) {
                    foreach($companies->toArray() as $company) {                        
                ?>
              
                <tr  class="<?php echo ($company->status == 0) ? 'active-tr' : ''?>">
                    <td>
                        <?php echo htmlspecialchars($company->company_name) ;?>
                    </td>
                    <td><?php echo htmlspecialchars($company->street) ;?></td>
                    <td><?php echo htmlspecialchars($company->street_number) ;?></td>
                    <td><?php echo htmlspecialchars($company->place) ;?></td>
                    <td><?php echo htmlspecialchars($company->zip) ;?></td>
                    <td><?php echo htmlspecialchars($company->place_of_incorporation) ;?></td>
                    <td><?php echo htmlspecialchars($company['legal_form_type']->name) ;?></td>
                    <!-- <td><?php echo htmlspecialchars($company['accounting_framework_type']->name) ;?></td> -->
                    <td>
                    <?= $this->Html->Link("<i class='fa fa-pencil-square-o'></i>".__('Edit'),
                               'javascript:void(0)',
                               [
                                   'escape'   => false,
                                   'class'    => 'edit-company',
                                   'data-url' =>   $this->Url->build(
                                               [  
                                                   "controller" => "Companies",
                                                   "action" => "edit",
                                                   base64_encode($company->id)
                                               ],true
                                           ),
                                    'title'  => 'edit'                           
                               ]
                           )
                       ?> 
                    <span class="switch-toggle custom-switch">
                        <?php echo $this->Form->input('',array(
                            "data-url" => $this->Url->build(array('controller' => 'Companies','action' => 'deactivate',base64_encode($company->id)),true),
                            "id"=> "test".$company->id,
                            "class" =>  "switch_custom",
                            "type" => "checkbox",
                            "hidden" => "hidden",
                            "label" => false,
                            'templates' => [
                               'inputContainer' => '{{content}}',
                               'inputContainerError' => '{{content}}{{error}}'
                             ],
                            "value"=>$company->id,
                            ($company->status == 1) ? 'checked' : ''
                        ))
                      ?>
                        <label for="test<?php echo $company->id?>" class="switch"></label>
                    </span>
                    </td>
                </tr>
                <?php } } else { ?>
                <tr>
                    <td colspan="6" class="text-center">
                        <?php echo __('No Record Found'); ?>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php echo $this->element('pagination') ?>
    </div>
</div>
<div class="modal addSubsidiary fade" id="editCompany" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times-circle" aria-hidden="true"></i></span></button>
                <h4 class="modal-title text-center"><?php echo __('Edit Organization Company'); ?></h4>
            </div>
            <div class="modal-body clearfix">
            </div>
        </div>
    </div>
</div>
<?php echo $this->element('Companies/add');?>
<?php echo $this->Html->script('Frontend/Company/add', ['block' => 'scriptBottom']); ?>
