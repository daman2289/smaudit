<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Specialize'), ['action' => 'edit', $specialize->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Specialize'), ['action' => 'delete', $specialize->id], ['confirm' => __('Are you sure you want to delete # {0}?', $specialize->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Specializes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Specialize'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="specializes view large-9 medium-8 columns content">
    <h3><?= h($specialize->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Title Eng') ?></th>
            <td><?= h($specialize->title_eng) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title German') ?></th>
            <td><?= h($specialize->title_german) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($specialize->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($specialize->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($specialize->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Activated') ?></th>
            <td><?= $specialize->is_activated ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Deleted') ?></th>
            <td><?= $specialize->is_deleted ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description Eng') ?></h4>
        <?= $this->Text->autoParagraph(h($specialize->description_eng)); ?>
    </div>
    <div class="row">
        <h4><?= __('Description German') ?></h4>
        <?= $this->Text->autoParagraph(h($specialize->description_german)); ?>
    </div>
</div>
