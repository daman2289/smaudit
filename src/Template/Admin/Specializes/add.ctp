<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Specializes'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="specializes form large-9 medium-8 columns content">
    <?= $this->Form->create($specialize) ?>
    <fieldset>
        <legend><?= __('Add Specialize') ?></legend>
        <?php
            echo $this->Form->control('title_eng');
            echo $this->Form->control('title_german');
            echo $this->Form->control('description_eng');
            echo $this->Form->control('description_german');
            echo $this->Form->control('is_activated');
            echo $this->Form->control('is_deleted');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
