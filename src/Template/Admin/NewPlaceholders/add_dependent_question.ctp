<section class="content-header clearfix">
    <div class="col-lg-12 heading-top">
        <h1 class="heading-text-color pull-left"><?php echo __('Add Placeholder'); ?></h1>
    </div>
</section>
<section class="content shoping-cart clearfix">
    <div class="col-lg-12">                     
        <div id="note-listing">
        <?= $this->Form->create($parentQuestion, 
            [
                'type' => 'post', 
                'class' => 'form-group'
            ]) 
        ?>             
            <div class="form-group">            
                <?= 
                    $this->Form->hidden('child_questions.0.new_placeholder.new_placeholder_type_id', 
                        
                        [
                            'empty' => 'Choose placeholder Category', 
                            'class' => 'placeholder-type form-control',
                            'value' => '3'
                        ]
                    ) 
                ?>
            </div>

            <div class="form-group" id = 'placeholder-input'>
                <?= 
                    $this->Form->input('child_questions.0.new_placeholder.name', [
                        'class' => 'form-control',
                        'placeholder' => 'Name Of Placeholder Like [Company]',
                        'label' => 'Name Of Placeholder',
                        'required'
                    ]) 
                ?>
            </div>
            <div class="form-group">
                <?php 
                    $options = [];
                    foreach ($parentQuestion->new_question_options as $key => $value) {
                        $options[$value->id] = $value->name_en;
                    }
                ?>
                <?= $this->Form->label(__('Select combination of value for this question will Show')) ?>
                <?= 
                    $this->Form->select('child_questions.0._joinData.condition_for_child', 
                        $options,
                    [
                        'class' => 'form-control multiple-select',
                        'placeholder' => 'Name Of Placeholder Like [Company]',
                        'label' => 'Name Of Placeholder',
                        'multiple'  => true,
                        'required'
                    ]) 
                ?>
            </div>
            <div class="form-group">
                <?php 
                    $options = [];
                    foreach ($parentQuestion->new_question_options as $key => $value) {
                        $options[$value->id] = $value->name_en;
                    }
                ?>
                <?= $this->Form->label(__('Show this question when? ')) ?>
                <?= 
                    $this->Form->select('child_questions.0._joinData.condition_type', 
                        [
                            'AND' => 'All of the above selected options are selected',
                            'OR'    => 'Any one of above selected option is selected'
                        ],
                    [
                        'class' => 'form-control',
                        'required'
                    ]) 
                ?>
            </div>
            <div id='question-block'>
                <div class="form-group">
                    <?=  
                        $this->Form->control('child_questions.0.new_question_type_id', [
                                'options' => $questionTypes,
                                'onChange' => 'placeOptionForQuestion(this.value, "child_questions")',
                                'class' => 'form-control'
                            ]
                        );
                    ?>
                </div>
                <div class="form-group" id='question-input'>
                    <?= $this->Form->control('child_questions.0.question_en', ['class' => 'form-control', 'required']); ?>
                </div>
            </div>
            <div class="related">
                <?= $this->element('OptionTemplates/options') ?>
                <?= $this->element('OptionTemplates/new') ?>
            </div>
            <div id='option-block'>
                <div id='option-block-body'>
                    
                </div> 
                <div id= 'option-add-new-button'>
                    
                </div>           
            </div>
            <div class="col-lg-8 form-group">
                <?= $this->Form->button(__('Save'), ['class' => "btn btn-primary submit-info submit_black", 'type' => 'submit']); ?>
            </div>
        <?= $this->Form->end() ?>
        </div>
    </div>
</section>
<?php 
    echo $this->Html->css([
        'Frontend/bootstrap-multiselect'
    ]);
    echo $this->Html->script([
            'Frontend/bootstrap-multiselect',
            'functions'
        ], 
        [
            'block' => 'scriptBottom'
        ]
    ); 
?>