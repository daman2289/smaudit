<section class="content-header clearfix">
    <div class="col-lg-12 heading-top">
        <h1 class="heading-text-color pull-left"><?php echo __('Placeholder Details'); ?></h1>
    </div>
</section>
<section class="content shoping-cart clearfix">
    <div class="col-lg-12">                     
        <div id="note-listing">
            <div class='row'>                
                <div class="col-md-6"> 
                    <h3> <?= h($placeholder->name) ?> </h3>
                </div>
            </div>
            <div class="form-group">
                <?= $this->Form->hidden('name', ['value' => $placeholder->name]) ?>
            </div>
            <div class="form-group">
                <h4>
                    Placeholder Type :
                <?php
                    echo ($placeholder->has('new_placeholder_type'))?$placeholder->new_placeholder_type->name : "" ;
                    $type =  $placeholder->new_placeholder_type_id;
                ?>
                </h4>   
                             
            </div>
            <div id='system-block' class='<?= $type == 1 ? "" :"hide"?>'>
                <h4>
                    <?php
                        if(!empty($placeholder->content_en) && $type == 1) {
                            echo "Replaced By : ". ucwords(str_replace('_', ' ', $placeholder->content_en));
                        }
                    ?>
                </h4>
            </div>
            <div id='text-block' class='<?= $type == 5 ? "" :"hide"?>'>
                <?php
                    echo '<h4> English Replacement Text : '. $placeholder->content_en . '</h4>';
                    echo '<h4> German Replacement Text : '. $placeholder->content_er. '</h4>';
                    echo '<h4> French Replacement Text : '. $placeholder->content_fr. '</h4>';
                ?>
            </div>
            <div id='text-block' class='<?= $type == 6 ? "" :"hide"?>'>
                <?php
                    if(!empty($placeholder->legal_form_replacements)):
                        foreach ($placeholder->legal_form_replacements as $key => $value) :
                ?>
                    <div style="border-bottom: 2px solid #ccc;">
                        <h3>
                            Legal Form Type : <?= $value->legal_form_type->name ?>
                        </h3>
                        <h4>
                            English Replacement Text : <?= $value->content_en ?>
                        </h4>
                        <h4>
                            German Replacement Text : <?= $value->content_er ?>
                        </h4>
                        <h4>
                            French Replacement Text : <?= $value->content_fr ?>
                        </h4>
                    </div>
                <?php        
                    endforeach;
                    endif;
                ?>
            </div>
            <div id='question-block' class='<?= $type == 3 ? "" :"hide"?>'>
                <?php if(!empty($placeholder->new_questions)): ?>
                    <h3>
                        Placeholder Question                     
                    </h3>
                    <h4><?php 
                            $types = $questionTypes->toArray();
                            echo 'Question Type : '.  $types[$placeholder->new_questions[0]->new_question_type_id];
                        ?>                    
                    </h4>
                                   
                    <?php                         
                        echo '<h4> Question in English : '. $placeholder->new_questions[0]->question_en. '</h4>';
                        echo '<h4> Question in German : '. $placeholder->new_questions[0]->question_er. '</h4>';
                        echo '<h4> Question in French : '. $placeholder->new_questions[0]->question_fr. '</h4>';
                    ?>
                    <?php 
                        $question = (!empty($placeholder->new_questions)) ?$placeholder->new_questions[0]  : '';
                        if($placeholder->new_placeholder_type_id == 3 && !empty($question) && !empty($question->new_question_options)):
                        
                    ?>
                        <div class='row'>                            
                            <div class="col-md-6"> 
                                <h3> Question Options </h3>
                            </div>   
                            <?php if(empty($question->parent_questions) || $question->parent_questions[0]->is_parent): ?>                         
                                <div class="col-md-6"> 
                                    <h3> 
                                        <?= 
                                            $this->Html->link('Add Dependent Question', [
                                                'action' => 'add-dependent-question',
                                                base64_encode($question->id)
                                            ],[
                                                'class' => 'btn btn-sm btn-success'
                                            ]) 
                                        ?> 
                                    </h3>
                                </div>
                            <?php endif; ?>
                        </div>   
                            <div class="row">
                                <div class="col-md-2 text-center"><h4>Type</h4></div>
                                <div class="col-md-5"><h4>Option Label</h4></div>
                                <div class="col-md-5"><h4>Replacement text</h4></div>
                            </div>
                    <?php
                            foreach ($question->new_question_options as $index => $option) :
                    ?>
                            <div class="row">  
                                <div class="col-md-2">
                                    <div class="form-group">        
                                        <input type="radio" name="" class="form-control" checked>
                                    </div>              
                                </div>      
                                <div class="col-md-5">              
                                    <div class="form-group">
                                        <?= $option->name_en ?>
                                    </div>
                                </div>      
                                <div class="col-md-5">      
                                    <div class="form-group"> 
                                        <?= $option->content_en ?>
                                    </div>  
                                </div>
                            </div>
                    <?php
                            endforeach;
                        endif;
                    ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php 
    echo $this->Html->script([
            'functions'
        ], 
        [
            'block' => 'scriptBottom'
        ]
    ); 
?>