<section class="content-header clearfix">
    <div class="col-lg-12 heading-top">
        <h1 class="heading-text-color pull-left"><?php echo __('Manage Placeholder'); ?></h1>
    </div>
</section>
<section class="content shoping-cart clearfix">
    <div class="col-lg-12">                     
        <div id="note-listing">
             <?= $this->Form->create('', 
            [
                'type' => 'post', 
                'url' => ['controller'=> 'new-placeholders', 'action' => 'edit',$placeholder->id] , 
                'class' => 'form-group'
            ]) 
        ?>
            <h3><?= h($placeholder->name) ?></h3>
            <div class="form-group">
                <?= $this->Form->hidden('name', ['value' => $placeholder->name]) ?>
            </div>
            <div class="form-group">                
                <?= 
                    $this->Form->select('new_placeholder_type_id', 
                        $types, 
                        [
                            'empty' => 'Choose placeholder type', 
                            'class' => 'placeholder-type form-control', 
                            'onChange' => 'showInputForPlaceholder(this.value)'
                        ]
                    ) 
                ?>
            </div>

            <?= $this->element('Placeholders/system_placeholder') ?>
            <?= $this->element('Placeholders/normal_placeholder') ?>
            <?= $this->element('Placeholders/question_placeholder') ?>
            <?= $this->element('Placeholders/text_placeholder') ?>            
            <?= $this->element('Placeholders/legal_form_dependent_placeholder') ?>
            <div class="related">
                <?= $this->element('OptionTemplates/options') ?>
                <?= $this->element('OptionTemplates/new') ?>
            </div>
            <div id='option-block'>
                <div id='option-block-body'>
                    
                </div> 
                <div id= 'option-add-new-button'>
                    
                </div>           
            </div>
            <div class="col-lg-8 form-group">
                <?= $this->Form->button(__('Save'), ['class' => "btn btn-primary submit-info submit_black", 'type' => 'submit']); ?>
            </div>
        <?= $this->Form->end() ?>
        </div>
    </div>
</section>
<?php 
    echo $this->Html->script([
            'functions'
        ], 
        [
            'block' => 'scriptBottom'
        ]
    ); 
?>