<!-- <?php
    use Cake\Core\Configure;
    $this->Html->addCrumb(__('Reports'), ['controller' => 'new-reports']);
    $this->Html->addCrumb(__('Add Placeholder'));
?>
<div class="col-sm-12 breadcrumb-no-bg">
    <div class="row">
       <?php
            echo $this->Html->getCrumbList(
                [
                    'firstClass' => 'text-primary',
                    'lastClass' => 'active',
                    'class' => 'breadcrumb'
                ]
            );
        ?>
    </div>
</div> -->
<section class="content-header clearfix">
    <div class="col-lg-12 heading-top">
        <h1 class="heading-text-color pull-left"><?php echo __('Add Placeholder'); ?></h1>
    </div>
</section>
<section class="content shoping-cart clearfix">
    <div class="col-lg-12">                     
        <div id="note-listing">
        <?= $this->Form->create($newPlaceholder, 
            [
                'type' => 'post', 
                'class' => 'form-group'
            ]) 
        ?>
            
            <div class="form-group">   
                <?= $this->Form->label('Choose Placeholder Report Category') ?>                
                <?= 
                    $this->Form->select('new_report_id', 
                        $newReports, 
                        [
                            'empty' => 'Choose Report Category', 
                            'class' => 'form-control',
                            'required'
                        ]
                    ) 
                ?>
            </div>
            <div class="form-group"> 
                <?= $this->Form->label('Choose Placeholder Category') ?>               
                <?= 
                    $this->Form->select('new_placeholder_type_id', 
                        $newPlaceholderTypes, 
                        [
                            'empty' => 'Choose placeholder Category', 
                            'class' => 'placeholder-type form-control', 
                            'onChange' => 'showInputForPlaceholder(this.value)',
                        ]
                    ) 
                ?>
            </div>

            <div class="form-group" id = 'placeholder-input'>
                <?= 
                    $this->Form->input('name', [
                        'class' => 'form-control',
                        'placeholder' => 'Name Of Placeholder Like [Company]',
                        'label' => 'Name Of Placeholder',
                        'required'
                    ]) 
                ?>
            </div>
            <?= $this->element('Placeholders/system_placeholder') ?>
            <?= $this->element('Placeholders/normal_placeholder') ?>
            <?= $this->element('Placeholders/question_placeholder') ?>
            <?= $this->element('Placeholders/text_placeholder') ?>
            <?= $this->element('Placeholders/legal_form_dependent_placeholder') ?>
            <?= $this->element('Placeholders/signature_gender_dependent_placeholder') ?>
            <div class="related">
                <?= $this->element('OptionTemplates/options') ?>
                <?= $this->element('OptionTemplates/new') ?>
            </div>
            <div id='option-block'>
                <div id='option-block-body'>
                    
                </div> 
                <div id= 'option-add-new-button'>
                    
                </div>           
            </div>
            <div class="col-lg-8 form-group">
                <?= $this->Form->button(__('Save'), ['class' => "btn btn-primary submit-info submit_black", 'type' => 'submit']); ?>
            </div>
        <?= $this->Form->end() ?>
        </div>
    </div>
</section>
<?php 
    echo $this->Html->script([
            'functions'
        ], 
        [
            'block' => 'scriptBottom'
        ]
    ); 
?>
<style type="text/css">
    .breadcrumb a{
        color: blue;
    }
</style>