<?php
/**
  * @var \App\View\AppView $this
  */
?>

<?php 
    $this->Html->addCrumb(__('Dashboard'), ['controller' => 'Users', 'action' => 'dashboard']);
    $this->Html->addCrumb(__('Reports'), ['controller' => 'NewReports']);
    $this->Html->addCrumb(__('Preview'));
?>
<div class="col-sm-12 breadcrumb-no-bg">
    <div class="row">
       <?php
            echo $this->Html->getCrumbList(
                [
                    'firstClass' => false,
                    'lastClass' => 'active',
                    'class' => 'breadcrumb'
                ]
            );
        ?>
    </div>
</div>
<section class="content">
    <div class="col-lg-12">                     
        <div id="emails">
        <?php
            echo $this->Form->create($placeholder, 
            [
                'type' => 'post', 
                'class' => 'form-group'
            ]) ;

            $type = $placeholder->new_placeholder_type_id;
        ?>
            <h3><?= h($placeholder->name) ?></h3>
            <div class="form-group">
                <?= $this->Form->hidden('name', ['value' => $placeholder->name]) ?>
            </div>
            <div class="form-group">                
                <?= 
                    $this->Form->select('new_placeholder_type_id', 
                        $types, 
                        [
                            'empty' => 'Choose placeholder type', 
                            'class' => 'placeholder-type form-control', 
                            'onChange' => 'showInputForPlaceholder(this.value)'
                        ]
                    ) 
                ?>
            </div>

            <div id='system-block' class='<?= $type == 1?"" :"hide"?>'>
                <div class="form-group">        
                    <?= $this->Form->select('content_en', [
                            'Organization' => [
                                'organization_name' => 'Name',
                                'organization_website' => 'Website',
                                'organization_phone' => 'Phone',
                                'organization_street' => 'Street',
                                'organization_place' => 'Place',
                            ],
                            'Company' => [
                                'company_name' => 'Name',
                                'company_phone' => 'Phone',
                                'company_street' => 'Street',
                                'company_place' => 'Place',
                                'company_place_of_incorporation' => 'Place Incorporation',
                            ],
                            'Subsidiary' => [
                                'subsidiary_name' => 'Name',
                                'subsidiary_phone' => 'Phone',
                                'subsidiary_street' => 'Street',
                                'subsidiary_place' => 'Place'
                            ]
                        ], [
                            'empty' => 'Choose option', 
                            'class' => 'form-control',
                            'value' => ($type == 1) ? $placeholder->content_en : "" 
                        ]) 
                    ?>
                </div>    
            </div>
            <div id='question-block' class='<?= $type == 3 ? "" :"hide"?>'>
                <div class="form-group">
                    <?=  
                        $this->Form->control('new_questions.0.new_question_type_id', [
                                'options' => $questionTypes,
                                'onChange' => 'placeOptionForQuestion(this.value)',
                                'class' => 'form-control'
                            ]
                        );
                    ?>
                     <?= $this->Form->hidden('new_questions.0.id', ['class' => 'form-control']); ?>
                </div>
                <div class="form-group">
                    <?= $this->Form->control('new_questions.0.question_en', ['class' => 'form-control']); ?>
                </div>
                <div class="form-group">
                    <?=  $this->Form->control('new_questions.0.question_er', ['class' => 'form-control']); ?>
                </div>
                <div class="form-group">
                    <?= $this->Form->control('new_questions.0.question_fr', ['class' => 'form-control']); ?>
                </div>
            </div>
            <div id='normal-block' class='<?= $type == 2 ? "" :"hide"?>'>    
                <div class="form-group">
                    <?= 
                        $this->Form->input('content_en', [
                            'label' => 'Replacement text in english', 'class' => 'form-control',
                            'value' => ($type == 2) ? $placeholder->content_en : ""
                        ])

                    ?>

                </div>    
                <div class="form-group">        
                    <?= 
                        $this->Form->input('content_er', ['label' => 'Replacement text in german', 'class' => 'form-control',
                            'value' => ($type == 2) ? $placeholder->content_er : ""]) 

                    ?>
                </div>    
                <div class="form-group">        
                    <?= 
                        $this->Form->input('content_fr', [
                            'label' => 'Replacement text in french', 
                            'class' => 'form-control',
                            'value' => ($type == 2) ? $placeholder->content_fr : ""
                        ]) 
                    ?>
                </div>
            </div>
            <?= $this->element('Placeholders/edit_legal_form_dependent') ?>
            </div>
            <div class="related">
                <?= $this->element('OptionTemplates/options') ?>
                <?= $this->element('OptionTemplates/new') ?>
            </div>
            <div id='option-block'>
                <div id='option-block-body'>
                <?php 
                    $question = (!empty($placeholder->new_questions)) ?$placeholder->new_questions[0]  : '';
                    if($placeholder->new_placeholder_type_id ==3 && !empty($question) && !empty($question->new_question_options)):
                        foreach ($question->new_question_options as $index => $option) :
                ?>
                        <div class="row">       
                            <div class="col-md-2">
                                <div class="form-group">        
                                    <input type="radio" name="" class="form-control" checked>
                                   <!--  <input type="hidden" name="new_questions[0][new_question_options][<?= $index ?>][id]" class="form-control" value ="<?= $option->id ?>" > -->
                                </div>              
                            </div>      
                            <div class="col-md-5">              
                                <div class="form-group">
                                    <input type="text" name="new_questions[0][new_question_options][<?= $index ?>][name_en]" class='radio-option-label form-control', placeholder="Set Label for this option" value="<?= $option->name_en ?>">
                                </div>
                            </div>      
                            <div class="col-md-5">      
                                <div class="form-group">    
                                    <input type="text" name="new_questions[0][new_question_options][<?= $index ?>][content_en]" class='radio-option-content form-control' placeholder=" Content to replace this option" value="<?= $option->content_en ?>">
                                </div>  
                            </div>
                        </div>
                <?php
                        endforeach;
                    endif;
                ?>
                </div> 
                <div id= 'option-add-new-button'>
                    <?php 
                        $question = (!empty($placeholder->new_questions)) ?$placeholder->new_questions[0]  : '';
                        if(!empty($question) && !empty($question->new_question_options)):
                    ?>
                        <div class="form-group">        
                            <button type='button' onclick='radioTemplate()' class="btn btn-danger">Add Another Option</button>
                        </div>
                    <?php
                        endif;
                    ?>
                </div>           
            </div>
            <div class="col-lg-8 form-group">
                <?= $this->Form->button(__('Save'), ['class' => "btn btn-primary submit-info submit_black", 'type' => 'submit', 'confirm' => 'Edit the question will remove all the dependet question. Are you sure to proceed']); ?>
            </div>
        <?= $this->Form->end() ?>
        </div>
    </div>
</section>
<?php 
    echo $this->Html->script([
            'functions'
        ], 
        [
            'block' => 'scriptBottom'
        ]
    ); 
    $this->Html->scriptStart(['block' => 'scriptBottom']);
        echo "$(document).ready(function(){
        manageEditVisibility();
    });";
    $this->Html->scriptEnd();
?>
