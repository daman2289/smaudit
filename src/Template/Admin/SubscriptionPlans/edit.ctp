<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $subscriptionPlan->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $subscriptionPlan->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Subscription Plans'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Plan Features'), ['controller' => 'PlanFeatures', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Plan Feature'), ['controller' => 'PlanFeatures', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="subscriptionPlans form large-9 medium-8 columns content">
    <?= $this->Form->create($subscriptionPlan) ?>
    <fieldset>
        <legend><?= __('Edit Subscription Plan') ?></legend>
        <?php
            echo $this->Form->control('title');
            echo $this->Form->control('stripe_plan_id');
            echo $this->Form->control('price');
            echo $this->Form->control('frequency');
            echo $this->Form->control('interval_count');
            echo $this->Form->control('currency');
            echo $this->Form->control('additional_detail');
            echo $this->Form->control('is_activated');
            echo $this->Form->control('is_deleted');
            echo $this->Form->control('plan_features._ids', ['options' => $planFeatures]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
