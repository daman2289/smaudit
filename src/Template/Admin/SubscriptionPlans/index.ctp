<?php use Cake\Core\Configure; ?>
<section class="content-header clearfix">
    <div class="col-lg-12 heading-top">
        <h1 class="heading-text-color pull-left"><?php echo __('Manage Subscription Plans'); ?></h1>
        
    </div>
</section>
<section class="content shoping-cart clearfix">
    <div class="col-sm-12">
        <div class="note-listing">
            <div class="table-responsive">
                <table class="table shoping-cart-table">
                    <thead>
                        <tr>
                            <th><?php echo __($this->Paginator->sort('title','Title'))?></th>
                            <th><?php echo __($this->Paginator->sort('price','Price'))?></th>
                            <th><?php echo __($this->Paginator->sort('frequency','Frequency'))?></th>
                            <th><?php echo __($this->Paginator->sort('currency','Currency'))?></th>
                            
                            <th><?php echo __('Action'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (!$subscriptionPlans->isEmpty()) {
                                foreach ($subscriptionPlans as $key => $value) {
                        ?>
                                    <tr>
                                        <td>    <?php echo $value->title; ?>            </td>
                                        <td>    
                                            <?= Cake\I18n\Number::currency($value['price']/100, $value['currency']) ?>
                                        </td>
                                        <td>    <?php echo $value->frequency; ?>      </td>
                                        <td>    <?php echo strtoupper($value->currency); ?>   </td>
                                        <td>
                                        <?= $this->Html->Link("<span class='fa fa-pencil'></span>",
                                               'javascript:void(0)',
                                               [
                                                   'escape'   => false,
                                                   'class'    => 'edit-faq btn btn-primary',
                                                   'data-url' =>   $this->Url->build(
                                                               [  
                                                                   "controller" => "subscription-plans",
                                                                   "action" => "edit-plan-features",
                                                                   base64_encode($value->id)
                                                               ],true
                                                           ),
                                                    'title'  => 'edit'                           
                                               ]
                                           )
                                       ?> 
                                       <?= $this->Html->Link("<span class='fa fa-pencil'>Edit Price</span>",
                                               'javascript:void(0)',
                                               [
                                                   'escape'   => false,
                                                   'class'    => 'edit-plan btn btn-primary',
                                                   'data-url' =>   $this->Url->build(
                                                               [  
                                                                   "controller" => "subscription-plans",
                                                                   "action" => "edit-plan",
                                                                   base64_encode($value->id)
                                                               ],true
                                                           ),
                                                    'title'  => 'edit'                           
                                               ]
                                           )
                                       ?> 
                                       <span class="switch-toggle custom-switch">
                                            <?php echo $this->Form->input('',array(
                                            "data-url" => $this->Url->build(array('controller' => 'SubscriptionPlans','action' => 'deactivate',base64_encode($value->id)),true),
                                            "id"=> "test".$value->id,
                                            "class" =>  "switch_custom",
                                            "type" => "checkbox",
                                            "hidden" => "hidden",
                                            "label" => false,
                                            'templates' => [
                                               'inputContainer' => '{{content}}',
                                               'inputContainerError' => '{{content}}{{error}}'
                                             ],
                                            "value"=>$value->id,
                                             ($value->is_activated == 1) ? 'checked' : ''
                                            ))
                                          ?>
                                            <label for="test<?php echo $value->id?>" class="switch"></label>
                                        </span>
                                        </td>
                                    </tr>
                        <?php
                                }
                            } else {
                        ?>
                                <tr>
                                    <td colspan="6" class="text-center">
                                        <?php echo __('No Record Found'); ?>
                                    </td>
                                </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php echo $this->element('pagination'); ?>
            </div>
        </div>
    </div>
</section>
<div class="modal Educational-info fade" id="edit-faq" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Edit Features'); ?></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<div class="modal Educational-info fade" id="edit-plan" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Add Trial'); ?></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<?php
    echo $this->element('Specializes/add');
    echo $this->Html->script([
            'Faq/add',
            'SubscriptionPlans/set-feature'
        ],
        [
            'block' => 'scriptBottom'
    ]);
?>

