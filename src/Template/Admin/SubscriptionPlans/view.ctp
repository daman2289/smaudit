<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Subscription Plan'), ['action' => 'edit', $subscriptionPlan->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Subscription Plan'), ['action' => 'delete', $subscriptionPlan->id], ['confirm' => __('Are you sure you want to delete # {0}?', $subscriptionPlan->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Subscription Plans'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Subscription Plan'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Plan Features'), ['controller' => 'PlanFeatures', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Plan Feature'), ['controller' => 'PlanFeatures', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="subscriptionPlans view large-9 medium-8 columns content">
    <h3><?= h($subscriptionPlan->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($subscriptionPlan->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Stripe Plan Id') ?></th>
            <td><?= h($subscriptionPlan->stripe_plan_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Frequency') ?></th>
            <td><?= h($subscriptionPlan->frequency) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Currency') ?></th>
            <td><?= h($subscriptionPlan->currency) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Additional Detail') ?></th>
            <td><?= h($subscriptionPlan->additional_detail) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($subscriptionPlan->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Price') ?></th>
            <td><?= $this->Number->format($subscriptionPlan->price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Interval Count') ?></th>
            <td><?= $this->Number->format($subscriptionPlan->interval_count) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Activated') ?></th>
            <td><?= $subscriptionPlan->is_activated ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Deleted') ?></th>
            <td><?= $subscriptionPlan->is_deleted ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Plan Features') ?></h4>
        <?php if (!empty($subscriptionPlan->plan_features)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($subscriptionPlan->plan_features as $planFeatures): ?>
            <tr>
                <td><?= h($planFeatures->id) ?></td>
                <td><?= h($planFeatures->title) ?></td>
                <td><?= h($planFeatures->created) ?></td>
                <td><?= h($planFeatures->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PlanFeatures', 'action' => 'view', $planFeatures->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PlanFeatures', 'action' => 'edit', $planFeatures->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PlanFeatures', 'action' => 'delete', $planFeatures->id], ['confirm' => __('Are you sure you want to delete # {0}?', $planFeatures->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
