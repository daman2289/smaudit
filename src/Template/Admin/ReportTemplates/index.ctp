<?php
/**
  * @var \App\View\AppView $this
  */
?>
<section class="content-header clearfix">
   <div class="col-lg-12 heading-top">
      <h1 class="heading-text-color pull-left"><?php echo __($title); ?></h1>
   </div>
</section>
<!-- Main content -->
<section class="content">
   <div class="box">
        <div class="table-responsive">
            <table class="table shoping-cart-table">
                <thead>
                    <tr>
                       
                        <th><?= $this->Paginator->sort(
                                '',
                                __('#'),
                                ['escape' => false]
                                )
                            ?>
                             
                        </th>
                        <th>
                            <?= $this->Paginator->sort(
                                'report_type_id',
                                __('Type'),
                                ['escape' => false]
                                )
                            ?>
                        </th>
                        <th>
                            <?= $this->Paginator->sort(
                                'created',
                                __('Created'),
                                ['escape' => false]
                                )
                            ?>
                        </th>
                        <th>
                            <?= $this->Paginator->sort(
                                'modified',
                                __('Last Modified'),
                                ['escape' => false]
                                )
                            ?>
                        </th>
                        
                        <th class="actions"><?= __('Actions') ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                       if (!empty($reportTemplates)) {
                            foreach ($reportTemplates as $key => $template) {
                    ?>
                                <tr>
                                    <td><?= $key+1; ?></td>
                                    <td><?= __($template->report_type->name) ?></td>
                                    <td><?= __($this->Time->nice($template->created)) ?></td>
                                    <td><?= __($this->Time->nice($template->modified)) ?></td>
                                    <td>
                                        <?= $this->Html->link(
                                                __('<span class="fa fa-edit"></span>'), 
                                                [
                                                    'action' => 'edit',
                                                    base64_encode($template->id)
                                                ],
                                                [
                                                    'escape' => false
                                                ]
                                            ) 
                                        ?>
                                    </td>
                                </tr>
                    <?php   
                            }//end of foreach loop
                        } else {
                              echo "<tr><td colspan=6>No Record found</td></tr>";
                        }
                    ?>
                </tbody>
            </table>
            <?= $this->element('pagination'); ?>
        </div>
    </div>
</section>

