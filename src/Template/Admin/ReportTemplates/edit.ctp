<?php
/**
  * @var \App\View\AppView $this
  */
?>
<section class="content-header clearfix">
   <div class="col-lg-12 heading-top">
      <h1 class="heading-text-color pull-left"><?php echo __($title); ?></h1>
   </div>
</section>
<!-- Main content -->
<section class="content clearfix">        
      <div class="col-lg-12">                     
        <div id="emails">
            <?= $this->Form->create($reportTemplate, ['class' => 'form-horizontal', 'novalidate' => true]); ?>
                <div class="form-group">
                    <label class="control-label" for="exampleInputEmail1"><?php echo __('Report Type'); ?></label>
                    <?= $this->Form->input(
                            'empty', 
                            ['class'=>'form-control', 'label' => false, 'disabled' => true,'options' => $reportTypes]
                        ); 
                    ?>
                </div>
                
                <div class="form-group">
                    <label class="control-label" for="exampleInputEmail1"><?php echo __('English'); ?></label>
                   <?php
                        echo $this->CKEditor->loadJs();
                        echo $this->Form->textarea('template_eng', ['class'=>'form-control', 'label' => false]);
                        echo $this->CKEditor->replace('template_eng');
                        if ($this->Form->isFieldError('template_eng')) {
                            echo $this->Form->error('template_eng');
                        }
                    ?> 
                </div>
           <!--       <div class="form-group">
                    <label class="control-label" for="exampleInputEmail1"><?php echo __('French'); ?></label>
                   <?php
                        echo $this->CKEditor->loadJs();
                        echo $this->Form->textarea('template_french', ['class'=>'form-control', 'label' => false]);
                        echo $this->CKEditor->replace('template_french');
                        if ($this->Form->isFieldError('template_french')) {
                            echo $this->Form->error('template_french');
                        }
                    ?> 
                </div> -->
                 <div class="form-group">
                    <label class="control-label" for="exampleInputEmail1"><?php echo __('German'); ?></label>
                   <?php
                        echo $this->CKEditor->loadJs();
                        echo $this->Form->textarea('template_german', ['class'=>'form-control', 'label' => false]);
                        echo $this->CKEditor->replace('template_german');
                        if ($this->Form->isFieldError('template_german')) {
                            echo $this->Form->error('template_german');
                        }
                    ?> 
                </div>
             <!--     <div class="form-group">
                    <label class="control-label" for="exampleInputEmail1"><?php echo __('Italian'); ?></label>
                   <?php
                        echo $this->CKEditor->loadJs();
                        echo $this->Form->textarea('template_italian', ['class'=>'form-control', 'label' => false]);
                        echo $this->CKEditor->replace('template_italian');
                        if ($this->Form->isFieldError('template_italian')) {
                            echo $this->Form->error('template_italian');
                        }
                    ?> 
                </div> -->
                <div class="col-lg-8">
                    <?= $this->Form->button(__('Save'), ['class' => "sbtn submit-info submit_black", 'type' => 'submit']); ?>
                </div>
            <?= $this->Form->end() ?>
         </div>
   </div>
</section>