<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Report Templates'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Report Types'), ['controller' => 'ReportTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Report Type'), ['controller' => 'ReportTypes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="reportTemplates form large-9 medium-8 columns content">
    <?= $this->Form->create($reportTemplate) ?>
    <fieldset>
        <legend><?= __('Add Report Template') ?></legend>
        <?php
            echo $this->Form->control('report_type_id', ['options' => $reportTypes]);
            echo $this->Form->control('template_eng');
            echo $this->Form->control('template_french');
            echo $this->Form->control('template_german');
            echo $this->Form->control('template_italian');
            echo $this->Form->control('status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
