<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Report Template'), ['action' => 'edit', $reportTemplate->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Report Template'), ['action' => 'delete', $reportTemplate->id], ['confirm' => __('Are you sure you want to delete # {0}?', $reportTemplate->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Report Templates'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Report Template'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Report Types'), ['controller' => 'ReportTypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Report Type'), ['controller' => 'ReportTypes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="reportTemplates view large-9 medium-8 columns content">
    <h3><?= h($reportTemplate->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Report Type') ?></th>
            <td><?= $reportTemplate->has('report_type') ? $this->Html->link($reportTemplate->report_type->name, ['controller' => 'ReportTypes', 'action' => 'view', $reportTemplate->report_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($reportTemplate->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($reportTemplate->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($reportTemplate->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $reportTemplate->status ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Template Eng') ?></h4>
        <?= $this->Text->autoParagraph(h($reportTemplate->template_eng)); ?>
    </div>
    <div class="row">
        <h4><?= __('Template French') ?></h4>
        <?= $this->Text->autoParagraph(h($reportTemplate->template_french)); ?>
    </div>
    <div class="row">
        <h4><?= __('Template German') ?></h4>
        <?= $this->Text->autoParagraph(h($reportTemplate->template_german)); ?>
    </div>
    <div class="row">
        <h4><?= __('Template Italian') ?></h4>
        <?= $this->Text->autoParagraph(h($reportTemplate->template_italian)); ?>
    </div>
</div>
