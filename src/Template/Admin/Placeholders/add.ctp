<?php use Cake\Core\Configure; ?>
<section class="content-header clearfix">
    <div class="col-lg-12 heading-top">
        <h1 class="heading-text-color pull-left"><?php echo __('Add Work In Progress'); ?></h1>
    </div>
</section>
<section class="content shoping-cart clearfix">
<div class="col-sm-12">
<?php
    echo $this->Form->create(null,
            [
                    'url' => [
                        'controller' => 'Placeholders',
                        'action' => 'add'
                    ],
                    'id' => 'add-placeholder-form',
                    'class' => 'form-horizontal'
            ]
        );
?>   
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Description in English'); ?></label>
        <div class="col-sm-4">
            <?php
                echo $this->CKEditor->loadJs();
                echo $this->Form->textarea('description_eng', ['class'=>'form-control', 'label' => false]);
                echo $this->CKEditor->replace('description_eng');
                if ($this->Form->isFieldError('description_eng')) {
                    echo $this->Form->error('description_eng');
                }
            ?>
        </div>
    </div>         
    <div class="form-group" id="legal">
        <label class="col-sm-4 control-label"><?php echo __('Description in German'); ?></label>
        <div class="col-sm-4">
            <?php
                echo $this->CKEditor->loadJs();
                echo $this->Form->textarea('description_ger', ['class'=>'form-control', 'label' => false]);
                echo $this->CKEditor->replace('description_ger');
                if ($this->Form->isFieldError('description_ger')) {
                    echo $this->Form->error('description_ger');
                }
            ?>
        </div>
    </div>
    <div class="append-item">
        <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo __('Date'); ?></label>
            <div class="col-sm-4">
                <?php
                    echo $this->Form->input('work_in_progress_features.date[]',
                            array(
                                    'class' => 'form-control datetimepicker',
                                    'placeholder' => __('Date'),
                                    'div' => false,
                                    'label' => false,
                                )
                        );
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo __('Release Number'); ?></label>
            <div class="col-sm-4">
                <?php
                    echo $this->Form->input('work_in_progress_features.release_number[]',
                            array(
                                    'class' => 'form-control',
                                    'placeholder' => __('Release Number'),
                                    'div' => false,
                                    'label' => false
                                )
                        );
                ?>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo __('Features'); ?></label>
            <div class="col-sm-4">
                <?php
                    echo $this->Form->input('work_in_progress_features.features[]',
                            array(
                                    'class' => 'form-control',
                                    'placeholder' => __('Add Features as comma separated'),
                                    'div' => false,
                                    'label' => false
                                )
                        );
                ?>
            </div>
        </div>
    </div>
    <div class="form-group text-right">
            <div class="col-sm-5 plus"><span class ="glyphicon glyphicon-plus add"><?php echo __('Add More');?></span></div>
    </div>
    <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
        <div class="col-lg-4">
            <?php
                echo $this->Form->button(__('Submit'), array(
                            'class' => 'btn submit-info submit_black',
                            'type' => 'submit'
                            ));
            ?>
            <?php
                echo $this->Html->link(__('Cancel'), 
                            '/admin/Placeholders/index',
                            array(
                            'class' => 'btn submit-info submit_black',
                            'type' => 'button',
                            'data-dismiss' => 'modal'
                            ));
            ?>
        </div>
    </div>
<?php echo $this->Form->end(); ?>
<?php echo $this->Html->script('Placeholder/add', ['block' => 'scriptBottom']); ?>
