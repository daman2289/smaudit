<section class="content-header clearfix">
    <div class="col-lg-12 heading-top">
        <h1 class="heading-text-color pull-left"><?php echo __('Manage Work in Progress'); ?></h1>
        <?php echo $this->Html->link(
            __('Add Work in Progress'),
            '/admin/Placeholders/add',
            array(
                'class' => 'btn btn-default add_button pull-right'
                )
        );?>
    </div>
</section>
<section class="content shoping-cart clearfix">
<div class="col-sm-12">
    <div class="tab-dashboard-shipper" data-example-id="togglable-tabs">
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade active in" role="tabpanel" id="home" aria-labelledby="home-tab">
                <div class="table-responsive">
                    <table class="table dasbaord-table">
                        <thead>
                            <tr>
                                <th><?php echo __('Description in English');?><i class="ti-arrows-vertical"></i></th> 
                                <th><?php echo __('Description in German');?><i class="ti-arrows-vertical"></i></th> 
                                <th><?php echo __('Action');?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($works->toArray())){
                                    foreach ($works as $key => $work) {
                                ?>
                            <tr>
                                <td>
                                   <?php echo $work->description_eng;?> 
                                </td>
                                <td>
                                   <?php echo $work->description_ger;?>         
                                </td>
                                <td>
                                    <?= $this->Form->postLink("<span class='fa fa-trash'></span>",
                                           '/admin/Placeholders/delete/'.base64_encode($work->id),
                                           [
                                               'escape'   => false,
                                               'class'    => 'delete-service btn btn-primary',
                                                'title'  => 'delete',
                                                'confirm' => 'Are you sure you want to delete Blog?'                           
                                           ]
                                       )
                                    ?> 
                                </td>
                            </tr>
                            <tr>
                            <td colspan = '3'>
                            <table class="table dasbaord-table">
                                       <thead>
                                        <tr>
                                            <th><?php echo __('Date');?><i class="ti-arrows-vertical"></i></th>
                                            <th><?php echo __('Release Number');?><i class="ti-arrows-vertical"></i></th>
                                            <th><?php echo __('Features');?><i class="ti-arrows-vertical"></i></th>
                                        </tr>
                                       </thead>
                                       <tbody>
                                            <?php
                                                foreach ($work['work_in_progress_features'] as $key => $features) {
                                            ?>
                                            <tr>
                                                <td>
                                                    <?php echo date('Y-m-d',strtotime($features->date)); ?>
                                                </td>
                                                <td>
                                                    <?php echo $features->release_number;?>
                                                </td>
                                                <td>
                                                    <?php echo $features->features;?>
                                                </td>
                                            <tr>
                                            <?php }  ?>
                                       </tbody>
                                   </table> 
                                   </td>
                                </tr>   
                        <?php } } else{?>
                            <tr>
                                <td colspan="6" class="text-center">
                                        <?php echo __('No Record Found'); ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php echo $this->element('pagination'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
