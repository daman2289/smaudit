<?php use Cake\Core\Configure; ?>
<section class="content-header clearfix">
    <div class="col-lg-12 heading-top">
        <h1 class="heading-text-color pull-left"><?php echo __('Edit Placeholders'); ?></h1>
    </div>
</section>
<section class="content shoping-cart clearfix">
<div class="col-sm-12">
<?php
    echo $this->Form->create($work,
            [
                    
                    'id' => 'add-placeholder-form',
                    'class' => 'form-horizontal'
            ]
        );
?>            
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Selection Type'); ?></label>
        <div class="col-sm-4">
            <?php
                echo $this->Form->input('selection_type',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('Selection Type'),
                                'div' => false,
                                'options' => Configure::read("selectionType"),
                                'label' => false,
                                'id' => 'selection_type'
                            )
                    );
            ?>
        </div>
    </div>
    <div class="form-group <?php echo ($placeholder->legal_form_type_id ==0) ? "hide" : ""?>" id="legal">
        <label class="col-sm-4 control-label"><?php echo __('Selection Name'); ?></label>
        <div class="col-sm-4">
            <?php
                echo $this->Form->input('legal_form_type_id',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('Selection Name'),
                                'div' => false,
                                'label' => false,
                                'options' => $legalform,
                                ($placeholder->legal_form_type_id ==0) ? "disabled" : ""
                            )
                    );
            ?>
        </div>
    </div>
    <div class="form-group <?php echo ($placeholder->accounting_framework_type_id ==0) ? "hide" : ""?>" id ="account">
        <label class="col-sm-4 control-label"><?php echo __('Selection Name'); ?></label>
        <div class="col-sm-4">
            <?php
                echo $this->Form->input('accounting_framework_type_id',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('Selection Name'),
                                'div' => false,
                                'label' => false,
                                'options' => $accountingframe,
                                ($placeholder->accounting_framework_type_id ==0) ? "disabled" : ""
                            )
                    );
            ?>
        </div>
    </div>
    
    <div class="append-item">
        <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo __('Place Holder Number '); ?></label>
            <div class="col-sm-4">
                <?php
                    echo $this->Form->input('place_holder_number',
                            array(
                                    'class' => 'form-control',
                                    'placeholder' => __('Place Holder Number '),
                                    'div' => false,
                                    'options' => range(1,100),
                                    'label' => false
                                    
                                )
                        );
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo __('English Value'); ?></label>
            <div class="col-sm-4">
                <?php
                    echo $this->Form->input('eng_value',
                            array(
                                    'class' => 'form-control',
                                    'placeholder' => __('English Value'),
                                    'div' => false,
                                    'label' => false
                                   
                                )
                        );
                ?>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo __('French Value'); ?></label>
            <div class="col-sm-4">
                <?php
                    echo $this->Form->input('fra_value',
                            array(
                                    'class' => 'form-control',
                                    'placeholder' => __('French Value'),
                                    'div' => false,
                                    'label' => false
                                )
                        );
                ?>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo __('Italian Value'); ?></label>
            <div class="col-sm-4">
                <?php
                    echo $this->Form->input('ita_value',
                            array(
                                    'class' => 'form-control',
                                    'placeholder' => __('Italian Value'),
                                    'div' => false,
                                    'label' => false
                                )
                        );
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo __('German Value'); ?></label>
            <div class="col-sm-4">
                <?php
                    echo $this->Form->input('ger_value',
                            array(
                                    'class' => 'form-control',
                                    'placeholder' => __('German Value'),
                                    'div' => false,
                                    'label' => false
                                )
                        );
                ?>
            </div>
        </div>
    </div>
    <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
        <div class="col-lg-4">
            <?php
                echo $this->Form->button('Submit', array(
                            'class' => 'btn submit-info submit_black',
                            'type' => 'submit'
                            ));
            ?>
            <?php
                echo $this->Html->link('Cancel', 
                            '/admin/Placeholders/index',
                            array(
                            'class' => 'btn submit-info submit_black',
                            'type' => 'button',
                            'data-dismiss' => 'modal'
                            ));
            ?>
        </div>
    </div>
<?php echo $this->Form->end(); ?>
<?php echo $this->Html->script('Placeholder/add', ['block' => 'scriptBottom']); ?>
