<?php use Cake\Core\Configure; ?>
<section class="content-header clearfix">
    <div class="col-lg-12 heading-top">
        <h1 class="heading-text-color pull-left"><?php echo __('Manage Services'); ?></h1>
        <button href="#" data-toggle="modal" data-backdrop="static" data-target="#add-diploma" class="btn btn-default add_button pull-right" type="button"><?php echo __('Add New'); ?></button>
    </div>
</section>
<section class="content shoping-cart clearfix">
    <div class="col-sm-12">
        <div class="note-listing">
            <div class="table-responsive">
                <table class="table shoping-cart-table">
                    <thead>
                        <tr>
                            <th><?php echo __($this->Paginator->sort('title_eng','English Title'))?></th>
                            <th><?php echo __($this->Paginator->sort('title_german','German Title'))?></th>
                            <th><?php echo __($this->Paginator->sort('description_eng','English Description'))?></th>
                            <th><?php echo __($this->Paginator->sort('description_German','German Description'))?></th>
                            
                            <th><?php echo __('Action'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (!$services->isEmpty()) {
                                foreach ($services as $key => $value) {
                        ?>
                                    <tr>
                                        <td>    <?php echo $value->title_eng; ?>            </td>
                                        <td>    <?php echo $value->title_german; ?>         </td>
                                        <td width="400px">    <?php echo $value->description_eng; ?>      </td>
                                        <td width="400px">    <?php echo $value->description_german; ?>   </td>
                                        <td width="200px">
                                        <span>
                                        <?= $this->Html->Link("<span class='fa fa-pencil'></span>",
                                               'javascript:void(0)',
                                               [
                                                   'escape'   => false,
                                                   'class'    => 'edit-service btn btn-primary',
                                                   'data-url' =>   $this->Url->build(
                                                               [  
                                                                   "controller" => "Services",
                                                                   "action" => "edit",
                                                                   base64_encode($value->id)
                                                               ],true
                                                           ),
                                                    'title'  => 'edit'                           
                                               ]
                                           )
                                       ?>
                                       </span>
                                       <span class="switch-toggle custom-switch">
                                            <?php echo $this->Form->input('',array(
                                            "data-url" => $this->Url->build(array('controller' => 'Services','action' => 'deactivate',base64_encode($value->id)),true),
                                            "id"=> "test".$value->id,
                                            "class" =>  "switch_custom",
                                            "type" => "checkbox",
                                            "hidden" => "hidden",
                                            "label" => false,
                                            'templates' => [
                                               'inputContainer' => '{{content}}',
                                               'inputContainerError' => '{{content}}{{error}}'
                                             ],
                                            "value"=>$value->id,
                                             ($value->is_activated == 1) ? 'checked' : ''
                                            ))
                                          ?>
                                            <label for="test<?php echo $value->id?>" class="switch"></label>
                                        </span>
                                        <?= $this->Form->postLink("<span class='fa fa-trash'></span>",
                                                   '/admin/Services/delete/'.base64_encode($value->id),
                                                   [
                                                       'escape'   => false,
                                                       'class'    => 'delete-service btn btn-primary',
                                                        'title'  => 'delete',
                                                        'confirm' => 'Are you sure you want to delete Service?'                           
                                                   ]
                                               )
                                           ?>
                                        </td>
                                    </tr>
                        <?php
                                }
                            } else {
                        ?>
                                <tr>
                                    <td colspan="6" class="text-center">
                                        <?php echo __('No Record Found'); ?>
                                    </td>
                                </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php echo $this->element('pagination'); ?>
            </div>
        </div>
    </div>
</section>
<div class="modal Educational-info fade" id="edit-service" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Edit Service'); ?></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<?php
    echo $this->element('Services/add');
    echo $this->Html->script([
            'Services/activate'
        ],
        [
            'block' => 'scriptBottom'
    ]);
?>
