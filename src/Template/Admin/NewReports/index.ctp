<section class="content-header clearfix">
    <div class="col-lg-12 heading-top">
        <h1 class="heading-text-color pull-left"><?php echo __('Manage Report types'); ?></h1>
        
        <?= $this->Html->link(__('Add Report Category'),[
                'controller' => 'new-reports',
                'action'    => 'add',
                'prefix'    => 'admin'
            ], [
                'class' => 'btn btn-default add_button pull-right'
            ]) 
        ?>
    </div>
</section>
<section class="content shoping-cart clearfix">
<div class="col-sm-12">
    <div class="note-listing">
        <div class="table-responsive">
            <table class="table col-lg-12">
                <thead>
                    <tr class="bg-primary">
                        <th><?php echo __($this->Paginator->sort('name',__('Report Name')))?></th>
                        <th><?php echo __($this->Paginator->sort('created',__('Date')))?></th>
                        <th><?php echo __($this->Paginator->sort('',__('Action')))?></th>
                        
                    </tr>
                </thead>
                <tbody>
                <?php 
                    if(!empty($reports->toArray())) {
                        foreach($reports as $key => $report) {
                ?>
                    <tr class="bg-success">
                        <td>
                            <span onclick="collapse(this)"> <i class="glyphicon glyphicon-chevron-right" style="cursor: pointer;"></i></span>

                            <strong>                            
                                <?php echo htmlspecialchars($report->name); ?>
                            </strong>
                        </td>
                        <td><?= htmlspecialchars($report->created) ?></td>
                        <td>
                            <?= 
                                $this->Form->postLink('<i class="fa fa-trash" aria-hidden="true"></i>',
                                    [  
                                       "controller" => "new-reports",
                                       "action" => "delete",
                                       $report->id
                                    ],
                                    [
                                       'escape'   => false,
                                        'title'  => 'Delete',
                                        'confirm' => __('Are you sure to delete report?'),
                                        'class' => 'text-primary'                          
                                    ]
                               )
                            ?> 
                            <?= 
                                $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i>',
                                    [  
                                       "controller" => "new-reports",
                                       "action" => "edit",
                                       $report->id
                                    ],
                                    [
                                       'escape'   => false,
                                        'title'  => 'edit'  ,
                                        'class' => 'text-primary'                       
                                    ]
                               )
                            ?> 
                            <?php
                                echo $this->Html->link(__('<br>Generate Sample Report'),
                                    [  
                                        "controller" => "test-reports",
                                        "action" => "index",
                                        base64_encode($report->id)
                                    ],
                                    [
                                        'escape'   => false,
                                        'title'  => 'Generate Report',
                                        'class' => 'text-primary'                       
                                    ]
                               );
                                if(!$report->is_published) {
                                    echo $this->Html->link(__('<br>Publish Report'),
                                        [  
                                            "controller" => "test-reports",
                                            "action" => "publishReport",
                                            base64_encode($report->id)
                                        ],
                                        [
                                            'escape'   => false,
                                            'title'  => 'publish'  ,
                                            'class' => 'text-primary',
                                            'confirm'   => __('Are you sure you want to publish this report')                       
                                        ]
                                   );
                                } else {
                                    echo $this->Html->link(__('<br>Un-publish Report'),
                                        [  
                                            "controller" => "test-reports",
                                            "action" => "unpublish-report",
                                            base64_encode($report->id)
                                        ],
                                        [
                                            'escape'   => false,
                                            'title'  => 'publish'  ,
                                            'class' => 'text-primary',
                                            'confirm'   => __('Are you sure you want to un-publish this report')                       
                                        ]
                                   );
                                }
                                if(!empty($report->new_final_reports)) {
                    ?>
                                    <div class="dropdown text-right">
                                        <a id="dLabel" data-target="#" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="btn btn-primary btn-sm">Sample Download 
                                            <span class="caret"></span>
                                        </a>

                                        <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                                            <?php $lang = [ 
                                                    ['en', '<i class="fa fa-file-pdf-o" aria-hidden="true"></i> English', 'pdf' ], 
                                                    ['er', '<i class="fa fa-file-pdf-o" aria-hidden="true"></i> German', 'pdf'], 
                                                    ['fr', '<i class="fa fa-file-pdf-o" aria-hidden="true"></i> French', 'pdf'], 
                                                    ['en', '<i class="fa fa-file-word-o" aria-hidden="true"></i> English', 'doc' ], 
                                                    ['er', '<i class="fa fa-file-word-o" aria-hidden="true"></i> German', 'doc'], 
                                                    ['fr', '<i class="fa fa-file-word-o" aria-hidden="true"></i> French', 'doc'],
                                                ] 
                                            ?>
                                            <?php foreach ($lang as $key => $value): ?>
                                                <li>
                                                    <?= $this->Html->link(__($value[1]),
                                                            [  
                                                                "controller" => "test-reports",
                                                                "action" => "generate-report",
                                                                base64_encode($report->id),
                                                               '_ext' => $value[2],
                                                               '?'  => ['language' => $value[0]]
                                                            ],
                                                            [
                                                                'escape'   => false,
                                                                'title'  => 'publish'  ,
                                                                'class' => 'text-primary'              
                                                            ]
                                                       );
                                                    ?> 

                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                            <?php                                    
                                }
                            ?>
                        </td>
                    </tr>
                    <?php
                        if($report->has('new_placeholders')):
                            $managedPlaceholders = 0;
                            $totalPlaceholders = count($report->new_placeholders)
                    ?>
                        <tr class="bg-danger hide">
                            <td colspan="3">
                                <table class="table sorted_table">
                                    <thead>
                                        <tr>
                                            <th>Placeholder</th>
                                            <th></th>
                                            <th>Placeholder Type</th>
                                            <th>Question</th>
                                            <th>Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                <?php 
                                    foreach ($report->new_placeholders as $k => $placeholder):
                                        $question_id = $placeholder_id= '';
                                    if(!empty($placeholder->new_question) ) {
                                        $question_id = $placeholder->new_question->id;
                                        $placeholder_id = $placeholder->id;
                                    } 
                                ?>
                                    <tr data-placholder_id="<?php echo $placeholder_id; ?>" data-question_id="<?php echo $question_id; ?>">
                                        <td><?= $placeholder->name ?></td>
                                        <td>
                                            <?php
                                                if(!$placeholder->has('new_placeholder_type')) {
                                                    echo  $this->Html->link('Manage', [
                                                        'controller' => 'new-placeholders',
                                                        'action'    => 'view',
                                                        $placeholder->id
                                                    ]); 
                                                }
                                            ?>                                        
                                        </td>
                                        <td>                                        
                                            <?php
                                                if($placeholder->has('new_placeholder_type')) {
                                                    $managedPlaceholders++;
                                                    echo $placeholder->new_placeholder_type->name;
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <p style="
                                                max-width: 200px;
                                                
                                            " class="t8-default">
                                                <?php
                                                    if(!empty($placeholder->new_question)) {
                                                        $managedPlaceholders++;
                                                        echo $placeholder->new_question->question_en;
                                                    }
                                                ?>
                                            
                                            </p>
                                        </td>
                                        <td>                                        
                                            <?php
                                                if($placeholder->has('new_placeholder_type')) {
                                                    echo $this->Html->link('<i class="glyphicon glyphicon-eye-open"></i>',[
                                                            'controller' => 'new-placeholders',
                                                            'action' => 'view-placeholder',
                                                            $placeholder->id
                                                        ], [
                                                        'escape' => false,
                                                        'title' => 'view detail',
                                                        'class' => 'text-primary'
                                                    ]);
                                                }else {
                                                   echo $this->Html->link('Manage',[
                                                            'controller' => 'new-placeholders',
                                                            'action' => 'view',
                                                            $placeholder->id
                                                        ], [
                                                        'escape' => false,
                                                        'title' => 'view detail',
                                                        'class' => 'text-primary'
                                                    ]); 
                                                }
                                            ?>
                                            <?php
                                                echo $this->Form->postLink('<i class="glyphicon glyphicon-trash"></i>',[
                                                        'controller' => 'new-placeholders',
                                                        'action'    => 'delete',
                                                        'prefix'    => 'admin',
                                                        base64_encode($placeholder->id)
                                                    ], [
                                                    'escape'    => false,
                                                    'title'     => 'view detail',
                                                    'class'     => 'text-danger',
                                                    'confirm'   => 'Are you sure to delete this placeholder ? Make sure to delete this placeholder from report also.'
                                                ]);
                                            ?>
                                            <?= 
                                                $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i>',
                                                    [  
                                                       "controller" => "new-placeholders",
                                                       "action" => "edit_placeholder",
                                                        base64_encode($placeholder->id)
                                                    ],
                                                    [
                                                       'escape'   => false,
                                                        'title'  => 'edit'  ,
                                                        'class' => 'text-primary'                       
                                                    ]
                                               )
                                            ?> 
                                            
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    <?php
                        endif;
                    ?>
                    <tr class="bg-warning">
                        <td colspan="3">
                            <?=  $this->Html->link('Add New Placeholder', 
                                [
                                    'controller'=> 'new-placeholders',
                                    'action' => 'add'
                                ],[
                                    'class' => 'pull-right btn btn-success'
                                ]) 
                            ?>
                        </td>
                    </tr>
                   <!--  <?php if($managedPlaceholders == $totalPlaceholders): ?>
                        <tr class="bg-warning">
                            <td colspan="3">
                                <?=  $this->Html->link('Answer questions', 
                                    [
                                        'controller'=> 'new-questions',
                                        'action' => 'index',
                                        $report->id
                                    ],[
                                        'class' => 'pull-right btn btn-success'
                                    ]) 
                                ?>

                                <?=
                                    $this->Html->link(__('Generate Report'), 
                                        [
                                            'controller' => 'new-reports',
                                            'action'    => 'report',
                                            $report->id

                                        ],
                                        [
                                            'class' => 'btn btn-primary',
                                            'escape' => false,
                                            'confirm' => 'It is mandatory to give answer before generating report. Are you sure you have given the answers'
                                        ]
                                    ) 
                                ?>
                            </td>
                        </tr>
                    <?php endif; ?> -->
                <?php } } else { ?>
                        <tr>
                            <td colspan="6" class="text-center">
                                <?php echo __('No Record Found'); ?>
                            </td>
                        </tr>
                 <?php } ?>
                </tbody>
            </table>
            <div class="text-right">
                
                <?php echo $this->element('pagination'); ?>
            </div>
        </div>
    </div>
</div>
<?php
    echo $this->element('empty_modal'); 
?>
<?php 
    echo $this->Html->script([
            'Frontend/Users/profile',
            // 'Dashboard/report',
            'Dashboard/view-report',
            'jquery-sortable',
            'trunk8',
            'function'
        ], 
        [
            'block' => 'scriptBottom'
        ]
    ); 
?>
<?php $this->Html->scriptStart(['block' => 'scriptBottom']); ?>
    // Sortable rows
    var m = $('.sorted_table').sortable({
        containerSelector: 'table',
        itemPath: '> tbody',
        itemSelector: 'tr',
        //group: 'serialization',
        placeholder: '<tr class="placeholder glyphicon glyphicon-move"/>',
       
        onDrop: function ($item, container, _super) { 
            var data = $($item).closest('table').sortable("serialize").get();
            var jsonString = JSON.stringify(data[0], null, ' ');
            setQuestionOrder(jsonString);
            _super($item, container);
        }
    });

    function setQuestionOrder(data) {
        $.ajax({
            type:"POST",
            url:"/admin/new-reports/set-question-order",
            data : data  ,  
            contentType: 'application/json'  ,  
            success: function(data){
                console.log(data);
                $('#edit-admin').find('.modal-body').html(data);
                $('#edit-admin').modal('show');
                $('#edit-sub-admin-form').validate({
                   'first_name': {
                        'required': true,
                        'minlength': 3
                    },
                    'last_name': {
                        'required': true,
                        'minlength': 3
                    },
                    'email': {
                        'required': true,
                        'email': true
                    },
                    'password': {
                        'minlength': 9,
                        'pwcheckallowedchars': true,
                        'pwcheckalphabets': true,
                        'pwchecknumerics': true
                    },
                    'confirm_password': {
                        'equalTo': '#admin-password'
                    }
                });
            }
        });
    }
    $('.t8-default').trunk8();
    function collapse(ele) {
        child = $(ele).closest('tr').next('.bg-danger');
        if($(child).hasClass('hide')) {
            $(child).removeClass('hide');
            $(ele).closest('tr').next('.bg-warning').removeClass('hide');
            $(ele).html('<i class="glyphicon glyphicon-chevron-down" style="cursor: pointer;"></i>');
        } else {
            $(child).addClass('hide');        
            $(ele).closest('tr').next('.bg-warning').addClass('hide');
            $(ele).html('<i class="glyphicon glyphicon-chevron-right" style="cursor: pointer;"></i>');
        }
    }
<?php $this->Html->scriptEnd() ?>
</section>
<style type="text/css">
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        
        padding: 8px; 
        /*font-size: 14px;*/
        color: #6e6e6e; 
        line-height: 2.42;
    }
</style>
