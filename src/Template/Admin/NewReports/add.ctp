<section class="content-header clearfix">
    <div class="col-lg-12 heading-top">
        <h1 class="heading-text-color pull-left"><?php echo __('Add Report Category'); ?></h1>
    </div>
</section>
<section class="content shoping-cart clearfix">
    <div class="col-lg-12">                     
        <div id="note-listing">
        <?= $this->Form->create($newReport) ?>
            <div class="form-group">
                <?= $this->Form->input('name', ['class' => 'form-control', 'label' => 'Report Name']) ?>
            </div>
            <div class="form-group">
                <?php 
                    echo $this->Form->label('Header');
                    echo $this->CKEditor->loadJs();

                    echo $this->Form->textarea('header', [
                            'class' => 'form-control', 
                            'label' => 'Report Header',
                            'rows'  => 200,
                        ]); 
                    
                    echo $this->CKEditor->replace('header');
                    if ($this->Form->isFieldError('header')) {
                        echo $this->Form->error('header');
                    }
                ?>
            </div>
            <div class="form-group">
                <?php 
                    echo $this->Form->label('Footer');
                    
                    echo $this->Form->textarea('footer', [
                            'class' => 'form-control', 
                            'rows'  => 5,
                        ]); 

                   
                ?>
            </div>
            <div class="form-group">
                <?php
                    echo $this->Form->label('English');
                    // echo $this->CKEditor->loadJs();
                    echo $this->Form->textarea('content_en', [
                        'class' =>'form-control', 
                        'label' => 'Content English',
                        'rows'  => 100,
                    ]);
                    echo $this->CKEditor->replace('content_en');
                    if ($this->Form->isFieldError('content_en')) {
                        echo $this->Form->error('content_en');
                    }
                ?>
            </div>
            <div class="form-group">
                <?php
                    echo $this->Form->label('German');
                    // echo $this->CKEditor->loadJs();
                    echo $this->Form->textarea('content_er', [
                        'class' =>'form-control', 
                        'label' => 'Content English',
                        'rows'  => 100,
                    ]);
                    echo $this->CKEditor->replace('content_er');
                    if ($this->Form->isFieldError('content_er')) {
                        echo $this->Form->error('content_er');
                    }
                ?>
            </div>
            <div class="form-group">
                <?php
                    echo $this->Form->label('French');
                    // echo $this->CKEditor->loadJs();
                    echo $this->Form->textarea('content_fr', [
                        'class' =>'form-control', 
                        'label' => 'Content English',
                        'rows'  => 100,
                    ]);
                    echo $this->CKEditor->replace('content_fr');
                    if ($this->Form->isFieldError('content_fr')) {
                        echo $this->Form->error('content_fr');
                    }
                ?>
            </div>
            <div class="col-lg-8">
                <?= $this->Form->button(__('Save'), ['class' => "btn btn-primary submit-info submit_black", 'type' => 'submit']); ?>
            </div>
        <?= $this->Form->end() ?>
        </div>
    </div>
</section>
<?php 
    echo $this->Html->script([
            'Dashboard/report-editor'
        ], 
        [
            'block' => 'scriptBottom'
        ]
    ); 
?>