<div class="col-md-5 login-left-bg  text-center">
    <?php
        echo $this->Html->image("admin_logo.png", [
            "alt" => "logo",
            'url' => []
        ]);
    ?>
    <div class="call-us">
        <div class="call-us-heading"></div>
        <div class="call-phone"><h3><?php echo __('Welcome to Smaudit Administration'); ?></h3></div>
    </div>
    <div class="share-bottom">
        <div class="follow-text"></div>
        <ul class="list-inline">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>
    </div>
</div>
<div class="col-md-7 login-right-bg">
    <div class="text-center top-right">
        <div class="user-login"><?php echo __('Admin Login'); ?></div>
    </div>
    <?php
        echo $this->Form->create(null,[
                'url' => [
                    'action' => 'login'
                ],
                'class' => 'login-section'
            ]);
    ?>
        <div class="form-group">
            <label><?php echo __('Email Address'); ?></label>
            <?php
                echo $this->Form->email('email', [
                        'class' => 'form-control',
                        'placeholder' => __('Email Address'),
                        'autocomplete' => 'on'
                    ]);
            ?>
        </div>
        <div class="form-group">
            <label><?php echo __('Password'); ?></label>
            <?php
                echo $this->Form->password('password', [
                        'class' => 'form-control',
                        'placeholder' => __('Password'),
                         'autocomplete' => 'on'
                    ]);
            ?>
        </div>
        <?php
            echo $this->Form->button(__('Login'),
                    [
                        'type' => 'submit',
                        'class' => 'btn btn-black btn-block'
                    ]
                );
        ?>
    <?php echo $this->Form->end(); ?>
</div>