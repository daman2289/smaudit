<?php use Cake\Core\Configure; ?>
<section class="content-header clearfix">
    <div class="col-lg-12 heading-top">
        <h1 class="heading-text-color pull-left"><?php echo __('Manage Sub-Admins'); ?></h1>
        <button href="#" data-toggle="modal" data-backdrop="static" data-target="#add-sub-admin" class="btn btn-default add_button pull-right" type="button"><?php echo __('Add SubAdmin'); ?></button>
        <?php
            echo $this->Form->Create(null, array(
                    'url' => ['action' => 'listSubAdmins'],
                    'type' => 'get',
                )
            );
        ?>
        <div class="input-group my-search col-sm-3 pull-right">
            <?php
                    echo $this->Form->input('search', array(
                            'class' => 'form-control',
                            'placeholder' => __('Search for...'),
                            'label'=>FALSE,
                            'templates' => [
                               'inputContainer' => '{{content}}',
                               'inputContainerError' => '{{content}}{{error}}'
                           ],
                            'value' => (isset($_GET['search']) && !empty($_GET['search'])) ? $_GET['search'] : ''
                        )
                    );
            ?>
            <span class="input-group-btn"> 
            <?= $this->Form->button(
                    __('Go!'),
                        [
                            'type' => 'submit',
                            'class' => 'btn btn-default'
                        ]
                    );
            ?>
             </span>
        </div>
        </form>
    </div>
</section>
<section class="content shoping-cart clearfix">
    <div class="col-sm-12">
        <div class="note-listing">
            <div class="table-responsive">
                <table class="table shoping-cart-table">
                    <thead>
                        <tr>
                            <th><?php echo __($this->Paginator->sort('first_name','First Name'))?></th>
                            <th><?php echo __($this->Paginator->sort('last_name','Last Name'))?></th>
                            <th><?php echo __($this->Paginator->sort('email','Email'))?></th>
                            <th><?php echo __('Status'); ?></th>
                            <th><?php echo __('Action'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (!$subAdmins->isEmpty()) {
                                foreach ($subAdmins as $subAdmin) {
                        ?>
                                    <tr>
                                        <td><?php echo $subAdmin->first_name; ?></td>
                                        <td><?php echo $subAdmin->last_name; ?></td>
                                        <td><?php echo $subAdmin->email; ?></td>
                                        <td class="show-status"><?php echo ($subAdmin->is_activated) ? Configure::read("UserStatusToShow.True") : Configure::read("UserStatusToShow.False"); ?></td>
                                        <td>
                                            <span class="switch-toggle custom-switch">
                                                <?php echo $this->Form->hidden('data',
                                                            [
                                                                'value' => 0,
                                                                'id' => 'test' . $subAdmin->id . '_',
                                                                'checked' => true
                                                            ]
                                                        );
                                                    echo $this->Form->checkbox('data',
                                                            [
                                                                'value' => 1,
                                                                'id' => 'test' . $subAdmin->id,
                                                                'class' => 'update-status-switch',
                                                                'hidden' => true,
                                                                'data-url' => $this->Url->build([
                                                                                        'controller' => 'Users',
                                                                                        'action' => 'updateUserStatus',
                                                                                        'prefix' => 'api',
                                                                                        $subAdmin->id,
                                                                                        'Users'
                                                                                    ],true),
                                                                'checked' => ($subAdmin->is_activated) ? true : false
                                                            ]
                                                        );
                                                ?>
                                                <label class="switch" for="<?php echo "test".$subAdmin->id ?>"></label>
                                            </span>
                                            <?php
                                                echo $this->Html->image('loader.gif', [
                                                    'class' => 'hide status-update-in-process'
                                                ]);
                                            ?>

                                            <?php 
                                                echo $this->Html->link("<i class='glyphicon glyphicon-pencil' style='color:#111;'>Edit</i>",
                                                    'javascript:void(0)', 
                                                    [
                                                        'data-url' => $this->Url->build([
                                                            'controller' => 'admins',
                                                            'action' => 'edit',
                                                            base64_encode($subAdmin->id)
                                                        ],true),
                                                        'escape' => false,
                                                        'class' => 'edit-admin'
                                                    ]);
                                            ?>
                                        </td>
                                    </tr>
                        <?php
                                }
                            } else {
                        ?>
                                <tr>
                                    <td colspan="6" class="text-center">
                                        <?php echo __('No Record Found'); ?>
                                    </td>
                                </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php echo $this->element('pagination'); ?>
            </div>
        </div>
    </div>
</section>
<?php
    echo $this->element('Administration/add_subadmin');
    echo $this->Html->script([
            'Administration/sub_admin',
            'common'
        ],
        [
            'block' => 'scriptBottom'
    ]);
?>
<div class="modal Educational-info fade" id="edit-admin" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-target="#edit-admin">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Edit Sub-admin'); ?></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>