<?php
    use Cake\Filesystem\File;
    use Cake\Core\Configure;
?>

    <div class="dashboard-top clearfix">
        <div class="col-xs-12 col-sm-6 col-lg-3">
            <div class="outer-box-blue clearfix">
                <div class="col-xs-5 left-blue-box text-center">
                    <i class="fa fa-sitemap" aria-hidden="true"></i>
                </div>
                <div class="col-xs-7 blue-box">
                    <b><?php echo count($Organizations->toArray());?></b>
                    <div>Oraganizations</div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg-3">
            <div class="outer-box-blue clearfix">
                <div class="col-xs-5 left-blue-box text-center">
                    <i class="fa fa-flag-o" aria-hidden="true"></i>
                </div>
                <div class="col-xs-7 blue-box">
                    <b><?php echo count($reports->toArray());?></b>
                    <div>Reports</div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg-3">
            <div class="outer-box-blue clearfix">
                <div class="col-xs-5 left-blue-box text-center">
                    <i class="fa fa-money" aria-hidden="true"></i>
                </div>
                <div class="col-xs-7 blue-box">
                    <b>0</b>
                    <div>Revenue</div>
                </div>
            </div>
        </div>
    </div>