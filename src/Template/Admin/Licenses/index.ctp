<?php use Cake\Core\Configure; ?>
<section class="content-header clearfix">
    <div class="col-lg-12 heading-top">
        <h1 class="heading-text-color pull-left"><?php echo __('Manage Licenses'); ?></h1>
        <button href="#" data-toggle="modal" data-backdrop="static" data-target="#add-diploma" class="btn btn-default add_button pull-right" type="button"><?php echo __('Add License'); ?></button>
    </div>
</section>
<section class="content shoping-cart clearfix">
    <div class="col-sm-12">
        <div class="note-listing">
            <div class="table-responsive">
                <table class="table shoping-cart-table">
                    <thead>
                        <tr>
                            <th><?php echo __($this->Paginator->sort('name','License Name'))?></th>
                            <th><?php echo __('Status'); ?></th>
                            <th><?php echo __('Action'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (!$licenses->isEmpty()) {
                                foreach ($licenses as $license) {
                        ?>
                                    <tr>
                                        <td><?php echo htmlspecialchars($license->name); ?></td>
                                        <td class="show-status"><?php echo ($license->status == 1) ? __("Active") : __("Inactive"); ?></td>
                                        <td>
                                            <?= $this->Html->Link("<span class='fa fa-pencil'></span>",
                                                   'javascript:void(0)',
                                                   [
                                                       'escape'   => false,
                                                       'class'    => 'edit-diploma btn btn-primary',
                                                       'data-url' =>   $this->Url->build(
                                                                   [  
                                                                       "controller" => "Licenses",
                                                                       "action" => "editDiploma",
                                                                       base64_encode($license->id)
                                                                   ],true
                                                               ),
                                                        'title'  => 'edit'                           
                                                   ]
                                               )
                                           ?> 
                                        </td>
                                    </tr>
                        <?php
                                }
                            } else {
                        ?>
                                <tr>
                                    <td colspan="6" class="text-center">
                                        <?php echo __('No Record Found'); ?>
                                    </td>
                                </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php echo $this->element('pagination'); ?>
            </div>
        </div>
    </div>
</section>
<div class="modal Educational-info fade" id="edit-diploma" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Edit License'); ?></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<?php
    echo $this->element('Diploma/add');
    echo $this->Html->script([
            'Diploma/add'
        ],
        [
            'block' => 'scriptBottom'
    ]);
?>