<?php use Cake\Core\Configure; ?>
<section class="content-header clearfix">
    <div class="col-lg-12 heading-top">
        <h1 class="heading-text-color pull-left"><?php echo __('Manage FAQs'); ?></h1>
        <button href="#" data-toggle="modal" data-backdrop="static" data-target="#add-diploma" class="btn btn-default add_button pull-right" type="button"><?php echo __('Add FAQs'); ?></button>
    </div>
</section>
<section class="content shoping-cart clearfix">
    <div class="col-sm-12">
        <div class="note-listing">
            <div class="table-responsive">
                <table class="table shoping-cart-table">
                    <thead>
                        <tr>
                            <th><?php echo __($this->Paginator->sort('ques_eng','Question in English'))?></th>
                            <th><?php echo __($this->Paginator->sort('ans_eng','Answer in English'))?></th>
                            
                            <th><?php echo __('Action'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (!$faqs->isEmpty()) {
                                foreach ($faqs as $faq) {
                        ?>
                                    <tr>
                                        <td><?php echo $faq->ques_eng; ?></td>
                                        <td><?php echo $faq->ans_eng; ?></td>
                                        <td>
                                            <?= $this->Html->Link("<span class='fa fa-pencil'></span>",
                                                   'javascript:void(0)',
                                                   [
                                                       'escape'   => false,
                                                       'class'    => 'edit-faq btn btn-primary',
                                                       'data-url' =>   $this->Url->build(
                                                                   [  
                                                                       "controller" => "Faqs",
                                                                       "action" => "edit",
                                                                       base64_encode($faq->id)
                                                                   ],true
                                                               ),
                                                        'title'  => 'edit'                           
                                                   ]
                                               )
                                           ?> 
                                           <?= $this->Form->postLink("<span class='fa fa-trash'></span>",
                                                   '/admin/Faqs/delete/'.base64_encode($faq->id),
                                                   [
                                                       'escape'   => false,
                                                       'class'    => 'delete-faq btn btn-primary',
                                                        'title'  => 'delete',
                                                        'confirm' => 'Are you sure you want to delete FAQ?'                           
                                                   ]
                                               )
                                           ?>
                                        </td>
                                    </tr>
                        <?php
                                }
                            } else {
                        ?>
                                <tr>
                                    <td colspan="6" class="text-center">
                                        <?php echo __('No Record Found'); ?>
                                    </td>
                                </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php echo $this->element('pagination'); ?>
            </div>
        </div>
    </div>
</section>
<div class="modal Educational-info fade" id="edit-faq" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Edit FAQs'); ?></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<?php
    echo $this->element('Faq/add');
    echo $this->Html->script([
            'Faq/add'
        ],
        [
            'block' => 'scriptBottom'
    ]);
?>