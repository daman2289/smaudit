<?php use Cake\Core\Configure; ?>
<section class="content-header clearfix">
    <div class="col-lg-12 heading-top">
        <h1 class="heading-text-color pull-left"><?php echo __('Manage Organizations'); ?></h1>
    </div>
</section>
<section class="content shoping-cart clearfix">
    <div class="col-sm-12">
        <div class="note-listing">
            <div class="table-responsive">
                <table class="table shoping-cart-table">
                    <thead>
                        <tr>
                            <th><?php echo __($this->Paginator->sort('name','Organization Name'))?></th>
                            <th><?php echo __($this->Paginator->sort('email','Email'))?></th>
                            <th><?php echo __($this->Paginator->sort('first_name','Name'))?></th>
                            <th><?php echo __('Status'); ?></th>
                            <th><?php echo __('Action'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (!$users->isEmpty()) {
                                foreach ($users as $user) {
                        ?>
                                    <tr>
                                        <td>
                                            <?php echo htmlspecialchars($user['organization']->name); 
                                            ?>                                            
                                        </td>
                                        <td><?php echo htmlspecialchars($user->email); ?></td>
                                        <td>
                                            <?php 
                                                echo htmlspecialchars($user->first_name.' '.$user->last_name); 
                                            ?>                                                
                                        </td>
                                        <td class="show-status">
                                            <?php echo ($user->is_admin_activated == 1) ? __("Active") : __("Inactive"); ?></td>
                                        <td>
                                            <span class="switch-toggle custom-switch">
                                                <?php echo $this->Form->input('',array(
                                                "data-url" => $this->Url->build(array('controller' => 'Organizations','action' => 'deactivate',base64_encode($user->id)),true),
                                                "id"=> "test".$user->id,
                                                "class" =>  "switch_custom",
                                                "type" => "checkbox",
                                                "hidden" => "hidden",
                                                "label" => false,
                                                'templates' => [
                                                   'inputContainer' => '{{content}}',
                                                   'inputContainerError' => '{{content}}{{error}}'
                                                 ],
                                                "value"=>$user->id,
                                                 ($user->is_admin_activated == 1) ? 'checked' : ''
                                                ))
                                              ?>
                                                <label for="test<?php echo $user->id?>" class="switch"></label>
                                            </span>
                                            <?php
                                                echo $this->Html->link(
                                                    '<i class="fa fa-trash text-danger"></i>', 
                                                    array(
                                                        'controller' => 'Organizations',
                                                        'action'    => 'delete-organization',
                                                        base64_encode($user['organization']->id)
                                                    ),
                                                    [
                                                        'escape' => false,
                                                        'confirm' => 'Are you sure want to delete this organization.'
                                                    ]
                                                );
                                            ?>
                                            <?php
                                                echo $this->Html->link(
                                                    '<i class="fa fa-refresh text-success"></i> Reset Trail', 
                                                    array(
                                                        'controller' => 'Organizations',
                                                        'action'    => 'reset-trial',
                                                        base64_encode($user['organization']->id)
                                                    ),
                                                    [
                                                        'escape' => false,
                                                        'confirm' => 'Are you sure want to reset the trial.'
                                                    ]
                                                );
                                            ?>

                                            <?php
                                                echo $this->Html->link(
                                                    '<i class="fa fa-users text-primary" aria-hidden="true"></i> Sub Users', 
                                                    array(
                                                        'controller' => 'Organizations',
                                                        'action'    => 'users',
                                                        base64_encode($user['organization']->id)
                                                    ),
                                                    [
                                                        'escape' => false,
                                                        'class'     => 'text-primary'
                                                    ]
                                                );
                                            ?>
                                            
                                        </td>
                                    </tr>
                        <?php
                                }
                            } else {
                        ?>
                                <tr>
                                    <td colspan="6" class="text-center">
                                        <?php echo __('No Record Found'); ?>
                                    </td>
                                </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php echo $this->element('pagination'); ?>
            </div>
        </div>
    </div>
</section>
<?php
    echo $this->element('Diploma/add');
    echo $this->Html->script([
            'Administration/active'
        ],
        [
            'block' => 'scriptBottom'
    ]);
?>