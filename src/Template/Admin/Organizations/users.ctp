<?php use Cake\Core\Configure; ?>
<section class="content-header clearfix">
    <div class="col-lg-12 heading-top">
        <div class="col-md-6">            
            <h1 class="heading-text-color pull-left"><?php echo __('Manage Users'); ?></h1>
        </div>
        <div class="col-md-6 text-right">
            <?php echo $this->Html->link('Add User', [
                    'action' => 'add-user',
                    base64_encode($users->id)
                ], [
                    'class' => 'btn btn-lg btn-success'
                ]) 
            ?>
        </div>
    </div>
</section>
<section class="content shoping-cart clearfix">
    <div class="col-sm-12">
        <div class="note-listing">
            <div class="table-responsive">
                <table class="table shoping-cart-table">
                    <thead>
                        <tr>
                            <th><?php echo __('Organization Name') ?></th>
                            <th><?php echo __('Email') ?></th>
                            <th><?php echo __('Name' )?></th>
                            <th><?php echo __('Status'); ?></th>
                            <th><?php echo __('Action'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (!empty($users) && !empty($users->users)) {
                                foreach ($users->users as $user) {
                        ?>
                                    <tr>
                                        <td>
                                            <?php echo htmlspecialchars($users->name); 
                                            ?>                                            
                                        </td>
                                        <td><?php echo htmlspecialchars($user->email); ?></td>
                                        <td>
                                            <?php 
                                                echo htmlspecialchars($user->first_name.' '.$user->last_name); 
                                            ?>                                                
                                        </td>
                                        <td class="show-status">
                                            <?php echo ($user->is_admin_activated == 1) ? __("Active") : __("Inactive"); ?></td>
                                        <td>
                                            <span class="switch-toggle custom-switch">
                                                <?php echo $this->Form->input('',array(
                                                "data-url" => $this->Url->build(array('controller' => 'Organizations','action' => 'deactivate',base64_encode($user->id)),true),
                                                "id"=> "test".$user->id,
                                                "class" =>  "switch_custom",
                                                "type" => "checkbox",
                                                "hidden" => "hidden",
                                                "label" => false,
                                                'templates' => [
                                                   'inputContainer' => '{{content}}',
                                                   'inputContainerError' => '{{content}}{{error}}'
                                                 ],
                                                "value"=>$user->id,
                                                    ($user->is_admin_activated == 1) ? 'checked' : ''
                                                ))
                                              ?>
                                                <label for="test<?php echo $user->id?>" class="switch"></label>
                                            </span>
                                            <?php
                                                echo $this->Html->link(
                                                    '<i class="fa fa-trash text-danger"></i>', 
                                                    array(
                                                        'controller' => 'Organizations',
                                                        'action'    => 'delete-user',
                                                        base64_encode($user->id)
                                                    ),
                                                    [
                                                        'escape' => false,
                                                        'confirm' => 'Are you sure want to delete this organization.'
                                                    ]
                                                );
                                            ?>
                                            <?php
                                                echo $this->Html->link(
                                                    '<i class="fa fa-edit text-primary"></i>', 
                                                    array(
                                                        'controller' => 'Organizations',
                                                        'action'    => 'edit-user',
                                                        base64_encode($user->id),
                                                        base64_encode($users->id)
                                                    ),
                                                    [
                                                        'escape' => false
                                                    ]
                                                );
                                            ?>
                                            
                                        </td>
                                    </tr>
                        <?php
                                }
                            } else {
                        ?>
                                <tr>
                                    <td colspan="6" class="text-center">
                                        <?php echo __('No Record Found'); ?>
                                    </td>
                                </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<?php
    echo $this->element('Diploma/add');
    echo $this->Html->script([
            'Administration/active'
        ],
        [
            'block' => 'scriptBottom'
    ]);
?>