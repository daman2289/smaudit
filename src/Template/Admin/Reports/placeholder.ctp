<section class="content-header clearfix">
    <!--div class="col-lg-12 heading-top">
        <h1 class="heading-text-color pull-left"><?php echo __('Manage Placeholders'); ?></h1>
        <?php echo $this->Html->link(
            __('Add Placeholder'),
            '/admin/Placeholders/add',
            array(
                'class' => 'btn btn-default add_button pull-right'
                )
        );?>
    </div-->
</section>
<section class="content shoping-cart clearfix">
<div class="col-sm-12">
    <div class="tab-dashboard-shipper" data-example-id="togglable-tabs">
        <ul class="nav nav-tabs" id="myTabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#profile" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false"><?php echo __('Modified Opinion');?></a>        
            </li>
            <li role="presentation" class="">
                <a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true"><?php echo __('References');?></a>        
            </li>
            <li role="presentation" class="">
                <a href="#emphasis" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false"><?php echo __('Emphasis Of Matter');?></a>        
            </li>
            <li role="presentation" class="">
                <a href="#opinion" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false"><?php echo __('Opinion Types');?></a>        
            </li>
            <li role="presentation" class="">
                <a href="#legal" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false"><?php echo __('Legal Form Values');?></a>        
            </li>
            <li role="presentation" class="">
                <a href="#accounting" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false"><?php echo __('Accounting Frameworks Values');?></a>        
            </li>
            <li role="presentation" class="">
                <a href="#other" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false"><?php echo __('Others');?></a>        
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade active in" role="tabpanel" id="profile" aria-labelledby="home-tab">
                <div class="table-responsive">
                    <table class="table dasbaord-table">
                        <thead>
                            <tr>
                                <th><?php echo __('Placeholder number');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('English Value');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('English Content');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('German Value');?><i class="ti-arrows-vertical"></i></th>
                                 <th><?php echo __('German Content');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('Action');?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($modified->toArray())){
                                    foreach ($modified as $key => $value) {
                                ?>
                            <tr>
                                <td><?php echo $value->place_holder_number;?></td>
                                <td>
                                   <?php echo $value->name;?> 
                                </td>
                                <td>
                                    <?php //$string = (strlen($value->content_eng) > 50) ? substr($value->content_eng,0,50).'...' : $value->content_eng; ?>
                                   <?php echo $value->content_eng;?>         
                                </td>
                                 <td>
                                   <?php echo $value->name_ger;?>         
                                </td>
                                 <td>
                                   <?php echo $value->content_ger;?>         
                                </td>
                                <td>
                                    <?= $this->Html->Link("<span class='fa fa-pencil'></span>",
                                           'javascript:void(0)',
                                           [
                                               'escape'   => false,
                                               'class'    => 'edit-modified btn btn-primary',
                                               'data-url' =>   $this->Url->build(
                                                           [  
                                                               "controller" => "Reports",
                                                               "action" => "editModified",
                                                               base64_encode($value->id)
                                                           ],true
                                                       ),
                                                'title'  => 'edit'                           
                                           ]
                                       )
                                   ?> 
                                </td>
                            </tr>
                        <?php } } else{?>
                            <tr>
                                <td colspan="6" class="text-center">
                                        <?php echo __('No Record Found'); ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane fade" role="tabpanel" id="home" aria-labelledby="profile-tab">
                <div class="table-responsive">
                    <table class="table dasbaord-table">
                        <thead>
                               <tr>
                               <th><?php echo __('Placeholder number');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('English Value');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('English Content');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('German Value');?><i class="ti-arrows-vertical"></i></th>
                                 <th><?php echo __('German Content');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('Action');?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($references->toArray())){
                                    foreach ($references as $key => $reference) {
                                ?>
                            <tr>
                                <td><?php echo $reference->place_holder_number;?></td>
                                <td>
                                   <?php echo $reference->name;?> 
                                </td>
                                <td>
                                   <?php echo $reference->content_eng;?>         
                                </td>
                                 <td>
                                   <?php echo $reference->name_ger;?>         
                                </td>
                                 <td>
                                   <?php echo $reference->content_ger;?>         
                                </td>
                                <td>
                                    <?= $this->Html->Link("<span class='fa fa-pencil'></span>",
                                           'javascript:void(0)',
                                           [
                                               'escape'   => false,
                                               'class'    => 'edit-ref btn btn-primary',
                                               'data-url' =>   $this->Url->build(
                                                           [  
                                                               "controller" => "Reports",
                                                               "action" => "editReference",
                                                               base64_encode($reference->id)
                                                           ],true
                                                       ),
                                                'title'  => 'edit'                           
                                           ]
                                       )
                                   ?> 
                                </td>
                            </tr>
                        <?php } } else{?>
                            <tr>
                                <td colspan="6" class="text-center">
                                        <?php echo __('No Record Found'); ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <div class="text-right bredcum-bottom-ship col-sm-12">
                        <ul class="pagination">
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" role="tabpanel" id="emphasis" aria-labelledby="home-tab">
                <div class="table-responsive">
                    <table class="table dasbaord-table">
                        <thead>
                            <tr>
                                <th><?php echo __('Placeholder number');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('English Value');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('English Content');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('German Value');?><i class="ti-arrows-vertical"></i></th>
                                 <th><?php echo __('German Content');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('Action');?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($emphasis->toArray())){
                                    foreach ($emphasis as $key => $emphase) {
                                ?>
                            <tr>
                                <td><?php echo $emphase->place_holder_number;?></td>
                                <td>
                                   <?php echo $emphase->name;?> 
                                </td>
                                <td>
                                   <?php echo $emphase->content_eng;?>         
                                </td>
                                 <td>
                                   <?php echo $emphase->name_ger;?>         
                                </td>
                                 <td>
                                   <?php echo $emphase->content_ger;?>         
                                </td>
                                <td>
                                    <?= $this->Html->Link("<span class='fa fa-pencil'></span>",
                                           'javascript:void(0)',
                                           [
                                               'escape'   => false,
                                               'class'    => 'edit-emphasis btn btn-primary',
                                               'data-url' =>   $this->Url->build(
                                                           [  
                                                               "controller" => "Reports",
                                                               "action" => "editEmphasis",
                                                               base64_encode($emphase->id)
                                                           ],true
                                                       ),
                                                'title'  => 'edit'                           
                                           ]
                                       )
                                   ?> 
                                </td>
                            </tr>
                        <?php } } else{?>
                            <tr>
                                <td colspan="6" class="text-center">
                                        <?php echo __('No Record Found'); ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane fade" role="tabpanel" id="opinion" aria-labelledby="home-tab">
                <div class="table-responsive">
                    <table class="table dasbaord-table">
                        <thead>
                            <tr>
                                <th><?php echo __('English Value');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('English Content');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('German Value');?><i class="ti-arrows-vertical"></i></th>
                                 <th><?php echo __('German Content');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('Action');?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($opinions->toArray())){
                                    foreach ($opinions as $key => $value) {
                                ?>
                            <tr>
                                <td>
                                   <?php echo $value->name;?> 
                                </td>
                                <td>
                                   <?php echo $value->content_eng;?>         
                                </td>
                                 <td>
                                   <?php echo $value->name_ger;?>         
                                </td>
                                 <td>
                                   <?php echo $value->content_ger;?>         
                                </td>
                                <td>
                                    <?= $this->Html->Link("<span class='fa fa-pencil'></span>",
                                           'javascript:void(0)',
                                           [
                                               'escape'   => false,
                                               'class'    => 'edit-opinion btn btn-primary',
                                               'data-url' =>   $this->Url->build(
                                                           [  
                                                               "controller" => "Reports",
                                                               "action" => "editOpinion",
                                                               base64_encode($value->id)
                                                           ],true
                                                       ),
                                                'title'  => 'edit'                           
                                           ]
                                       )
                                   ?> 
                                </td>
                            </tr>
                        <?php } } else{?>
                            <tr>
                                <td colspan="6" class="text-center">
                                        <?php echo __('No Record Found'); ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane fade" role="tabpanel" id="legal" aria-labelledby="home-tab">
                <div class="table-responsive">
                    <table class="table dasbaord-table">
                        <thead>
                            <tr>
                                <th><?php echo __('Placeholder Number');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('Selection Name');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('English Value');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('German Value');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('Action');?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($legals->toArray())){
                                    foreach ($legals as $key => $legal) {
                                ?>
                            <tr>
                                <td>
                                    <?php echo $legal->placeholder_number;?>
                                </td>
                                <td>
                                   <?php echo $legal['legal_form_type']->name;?> 
                                </td>
                                <td>
                                   <?php echo $legal->eng_value;?>         
                                </td>
                                 <td>
                                   <?php echo $legal->ger_value;?>         
                                </td>
                                <td>
                                    <?= $this->Html->Link("<span class='fa fa-pencil'></span>",
                                           'javascript:void(0)',
                                           [
                                               'escape'   => false,
                                               'class'    => 'edit-legal btn btn-primary',
                                               'data-url' =>   $this->Url->build(
                                                           [  
                                                               "controller" => "Reports",
                                                               "action" => "editLegal",
                                                               base64_encode($legal->id)
                                                           ],true
                                                       ),
                                                'title'  => 'edit'                           
                                           ]
                                       )
                                   ?> 
                                </td>
                            </tr>
                        <?php } } else{?>
                            <tr>
                                <td colspan="6" class="text-center">
                                        <?php echo __('No Record Found'); ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        <div class="tab-pane fade" role="tabpanel" id="accounting" aria-labelledby="home-tab">
                <div class="table-responsive">
                    <table class="table dasbaord-table">
                        <thead>
                            <tr>
                                <th><?php echo __('Placeholder Number');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('Selection Name');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('English Value');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('German Value');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('Action');?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($accountings->toArray())){
                                    foreach ($accountings as $key => $accounting) {
                                ?>
                            <tr>
                                <td><?php echo $accounting->placeholder_number;?></td>
                                <td>
                                   <?php echo $accounting['accounting_framework_type']->name;?> 
                                </td>
                                <td>
                                   <?php echo $accounting->eng_value;?>         
                                </td>
                                 <td>
                                   <?php echo $accounting->ger_value;?>         
                                </td>
                                <td>
                                    <?= $this->Html->Link("<span class='fa fa-pencil'></span>",
                                           'javascript:void(0)',
                                           [
                                               'escape'   => false,
                                               'class'    => 'edit-accounting btn btn-primary',
                                               'data-url' =>   $this->Url->build(
                                                           [  
                                                               "controller" => "Reports",
                                                               "action" => "editAccounting",
                                                               base64_encode($accounting->id)
                                                           ],true
                                                       ),
                                                'title'  => 'edit'                           
                                           ]
                                       )
                                   ?> 
                                </td>
                            </tr>
                        <?php } } else{?>
                            <tr>
                                <td colspan="6" class="text-center">
                                        <?php echo __('No Record Found'); ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane fade" role="tabpanel" id="other" aria-labelledby="home-tab">
                <div class="table-responsive">
                    <table class="table dasbaord-table">
                        <thead>
                            <tr>
                                <th><?php echo __('Placeholder Number');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('Selection Name');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('English Value');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('German Value');?><i class="ti-arrows-vertical"></i></th>
                                <th><?php echo __('Action');?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($others->toArray())){
                                    foreach ($others as $key => $other) {
                                ?>
                            <tr>
                                <td><?php echo $other->placeholder_number;?></td>
                                <td>
                                   <?php echo $other->selection_name;?> 
                                </td>
                                <td>
                                   <?php echo $other->eng_value;?>         
                                </td>
                                 <td>
                                   <?php echo $other->ger_value;?>         
                                </td>
                                <td>
                                    <?= $this->Html->Link("<span class='fa fa-pencil'></span>",
                                           'javascript:void(0)',
                                           [
                                               'escape'   => false,
                                               'class'    => 'edit-other btn btn-primary',
                                               'data-url' =>   $this->Url->build(
                                                           [  
                                                               "controller" => "Reports",
                                                               "action" => "editOther",
                                                               base64_encode($other->id)
                                                           ],true
                                                       ),
                                                'title'  => 'edit'                           
                                           ]
                                       )
                                   ?> 
                                </td>
                            </tr>
                        <?php } } else{?>
                            <tr>
                                <td colspan="6" class="text-center">
                                        <?php echo __('No Record Found'); ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
</section>
<div class="modal Educational-info fade" id="edit-modified" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Edit Modified Opinion'); ?></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<div class="modal Educational-info fade" id="edit-ref" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Edit References'); ?></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<div class="modal Educational-info fade" id="edit-emphasis" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Edit Emphasis of Matter'); ?></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<div class="modal Educational-info fade" id="edit-opinion" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Edit Opinion Types'); ?></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<div class="modal Educational-info fade" id="edit-legal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Edit Legal Values'); ?></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<div class="modal Educational-info fade" id="edit-accounting" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Edit Accounting Values'); ?></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<div class="modal Educational-info fade" id="edit-other" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Edit LSE Others'); ?></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<?php
    echo $this->Html->script([
            'Administration/placeholder'
        ],
        [
            'block' => 'scriptBottom'
    ]);
?>
