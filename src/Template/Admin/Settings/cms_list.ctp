<section class="content-header clearfix">
   <div class="col-lg-12 heading-top">
      <h1 class="heading-text-color pull-left"><?php echo __('CMS Pages'); ?></h1>
   </div>
</section>
<!-- Main content -->
<section class="content">
   <div class="box">
        <div class="table-responsive">
            <table class="table shoping-cart-table">
                <thead>
                    <tr>
                       
                        <th><?= $this->Paginator->sort(
                                'meta_title',
                                __('Meta Title'),
                                ['escape' => false]
                                )
                            ?>
                             
                        </th>
                        <th>
                            <?= $this->Paginator->sort(
                                'page_name',
                                __('Subject'),
                                ['escape' => false]
                                )
                            ?>
                        </th>
                        <th>
                            <?= $this->Paginator->sort(
                                'meta_keyword',
                                __('Meta Keywords'),
                                ['escape' => false]
                                )
                            ?>
                        </th>
                        <th><?= $this->Paginator->sort(
                                'modified',
                                __('Modified'),
                                ['escape' => false]
                                )
                            ?>
                            
                        </th>
                        <th class="actions"><?= __('Actions') ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                       if (!empty($cmspages)) {
                            foreach ($cmspages as $cmapage) {
                    ?>
                                <tr>
                                    <td><?= $cmapage->meta_title ?></td>
                                    <td><?= $cmapage->page_name ?></td>
                                    <td><?= $cmapage->meta_keyword ?></td>
                                    <td><?= $this->Time->nice($cmapage->modified) ?></td>
                                    <td>
                                        <?= $this->Html->link(
                                                __('<span class="fa fa-edit"></span>'), 
                                                [
                                                    'action' => 'editCms',
                                                    base64_encode($cmapage->id)
                                                ],
                                                [
                                                    'escape' => false
                                                ]
                                            ) 
                                        ?>
                                    </td>
                                </tr>
                    <?php   
                            }//end of foreach loop
                        } else {
                              echo "<tr><td colspan=6>No Record found</td></tr>";
                        }
                    ?>
                </tbody>
            </table>
            <?= $this->element('pagination'); ?>
        </div>
    </div>
</section>