<?php use Cake\Core\Configure; ?>
<section class="content-header clearfix">
    <div class="col-lg-12 heading-top">
        <h1 class="heading-text-color pull-left"><?php echo __('Manage Report Categories'); ?></h1>
        <button href="#" data-toggle="modal" data-backdrop="static" data-target="#add-category" class="btn btn-default add_button pull-right" type="button"><?php echo __('Add Report Category'); ?></button>
        <?php
            echo $this->Form->Create(null, array(
                    'url' => ['action' => 'reportCategory'],
                    'type' => 'get',
                )
            );
        ?>
        <div class="input-group my-search col-sm-3 pull-right">
            <?php
                    echo $this->Form->input('search', array(
                            'class' => 'form-control',
                            'placeholder' => __('Search for...'),
                            'label'=>FALSE,
                            'templates' => [
                               'inputContainer' => '{{content}}',
                               'inputContainerError' => '{{content}}{{error}}'
                           ],
                            'value' => (isset($_GET['search']) && !empty($_GET['search'])) ? $_GET['search'] : ''
                        )
                    );
            ?>
            <span class="input-group-btn"> 
            <?= $this->Form->button(
                    __('Go!'),
                        [
                            'type' => 'submit',
                            'class' => 'btn btn-default'
                        ]
                    );
            ?>
             </span>
        </div>
        </form>
    </div>
</section>
<section class="content shoping-cart clearfix">
    <div class="col-sm-12">
        <div class="note-listing">
            <div class="table-responsive">
                <table class="table shoping-cart-table">
                    <thead>
                        <tr>
                            <th><?php echo __($this->Paginator->sort('name','Report Category Name'))?></th>
                            <th><?php echo __('Action'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (!$categories->isEmpty()) {
                                foreach ($categories as $category) {
                        ?>
                                    <tr>
                                        <td><?php echo $category->name; ?></td>
                                        <td>
                                           <?= $this->Html->Link("<span class='fa fa-pencil'></span>",
                                                   'javascript:void(0)',
                                                   [
                                                       'escape'   => false,
                                                       'class'    => 'edit-category',
                                                       'data-url' =>   $this->Url->build(
                                                                   [  
                                                                       "controller" => "Settings",
                                                                       "action" => "editCategory",
                                                                       base64_encode($category->id)
                                                                   ],true
                                                               ),
                                                        'title'  => 'edit'                           
                                                   ]
                                               )
                                           ?> 
                                           <?= $this->Form->postLink("<span class='fa fa-trash'></span>",
                                                   '/admin/Settings/deleteCategory/'.base64_encode($category->id),
                                                   [
                                                       'escape'   => false,
                                                       'class'    => 'delete-category',
                                                        'title'  => 'delete',
                                                        'confirm' => 'Are you sure you want to delete Report Category?'                           
                                                   ]
                                               )
                                           ?> 
                                        </td>
                                    </tr>
                        <?php
                                }
                            } else {
                        ?>
                                <tr>
                                    <td colspan="6" class="text-center">
                                        <?php echo __('No Record Found'); ?>
                                    </td>
                                </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php echo $this->element('pagination'); ?>
            </div>
        </div>
    </div>
</section>
<div class="modal Educational-info fade" id="edit-category" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Edit Report category'); ?></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

<?php
    echo $this->element('Administration/add_category');
    echo $this->Html->script([
            'Administration/add_category',
        ],
        [
            'block' => 'scriptBottom'
    ]);
?>