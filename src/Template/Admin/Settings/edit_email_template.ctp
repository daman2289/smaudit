<section class="content-header clearfix">
   <div class="col-lg-12 heading-top">
      <h1 class="heading-text-color pull-left"><?php echo __('Email Templates'); ?></h1>
   </div>
</section>
<!-- Main content -->
<section class="content">
   <div class="box">         
      <div class="col-lg-12">                     
        <div id="emails">
            <?= $this->Form->create($emailTemplate, ['class' => 'form-horizontal', 'novalidate' => true]); ?>
                <div class="form-group">
                    <label class="control-label" for="exampleInputEmail1"><?php echo __('Template Used For'); ?></label>
                    <?= $this->Form->input('template_used_for', ['class'=>'form-control', 'label' => false, 'disabled' => true]); ?>
                </div>
                <div class="form-group">
                    <label class="control-label" for="exampleInputEmail1"><?php echo __('Subject'); ?></label>
                    <?= $this->Form->input('subject', ['class'=>'form-control', 'label' => false]); ?>
                </div>
                <div class="form-group">
                    <label class="control-label" for="exampleInputEmail1"><?php echo __('Body'); ?></label>
                    <?php
                        echo $this->CKEditor->loadJs();
                        echo $this->Form->textarea('mail_body_eng', ['class'=>'form-control', 'label' => false]);
                        echo $this->CKEditor->replace('mail_body_eng');
                        if ($this->Form->isFieldError('mail_body_eng')) {
                            echo $this->Form->error('mail_body_eng');
                        }
                    ?>
                </div>
                <div class="form-group">
                    <label class="control-label" for="exampleInputEmail1"><?php echo __('Body'); ?></label>
                    <?php
                        echo $this->CKEditor->loadJs();
                        echo $this->Form->textarea('mail_body_ger', ['class'=>'form-control', 'label' => false]);
                        echo $this->CKEditor->replace('mail_body_ger');
                        if ($this->Form->isFieldError('mail_body_ger')) {
                            echo $this->Form->error('mail_body_ger');
                        }
                    ?>
                </div>
                <div class="col-lg-8">
                    <?= $this->Form->button(__('Save'), ['class' => "sbtn submit-info submit_black", 'type' => 'submit']); ?>
                </div>
            <?= $this->Form->end() ?>
         </div>
      </div>
   </div>
</section>