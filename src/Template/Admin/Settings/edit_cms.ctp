<section class="content-header clearfix">
   <div class="col-lg-12 heading-top">
      <h1 class="heading-text-color pull-left"><?php echo __('CMS Page'); ?></h1>
   </div>
</section>
<!-- Main content -->
<section class="content">
   <div class="box">         
      <div class="col-lg-12">                     
        <div id="emails">
            <?= $this->Form->create($cms, ['class' => 'form-horizontal', 'novalidate' => true]) ?>
                <div class="form-group">
                    <label class="control-label" for="exampleInputEmail1"><?php echo __('Meta Title'); ?></label>
                    <?= $this->Form->input('meta_title', ['class'=>'form-control', 'label' => false]); ?>
                </div>
                <div class="form-group">
                    <label class="control-label" for="exampleInputEmail1"><?php echo __('Meta Keywords'); ?></label>
                    <?= $this->Form->input('meta_keyword', ['class'=>'form-control', 'label' => false]); ?>
                </div>
                <div class="form-group">
                    <label class="control-label" for="exampleInputEmail1"><?php echo __('Page Title'); ?></label>
                    <?= $this->Form->input('page_title', ['class'=>'form-control', 'label' => false]); ?>
                </div>
                <div class="form-group">
                    <label class="control-label" for="exampleInputEmail1"><?php echo __('Page Name'); ?></label>
                    <?= $this->Form->input('page_name', ['class'=>'form-control', 'label' => false]); ?>
                </div>
                <div class="form-group">
                    <?php echo $this->CKEditor->loadJs(); ?>
                    <label class="control-label" for="exampleInputEmail1"><?php echo __('Meta Description'); ?></label>
                    <?php
                        echo $this->Form->input('meta_description', ['class'=>'form-control', 'label' => false]);
                        echo $this->CKEditor->replace('meta_description');
                    ?>
                </div>
                <div class="col-lg-8">
                    <?= $this->Form->button(__('Save'), ['class' => "sbtn submit-info submit_black", 'type' => 'submit']); ?>
                </div>
            <?= $this->Form->end() ?>
         </div>
      </div>
   </div>
</section>