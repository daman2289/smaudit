<div class='fourth-form' style="display: none">
    <?php
        echo $this->Form->create($report, [
            'type' => 'post',
            'url' => [
                'controller' => 'Reports',
                'action' => 'edit-report',
                base64_encode($report->id)
            ],
            'id' => 'first-form'    
        ]); 
    ?>
    <div class="tab-pane auditor-chk" role="tabpanel" id="step4">
        <div class="form-group clearfix">
            <div class="col-sm-6">
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label><?php echo __('First Signature');?></label>
                        </div>
                        <div class="col-sm-12">
                            <?php
                                echo $this->Form->input('first_signature_user_id',
                                    array(
                                        'class' => 'form-control',
                                        'placeholder' => __(''),
                                        'div' => false,
                                        'label' => false,
                                        'required' => true,
                                        'options' => $usersList,
                                        'empty' => 'Select'
                                    )
                                );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label><?php echo __('Auditor Incharge');?></label>
                        </div>
                        <div class="col-sm-12">
                            <div class="works-in-ie-login">
                                <label>
                                <input type="checkbox" value="0" name='auditor_incharge' checked class="row-checkbox">
                                <i class="faa fa-unchecked unchecked"></i>
                                <i class="faa fa-checked  checked"></i>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group clearfix">
            <div class="col-sm-6">
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label><?php echo __('Second Signature');?></label>
                        </div>
                        <div class="col-sm-12">
                           <?php
                                echo $this->Form->input('second_signature_user_id',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __(''),
                                                'div' => false,
                                                'label' => false,
                                                'required' => true,
                                                'options' => $usersList,
                                                'empty' => 'Select'
                                            )
                                    );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label><?php echo __('Auditor Incharge');?></label>
                        </div>
                        <div class="col-sm-12">
                            <div class="works-in-ie-login">
                                <label>
                                <input type="checkbox" value="1" name='auditor_incharge' class="row-checkbox">
                                <i class="faa fa-unchecked unchecked"></i>
                                <i class="faa fa-checked  checked"></i>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group clearfix text-right">
            <div class="col-lg-8">
                <button type="submit" class="btn btn-blue btn-info-full next-step"><?php echo __('Save');?></button>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end() ?>
</div>