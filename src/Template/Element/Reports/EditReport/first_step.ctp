<div class='first-form' style="display: none">
    <?php
        echo $this->Form->create($report, [
            'type' => 'post',
            'url' => [
                'controller' => 'Reports',
                'action' => 'edit-report',
                base64_encode($report->id)
            ],
            'id' => 'first-form'    
        ]); 
    ?>
    <div class="tab-pane active" role="tabpanel" id="step1">
        <div class="col-xs-12">
            <div class="form-group clearfix">
            <div class="row">
                <div class="col-lg-6 ui-widget">
                    <label><?php echo __('Select Company');?></label>
                    <div class="search-input">
                        <?php
                            echo $this->Form->input('organization_company',
                                array(
                                    'class' => 'form-control',
                                    'placeholder' => __('Search here'),
                                    'div' => false,
                                    'label' => false,
                                    'required' => true,
                                    'type' => 'text',
                                    'id' => 'tag-tagname',
                                    'disabled',
                                    'value' => ($report->has('organization_company')) ? 
                                        __($report->organization_company->company_name) : 
                                        '' 
                                )
                            );
                            echo $this->Form->hidden(
                                'organization_company_id',
                                array(
                                    'id' => 'company-id',
                                    'value' => ($report->has('organization_company')) ? 
                                        __($report->organization_company->id) : 
                                        '' 
                                )
                            )
                        ?>
                       <span class="btn"><i class="glyphicon glyphicon-search"></i></span>
                    </div>
                </div>
                <div class="col-lg-6 ui-widget">
                    <label><?php echo __('Report Date');?><span class="red-star">*</span></label>
                    <div class='input-group date'  id='reportDate'>
                        <?php
                            echo $this->Form->input('report_date',
                                array(
                                    'class'     => 'form-control',
                                    'placeholder' => __('Report Date'),
                                    'div'       => false,
                                    'label'     => false,
                                    'required'  => true,
                                    'type'      => 'text',
                                    'value'     => date('d/m/Y',strtotime($report->report_date))
                                
                                )
                            );
                        ?>
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            </div>       
            <div class="form-group clearfix">           
                <div class="row">
                    <div class="col-lg-12">
                        <label><?php echo __('Financial year');?></label>
                    </div>
                    <div class="col-md-6">
                        <label><?php echo __('From');?></label>
                        <div class='input-group date' id='financialFrom'>
                            <?php
                                echo $this->Form->input('financial_year_from',
                                    array(
                                        'class' => 'form-control',
                                        'placeholder' => __('21/03/2017'),
                                        'div'   => false,
                                        'label'     => false,
                                        'required'  => true,
                                        'type'  => 'text',
                                        'id'    => 'fromdate',
                                        'value'     => date('d/m/Y',strtotime($report->financial_year_from))
                                    )
                                );
                            ?>
                            <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label><?php echo __('To');?></label>
                        <div class='input-group date' id='financialTo'>
                            <?php
                                echo $this->Form->input('financial_year_to',
                                    array(
                                        'class'     => 'form-control',
                                        'placeholder' => __('21/03/2018'),
                                        'div'       => false,
                                        'label'     => false,
                                        'required' => true,
                                        'type' => 'text',
                                        'id'    => 'todate',
                                        'value'     => date('d/m/Y',strtotime($report->financial_year_to))
                                    )
                                );
                            ?>
                            <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="row"> 
                    <div class="col-lg-6">
                        <label><?php echo __('Financial Year');?></label>
                        <?php
                            echo $this->Form->input('financial_year',
                                array(
                                    'class' => 'form-control',
                                    'placeholder' => __('2017'),
                                    'div' => false,
                                    'label' => false,
                                    'required' => true,
                                    'type' => 'text',
                                    'id' => 'financeYear',
                                    'value'     => $report->financial_year
                                )
                            );
                        ?>
                    </div>               
                    <div class="col-lg-2">
                        <label><?php echo __('Short/long');?></label>
                        <div class="works-in-ie-login">
                            <label>
                            <?php 
                                echo $this->Form->checkbox(
                                    'short_long', 
                                    [
                                        'div' => false,
                                        'label' => false,
                                        ($report->short_long) ?'checked':''
                                    ]
                                );
                            ?>
                            <i class="faa fa-unchecked unchecked"></i>
                            <i class="faa fa-checked  checked"></i>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group text-right clearfix">
                <div class="col-lg-8">
                    <button type="submit" class="btn btn-blue first-step next-step"><?php echo __('Save');?></button>
                </div>
            </div>
        </div>
    </div>
    <?php 
        echo $this->Form->end();
    ?>
</div>