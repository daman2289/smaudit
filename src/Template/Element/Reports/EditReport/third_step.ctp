<div class='third-form' style="display: none">
    <?php
        echo $this->Form->create($report, [
            'type' => 'post',
            'url' => [
                'controller' => 'Reports',
                'action' => 'edit-report',
                base64_encode($report->id)
            ],
            'id' => 'first-form'    
        ]); 
    ?>
    <?php
    	$reportEmphasis = explode(',', $report->emphasis_of_matter_ids);
    	$reportReferences = explode(',', $report->reference_ids);
    ?>
    <script type="text/javascript">
    	var emphasis = '<?php echo json_encode($reportEmphasis) ?>';
    	var references = '<?php echo json_encode($reportReferences) ?>';
    </script>
	<div class="tab-pane" role="tabpanel" id="step3">
	    <div class="col-xs-12">
	        <div class="form-group clearfix">
	            <div class="col-sm-12">
	                <label><?php echo __('Assistance in book Keepings?');?></label>
	            </div>
	            <div class="works-in-ie-login">
	                <label>
	                <?php echo $this->Form->checkbox('assistance_book_keeping', 
	                    [
	                    // 'hiddenField' => false,
	                    'templates' => [
	                       'inputContainer' => '{{content}}',
	                       'inputContainerError' => '{{content}}{{error}}'
	                     ],
	                     'label' =>false
	                    ]
	                );?>
	                <i class="faa fa-unchecked unchecked"></i>
	                <i class="faa fa-checked  checked"></i>
	                </label>
	            </div>
	        </div>
	        <div class="form-group clearfix">
	            <div class="col-sm-12">
	                <label><?php echo __('Appropriation of Available Earnings?');?></label>
	            </div>
	            <div class="works-in-ie-login">
	                <label>
	                <?php echo $this->Form->checkbox('appropriation_available_earning', 
	                    [
	                    // 'hiddenField' => false,
	                    'templates' => [
	                       'inputContainer' => '{{content}}',
	                       'inputContainerError' => '{{content}}{{error}}'
	                     ],
	                     'label' =>false
	                    ]
	                );?>
	                <i class="faa fa-unchecked unchecked"></i>
	                <i class="faa fa-checked  checked"></i>
	                </label>
	            </div>
	        </div>
	        <div class="form-group clearfix">
	            <div class="col-lg-6">
	                <label><?php echo __('Accounting Framework');?></label>
	                <?php
	                    echo $this->Form->input('accounting_framework_type_id',
                            array(  
                        		'default' => 4,
                                'class' => 'form-control',
                                'placeholder' => __(''),
                                'div' => false,
                                'label' => false,
                                'required' => true,
                                'options' => $accounting
                            )
                        );
	                ?>
	            </div>
	        </div>
	        <div class="form-group audi-terminated clearfix">
	            <div class="col-sm-12">
	                <label><?php echo __('Was Audit Terminated Later Than report Date?');?></label>
	            </div>
	            <div class="works-in-ie-signup">
	                <label>
	                <input <?= ($report->audit_terminated_later_than_report == 1)?'checked':''; ?> type="radio" value="1" name="audit_terminated_later_than_report">
	                <i class="fa-radio fa-unchecked unchecked"></i>
	                <i class="fa-radio fa-checked  checked"></i>
	                <span class="text-checkbox">Yes</span>
	                </label>
	                <label>
	                <input type="radio" name="audit_terminated_later_than_report" <?= ($report->audit_terminated_later_than_report == 0)?'checked':''; ?> value="0">
	                <i class="fa-radio fa-unchecked unchecked"></i>
	                <i class="fa-radio fa-checked checked"></i>
	                <span class="text-checkbox">No</span>
	                </label>
	            </div>
	        </div>
	        <div class="form-group clearfix terminated" style="<?= ($report->audit_terminated_later_than_report != 1)?'display:none':''; ?>">
	            <div class="col-lg-6">
	                <label><?php echo __('When Was Audit Terminated?');?></label>
	                <div class='input-group date' id='audit-terminated-date'>
	                    <?php
	                    echo $this->Form->input('audit_terminated_date',
	                            array(
	                                'class' => 'form-control',
	                                'placeholder' => __(''),
	                                'div' => false,
	                                'label' => false,
	                                'required' => true,
	                                'type' => 'text',
	                                ($report->audit_terminated_later_than_report != 1)?'disabled':''
	                            )
	                        );
	                    ?>
	                    <span class="input-group-addon">
	                    <span class="glyphicon glyphicon-calendar"></span>
	                    </span>
	                </div>
	            </div>
	        </div>
	        <div class="form-group clearfix">
	            <div class="col-lg-6">
	                <label><?php echo __('Emphasis of matter?');?></label>
	                <div>
	                    <?php
	                        echo $this->Form->input('emphasis_of_matter_ids',
	                            array(
	                            	'default' => (!empty($reportEmphasis))?$reportEmphasis['0']:'',
	                                'class' => 'form-control',
	                                'placeholder' => __(''),
	                                'div' => false,
	                                'label' => false,
	                                'required' => false,
	                                'options' => $emphasis,
	                                'multiple',
	                                'id' => "emphasis",
	                                'name'  => 'emphasis_of_matter_ids[]'
	                            )
	                        );
	                    ?>
	                </div>
	            </div>
	        </div>
	        <div class="form-group clearfix">
	            <div class="col-lg-6">
	                <label><?php echo __('References');?></label>
	                <div>
	                    <?php
	                        echo $this->Form->input('reference_ids',
	                            array(
	                                'class' => 'form-control',
	                                'placeholder' => __(''),
	                                'div' => false,
	                                'label' => false,
	                                'required' => false,
	                                'options' => $reference,
	                                'multiple',
	                                'id' => "references",
	                                'name'  => 'reference_ids[]'
	                            )
	                        );
	                    ?>
	                </div>
	            </div>
	        </div>
	        <?php 
	        	$is_subordinate = false;
	        	if(in_array(1, $reportEmphasis) || in_array(7, $reportEmphasis)) {
	        		$is_subordinate = true;
	        	}
	        	$is_concern = false;
	        	if(in_array(2, $reportEmphasis) || in_array(3, $reportEmphasis)) {
	        		$is_concern = true;
	        	}
	        ?>
	        <div class="form-group Subordination clearfix <?= ($is_subordinate)?'':'hide'?>">
	            <div class="col-lg-6">
	                <label><?php echo __('Subordination Amount');?></label>
	                <?php
	                    echo $this->Form->input('subordination_amount',
                            array(  
                                'class' => 'form-control',
                                'placeholder' => __('CHF 578,542'),
                                'div' => false,
                                'label' => false,
                                'required' => true,
                                'type' => 'text',
                                ($is_subordinate)?'':'disabled'                              
                            )
                        );
	                ?>
	            </div>
	        </div>
	        <div class="form-group Concern clearfix <?= ($is_concern)?'':'hide'?>">
	            <div class="col-lg-6">
	                <label><?php echo __('Going Concern Note No');?></label>
	                <?php
	                    echo $this->Form->input('going_concern_note_no',
                            array(
                                'class' => 'form-control',
                                'placeholder' => __('8'),
                                'div' => false,
                                'label' => false,
                                'required' => true,
                                'type' => 'text',
                                ($is_concern)?'':'disabled'                            
                            )
                        );
	                ?>
	            </div>
	        </div>
	        <?php 
	        	$is_shareholder_loan = false;
	        	if(in_array(2, $reportReferences) || in_array(7, $reportReferences)) {
	        		$is_shareholder_loan = true;
	        	}
	        	$is_dividend = false;
	        	if(in_array(4, $reportReferences)/* || in_array(3, $reportReferences)*/) {
	        		$is_dividend = true;
	        	}
	        ?>
	        <div class="form-group Shareholders clearfix <?= ($is_shareholder_loan)?'':'hide'?>">
	            <div class="col-lg-6">
	                <label><?php echo __('Amount of Shareholders Loan');?></label>
	                <?php
	                    echo $this->Form->input('amount_shareholder_loan',
                            array(
                                'class' => 'form-control',
                                'placeholder' => __("999'999"),
                                'div' => false,
                                'label' => false,
                                'required' => true,
                                'type' => 'text',
                                ($is_shareholder_loan)?'':'disabled'
                            )
                        );
	                ?>
	            </div>
	        </div>
	        <div class="form-group dividend clearfix <?= ($is_dividend)?'':'hide'?>">
	            <div class="col-lg-6">
	                <label><?php echo __('Amount Dividend');?></label>
	                <?php
	                    echo $this->Form->input('amount_dividend',
                            array(  
                                'class' => 'form-control',
                                'placeholder' => __('CHF 578,542'),
                                'div' => false,
                                'label' => false,
                                'required' => true,
                                'type' => 'text',
                                ($is_dividend)?'':'disabled'
                            )
                        );
	                ?>
	            </div>
	        </div>	        
	        <div class="form-group clearfix">
	            <div class="col-lg-6">
	                <label><?php echo __('Modified Opinion?');?></label>
	                <?php
	                    echo $this->Form->input('modified_opinion_id',
                            array(
                                'default' => 3,
                                'class' => 'form-control',
                                'placeholder' => __(''),
                                'div' => false,
                                'label' => false,
                                'required' => true,
                                'options' => $modified
                            )
                        );
	                ?>
	            </div>
	        </div>
	        <div class="form-group clearfix modifaction hide">
	            <div class="col-lg-6">
	                <label><?php echo __('Basis for Modification'); ?></label>
	                <?php
	                    echo $this->Form->input('basis_for_modification',
	                            array(
	                                    'class' => 'form-control',
	                                    'placeholder' => __('With regard to valuation and disclosure we note the following: Our limited statutory examination revealed that litigation is pending against you for breach of patent rights and that no provisions have been recognised regarding this matter. At present, it is not possible to predict whether, and to what extent, you will have to pay for any damages. As a result, a material uncertainty exists in relation to the measurement of provisions. There are no respective disclosures made in the notes to the financial statements'),
	                                    'div' => false,
	                                    'label' => false,
	                                    'required' => true,
	                                    'type' => 'textarea',
	                                    'rows' => 5
	                                
	                                )
	                        );
	                ?>
	            </div>
	        </div>
	        <div class="form-group clearfix text-right">
	            <div class="col-lg-8">
	                <button type="submit" class="btn btn-blue btn-info-full next-step"><?php echo __('save');?></button>
	            </div>
	        </div>
	    </div>
	</div>
	<?= $this->Form->end() ?>
</div>