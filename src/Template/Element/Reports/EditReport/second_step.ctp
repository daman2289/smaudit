<div class='second-form' style="display: none">
    <?php
        echo $this->Form->create($report, [
            'type' => 'post',
            'url' => [
                'controller' => 'Reports',
                'action' => 'edit-report',
                base64_encode($report->id)
            ],
            'id' => 'first-form'    
        ]); 
    ?>
	<div class="tab-pane" role="tabpanel" id="step2">
	    <div class="col-xs-12">
	    	<div class="row">    	
		    	<div class="col-md-6">        		
		        	<div class="form-group clearfix">
			            <div class="col-sm-12">
			                <label><?php echo __('Is First Year Audit?')?></label>
			            </div>
			            <div class="works-in-ie-login">
			                <label>
			                <?php echo $this->Form->checkbox('first_year_audit', 
			                    [
			                    // 'hiddenField' => false,
			                    'templates' => [
			                       'inputContainer' => '{{content}}',
			                       'inputContainerError' => '{{content}}{{error}}'
			                     ],
			                     'label' =>false
			                    ]
			                );?>
			                <i class="faa fa-unchecked unchecked"></i>
			                <i class="faa fa-checked  checked"></i>
			                </label>
			            </div>
		        	</div>
		        </div>
		    	<div class="col-md-6">
		    		<div class="form-group optingout clearfix" style="<?= ($report->first_year_audit != 1)?'display:none':''; ?>">
			            <div class="col-sm-12">
			                <label><?php echo __('Opting Out?');?></label>
			            </div>
			            <div class="works-in-ie-signup">
			                <label>
				                <input <?= ($report->opting_out == 1)?'checked':'' ?> type="radio" value="1" name="opting_out" >
				                <i class="fa-radio fa-unchecked unchecked"></i>
				                <i class="fa-radio fa-checked  checked"></i>
				                <span class="text-checkbox">Yes</span>
			                </label>
			                <label>
				                <input type="radio" name="opting_out" value="0" <?= ($report->opting_out == 0)?'checked':'' ?>>
				                <i class="fa-radio fa-unchecked unchecked"></i>
				                <i class="fa-radio fa-checked checked"></i>
				                <span class="text-checkbox">No</span>
			                </label>
			            </div>
			        </div>
		    	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="form-group dateprior clearfix" style="<?= ($report->opting_out == 1)?'display:none':'' ?>">
		                <label><?php echo __('Date Prior Audit Report');?></label>
		                <div class='input-group date' id='auditReport'>
		                    <?php
		                    echo $this->Form->input('date_prior_audit_report',
		                            array(
		                                'class' => 'form-control',
		                                'placeholder' => __(''),
		                                'div' => false,
		                                'label' => false,
		                                'required' => true,
		                                'type' => 'text',
		                        		($report->opting_out == 1)?'disabled':''                                 
		                            )
		                        );
		                    ?>
		                    <span class="input-group-addon">
		                    <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>
			        </div>
	        	</div>
	        	<div class="col-md-6"> 
	        		<div class="form-group opiniontypes dateprior clearfix" style="<?= ($report->opting_out == 1)?'display:none':'' ?>">
		                <label><?php echo __('Opinion Types');?></label>
		                <?php
		                    echo $this->Form->input('opinion_type_id',
		                        array(
		                            'class' => 'form-control',
		                            'placeholder' => __(''),
		                            'div' => false,
		                            'label' => false,
		                            'required' => true,
		                            'options' => $opinion,
		                            ($report->opting_out == 1)?'disabled':''                       
		                        )
		                    );
		                ?>
			        </div>
	        	</div>
	        </div>
	        <div class="form-group clearfix text-right">
	            <div class="col-lg-8">
	                <button type="submit" class="btn btn-blue next-step"><?php echo __('Save');?></button>
	            </div>
	        </div>
	    </div>
	</div>
	<?php echo $this->Form->end() ?>
</div>