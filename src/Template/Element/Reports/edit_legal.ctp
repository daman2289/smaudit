<?php
    echo $this->Form->create($legal,
            [
                    'type' => 'post',
                    'id' => 'edit-legal-form',
                    'class' => 'form-horizontal'
            ]
        );
?>            
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Selection Name'); ?></label>
        <div class="col-sm-8">
            <?php echo $this->Form->hidden('id');?>
            <label><?php echo $legal['legal_form_type']->name;?></label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Placeholder Number'); ?></label>
        <div class="col-sm-8">
            <label><?php echo $legal->placeholder_number;?></label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('English Value'); ?></label>
        <div class="col-sm-8">
            <?php
                echo $this->Form->input('eng_value',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('English Value'),
                                'div' => false,
                                'label' => false,
                                'type' => 'textarea'
                            )
                    );
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('German Value'); ?></label>
        <div class="col-sm-8">
            <?php
                echo $this->Form->input('ger_value',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('German Value'),
                                'div' => false,
                                'label' => false,
                                'type' => 'textarea'
                            )
                    );
            ?>
        </div>
    </div>
    <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
        <div class="col-lg-8">
            <?php
                echo $this->Form->button('Submit', array(
                            'class' => 'btn submit-info submit_black',
                            'type' => 'submit'
                            ));
            ?>
            <?php
                echo $this->Form->button('Cancel', array(
                            'class' => 'btn submit-info submit_black',
                            'type' => 'button',
                            'data-dismiss' => 'modal'
                            ));
            ?>
        </div>
    </div>
<?php echo $this->Form->end(); ?>