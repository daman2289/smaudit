<div class="tab-pane" role="tabpanel" id="step2">
    <div class="col-xs-12">
        <div class="form-group clearfix">
            <div class="col-sm-12">
                <label><?php echo __('Is First Year Audit?')?></label>
            </div>
            <div class="works-in-ie-login">
                <label>
                <?php echo $this->Form->checkbox('first_year_audit', 
                    [
                    // 'hiddenField' => false,
                    'templates' => [
                       'inputContainer' => '{{content}}',
                       'inputContainerError' => '{{content}}{{error}}'
                     ],
                     'label' =>false
                    ]
                );?>
                <i class="faa fa-unchecked unchecked"></i>
                <i class="faa fa-checked  checked"></i>
                </label>
            </div>
        </div>
        <div class="form-group optingout clearfix" style="display:none">
            <div class="col-sm-12">
                <label><?php echo __('Opting Out?');?></label>
            </div>
            <div class="works-in-ie-signup">
                <label>
                <input checked="cehcked" type="radio" value="1" name="opting_out" disabled="">
                <i class="fa-radio fa-unchecked unchecked"></i>
                <i class="fa-radio fa-checked  checked"></i>
                <span class="text-checkbox">Yes</span>
                </label>
                <label>
                <input type="radio" name="opting_out" value="0" disabled="">
                <i class="fa-radio fa-unchecked unchecked"></i>
                <i class="fa-radio fa-checked checked"></i>
                <span class="text-checkbox">No</span>
                </label>
            </div>
        </div>
        <div class="form-group dateprior clearfix" style="display:none">
            <div class="col-lg-6">
                <label><?php echo __('Date Prior Audit Report');?></label>
                <div class='input-group date' id='auditReport'>
                    <?php
                    echo $this->Form->input('date_prior_audit_report',
                            array(
                                    'class' => 'form-control',
                                    'placeholder' => __(''),
                                    'div' => false,
                                    'label' => false,
                                    'required' => true,
                                    'type' => 'text',
                                    'disabled'
                                
                                )
                        );
                    ?>
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group opiniontypes dateprior clearfix" style="display:none">
            <div class="col-lg-6">
                <label><?php echo __('Opinion Types');?></label>
                <?php
                    echo $this->Form->input('opinion_type_id',
                        array(
                            'class' => 'form-control',
                            'placeholder' => __(''),
                            'div' => false,
                            'label' => false,
                            'required' => true,
                            'options' => $opinion,
                            'disabled'                        
                        )
                    );
                ?>
            </div>
        </div>
        <div class="form-group clearfix text-right">
            <div class="col-lg-8">
                <button type="button" class="btn btn-default-step prev-step"><?php echo __('Back');?></button>
                <button type="button" class="btn btn-blue next-step"><?php echo __('Next');?></button>
            </div>
        </div>
    </div>
</div>