<div class="tab-pane active" role="tabpanel" id="step1">
    <div class="col-xs-12">
        <div class="form-group clearfix">
            <div class="col-lg-6 ui-widget">
                <label><?php echo __('Select Company');?></label>
                <div class="search-input">
                    <?php
                        echo $this->Form->input('organization_company',
                            array(
                                'class' => 'form-control',
                                'placeholder' => __('Search here'),
                                'div' => false,
                                'label' => false,
                                'required' => true,
                                'type' => 'text',
                                'id' => 'tag-tagname'
                            )
                        );
                        echo $this->Form->hidden(
                            'organization_company_id',
                            array(
                                'id' => 'company-id'
                            )
                        )
                    ?>
                   <span class="btn"><i class="glyphicon glyphicon-search"></i></span>
                </div>
            </div>
        </div>
        <div class="form-group clearfix">
            <div class="col-lg-6">
                <label><?php echo __('Report Date');?><span class="red-star">*</span></label>
                <div class='input-group date'  id='reportDate'>
                    <?php
                        echo $this->Form->input('report_date',
                            array(
                                'class' => 'form-control',
                                'placeholder' => __('Report Date'),
                                'div' => false,
                                'label' => false,
                                'required' => true,
                                'type' => 'text'
                            
                            )
                        );
                    ?>
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group clearfix">
            <div class="col-lg-12">
                <label><?php echo __('Financial year');?></label>
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-5">
                        <label><?php echo __('From');?></label>
                        <div class='input-group date' id='financialFrom'>
                            <?php
                                echo $this->Form->input('financial_year_from',
                                    array(
                                        'class' => 'form-control',
                                        'placeholder' => __('21/03/2017'),
                                        'div' => false,
                                        'label' => false,
                                        'required' => true,
                                        'type' => 'text',
                                        'id' => 'fromdate'
                                    )
                                );
                            ?>
                            <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <label><?php echo __('To');?></label>
                        <div class='input-group date' id='financialTo'>
                            <?php
                                echo $this->Form->input('financial_year_to',
                                    array(
                                        'class' => 'form-control',
                                        'placeholder' => __('21/03/2018'),
                                        'div' => false,
                                        'label' => false,
                                        'required' => true,
                                        'type' => 'text',
                                        'id' => 'todate'
                                    )
                                );
                            ?>
                            <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <label><?php echo __('Short/long');?></label>
                        <div class="works-in-ie-login">
                            <label>
                            <?php 
                                echo $this->Form->checkbox(
                                    'short_long', 
                                    [
                                        'div' => false,
                                        'label' => false
                                    ]
                                );
                            ?>
                            <i class="faa fa-unchecked unchecked"></i>
                            <i class="faa fa-checked  checked"></i>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group  clearfix">
            <div class="col-lg-3">
                <label><?php echo __('Financial Year');?></label>
                <?php
                    echo $this->Form->input('financial_year',
                        array(
                            'class' => 'form-control',
                            'placeholder' => __('2017'),
                            'div' => false,
                            'label' => false,
                            'required' => true,
                            'type' => 'text',
                            'id' => 'financeYear'
                        )
                    );
                ?>
            </div>
        </div>
        <div class="form-group text-right clearfix">
            <div class="col-lg-8">
                <a type="button" class="btn btn-blue first-step next-step"><?php echo __('Next');?></a>
            </div>
        </div>
    </div>
</div>