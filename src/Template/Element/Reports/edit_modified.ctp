<?php
    echo $this->Form->create($modified,
            [
                    'type' => 'post',
                    'id' => 'edit-modified-form',
                    'class' => 'form-horizontal'
            ]
        );
?>  
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('PlaceHolder Number'); ?></label>
        <div class="col-sm-8">
            <label><?php echo $modified->place_holder_number; ?></label>
        </div>
    </div>          
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('English Name'); ?></label>
        <div class="col-sm-8">
            <?php echo $this->Form->hidden('id');?>
            <?php
                echo $this->Form->input('name',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('English Name'),
                                'div' => false,
                                'label' => false
                            )
                    );
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('English Content'); ?></label>
        <div class="col-sm-8">
            <?php
                echo $this->Form->input('content_eng',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('English Content'),
                                'div' => false,
                                'label' => false,
                                'type' => 'textarea'
                            )
                    );
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('German Name'); ?></label>
        <div class="col-sm-8">
            <?php
                echo $this->Form->input('name_ger',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('English Name'),
                                'div' => false,
                                'label' => false
                            )
                    );
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('German Content'); ?></label>
        <div class="col-sm-8">
            <?php
                echo $this->Form->input('content_ger',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('German Content'),
                                'div' => false,
                                'label' => false,
                                'type' => 'textarea'
                            )
                    );
            ?>
        </div>
    </div>
    <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
        <div class="col-lg-8">
            <?php
                echo $this->Form->button('Submit', array(
                            'class' => 'btn submit-info submit_black',
                            'type' => 'submit'
                            ));
            ?>
            <?php
                echo $this->Form->button('Cancel', array(
                            'class' => 'btn submit-info submit_black',
                            'type' => 'button',
                            'data-dismiss' => 'modal'
                            ));
            ?>
        </div>
    </div>
<?php echo $this->Form->end(); ?>