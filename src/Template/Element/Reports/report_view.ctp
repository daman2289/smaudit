<?php
	use Cake\Core\Configure;

 if(!empty($report)) {
?>

<div>
	<div class="stepwizard">
	    <div class="stepwizard-row">
	        <div class="stepwizard-step col-md-1">
	            <button type="button" class="btn btn-primary btn-square form-button-first">1</button>
	        </div>
	        <div class="col-md-11">
	        	<?php 
	        		echo $this->element('Reports/EditReport/first_step');
	        	?>
	        	<div class="first-form-view">
	        		<div class="col-md-4">
		        		<dl>
						  	<dt>Company</dt>
						  	<dd>
						  		<?= 
						  			($report->has('organization_company')) ? 
	                                    __($report->organization_company->company_name) : 
	                                    'N/A' ;
	                            ?>
	                        </dd>
						</dl>
		        	</div>
		        	<div class="col-md-4">
		        		<dl >
						  <dt>Report Date</dt>
						  <dd><?php echo date('d/m/Y',strtotime($report->report_date) ) ?></dd>
						</dl>
		        	</div>
		        	<div class="col-md-4">
		        		<dl>
						  <dt>Financial year</dt>
						  <dd>From :<?php echo date('d/m/Y',strtotime($report->financial_year_from)) ?></dd>
						  <dd>To : <?php echo date('d/m/Y',strtotime($report->financial_year_to)) ?></dd>
						</dl>
		        	</div>
		        	<div class="col-md-4">
		        		<dl>
						  <dt>Short/Long</dt>
						  <dd>
						  		<?=	($report->short_long == 1) ? 
						  			'<i class="glyphicon glyphicon-ok-circle text-success"></i>':
						  			'<i class="glyphicon glyphicon-remove-circle text-danger"></i>'	
						  		?>
						  </dd>
						</dl>
		        	</div>
	        	</div>
	        </div>
	    </div>
	    <div class="stepwizard-row">   
	        <div class="stepwizard-step col-md-1">
	            <button type="button" class="btn btn-primary btn-square form-button-second">2</button>
	        </div>
	        <div class="col-md-11">
	        	<?php 
	        		echo $this->element('Reports/EditReport/second_step');
	        	?>
	        	<div class="second-form-view">
	        	<div class="col-md-4">
	        		<dl>
					  	<dt>Is First Year Audit?</dt>
					  	<dd>
					  		<?=	
					  			($report->first_year_audit) ? 
					  			'<i class="glyphicon glyphicon-ok-circle text-success"></i>':
					  			'<i class="glyphicon glyphicon-remove-circle text-danger"></i>'	
					  		?>
					  	</dd>
					</dl>
	        	</div>
	        	<?php 
	        		if($report->first_year_audit) {
	        	?>
		        	<div class="col-md-4">
		        		<dl >
						  	<dt>Opting Out</dt>
						  	<dd>
						  		<?=	
						  			($report->opting_out) ? 
						  			'<i class="glyphicon glyphicon-ok-circle text-success"></i>':
						  			'<i class="glyphicon glyphicon-remove-circle text-danger"></i>'	
					  			?>
					  		</dd>
						</dl>
		        	</div>
		        	<div class="col-md-4">
		        		<dl>
						  	<dt>Opinion Types</dt>
						  	<dd>
						  		<?= ($report->has('opinion_type')) ? 
                                    __($report->opinion_type->name) : 
                                    'N/A' ;
                                ?>
                            </dd>
						  <dd></dd>
						</dl>
		        	</div>
		        	<?php 
		        		if(!$report->opting_out) {
        			?>
			        	<div class="col-md-6">
			        		<dl>
							  	<dt>Date Prior Audit Report</dt>
							  	<dd>
							  	<?=
							  		(!empty($report->date_prior_audit_report)) ?
							  		date('d/m/Y',strtotime($report->date_prior_audit_report)) :
							  		'N/A'
							  	?>
							  	</dd>
							</dl>
			        	</div>
			        <?php  } ?>

		        <?php } ?>
	        </div>

	        </div>
	    </div>
	    <div class="stepwizard-row">   
	        <div class="stepwizard-step col-md-1">
	            <button type="button" class="btn btn-primary btn-square form-button-third" >3</button>
	        </div>
	        <div class="col-md-11">
	        	<?php 
	        		echo $this->element('Reports/EditReport/third_step');
	        	?>
	        	<div class="third-form-view">
	        	<div class= 'row'>
		        	<div class="col-md-4">
		        		<dl>
						  	<dt>Assistance in book Keepings?</dt>
						  	<dd>
						  		<?=	
						  			($report->assistance_book_keeping == 1) ? 
						  			'<i class="glyphicon glyphicon-ok-circle text-success"></i>':
						  			'<i class="glyphicon glyphicon-remove-circle text-danger"></i>'	
					  			?>
						  	</dd>
						</dl>
		        	</div>
		        	<div class="col-md-4">
		        		<dl >
						  	<dt>Appropriation of Available Earnings?</dt>
						  	<dd>
						  		<?=	
						  			($report->appropriation_available_earning) ? 
						  			'<i class="glyphicon glyphicon-ok-circle text-success"></i>':
						  			'<i class="glyphicon glyphicon-remove-circle text-danger"></i>'	
					  			?>
						  	</dd>
						</dl>
		        	</div>
	        	
		        	<div class="col-md-4">
		        		<dl>
						  	<dt>Accounting Framework</dt>
						  	<dd>
						  		<?= ($report->has('accounting_framework_type')) ? 
                                    __($report->accounting_framework_type->name) : 
                                    'N/A' ;
                                ?>
						  	</dd>
						</dl>
		        	</div>

	        	</div>
	        	<div class= 'row'>
		        	<div class="col-md-4">
		        		<dl>
						  	<dt>Was Audit Terminated Later Than report Date?</dt>
						  	<dd>
						  		<?=	
						  			($report->audit_terminated_later_than_report) ? 
						  			'<i class="glyphicon glyphicon-ok-circle text-success"></i>':
						  			'<i class="glyphicon glyphicon-remove-circle text-danger"></i>'	
					  			?>
						  	</dd>
						</dl>
		        	</div>
		        	<?php
		        		if($report->audit_terminated_later_than_report) {
		        	?>	
		        	
			        	<div class="col-md-4">
			        		<dl>
							  	<dt>When Was Audit Terminated?</dt>
							  	<dd>
							  		<?=
								  		(!empty($report->audit_terminated_date)) ?
								  		date('d/m/Y',strtotime($report->audit_terminated_date)) :
								  		'N/A'
								  	?>
							  	</dd>
							</dl>
			        	</div>
		        	<?php  } ?>
	        		<div class="col-md-4">
		        		<dl>
						  	<dt>Emphasis of matter?</dt>
						  	<dd>
						  		<?php 
		                            array_map(
		                                function ($n){
		                                    if(!empty($n)) {
		                                        echo __(Configure::read('Emphasis.'.$n)). "<br>";
		                                    }else { echo 'N/A';}
		                                }, 
		                                explode(',', $report->emphasis_of_matter_ids)
		                            );
		                        ?>
						  	</dd>
						</dl>
		        	</div>
	        	</div>
	        	<div class= 'row'>
	        		<?php
	        			if(!empty($report->emphasis_of_matter_ids)) {
	        				$emphasisArray = explode(',', $report->emphasis_of_matter_ids);
	        		?>
			        		
			        	<?php 
			        		if(in_array('1', $emphasisArray)) {
			        	?>		
				        	<div class="col-md-4">
				        		<dl>
								  <dt>Subordination Amount</dt>
								  <dd><?= $report->subordination_amount ?></dd>
								  <dd></dd>
								</dl>
				        	</div>
			        	<?php }  ?>

			        	<?php 
			        		if(in_array('2', $emphasisArray) || in_array('3', $emphasisArray)) {
			        	?>		
				        	<div class="col-md-4">
				        		<dl>
								  	<dt>Going Concern Note No</dt>
								  	<dd>
								  		<?= $report->going_concern_note_no ?>
								  	</dd>
						  			
								</dl>
				        	</div>
			        	<?php }  ?>

			        	<?php 
			        		if(in_array('7', $emphasisArray) ) {
			        	?>		
				        	<div class="col-md-4">
				        		<dl>
								  	<dt>Amount of Shareholders Loan</dt>
								  	<dd><?= $report->amount_shareholder_loan ?></dd>
								</dl>
				        	</div>
			        	<?php }  ?>
			        	
			        	
			        <?php  } ?>
		        </div>
		        <div class= 'row'>
		        	<div class="col-md-4">
		        		<dl>
						  	<dt>Modified Opinion?</dt>
						  	<dd>
						  		<?= ($report->has('modified_opinion')) ? 
                                    __($report->modified_opinion->name) : 
                                    'N/A' ;
                                ?>
						  	</dd>
						  	
						</dl>
		        	</div>
		        	<div class="col-md-4">
		        		<dl>
						  	<dt>Basis for Modification</dt>
						  	<dd>
						  		<?php
						  			$basicsForModification = (!empty($report->basis_for_modification)) ? 
	                                    __($report->basis_for_modification) : 
	                                    'N/A' ;
						  		?>
						  		<span title="<?= $basicsForModification ?>">
							  		<?= 
							  			substr($basicsForModification,0,20).'....'
	                                ?>
						  		</span>
						  	</dd>
						</dl>
		        	</div>
	        	</div>
	        </div>
	        </div>
	    </div>
	    <div class="stepwizard-row">   
	        <div class="stepwizard-step col-md-1">
	            <button type="button" class="btn btn-primary btn-square form-button-fourth" >4</button>
	        </div>
	        <div class="col-md-11">	
	        	<?php 
	        		echo $this->element('Reports/EditReport/fourth_step');
	        	?>        	
		        <div class="fourth-form-view">
		        	<?php
		        		if(!empty($firstSignature)) {
        			?>
		        	<div class="col-md-4">
		        		<dl>
						  	<dt>First Signature</dt>
						  	<dd>
						  		<?= 
						  			(!empty($firstSignature->full_name)) ? 
                                    __($firstSignature->full_name) : 
                                    'N/A' ;
                                ?>
                                <?=	
						  			($report->auditor_incharge == 0) ? 
						  			'<i class="glyphicon glyphicon-ok-circle text-success"></i>':
						  			'<i class="glyphicon glyphicon-remove-circle text-danger"></i>'	
					  			?>

						  	</dd>
						</dl>
		        	</div>
        			<?php
		        		}
		        	?>

		        	<?php
		        		if(!empty($secondSignature)) {
        			?>
		        	<div class="col-md-4">
		        		<dl>
						  	<dt>Second Signature</dt>
						  	<dd>
						  		<?= 
						  			(!empty($secondSignature->full_name)) ? 
                                    __($secondSignature->full_name) : 
                                    'N/A' ;
                                ?>
                                 <?=	
						  			($report->auditor_incharge == 1) ? 
						  			'<i class="glyphicon glyphicon-ok-circle text-success"></i>':
						  			'<i class="glyphicon glyphicon-remove-circle text-danger"></i>'	
					  			?>
						  	</dd>
						</dl>
		        	</div>
        			<?php
		        		}
		        	?>
	        	</div>
	        </div>
	    </div>
	</div>
</div>
<?php } ?>
