<div class="tab-pane" role="tabpanel" id="step3">
    <div class="col-xs-12">
        <div class="form-group clearfix">
            <div class="col-sm-12">
                <label><?php echo __('Assistance in book Keepings?');?></label>
            </div>
            <div class="works-in-ie-login">
                <label>
                <?php echo $this->Form->checkbox('assistance_book_keeping', 
                    [
                    // 'hiddenField' => false,
                    'templates' => [
                       'inputContainer' => '{{content}}',
                       'inputContainerError' => '{{content}}{{error}}'
                     ],
                     'label' =>false
                    ]
                );?>
                <i class="faa fa-unchecked unchecked"></i>
                <i class="faa fa-checked  checked"></i>
                </label>
            </div>
        </div>
        <div class="form-group clearfix">
            <div class="col-sm-12">
                <label><?php echo __('Appropriation of Available Earnings?');?></label>
            </div>
            <div class="works-in-ie-login">
                <label>
                <?php echo $this->Form->checkbox('appropriation_available_earning', 
                    [
                    // 'hiddenField' => false,
                    'templates' => [
                       'inputContainer' => '{{content}}',
                       'inputContainerError' => '{{content}}{{error}}'
                     ],
                     'label' =>false
                    ]
                );?>
                <i class="faa fa-unchecked unchecked"></i>
                <i class="faa fa-checked  checked"></i>
                </label>
            </div>
        </div>
        <div class="form-group clearfix">
            <div class="col-lg-6">
                <label><?php echo __('Accounting Framework');?></label>
                <?php
                    echo $this->Form->input('accounting_framework_type_id',
                            array(  'default' => 4,
                                    'class' => 'form-control',
                                    'placeholder' => __(''),
                                    'div' => false,
                                    'label' => false,
                                    'required' => true,
                                    'options' => $accounting
                                
                                )
                        );
                ?>
            </div>
        </div>
        <div class="form-group audi-terminated clearfix">
            <div class="col-sm-12">
                <label><?php echo __('Was audit terminated before report date?');?></label>
            </div>
            <div class="works-in-ie-signup">
                <label>
                <input checked="cehcked" type="radio" value="1" name="audit_terminated_later_than_report">
                <i class="fa-radio fa-unchecked unchecked"></i>
                <i class="fa-radio fa-checked  checked"></i>
                <span class="text-checkbox">Yes</span>
                </label>
                <label>
                <input type="radio" name="audit_terminated_later_than_report" checked value="0">
                <i class="fa-radio fa-unchecked unchecked"></i>
                <i class="fa-radio fa-checked checked"></i>
                <span class="text-checkbox">No</span>
                </label>
            </div>
        </div>
        <div class="form-group clearfix terminated" style="display:none">
            <div class="col-lg-6">
                <label><?php echo __('When Was Audit Terminated?');?></label>
                <div class='input-group date' id='auditReports'>
                    <?php
                    echo $this->Form->input('audit_terminated_date',
                            array(
                                'class' => 'form-control',
                                'placeholder' => __(''),
                                'div' => false,
                                'label' => false,
                                'required' => true,
                                'type' => 'text',
                                'disabled'
                            )
                        );
                    ?>
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group clearfix">
            <div class="col-lg-6">
                <label><?php echo __('Emphasis of matter?');?></label>
                <div>
                    <?php
                        echo $this->Form->input('emphasis_of_matter_ids',
                            array(
                                'class' => 'form-control',
                                'placeholder' => __(''),
                                'div' => false,
                                'label' => false,
                                'required' => false,
                                'options' => $emphasis,
                                'multiple',
                                'id' => "emphasis",
                                'name'  => 'emphasis_of_matter_ids[]'
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group clearfix">
            <div class="col-lg-6">
                <label><?php echo __('References (Breach of Law)');?></label>
                <div>
                    <?php
                        echo $this->Form->input('reference_ids',
                            array(
                                'class' => 'form-control',
                                'placeholder' => __(''),
                                'div' => false,
                                'label' => false,
                                'required' => false,
                                'options' => $reference,
                                'multiple',
                                'id' => "references",
                                'name'  => 'reference_ids[]'
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group Subordination clearfix hide">
            <div class="col-lg-6">
                <label><?php echo __('Subordination Amount');?></label>
                <?php
                    echo $this->Form->input('subordination_amount',
                            array(  
                                'class' => 'form-control',
                                'placeholder' => __('CHF 578,542'),
                                'div' => false,
                                'label' => false,
                                'required' => true,
                                'type' => 'text'                                
                            )
                        );
                ?>
            </div>
        </div>
         <div class="form-group dividend clearfix hide">
            <div class="col-lg-6">
                <label><?php echo __('Amount Dividend');?></label>
                <?php
                    echo $this->Form->input('amount_dividend',
                            array(  
                                'class' => 'form-control',
                                'placeholder' => __('CHF 578,542'),
                                'div' => false,
                                'label' => false,
                                'required' => true,
                                'type' => 'text'
                            )
                        );
                ?>
            </div>
        </div>
        <div class="form-group Concern clearfix hide">
            <div class="col-lg-6">
                <label><?php echo __('Going Concern Note No');?></label>
                <?php
                    echo $this->Form->input('going_concern_note_no',
                            array(
                                    'class' => 'form-control',
                                    'placeholder' => __('8'),
                                    'div' => false,
                                    'label' => false,
                                    'required' => true,
                                    'type' => 'text'
                                
                                )
                        );
                ?>
            </div>
        </div>
        <div class="form-group Shareholders clearfix hide">
            <div class="col-lg-6">
                <label><?php echo __('Amount of Shareholders Loan');?></label>
                <?php
                    echo $this->Form->input('amount_shareholder_loan',
                            array(
                                    'class' => 'form-control',
                                    'placeholder' => __("999'999"),
                                    'div' => false,
                                    'label' => false,
                                    'required' => true,
                                    'type' => 'text'
                                
                                )
                        );
                ?>
            </div>
        </div>
        <div class="form-group clearfix">
            <div class="col-lg-6">
                <label><?php echo __('Modified Opinion?');?></label>
                <?php
                    echo $this->Form->input('modified_opinion_id',
                            array(
                                'default' => 3,
                                'class' => 'form-control',
                                'placeholder' => __(''),
                                'div' => false,
                                'label' => false,
                                'required' => true,
                                'options' => $modified                            
                            )
                        );
                ?>
            </div>
        </div>
        <div class="form-group clearfix modifaction hide">
            <div class="col-lg-6">
                <label><?php echo __('Basis for Modification'); ?></label>
                <?php
                    echo $this->Form->input('basis_for_modification',
                        array(
                            'class' => 'form-control',
                            'placeholder' => __('With regard to valuation and disclosure we note the following: Our limited statutory examination revealed that litigation is pending against you for breach of patent rights and that no provisions have been recognised regarding this matter. At present, it is not possible to predict whether, and to what extent, you will have to pay for any damages. As a result, a material uncertainty exists in relation to the measurement of provisions. There are no respective disclosures made in the notes to the financial statements'),
                            'div' => false,
                            'label' => false,
                            'required' => true,
                            'type' => 'textarea',
                            'rows' => 5                                
                        )
                    );
                ?>
            </div>
        </div>
        <div class="form-group clearfix text-right">
            <div class="col-lg-8">
                <button type="button" class="btn btn-default-step prev-step"><?php echo __('Back');?></button>
                <button type="button" class="btn btn-blue btn-info-full next-step"><?php echo __('Next');?></button>
            </div>
        </div>
    </div>
</div>