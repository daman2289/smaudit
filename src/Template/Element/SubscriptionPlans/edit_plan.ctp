
<?php
    echo $this->Form->create($plan,
        [
            'id' => 'edit-plan-form',
            'class' => 'form-horizontal'
        ]
    );
?>            
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Name'); ?></label>
        <div class="col-sm-8">
        <?php echo $this->Form->hidden('id');?>
            <?php
                echo $this->Form->input('title',
                    array(
                        'class' => 'form-control',
                        'placeholder' => __('Plan'),
                        'div' => false,
                        'label' => false
                    )
                );
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Trial Days'); ?></label>
        <div class="col-sm-8">
        <?php echo $this->Form->hidden('id');?>
            <?php
                echo $this->Form->input('trial_days',
                    array(
                        'class' => 'form-control',
                        'placeholder' => __('Price'),
                        'div' => false,
                        'label' => false
                    )
                );
            ?>
        </div>
    </div>
    <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
        <div class="col-lg-8">
            <?php
                echo $this->Form->button('Submit', array(
                            'class' => 'btn submit-info submit_black',
                            'type' => 'submit'
                            ));
            ?>
            <?php
                echo $this->Form->button('Cancel', array(
                            'class' => 'btn submit-info submit_black',
                            'type' => 'button',
                            'data-dismiss' => 'modal'
                            ));
            ?>
        </div>
    </div>
<?php echo $this->Form->end(); ?>
