<?php
    if(!empty($subscriptionPlan->toArray())) {
?>
<div class="col-sm-12">
    <?php
        foreach ($subscriptionPlan->plan_features as $key => $value) {
    ?>
        <div class="row form-group">
            <div class="col-sm-6">
                <label><?= __($value->title) ?></label>
            </div>
            <div class="col-sm-6 text-right">
                <span class="switch-toggle custom-switch">
                    <?php echo $this->Form->input('',
                            array(
                            "data-url" => $this->Url->build(
                                array(
                                    'controller' =>  'subscription-plans',
                                    'action' => 'update-plan-features',
                                    base64_encode($subscriptionPlan->id),
                                    base64_encode($value->id),
                                    (!empty($value->_joinData->is_available) && $value->_joinData->is_available == 1) ? base64_encode(0) : base64_encode(1)
                                ),
                                true
                            ),
                            "id"=> "testt".$key,
                            "class" =>  "switch_custom prmission_switch",
                            "type" => "checkbox",
                            "hidden" => "hidden",
                            "label" => false,
                            'templates' => [
                               'inputContainer' => '{{content}}',
                               'inputContainerError' => '{{content}}{{error}}'
                             ],
                            "value"=>$value->id,
                            ($value->_joinData->is_available == 1) ? 'checked' : ''
                        )
                    )
                  ?>
                    <label for="testt<?php echo $key ?>" class="switch"></label>
                </span>  
            </div>
        </div>
    <?php 
            }
        }  else {
            echo "<h3 class='text-center'>You don't have permission.</h3>";
        }
    ?>
</div>


