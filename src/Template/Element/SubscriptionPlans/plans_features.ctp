<div class="block-container">
	<?php 
		foreach ($features as $index => $feature) {
			$available = ($feature->_joinData->is_available) ? 'glyphicon-ok':'glyphicon-remove';
			$color = ($feature->_joinData->is_available) ? '#45BA76':'#FF0000';

	?>
		<p>
			<span class="glyphicon <?= $available ?> " data-unicode="e013" style="color:<?= $color ?>;"> </span>
			<?= __($feature->title) ?>
		</p>
	<?php
		}
	?>	     
</div>