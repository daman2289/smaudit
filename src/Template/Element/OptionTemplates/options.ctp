<template id="option-multiselect">
	<div class="row">
		<span onclick="removeOption(this)" class="btn btn-sm btn-danger remove hide">
			Remove
		</span>
		<div class="col-md-5">				
			<div class="form-group">
				<input type="text" name="" class='multiselect-option-label form-control', placeholder="Set Label for this option" required>
			</div>
		</div>		
		<div class="col-md-5">		
			<div class="form-group">	
				<input type="text" name="" class='multiselect-option-content-en form-control' placeholder="English Content to replace this option" required>
			</div>	
		</div>
		<div class="col-md-5">		
			<div class="form-group">	
				<input type="text" name="" class='multiselect-option-content-er form-control' placeholder="German Content to replace this option">
			</div>	
		</div>
		<div class="col-md-5">		
			<div class="form-group">	
				<input type="text" name="" class='multiselect-option-content-fr form-control' placeholder="French Content to replace this option">
			</div>	
		</div>
	</div>
</template>

<template id="option-radio">
	<div class="row">		
		<div class="col-md-2">
			<div class="form-group">		
				<input type="radio" name="" class="form-control" checked>
			</div>	
			<span onclick="removeOption(this)" class="btn btn-sm btn-danger remove hide">
				Remove
			</span>			
		</div>		
		<div class="col-md-5">				
			<div class="form-group">
				<input type="text" name="" class='radio-option-label form-control', placeholder="Set Label for this option" required>
			</div>
		</div>		
		<div class="col-md-5">		
			<div class="form-group">	
				<input type="text" name="" class='radio-option-content-en form-control' placeholder="English Content to replace this option" required>
			</div>	
		</div>
		<div class="col-md-5">		
			<div class="form-group">	
				<input type="text" name="" class='radio-option-content-er form-control' placeholder="German Content to replace this option">
			</div>	
		</div>
		<div class="col-md-5">		
			<div class="form-group">	
				<input type="text" name="" class='radio-option-content-fr form-control' placeholder="French Content to replace this option">
			</div>	
		</div>
	</div>
</template>

<template id="option-select">
	<div class="row">
		<span onclick="removeOption(this)" class="btn btn-sm btn-danger remove hide">
			Remove
		</span>	
		<div class="col-md-5">				
			<div class="form-group">
				<input type="text" name="" class='select-option-label form-control', placeholder="Set Label for this option" required>
			</div>
		</div>		
		<div class="col-md-5">		
			<div class="form-group">	
				<input type="text" name="" class='select-option-content-en form-control' placeholder="English Content to replace this option" required>
			</div>	
		</div>
		<div class="col-md-5">		
			<div class="form-group">	
				<input type="text" name="" class='select-option-content-er form-control' placeholder="German Content to replace this option">
			</div>	
		</div>
		<div class="col-md-5">		
			<div class="form-group">	
				<input type="text" name="" class='select-option-content-fr form-control' placeholder="French Content to replace this option">
			</div>	
		</div>
	</div>
</template>

<template id='option-checkbox'>
	<div class="row">
		<input type="hidden" name="[0][new_question_options][0][name_en]" class='checkbox-option-label form-control', placeholder="Set Label for this option" value="yes">
		<div class="col-md-5">		
			<div class="form-group">	
				<input type="text" name="[0][new_question_options][0][content_en]" class='checkbox-option-content-en form-control' placeholder="English Content to replace when checked" required>
			</div>	
		</div>
		<div class="col-md-5">		
			<div class="form-group">	
				<input type="text" name="[0][new_question_options][0][content_er]" class='checkbox-option-content-er form-control' placeholder="German Content to replace when checked">
			</div>	
		</div>
		<div class="col-md-5">		
			<div class="form-group">	
				<input type="text" name="[0][new_question_options][0][content_fr]" class='checkbox-option-content-fr form-control' placeholder="French Content to replace when checked">
			</div>	
		</div>
	</div>
	<div class="row">
		<input type="hidden" name="[0][new_question_options][1][name_en]" class='checkbox-option-label form-control', placeholder="Set Label for this option" value= 'no'>
		<div class="col-md-5">		
			<div class="form-group">	
				<input type="text" name="[0][new_question_options][1][content_en]" class='checkbox-option-content-en form-control' placeholder="English Content to replace when not checked" required>
			</div>	
		</div>
		<div class="col-md-5">		
			<div class="form-group">	
				<input type="text" name="[0][new_question_options][1][content_er]" class='checkbox-option-content-er form-control' placeholder="German Content to replace when not checked">
			</div>	
		</div>
		<div class="col-md-5">		
			<div class="form-group">	
				<input type="text" name="[0][new_question_options][0][content_fr]" class='checkbox-option-content-fr form-control' placeholder="French Content to replace when not checked">
			</div>	
		</div>
	</div>
</template>
<template id='signature-subquestion'>
	<div class="row">
		<div class="col-md-5">		
			<div class="form-group">	
				<input type="checkbox" name="auditor_in_charge" class='checkbox-option-content-en ' placeholder="Do you want to add Auditor in charge subquestion" > Add Auditor in charge subquestion
			</div>	
		</div>
	</div>
</template>