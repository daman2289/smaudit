
<?php
    echo $this->Form->hidden("new_question_id",['name' => $key."[new_question_id]", 'value' => $question['id']]);
    echo $this->Form->hidden("new_placeholder_id",['name' => $key."[new_placeholder_id]", 'value' => $question['new_placeholder_id']]);
?>
<div class="input-group form-group dateInput"> 
    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>

    <?php
        // echo $this->Form->label($question['question_en']);
        echo $this->Form->input('registered', [
            'name' => $key."[answer]",
            'class' => 'form-control input-lg ',
            'aria-describedby'  => "basic-addon1", 
            'placeholder' => $question['question_en'],
            'label' => false,
            'required'
        ]);
    ?>
</div>