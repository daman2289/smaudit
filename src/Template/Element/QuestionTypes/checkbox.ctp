
<?php

	$options = [];
	
	if(!empty($question['new_question_options'])) {
		foreach ($question['new_question_options'] as $k => $value) {
			$options[$value['id']] = $value['name_en'];
		}
	}
?>
	<div class="form-group">		
		<?php
			echo $this->Form->hidden("new_question_id",['name' => $key."[new_question_id]", 'value' => $question['id']]);
			echo $this->Form->hidden("new_placeholder_id",['name' => $key."[new_placeholder_id]", 'value' => $question['new_placeholder_id']]);
			echo $this->Form->label($question['question_en']);
			echo $this->Form->checkbox('option_id', [
		    	'value' => array_search('yes', $options),
		    	'hiddenField' => array_search('no', $options),
		    	'name' => $key."[new_question_option_id]",
				'onchange'=> ($is_parent) ? "checkboxChild(this)" : "void(0)"
			]); 
		?>
	</div>
	<style type="text/css">
		.question-block label {
			display: flex !important;
		}
	</style>