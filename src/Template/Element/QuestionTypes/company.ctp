<div class="form-group">	
	<?php			
		echo $this->Form->hidden("new_question_id",['name' => $key."[new_question_id]", 'value' => $question['id']]);
		echo $this->Form->hidden("new_placeholder_id",['name' => $key."[new_placeholder_id]", 'value' => $question['new_placeholder_id']]);
		// echo $this->Form->hidden(
  //           'organization_company_id',
  //           array(
  //               'id' => 'company-id',                
		// 		'name' => $key."[answer]",
		// 		'required'
  //           )
  //       );
		echo $this->Form->label(__($question['question_en']));
		echo $this->Form->select('organization_company_id', 
			$companies,
			[
				'label' => $question['question_en'],
				'id' => 'company-id',                
				'name' => $key."[answer]",
				'class' => 'form-control input-lg tag-tagname',
				'empty'	=> 'Select Company',
				'required',
			// 'required',
		]);

		// echo $this->Form->input('text', [
		// 	'label' => $question['question_en'],
		// 	'class' => 'form-control input-lg tag-tagname',
		// 	'name'	=> '',
		// 	// 'required',
		// ]);
	?>
</div>