<div class="form-group">	
	<?php			
		echo $this->Form->hidden("new_question_id",['name' => $key."[new_question_id]", 'value' => $question['id']]);
		echo $this->Form->hidden("new_placeholder_id",['name' => $key."[new_placeholder_id]", 'value' => $question['new_placeholder_id']]);
		echo $this->Form->input('text', [
			'name' => $key."[answer]",
			'label' => $question['question_en'],
			'class' => 'form-control input-lg',
			'required'
		]);
	?>
</div>