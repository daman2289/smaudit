<?php
    if(!empty($userPermissions) && !empty($userPermissions->toArray())) {
?>
<div class="col-sm-12">
    <?php
        foreach ($userPermissions as $key => $value) {
    ?>
        <div class="row form-group">
            <div class="col-sm-6">
                <label><?= __($value->permission_area->title) ?></label>
            </div>
            <div class="col-sm-6 text-right">
                <span class="switch-toggle custom-switch">
                    <?php echo $this->Form->input('',
                            array(
                            "data-url" => $this->Url->build(
                                array(
                                    'controller' =>  'Subsidiaries',
                                    'action' => 'userPermissions',
                                    base64_encode($value->id)
                                ),
                                true
                            ),
                            "id"=> "test".$key,
                            "class" =>  "switch_custom prmission_switch",
                            "type" => "checkbox",
                            "hidden" => "hidden",
                            "label" => false,
                            'templates' => [
                               'inputContainer' => '{{content}}',
                               'inputContainerError' => '{{content}}{{error}}'
                             ],
                            "value"=>$value->id,
                            ($value->is_allowed == 1) ? 'checked' : ''
                        )
                    )
                  ?>
                    <label for="test<?php echo $key ?>" class="switch"></label>
                </span>  
            </div>
        </div>
    <?php 
            }
        }  else {
            echo "<h3 class='text-center'>You don't have permission.</h3>";
        }
    ?>
</div>
