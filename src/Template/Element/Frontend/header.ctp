<?php
    use Cake\Filesystem\File; 
    use Cake\Core\Configure; 
?>
<div class="top-header">
    <div class="container">
        <div class="col-sm-4 padding-left top-logo">
            <a href="/" class="logo">
            <?php echo $this->Html->image('logo.png',array('class' => 'img-responsive'));?>
            </a>
        </div>
        <div class="col-sm-8 text-right">
            <div class="col-sm-6 col-lg-6 langauge-top">
                <ul class="list-inline">

                    <li>
                        <?php echo $this->Html->link(
                                   $this->Html->image('english.png',array('alt' => '')).__(' EN'),
                                    '/Languages/changeLanguage/en',
                                    [
                                        'escape' => false
                                    ]
                                );
                            ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link(
                                   $this->Html->image('flag-germany.png',array('alt' => '')).__(' DE'),
                                    '/Languages/changeLanguage/ar',
                                    [
                                        'escape' => false
                                    ]
                                );
                            ?>
                    </li>
                   
                </ul>
            </div>
            <div class="col-sm-6 col-lg-6  home-top-menu">
            <?php 
                    $user =  htmlspecialchars($this->request->session()->read('Auth.User.organization.name'));
                    if(!empty($user)) {
            ?>
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="hidden-xs hidden-sm">
                    <?php echo htmlspecialchars(trim($this->request->session()->read('Auth.User.organization.name')));?>
                    </span>
                   <?php 
                        $profile = $this->request->session()->read('Auth.User.profile_pic'); 
                        $profilePicPath = 'images.jpeg';
                        if(!empty($profile)) {
                            $profilePic = new File(Configure::read('ProfilePicPathToCheck') . $this->request->session()->read('Auth.User.profile_pic_dir') . DS . 'square_' . $this->request->session()->read('Auth.User.profile_pic'), true, 0644);
                            if ($profilePic->exists()) {
                                $profilePicPath = Configure::read('ProfilePicPath') . $this->request->session()->read('Auth.User.profile_pic_dir') . DS . 'square_' . $this->request->session()->read('Auth.User.profile_pic');
                            }
                        }
                        echo $this->Html->image($profilePicPath, [
                                    'class' => 'user-image', 'alt' => 'User Image'
                                ]);
                    ?>
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                    <ul aria-labelledby="drop3" class="dropdown-menu">
                        <!-- <li>
                            <?php echo $this->Html->link(
                                '<i class="fa fa-key" aria-hidden="true"></i>'. __('Change Password'),
                                '#',
                                array(
                                    'escape' => false,
                                    'data-toggle' => 'modal',
                                    'data-target' => '#changePasswordModal'
                                    )
                                );
                            ?>
                        </li> -->
                       <!--  <li>
                            <a href="#"><i class="fa fa-ticket" aria-hidden="true"></i> Support</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-wrench" aria-hidden="true"></i> Improvement</a>
                        </li> -->
                         <li>
                           <?php echo $this->Html->link(
                                '<i class="fa fa-sign-out" aria-hidden="true"></i>'.
                                __('Dashboard'),
                                [
                                    'controller' => 'Reports', 
                                    'action' => 'reportListing'
                                ],
                                array(
                                    'escape' => false
                                )
                            );
                            ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                '<i class="fa fa-sign-out" aria-hidden="true"></i>'.
                                    __('Logout'),
                                '/Users/logout',
                                array(
                                    'escape' => false
                                )
                            );
                            ?>
                        </li>
                    </ul>
                </li>
                </ul>
                <?php } else { ?>
                 
                <?php
                    echo $this->Html->link(
                            __('Login'),
                            array(
                                'controller' => 'users',
                                'action'    => 'login'
                            ),
                            [
                                'escape' => false,
                                'class'=> 'btn btn-blue'
                            ]
                        );
                ?>
                <?php
                    echo $this->Html->link(
                            __('Register Now'),
                            '/Users/signup',
                            [
                                'escape' => false,
                                'class'=> 'btn btn-orange'
                            ]
                        );
                ?>
                <?php } ?> 
            </div>
        </div>
    </div>
</div>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav ">
                <li class="page-scroll">
                    <?php
                        echo $this->Html->link(
                                __('Reports'),
                                "/#specialize-section"
                            );
                    ?>
                </li>
                <li class="page-scroll">
                    <a href="/#pricing"><?= __('Pricing') ?></a>
                </li>
                <li class="page-scroll">
                    <a href="/#work-in-progress"><?= __('RoadMap') ?></a>
                </li>
                <li class="page-scroll">
                    <a href="/#help"><?= __('Help') ?></a>
                </li>
                <li class="page-scroll">
                    <a href="/#contact"><?= __('Contact') ?></a>
                </li>
            </ul>
        </div>
    </div>
</nav>
