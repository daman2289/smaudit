<div class="modal Educational-info fade" id="improvemnet" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Improvement'); ?></h4>
            </div>
            <div class="modal-body">
                <?php
                    echo $this->Form->create('User', [
                                'url' => [
                                    'controller' =>'Users',
                                    'action' => 'support'
                                ],
                                'type' => 'post',
                                'id' => 'support-form'
                        ]);
                ?>   
                    <div class="form-group">
                        <label class="control-label"><?php echo __('Message'); ?><span class="required_ast">*</span></label>
                        <?php
                          echo $this->Form->input('message', array(
                                        'class' => 'form-control',
                                        'label' => false,
                                        'div' => false,
                                        'placeholder' => 'Message',
                                        'type' => 'textarea'
                                        ));
                        ?>
                    </div>
                    <div class="form-group clearfix"> <label  class="col-sm-4 control-label"></label>
                        <div class="col-lg-8">
                            <?php
                                echo $this->Form->button(__('Send'),[
                                        'class' => 'btn submit-info submit_black',
                                        'type' => 'submit'
                                    ]);
                            ?>
                            <?php
                                echo $this->Form->button(__('Cancel'),[
                                        'class' => 'btn submit-info submit_black',
                                        'type' => 'button',
                                        'data-dismiss' => 'modal'
                                    ]);
                            ?>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->script('Administration/support', ['block' => 'scriptBottom']); ?>