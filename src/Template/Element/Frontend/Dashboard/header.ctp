<?php
    use Cake\Filesystem\File;
    use Cake\Core\Configure;
?>
<header class="main-header">
    <!-- Logo -->
   

    <a href="/" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
        <?php $this->Html->image('logo-mini');?>
        </span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
        <?php echo $this->Html->image('logo.png');?>
        </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top nav-custom" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle hidden-lg" data-toggle="offcanvas" role="button">
            <span class="glyphicon glyphicon-menu-hamburger"></span>
        </a>
        <div class="col-sm-3 col-lg-3 hidden-xs">
            <div class="langauge-top">
                <ul class="list-inline">
                    <li>
                    <?php echo $this->Html->link(
                        $this->Html->image('english.png'). __(' EN'),
                        '/Languages/changeLanguage/en',
                        array(
                            'escape' => false
                            )
                    );?>
                    </li>

                    <li>
                         <?php echo $this->Html->link(
                        $this->Html->image('flag-germany.png'). __(' DE'),
                        '/Languages/changeLanguage/ar',
                        array(
                            'escape' => false
                            )
                        );?>
                    </li>
                </ul>
            </div>
        </div>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="hidden-xs hidden-sm">
                    <?php echo htmlspecialchars($this->request->session()->read('Auth.User.organization.name'));?>
                    </span>
                   <?php 
                        $profile = $this->request->session()->read('Auth.User.profile_pic'); 
                        $profilePicPath = 'images.jpeg';
                        if(!empty($profile)) {
                            $profilePic = new File(Configure::read('ProfilePicPathToCheck') . $this->request->session()->read('Auth.User.profile_pic_dir') . DS . 'square_' . $this->request->session()->read('Auth.User.profile_pic'), true, 0644);
                            if ($profilePic->exists()) {
                                $profilePicPath = Configure::read('ProfilePicPath') . $this->request->session()->read('Auth.User.profile_pic_dir') . DS . 'square_' . $this->request->session()->read('Auth.User.profile_pic');
                            }
                        }
                        echo $this->Html->image($profilePicPath, [
                                    'class' => 'user-image', 'alt' => 'User Image'
                                ]);
                    ?>
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                    <ul aria-labelledby="drop3" class="dropdown-menu  dropdown-menu-right">
                        <li>
                            <?php echo $this->Html->link(
                                '<i class="fa fa-key" aria-hidden="true"></i>'. __('Change Password'),
                                '#',
                                array(
                                    'escape' => false,
                                    'data-toggle' => 'modal',
                                    'data-target' => '#changePasswordModal'
                                    )
                                );
                            ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                '<i class="fa fa-ticket" aria-hidden="true"></i>'. __('Support'),
                                '#',
                                array(
                                    'escape' => false,
                                    'data-toggle' => 'modal',
                                    'data-target' => '#support'
                                    )
                                );
                            ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                '<i class="fa fa-wrench" aria-hidden="true"></i>'. __('Improvement'),
                                '#',
                                array(
                                    'escape' => false,
                                    'data-toggle' => 'modal',
                                    'data-target' => '#improvemnet'
                                    )
                                );
                            ?>
                        </li>
                        <li>
                           <?php echo $this->Html->link(
                                '<i class="fa fa-sign-out" aria-hidden="true"></i>'.
                                __('Logout'),
                                '/Users/logout',
                                array(
                                        'escape' => false
                                    )
                                );
                            ?>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<?php echo $this->element('Frontend/Dashboard/change_password');?>
<?php echo $this->element('Frontend/Dashboard/support');?>
<?php echo $this->element('Frontend/Dashboard/improvement');?>