<div class="col-xs-12">
    <?php
        echo $this->Form->create($user,
                array(
                        'type' => 'file',
                        'id' => 'organization-profile'
                        
                    )
            );
    ?>
      <div class="form-group">
            <label><?php echo __('Logo'); ?></label>
            <input type="file" name="organization[logo]" class="form-control">
        </div>
        <div class="form-group">
        <strong><note><?php echo __('note :Upload only png,jpg,jpeg,gif extension'); ?></note></strong>
        </div>
        <div class="form-group">
            <label><?php echo __('Profile Picture'); ?></label>
            <input type="file" name="profile_pic" class="form-control">
        </div>
        <div class="form-group">
        <strong><note><?php echo __('note :Upload only png,jpg,jpeg,gif extension'); ?></note></strong>
        </div>
        <div class="form-group">
            <label><?php echo __('Organization Name'); ?></label>
            <?php
                echo $this->Form->input('organization.name',
                        array(
                                'class' => 'form-control scriptTagCheck',
                                'placeholder' => __('Organization Name'),
                                'div' => false,
                                'label' => false,
                                'required' => true
                            )
                    );
            ?>

        </div>

          <div class="form-group">
          <div class="row">
          <div class="col-sm-6">
            <label><?php echo __('First Name'); ?></label>
            <?php
                echo $this->Form->input('first_name',
                        array(
                                'class' => 'form-control scriptTagCheck',
                                'placeholder' => __('First Name'),
                                'div' => false,
                                'label' => false,
                                'required' => true
                            )
                    );
            ?>
            </div>
             <div class="col-sm-6">
            <label><?php echo __('Last Name'); ?></label>
            <?php
                echo $this->Form->input('last_name',
                        array(
                                'class' => 'form-control scriptTagCheck',
                                'placeholder' => __('Last Name'),
                                'div' => false,
                                'label' => false,
                                'required' => true
                            )
                    );
            ?>
            </div>
            </div>
        </div>
       
        <div class="form-group">
        <div class="row">
        <div class="col-sm-6">
            <label><?php echo __('Phone'); ?></label>
            <?php
                echo $this->Form->input('organization.phone',
                        array(
                                'class' => 'form-control scriptTagCheck',
                                'placeholder' => __('Phone'),
                                'div' => false,
                                'label' => false,
                                'required' => true,
                                'id' => 'user-phone'
                            )
                    );
            ?>
            </div>
             <div class="col-sm-6">
            <label><?php echo __('Fax'); ?></label>
            <?php
                echo $this->Form->input('organization.fax',
                        array(
                                'class' => 'form-control scriptTagCheck',
                                'placeholder' => __('Fax'),
                                'div' => false,
                                'label' => false,
                                'required' => true,
                                'id' => 'user-fax'
                            )
                    );
            ?>
            </div>
            </div>
        </div>
        
        <div class="form-group">
            <label><?php echo __('Street'); ?></label>
            <?php
                echo $this->Form->input('organization.street',
                        array(
                                'class' => 'form-control scriptTagCheck',
                                'placeholder' => __('Street'),
                                'div' => false,
                                'label' => false,
                                'required' => true
                            )
                    );
            ?>
        </div>
        <div class="form-group">
            <label><?php echo __('Street No'); ?></label>
            <?php
                echo $this->Form->input('organization.street_number',
                        array(
                                'class' => 'form-control scriptTagCheck',
                                'placeholder' => __('Street No'),
                                'div' => false,
                                'label' => false,
                                'required' => true
                            )
                    );
            ?>
        </div>
        <div class="form-group">
            <label><?php echo __('Zip Code'); ?></label>
            <?php
                echo $this->Form->input('organization.zip',
                        array(
                                'class' => 'form-control scriptTagCheck',
                                'placeholder' => __('Zip Code'),
                                'div' => false,
                                'label' => false,
                                'required' => true
                            )
                    );
            ?>
        </div>
        <div class="form-group">
            <label><?php echo __('Website'); ?></label>
            <?php
                echo $this->Form->input('organization.website',
                        array(
                                'class' => 'form-control scriptTagCheck',
                                'placeholder' => __('Website'),
                                'div' => false,
                                'label' => false,
                                'required' => true
                            )
                    );
            ?>
        </div>
        <div class="form-group">
            <label><?php echo __('Place'); ?></label>
            <?php
                echo $this->Form->input('organization.place',
                        array(
                                'class' => 'form-control scriptTagCheck',
                                'placeholder' => __('Place'),
                                'div' => false,
                                'label' => false,
                                'required' => true
                            )
                    );
            ?>
        </div>
        <div class="form-group text-center">
            <div>
                <?php
                    echo $this->Form->button(__('Save'),[
                            'type' => 'submit',
                            'class' => 'btn btn-blue'
                        ]);
                ?>
            </div>
        </div>
    </form>
</div>
                   