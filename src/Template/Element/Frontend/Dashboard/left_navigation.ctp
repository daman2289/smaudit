<!-- Sidebar Left-->
<aside class="main-sidebar">
    <div class="sidebar">
        <ul class="sidebar-menu">
            <li class="treeview invite-friends <?php echo ($this->request->params['controller'] == 'NewReports' && $this->request->params['action'] == 'index') ? 'active' : ''; ?>">
                    <?php
                        echo $this->Html->link(
                            '<i class="fa fa-flag-o" aria-hidden="true"></i>' . __('Reports') . '</span>',
                            [  
                               "controller" => "new-reports",
                            ],
                            [
                                'escape' => false,
                                'class'    => 'edit-profile',
                            ]
                        );
                    ?>
            </li>
            <!-- 
            <li class="treeview <?php echo ($this->request->params['controller'] == 'Reports' && $this->request->params['action'] == 'reportListing') ? 'active' : ''; ?>">
                <?php
                    echo $this->Html->link(
                            '<i class="fa fa-flag-o" aria-hidden="true"></i>' . __('Reports') . '</span>',
                            [
                                'controller' => 'Reports',
                                'action'    => 'report-listing'
                            ],
                            [
                                'escape' => false
                            ]
                        );
                ?>
            </li> -->
            
            <li class="treeview <?php echo (in_array($this->request->params['controller'],array('Subsidiaries','Companies','Users')) && in_array($this->request->params['action'],array('index','companyInfo','organizationUser','dashboard'))) ? 'active' : ''; ?>">
                <a href="#">
                <i class="fa fa-cog" aria-hidden="true"></i>
                <span><?php echo __('Settings'); ?></span>
                <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <?php
                            echo $this->Html->link(
                                '<i class="fa fa-building" aria-hidden="true"></i> <span>' . __('Subsidiaries') . '</span>',
                                '/Subsidiaries/index',
                                [
                                    'escape' => false
                                ]
                            );
                        ?>
                    </li>
                    <li>
                        <?php
                            echo $this->Html->link(
                                    '<i class="fa fa-info-circle" aria-hidden="true"></i>' . __('Company Info') . '</span>',
                                    [
                                        'controller' => 'Companies',
                                        'action' => 'company-info'
                                    ],
                                    [
                                        'escape' => false
                                    ]
                                );
                        ?>
                    </li>
                    <li class="treeview invite-friends <?php echo ($this->request->params['controller'] == 'Companies' && $this->request->params['action'] == 'indexCompany') ? 'active' : ''; ?>">
                        <?php
                            echo $this->Html->link(
                                    '<i class="fa fa-sitemap" aria-hidden="true"></i>' . __('Companies') . '</span>',
                                    [
                                        'controller' => 'Companies',
                                        'action'    => 'indexCompany'
                                    ],
                                    [
                                        'escape' => false
                                    ]
                                );
                        ?>
                    </li>
                    <li class="treeview invite-friends <?php echo ($this->request->params['controller'] == 'Subsidiaries' && $this->request->params['action'] == 'organizationUser') ? 'active' : ''; ?>">
                        <?php
                            echo $this->Html->link(
                                '<i class="fa fa-users" aria-hidden="true"></i>' . __('Users') . '</span>',
                                '/Subsidiaries/organizationUser',
                                [
                                    'escape' => false
                                ]
                            );
                        ?>
                    </li>
                    <li class="treeview invite-friends">
                        <?php 
                            echo $this->Html->link(
                                '<i class="fa fa-users" aria-hidden="true"></i>' . __('Edit Company Profile') . '</span>',
                                'javascript:void(0)',
                                [
                                    'escape' => false,
                                    'class'    => 'edit-profile',
                                    'data-url' =>   $this->Url->build(
                                       [  
                                           "controller" => "Users",
                                           "action" => "Profile",
                                           base64_encode($this->request->session()->read('Auth.User.organization.user_id'))
                                       ],true
                                   )
                                ]
                            );
                        ?>
                    </li>
                    <li class="treeview <?php echo ($this->request->params['controller'] == 'Users' && $this->request->params['action'] == 'dashboard') ? 'active' : ''; ?>">
                        <?php
                            echo $this->Html->link(
                                '<i class="fa fa-tachometer" aria-hidden="true"></i> <span>' . __('Dashboard') . '</span>',
                                [
                                    'controller' => 'Users',
                                    'action'    => 'dashboard'
                                ],
                                [
                                    'escape' => false
                                ]
                            );
                        ?>
                    </li>
                    
                    <li class="treeview invite-friends <?php echo ($this->request->params['controller'] == 'SubscriptionPlas' && $this->request->params['action'] == 'index') ? 'active' : ''; ?>">
                        <?php 
                            $uerRole = $this->request->session()->read('Auth.User.role_id');
                            if($uerRole == 2) {
                                echo $this->Html->link(
                                    '<i class="fa fa-money" aria-hidden="true"></i>' . __('My Subscription Plan') . '</span>',
                                    [  
                                       "controller" => "subscription-plans",
                                    ],
                                    [
                                        'escape' => false,
                                        'class'    => 'edit-profile',
                                    ]
                                );
                            }
                        ?>
                    </li> 

                    
                    <li class="treeview invite-friends <?php echo ($this->request->params['controller'] == 'homes' && $this->request->params['action'] == 'footer') ? 'active' : ''; ?>">
                        <?php 
                            $uerRole = $this->request->session()->read('Auth.User.role_id');
                            if($uerRole == 2) {
                                echo $this->Html->link(
                                    '<i class="fa fa-subscript" aria-hidden="true"></i>' . __('Manage Report Footer') . '</span>',
                                    [  
                                       "controller" => "homes",
                                       'action' => 'footer'
                                    ],
                                    [
                                        'escape' => false,
                                        'class'    => 'edit-profile',
                                    ]
                                );
                            }
                        ?>
                    </li>   

                    
                    <li class="treeview invite-friends <?php echo ($this->request->params['controller'] == 'homes' && $this->request->params['action'] == 'header') ? 'active' : ''; ?>">
                        <?php 
                            $uerRole = $this->request->session()->read('Auth.User.role_id');
                            if($uerRole == 2) {
                                echo $this->Html->link(
                                    '<i class="fa fa-superscript" aria-hidden="true"></i>' . __('Manage Report Header') . '</span>',
                                    [  
                                       "controller" => "homes",
                                       'action' => 'header'
                                    ],
                                    [
                                        'escape' => false,
                                        'class'    => 'edit-profile',
                                    ]
                                );
                            }
                        ?>
                    </li>                   
                </ul>
            </li>            
            <li class="treeview invite-friends">
                <?php 
                    echo $this->Html->link(
                        '<i class="fa fa-sign-out" aria-hidden="true"></i>
                <span>'. __('Logout') .'</span>',
                            '/Users/logout',
                            array(
                                'escape' => false
                            )
                        );
                ?>
            </li>
        </ul>
    </div>
</aside>
<div class="modal addSubsidiary fade" id="organization" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times-circle" aria-hidden="true"></i></span></button>
                <h4 class="modal-title text-center"><?php echo __('Update Organization Profile'); ?></h4>
            </div>
            <div class="modal-body clearfix">
             </div>
        </div>
    </div>
</div>
<?php echo $this->Html->script('Frontend/Users/profile', ['block' => 'scriptBottom']); ?>