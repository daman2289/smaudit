<?php
    echo $this->Form->create($service,
        [
            'type' => 'file',
            'url' => [
                'controller' => 'services',
                'action' => 'edit',
                base64_encode($service->id)
            ],
            'id' => 'edit-service-form',
            'class' => 'form-horizontal'
        ]
    );
?>            
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('English Title'); ?></label>
        <div class="col-sm-8">
            <?php
                echo $this->Form->input('title_eng',
                    array(
                        'class' => 'form-control',
                        'placeholder' => __('Question in English'),
                        'div' => false,
                        'label' => false
                    )
                );
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('English Description'); ?></label>
        <div class="col-sm-8">
            <?php
                echo $this->Form->input('description_eng',
                    array(
                        'class' => 'form-control',
                        'placeholder' => __('Description in English'),
                        'div' => false,
                        'label' => false,
                        'type' => 'textarea'
                    )
                );
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('German Title'); ?></label>
        <div class="col-sm-8">
            <?php
                echo $this->Form->input('title_german',
                    array(
                        'class' => 'form-control',
                        'placeholder' => __('German Title'),
                        'div' => false,
                        'label' => false
                    )
                );
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('German Description'); ?></label>
        <div class="col-sm-8">
            <?php
                echo $this->Form->input('description_german',
                    array(
                        'class' => 'form-control',
                        'placeholder' => __('Description in German'),
                        'div' => false,
                        'label' => false,
                        'type' => 'textarea'
                    )
                );
            ?>
        </div>
    </div>
    <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
        <div class="col-lg-8">
            <?php
                echo $this->Form->button('Submit', array(
                            'class' => 'btn submit-info submit_black',
                            'type' => 'submit'
                            ));
            ?>
            <?php
                echo $this->Form->button('Cancel', array(
                            'class' => 'btn submit-info submit_black',
                            'type' => 'button',
                            'data-dismiss' => 'modal'
                            ));
            ?>
        </div>
    </div>
<?php echo $this->Form->end(); ?>