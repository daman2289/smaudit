<?php
    $startName = 'from';
    $endName = 'from';
    echo $this->Form->date('from', [
            'minYear' => 2018,
            'monthNames' => false,
            'empty' => [
                'year' => false,
                'month' => 'Choose month...'
            ],
            'day' => false,
            'year' => [
                'class' => 'cool-years',
                'title' => 'Registration Year'
            ]
        ], [
            'name' => $startName
        ]
    );
?>
<?php
    echo $this->Form->date('to', [
            'minYear' => 2018,
            'monthNames' => false,
            'empty' => [
                'year' => false,
                'month' => 'Choose month...'
            ],
            'day' => false,
            'year' => [
                'class' => 'cool-years',
                'title' => 'Registration Year'
            ]
        ],[
            'name' => $endName
        ]
    );
?>