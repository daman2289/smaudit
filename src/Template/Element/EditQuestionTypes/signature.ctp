<div class="row">	
	<div class="form-group col-md-6">	
		<?php
			$name = $question['question_en'];
			echo $this->Form->label($name);
			echo $this->Form->select($name, $usersList,[
				'name' => $key."[answer]",
				'empty' => 'Select',
				'onchange'=> ($is_parent) ? "selectChid(this)" : "void(0)",
				'required',
				'class' => 'form-control input-lg',
				'value' => $answer->answer
			]);
		?>
	    <?php
	    	echo $this->Form->hidden("new_question_id",['name' => $key."[new_question_id]", 'value' => $question['id']]);
			echo $this->Form->hidden("new_placeholder_id",['name' => $key."[new_placeholder_id]", 'value' => $question['new_placeholder_id']]); 
	        echo $this->Form->hidden('id', ['name' => $key."[id]", 'value' => (!empty($answer)) ? $answer->id:'']);
	    ?>
	</div>
	<?php
		if(!empty($question['child_questions'])) {
			foreach ($question['child_questions'] as $index => $value) {
	?>
				<div class="form-group col-md-6">		
					<div> 						
						<?php echo $this->Form->label($value['question_en']); ?>
					</div>
					<?php
						echo $this->Form->hidden("new_question_id",['name' => ($key+100)."[new_question_id]", 'value' => $value['id']]);
						echo $this->Form->hidden("new_placeholder_id",['name' => ($key+100)."[new_placeholder_id]", 'value' => $value['new_placeholder_id']]);
						echo $this->Form->hidden('id', ['name' => ($key+100)."[id]", 'value' => $value['child_answer']['id']]);
						echo $this->Form->checkbox('option_id', [
					    	'value' => 1,
					    	// 'hiddenField' => 0,
					    	'name' => ($key+100)."[answer]",
					    	'class'	=> 'in-charge',
					    	(!empty($value['child_answer']['answer']))? 'checked': ''
						]); 
					?>
				</div>
	<?php
			}
		}
	?>
</div>
