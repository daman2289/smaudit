<?php
	$options = [];
	
	if(!empty($question['new_question_options'])) {
		foreach ($question['new_question_options'] as $k => $value) {
			$options[$value['id']] = $value['name_en'];
		}
	}
?>
<div class="form-group">
	<?php
		echo $this->Form->hidden("new_question_id",['name' => $key."[new_question_id]", 'value' => $question['id']]);
		echo $this->Form->hidden("new_placeholder_id",['name' => $key."[new_placeholder_id]", 'value' => $question['new_placeholder_id']]);

		echo $this->Form->label($question['question_en']);
		echo $this->Form->select('option_id', $options,[
			'name' => $key."[new_question_option_id]",
			'label' => $question['question_en'],
			'empty' => 'Select',
			'onchange'=> ($is_parent) ? "selectChid(this)" : "void(0)",
			'required',
			'class' => 'form-control input-lg',
			'value' => (!empty($answer)) ? $answer->new_question_option_id: '',
		]);
	?>
</div>
<div class="form-group"> 
    <?php 
        echo $this->Form->hidden('id', ['name' => $key."[id]", 'value' => (!empty($answer)) ? $answer->id:'']);
    ?>
</div>