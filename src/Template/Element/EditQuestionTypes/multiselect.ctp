<?php
	$options = [];
	
	if(!empty($question['new_question_options'])) {
		foreach ($question['new_question_options'] as $k => $value) {
			$options[$value['id']] = $value['name_en'];
		}
	}
?>
<div class="form-group">
	<?php
		echo $this->Form->hidden("new_question_id",['name' => $key."[new_question_id]", 'value' => $question['id']]);
		echo $this->Form->hidden("new_placeholder_id",['name' => $key."[new_placeholder_id]", 'value' => $question['new_placeholder_id']]);

		echo $this->Form->label($question['question_en']);
		echo $this->Form->select('answer', $options, [
			'name' => $key."[answer]",
			'label' => $question['question_en'],
			'onchange'=> ($is_parent) ? "multiSelectChild(this)" : "void(0)",
			'required',
			'class' => 'form-control input-lg multiple-select',
			'multiple' => true,
			'value' => (!empty($answer)) ? explode(',', $answer->answer) : ''
		]);
	?>
</div>
<div class="form-group"> 
    <?php 
        echo $this->Form->hidden('id', ['name' => $key."[id]", 'value' => (!empty($answer)) ? $answer->id :'']);
    ?>
</div>