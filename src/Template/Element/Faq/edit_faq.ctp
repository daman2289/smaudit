<?php
    echo $this->Form->create($faq,
        [   
            'type' => 'post',
            'url' => [
                'controller' => 'Faqs',
                'action' => 'edit',
                base64_encode($faq->id)
            ],
            'id' => 'add-faq-form',
            'class' => 'form-horizontal'
        ]
    );
?>            
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Question in English'); ?></label>
        <div class="col-sm-8">
            <?php
                echo $this->Form->input('ques_eng',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('Question in English'),
                                'div' => false,
                                'label' => false
                            )
                    );
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Answer in English'); ?></label>
        <div class="col-sm-8">
            <?php
                echo $this->Form->input('ans_eng',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('Answer in English'),
                                'div' => false,
                                'label' => false,
                                'type' => 'textarea'
                            )
                    );
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Question in German'); ?></label>
        <div class="col-sm-8">
            <?php
                echo $this->Form->input('ques_german',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('Question in German'),
                                'div' => false,
                                'label' => false
                            )
                    );
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Answer in German'); ?></label>
        <div class="col-sm-8">
            <?php
                echo $this->Form->input('ans_german',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('Answer in German'),
                                'div' => false,
                                'label' => false,
                                'type' => 'textarea'
                            )
                    );
            ?>
        </div>
    </div>
    <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
        <div class="col-lg-8">
            <?php
                echo $this->Form->button('Submit', array(
                            'class' => 'btn submit-info submit_black',
                            'type' => 'submit'
                            ));
            ?>
            <?php
                echo $this->Form->button('Cancel', array(
                            'class' => 'btn submit-info submit_black',
                            'type' => 'button',
                            'data-dismiss' => 'modal'
                            ));
            ?>
        </div>
    </div>
<?php echo $this->Form->end(); ?>