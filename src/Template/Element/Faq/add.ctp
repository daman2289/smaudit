<div class="modal Educational-info fade" id="add-diploma" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Add Faq'); ?></h4>
            </div>
            <div class="modal-body">
                <?php
                    echo $this->Form->create(null,
                            [
                                    'url' => [
                                        'controller' => 'Faqs',
                                        'action' => 'add'
                                    ],
                                    'id' => 'add-faq-form',
                                    'class' => 'form-horizontal'
                            ]
                        );
                ?>            
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><?php echo __('Question in English'); ?></label>
                        <div class="col-sm-8">
                            <?php
                                echo $this->Form->input('ques_eng',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('Question in English'),
                                                'div' => false,
                                                'label' => false
                                            )
                                    );
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><?php echo __('Answer in English'); ?></label>
                        <div class="col-sm-8">
                            <?php
                                echo $this->Form->input('ans_eng',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('Answer in English'),
                                                'div' => false,
                                                'label' => false,
                                                'type' => 'textarea'
                                            )
                                    );
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><?php echo __('Question in German'); ?></label>
                        <div class="col-sm-8">
                            <?php
                                echo $this->Form->input('ques_german',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('Question in German'),
                                                'div' => false,
                                                'label' => false
                                            )
                                    );
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><?php echo __('Answer in German'); ?></label>
                        <div class="col-sm-8">
                            <?php
                                echo $this->Form->input('ans_german',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('Answer in German'),
                                                'div' => false,
                                                'label' => false,
                                                'type' => 'textarea'
                                            )
                                    );
                            ?>
                        </div>
                    </div>
                    <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
                        <div class="col-lg-8">
                            <?php
                                echo $this->Form->button('Submit', array(
                                            'class' => 'btn submit-info submit_black',
                                            'type' => 'submit'
                                            ));
                            ?>
                            <?php
                                echo $this->Form->button('Cancel', array(
                                            'class' => 'btn submit-info submit_black',
                                            'type' => 'button',
                                            'data-dismiss' => 'modal'
                                            ));
                            ?>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
