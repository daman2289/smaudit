<div>
    <div class="btn-toolbar">
           <nav aria-label="Page navigation">
                  <ul class="pagination">
                  <?php 
                    if ($this->Paginator->hasPrev() ){
                       echo $this->Paginator->prev( '<', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li' ) );
                    } 
                  ?>
                  
                    <?php echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a' ) ); ?>
                    <?php
                      if ($this->Paginator->hasNext() ){
                        echo $this->Paginator->next( '>', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li' ) );
                      }
                    ?>
                  </ul>
            </nav>
        
    </div>
</div>