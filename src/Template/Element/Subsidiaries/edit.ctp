<div class="col-xs-12">
    <?php
        echo $this->Form->create($subsidiary,
                [
                        'type' => 'post',
                        'id' => 'edit-subsidiary-form',
                        'class' => 'form-horizontal'
                ]
            );
    ?>
        <div class="form-group">
            <label><?php echo __('Name'); ?></label>
            <?php echo $this->Form->hidden('id');?>
            <?php
                echo $this->Form->input('name',
                        array(
                                'class' => 'form-control scriptTagCheck',
                                'placeholder' => __('Name'),
                                'div' => false,
                                'label' => false
                            )
                    );
            ?>
        </div>
        <div class="form-group">
            <label><?php echo __('Phone'); ?></label>
            <?php
                echo $this->Form->input('phone',
                        array(
                                'class' => 'form-control scriptTagCheck',
                                'placeholder' => __('Phone'),
                                'div' => false,
                                'label' => false,
                                'id' => 'phone-no'
                            )
                    );
            ?>
        </div>
        <div class="form-group">
            <label><?php echo __('Fax'); ?></label>
            <?php
                echo $this->Form->input('fax',
                        array(
                                'class' => 'form-control scriptTagCheck',
                                'placeholder' => __('Fax'),
                                'div' => false,
                                'label' => false,
                                'id' => 'fax-no'
                            )
                    );
            ?>
        </div>
        <div class="form-group">
            <label><?php echo __('Street'); ?></label>
            <?php
                echo $this->Form->input('street',
                        array(
                                'class' => 'form-control scriptTagCheck',
                                'placeholder' => __('Street'),
                                'div' => false,
                                'label' => false
                            )
                    );
            ?>
        </div>
        <div class="form-group">
            <label><?php echo __('Street No'); ?></label>
            <?php
                echo $this->Form->input('street_number',
                        array(
                                'class' => 'form-control scriptTagCheck',
                                'placeholder' => __('Street Number'),
                                'div' => false,
                                'label' => false
                            )
                    );
            ?>
        </div>
        <div class="form-group">
            <label><?php echo __('Zip Code'); ?></label>
            <?php
                echo $this->Form->input('zip',
                        array(
                                'class' => 'form-control scriptTagCheck',
                                'placeholder' => __('Zip Code'),
                                'div' => false,
                                'label' => false
                            )
                    );
            ?>
        </div>
        <div class="form-group">
            <label><?php echo __('Place'); ?></label>
            <?php
                echo $this->Form->input('place',
                        array(
                                'class' => 'form-control scriptTagCheck',
                                'placeholder' => __('Place'),
                                'div' => false,
                                'label' => false
                            )
                    );
            ?>
        </div>
        <div class="form-group text-center">
            <div>
            <?php
                echo $this->Form->button('Submit', array(
                        'class' => 'btn btn-blue',
                        'type' => 'submit'
                    )
                );
            ?>
            </div>
        </div>
    </form>
</div>