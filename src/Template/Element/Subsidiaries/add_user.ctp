<div class="modal addSubsidiary fade" id="addUser" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times-circle" aria-hidden="true"></i></span></button>
                <h4 class="modal-title text-center"><?php echo __('Add User'); ?></h4>
            </div>
            <div class="modal-body clearfix">
                <div class="col-xs-12">
                    <?php
                        echo $this->Form->create(null,
                            [
                                'url' => [
                                    'controller' => 'Subsidiaries',
                                    'action' => 'addUser'
                                ],
                                'id' => 'add-organization-user',
                                'class' => 'form-horizontal'
                            ]
                        );
                    ?>
                        <div class="form-group">
                            <label><?php echo __('First Name'); ?></label>
                            <?php
                                echo $this->Form->input('first_name',
                                    array(
                                        'class' => 'form-control scriptTagCheck',
                                        'placeholder' => __('First Name'),
                                        'div' => false,
                                        'label' => false
                                    )
                                );
                            ?>
                        </div>
                        <div class="form-group">
                            <label><?php echo __('Last Name'); ?></label>
                            <?php
                                echo $this->Form->input('last_name',
                                    array(
                                        'class' => 'form-control scriptTagCheck',
                                        'placeholder' => __('Last Name'),
                                        'div' => false,
                                        'label' => false
                                    )
                                );
                            ?>
                        </div>
                        <div class="form-group">
                            <label><?php echo __('Email'); ?></label>
                            <?php
                                echo $this->Form->input('email',
                                    array(
                                        'class' => 'form-control scriptTagCheck',
                                        'placeholder' => __('Email'),
                                        'div' => false,
                                        'label' => false
                                    )
                                );
                            ?>
                        </div>
                        <div class="form-group">
                            <label><?php echo __('Password'); ?></label>
                            <?php
                                echo $this->Form->input('password',
                                    array(
                                        'class' => 'form-control',
                                        'placeholder' => __('Password'),
                                        'div' => false,
                                        'label' => false
                                    )
                                );
                            ?>
                        </div>
                        <div class="form-group">
                            <label><?= __('Gender') ?></label>
                            <div class="gender">                                
                                <?php
                                    echo $this->Form->radio('organization_user.gender', [
                                            '1'=>'Male', 
                                            '2' => 'Female'
                                        ], [
                                            'value' => 1
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label><?php echo __('License'); ?></label>
                            <?php
                                echo $this->Form->input('organization_user.license_id',
                                    array(
                                        'class' => 'form-control scriptTagCheck',
                                        'placeholder' => __('License'),
                                        'div' => false,
                                        'label' => false,
                                        'options' =>$license
                                    )
                                );
                            ?>
                        </div>
                        <div class="form-group">
                            <label><?php echo __('Subsidiary'); ?></label>
                            <?php
                                echo $this->Form->input('organization_user.organization_subsidiary_id',
                                    array(
                                        'class' => 'form-control scriptTagCheck',
                                        'placeholder' => __('Subsidiary'),
                                        'div' => false,
                                        'label' => false,
                                        'options' => $subsidiary
                                    )
                                );
                            ?>
                        </div>
                        <div class="form-group text-center">
                            <div>
                            <?php
                                echo $this->Form->button('Save', array(
                                        'class' => 'btn btn-blue',
                                        'type' => 'submit'
                                    )
                                );
                            ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .gender label {
        margin-left: 15px;
    }
</style>