<div class="modal Educational-info fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Change Password'); ?></h4>
            </div>
            <div class="modal-body">
                <?php
                    echo $this->Form->create('User', [
                                'url' => [
                                    'controller' =>'Admins',
                                    'action' => 'changePassword'
                                ],
                                'type' => 'post',
                                'id' => 'admin-change-password'
                        ]);
                ?>   
                    <div class="form-group">
                        <label class="control-label"><?php echo __('Old Password'); ?><span class="required_ast">*</span></label>
                        <?php
                          echo $this->Form->input('old_password', array(
                                        'class' => 'form-control',
                                        'type' => 'password',
                                        'label' => false,
                                        'div' => false,
                                        'placeholder' => 'Old Password'

                                        ));
                        ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __('New Password'); ?><span class="required_ast">*</span></label>
                        <?php
                            echo $this->Form->input('password', array(
                                        'class' => 'form-control',
                                        'type' => 'password',
                                        'label' => false,
                                        'div' => false,
                                        'placeholder' => 'New Password',
                                        'id' => 'new-admin-password',
                                        'data-display' => 'mainPassword'
                                        ));
                        ?>
                        <div class="left passwordStrength" id="mainPassword"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __('Confirm Password'); ?><span class="required_ast">*</span></label>
                        <?php
                            echo $this->Form->input('confirm_password', array(
                                        'class' => 'form-control',
                                        'type' => 'password',
                                        'label' => false,
                                        'div' => false,
                                        'placeholder' => 'Confirm Password'
                                        ));
                        ?>
                    </div>
                    <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
                        <div class="col-lg-8">
                            <?php
                                echo $this->Form->button(__('Submit'),[
                                        'class' => 'btn submit-info submit_black',
                                        'type' => 'submit'
                                    ]);
                            ?>
                            <?php
                                echo $this->Form->button(__('Cancel'),[
                                        'class' => 'btn submit-info submit_black',
                                        'type' => 'button',
                                        'data-dismiss' => 'modal'
                                    ]);
                            ?>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->script('Administration/change_password', ['block' => 'scriptBottom']); ?>