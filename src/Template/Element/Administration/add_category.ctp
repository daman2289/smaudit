<div class="modal Educational-info fade" id="add-category" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Add Report category'); ?></h4>
            </div>
            <div class="modal-body">
                <?php
                    echo $this->Form->create(null,
                            [
                                    'url' => [
                                        'controller' => 'Settings',
                                        'action' => 'addCategory'
                                    ],
                                    'id' => 'add-category-form',
                                    'class' => 'form-horizontal'
                            ]
                        );
                ?>            
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><?php echo __('Report Category Name'); ?></label>
                        <div class="col-sm-8">
                            <?php
                                echo $this->Form->input('name',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('Report Category Name'),
                                                'div' => false,
                                                'label' => false
                                            )
                                    );
                            ?>
                        </div>
                    </div>
                    <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
                        <div class="col-lg-8">
                            <?php
                                echo $this->Form->button('Submit', array(
                                            'class' => 'btn submit-info submit_black',
                                            'type' => 'submit'
                                            ));
                            ?>
                            <?php
                                echo $this->Form->button('Cancel', array(
                                            'class' => 'btn submit-info submit_black',
                                            'type' => 'button',
                                            'data-dismiss' => 'modal'
                                            ));
                            ?>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
