<div class="modal Educational-info fade" id="add-sub-admin" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-target="#add-sub-admin">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Add Sub-admin'); ?></h4>
            </div>
            <div class="modal-body">
                <?php
                    echo $this->Form->create(null,
                            [
                                    'url' => [
                                        'controller' => 'Admins',
                                        'action' => 'add'
                                    ],
                                    'id' => 'add-sub-admin-form',
                                    'class' => 'form-horizontal'
                            ]
                        );
                ?>            
                    <div class="form-group">
                        <label  class="col-sm-4 control-label"><?php echo __('First Name'); ?><span class="required_ast">*</span></label>
                        <div class="col-sm-4">
                            <?php
                                echo $this->Form->input('first_name',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('First Name'),
                                                'div' => false,
                                                'label' => false
                                            )
                                    );
                            ?>
                        </div>
                        <div class="col-sm-4">
                            <?php
                                echo $this->Form->input('last_name',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('Last Name'),
                                                'div' => false,
                                                'label' => false
                                            )
                                    );
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-4 control-label"><?php echo __('Email Address'); ?><span class="required_ast">*</span></label>
                        <div class="col-sm-8">
                            <?php
                                echo $this->Form->input('email',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('Email Address'),
                                                'div' => false,
                                                'label' => false
                                            )
                                    );
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-4 control-label"><?php echo __('Create A Password'); ?><span class="required_ast">*</span></label>
                        <div class="col-sm-8">
                            <?php
                                echo $this->Form->password('password',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('Minimum nine characters long'),
                                                'div' => false,
                                                'label' => false,
                                                'id' => 'sub-admin-password'
                                            )
                                    );
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-4 control-label"><?php echo __('Confirm Password'); ?><span class="required_ast">*</span></label>
                        <div class="col-sm-8">
                            <?php
                                echo $this->Form->password('confirm_password',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('Minimum nine characters long'),
                                                'div' => false,
                                                'label' => false
                                            )
                                    );
                            ?>
                        </div>
                    </div>
                    <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
                        <div class="col-lg-8">
                            <?php
                                echo $this->Form->button('Submit', array(
                                            'class' => 'btn submit-info submit_black',
                                            'type' => 'submit'
                                            ));
                            ?>
                            <?php
                                echo $this->Form->button('Cancel', array(
                                            'class' => 'btn submit-info submit_black',
                                            'type' => 'button',
                                            'data-dismiss' => 'modal'
                                            ));
                            ?>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
