<?php
    echo $this->Form->create($category,
            [
                    'url' => [
                        'controller' => 'Settings',
                        'action' => 'editCategory'
                    ],
                    'type' => 'post',
                    'id' => 'edit-category-form',
                    'class' => 'form-horizontal'
            ]
        );
?>            
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Report Category Name'); ?></label>
        <div class="col-sm-8">
            <?php echo $this->Form->hidden('id');?>
            <?php
                echo $this->Form->input('name',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('Report Category Name'),
                                'div' => false,
                                'label' => false
                            )
                    );
            ?>
        </div>
    </div>
    <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
        <div class="col-lg-8">
            <?php
                echo $this->Form->button('Submit', array(
                            'class' => 'btn submit-info submit_black',
                            'type' => 'submit'
                            ));
            ?>
            <?php
                echo $this->Form->button('Cancel', array(
                            'class' => 'btn submit-info submit_black',
                            'type' => 'button',
                            'data-dismiss' => 'modal'
                            ));
            ?>
        </div>
    </div>
<?php echo $this->Form->end(); ?>