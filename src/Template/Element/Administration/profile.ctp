<?php use Cake\Core\Configure; ?>
<div class="modal Educational-info fade" id="updateProfile" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Update Profile'); ?></h4>
            </div>
            <div class="modal-body">
                <?php
                    echo $this->Form->create(null,
                            array(
                                    'url' => array(
                                            'controller' => 'Admins',
                                            'action' => 'editProfile'
                                        ),
                                    'class' => 'form-horizontal',
                                    'id' => 'admin-profile-update',
                                    'type' => 'file'
                                )
                        );
                ?>  
                    <div class="form-group">
                        <label  class="col-sm-4 control-label"><?php echo __('Full Name'); ?><span class="required_ast">*</span></label>
                        <div class="col-sm-4">
                            <?php
                                echo $this->Form->input('title',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('Title'),
                                                'div' => false,
                                                'label' => false,
                                                'options' => Configure::read("title"),
                                                'required' => true
                                            )
                                    );
                            ?>
                        </div>
                    </div>          
                    <div class="form-group">
                        <label  class="col-sm-4 control-label"><?php echo __('Full Name'); ?><span class="required_ast">*</span></label>
                        <div class="col-sm-4">
                            <?php
                                echo $this->Form->input('first_name',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('First Name'),
                                                'div' => false,
                                                'label' => false,
                                                'value' => $this->request->session()->read('Auth.User.first_name'),
                                                'required' => true
                                            )
                                    );
                            ?>
                        </div>
                        <div class="col-sm-4">
                            <?php
                                echo $this->Form->input('last_name',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('Last Name'),
                                                'div' => false,
                                                'label' => false,
                                                'value' => $this->request->session()->read('Auth.User.last_name'),
                                                'required' => true
                                            )
                                    );
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-4 control-label"><?php echo __('Email Address'); ?><span class="required_ast">*</span></label>
                        <div class="col-sm-8">
                            <?php
                                echo $this->Form->input('email',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('Type your Email Address'),
                                                'div' => false,
                                                'label' => false,
                                                'value' => $this->request->session()->read('Auth.User.email'),
                                                'required' => true
                                            )
                                    );
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-4 control-label"><?php echo __('Profile Pic'); ?></label>
                        <div class="col-sm-8">
                            <?php
                                echo $this->Form->file('profile_pic',
                                        [
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false
                                        ]
                                    );
                            ?>
                        </div>
                    </div>
                    <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
                        <div class="col-lg-8">
                            <?php
                                echo $this->Form->button(__('Submit'),[
                                        'class' => 'btn submit-info submit_black',
                                        'type' => 'submit'
                                    ]);
                            ?>
                            <?php
                                echo $this->Form->button(__('Cancel'),[
                                        'class' => 'btn submit-info submit_black',
                                        'type' => 'button',
                                        'data-dismiss' => 'modal'
                                    ]);
                            ?>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->script('Administration/edit_profile', ['block' => 'scriptBottom']); ?>