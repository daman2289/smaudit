<?php
    use Cake\Core\Configure;
    use Cake\Filesystem\File;
?>
<header class="main-header">
    <!-- Logo -->
    <?php
        echo $this->Html->link(
                '<span class="logo-mini">' . $this->Html->image('logo-mini.png', ['alt' => 'logo']) . '</span>
                <span class="logo-lg">' . $this->Html->image('logo.png', ['alt' => 'logo']) . '</span>',
                '/admin/admins/dashboard',
                [
                    'class' => 'logo',
                    'escape' => false
                ]
            );
    ?>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top nav-custom">
        <!-- Sidebar toggle button-->
        <?php echo $this->Html->link(
                    '<i class="fa fa-bars fa-1" aria-hidden="true"></i>',
                    '#',
                    [
                        'class' => 'sidebar-toggle',
                        'data-toggle' => 'offcanvas',
                        'role' => 'button',
                        'escape' => false
                    ]
                );
        ?>
        <div class="col-sm-12  col-md-12 col-lg-4 col-lg-offset-3 search-top">
            <div class="right-inner-addon">
                <h2><?php echo __('Admin Panel'); ?>
                    <?php echo $this->Html->link(
                           $this->Html->image('english.png',array('alt' => '','height' => 14, 'style' => 'margin-left:8px;')).__(' EN'),
                            '/Languages/changeLanguage/en',
                            [
                                'escape' => false,
                                'class' => 'pull-right',
                                'style' => 'font-size:14px;'
                            ]
                        );
                    ?>
                    <?php echo $this->Html->link(
                           $this->Html->image('flag-germany.png',array('alt' => '','height' => 14)).__(' DE'),
                            '/Languages/changeLanguage/ar',
                            [
                                'escape' => false,
                                'class' => 'pull-right',
                                'style' => 'font-size:14px;'
                            ]
                        );
                    ?>
                </h2>
            </div>
            
        </div>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">            
                <li class="dropdown user user-menu">
                    <?php

                        $profile = $this->request->session()->read('Auth.User.profile_pic'); 
                        if(!empty($profile)) {                            
                            echo $this->Html->link(
                                $this->Html->image($this->Common->specializesIcon(
                                        'users',
                                        'prifile_pic', 
                                        $this->request->session()->read('Auth.User.profile_pic_dir'), 
                                        'square_', 
                                        $this->request->session()->read('Auth.User.profile_pic')
                                    ),['class' => 'user-image']
                                ) . 
                                '<span class="hidden-xs hidden-sm">'.$this->request->session()->read('Auth.User.first_name').' '.$this->request->session()->read('Auth.User.last_name').'</span> ',
                                '#',
                                [
                                    'escape' => false,
                                    'class' => 'dropdown-toggle',
                                    'data-toggle' => 'dropdown'
                                ]
                            );
                        }else {
                            echo $this->Html->link(
                                $this->Html->image('profile-logo.png', ['class' => 'user-image']) . 
                                '<span class="hidden-xs hidden-sm">'.$this->request->session()->read('Auth.User.first_name').' '.$this->request->session()->read('Auth.User.last_name').'</span> ',
                                '#',
                                [
                                    'escape' => false,
                                    'class' => 'dropdown-toggle',
                                    'data-toggle' => 'dropdown'
                                ]
                            );
                        }
                        
                    ?>
                   
                    <ul aria-labelledby="drop3" class="dropdown-menu">
                        <li>
                            <?php echo $this->Html->link(__('Profile'),
                                                    'javascript:void(0)',
                                                    array(
                                                            'class' => 'editprofile',
                                                            'escape' => false,
                                                            'data-toggle' => 'modal',
                                                            'data-target' => '#updateProfile',
                                                        )
                                        );?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(__('Change Password'),
                                        '#',
                                        array(
                                                    'data-toggle' => 'modal',
                                                    'data-target' => '#changePasswordModal',
                                                    'escape' => false
                                            )
                                    );
                            ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(__('Logout'),
                                        '/admin/Admins/logout'
                                    );
                            ?>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>