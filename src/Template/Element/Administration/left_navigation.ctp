<?php use Cake\Core\Configure; ?>
<!-- Sidebar Left-->
<aside class="main-sidebar">
    <div class="sidebar">
        <ul class="sidebar-menu">
            <li class="treeview">
                <?php
                    echo $this->Html->link(
                            '<i class="fa fa-dashboard" aria-hidden="true"></i> <span>' . __('Dashboard') . '</span>',
                            '/admin/admins/dashboard',
                            [
                                'escape' => false
                            ]
                        );
                ?>
            </li>
            <li class="treeview">
                <?php
                    echo $this->Html->link(
                            '<i class="fa fa-gear" aria-hidden="true"></i> <span>' . __('Cms') . '</span>',
                            'javascript:void(0)',
                            [
                                'escape' => false
                            ]
                        );
                ?>
                <ul class="treeview-menu <?php echo (in_array($this->request->params['controller'], ['Services', 'Faqs', 'Placeholders', 'SubscriptionPlans', 'Specializes', 'Settings'])) ? 'menu-open' : ''?>">
                    <li class="treeview">
                        <?php
                            echo $this->Html->link(
                                    '<i class="fa fa-folder-open" aria-hidden="true"></i> <span>' . __('Manage Work in Progress') . '</span>',
                                    '/admin/Placeholders/index',
                                    [
                                        'escape' => false
                                    ]
                                );
                        ?>
                    </li>
                    <li class="treeview">
                        <?php
                            echo $this->Html->link(
                                    '<i class="fa fa-question-circle-o" aria-hidden="true"></i> <span>' . __('Manage FAQs') . '</span>',
                                    '/admin/Faqs/index',
                                    [
                                        'escape' => false
                                    ]
                                );
                        ?>
                    </li>

                    <li class="treeview">
                        <?php
                            echo $this->Html->link(
                                    '<i class="fa fa-scribd" aria-hidden="true"></i> <span>' . __('Manage Services') . '</span>',
                                    '/admin/Services/index',
                                    [
                                        'escape' => false
                                    ]
                                );
                        ?>
                    </li>
                    <li class="treeview">
                        <?php
                            echo $this->Html->link(
                                    '<i class="fa fa-list-alt" aria-hidden="true"></i> <span>' . __('Manage Subscription Plans') . '</span>',
                                    '/admin/SubscriptionPlans/index',
                                    [
                                        'escape' => false
                                    ]
                                );
                        ?>
                    </li>
                    <li class="treeview">
                        <?php
                            echo $this->Html->link(
                                    '<i class="fa fa-scribd" aria-hidden="true"></i> <span>' . __('Manage Specialization') . '</span>',
                                    '/admin/Specializes/index',
                                    [
                                        'escape' => false
                                    ]
                                );
                        ?>
                    </li>        
                    <li class="<?php echo (($this->request->params['controller'] == 'Settings') && ($this->request->params['action'] == 'emailTemplateList')) ? 'active' : '' ?>">
                        <?php
                            echo $this->Html->link(
                                    '<i class="fa fa-envelope"></i> <span>' . __('Manage Email Template') . '</span>',
                                    '/admin/Settings/emailTemplateList',
                                    [
                                        'escape' => false
                                    ]
                                );
                        ?>
                    </li>         
                    <li class="<?php echo (($this->request->params['controller'] == 'Settings') && ($this->request->params['action'] == 'cmsList')) ? 'active' : '' ?>">
                        <?php
                            echo $this->Html->link(
                                    '<i class="fa fa-book"></i> <span>' . __('Manage CMS Pages') . '</span>',
                                    '/admin/Settings/cmsList',
                                    [
                                        'escape' => false
                                    ]
                                );
                        ?>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <?php
                    echo $this->Html->link(
                        '<i class="fa fa-dashboard" aria-hidden="true"></i> <span>' . __('Manage Organization') . '</span>',
                        ['controller' => 'organizations', 'action' => 'index'],
                        [
                            'escape' => false
                        ]
                    );
                ?>
            </li>

            <li class="treeview">
                <?php
                    echo $this->Html->link(
                            '<i class="fa fa-gear" aria-hidden="true"></i> <span>' . __('Manage Reports') . '</span>',
                            'javascript:void(0)',
                            [
                                'escape' => false
                            ]
                        );
                ?>
                <ul class="treeview-menu <?php echo (in_array($this->request->params['controller'], ['Settings', 'Reports', 'ReportTemplates', 'Licenses'])) ? 'menu-open' : ''?>">
                    <li class="<?php echo (($this->request->params['controller'] == 'Settings') && ($this->request->params['action'] == 'reportCategory')) ? 'active' : '' ?>">
                        <?php
                            echo $this->Html->link(
                                    '<i class="fa fa-list"></i> <span>' . __('Manage Report Categories') . '</span>',
                                    '/admin/Settings/reportCategory',
                                    [
                                        'escape' => false
                                    ]
                                );
                        ?>
                    </li>
                    <li class="<?php echo (($this->request->params['controller'] == 'Settings') && ($this->request->params['action'] == 'cmsList')) ? 'active' : '' ?>">
                        <?php
                            echo $this->Html->link(
                                '<i class="fa fa-book"></i> <span>' . __('Manage Report Template') . '</span>',
                                [
                                    'controller' => 'report-templates'
                                ],
                                [
                                    'escape' => false
                                ]
                            );
                        ?>
                    </li>
                    <li class="treeview">
                            <?php
                            echo $this->Html->link(
                                    '<i class="fa fa-th-list" aria-hidden="true"></i> <span>' . __('Manage LSE Placeholders') . '</span>',
                                    '/admin/Reports/placeholder',
                                    [
                                        'escape' => false
                                    ]
                                );
                        ?>
                    </li>
                    <li class="treeview">
                        <?php
                            echo $this->Html->link(
                                    '<i class="fa fa-folder-open" aria-hidden="true"></i> <span>' . __('Manage License') . '</span>',
                                    '/admin/Licenses/index',
                                    [
                                        'escape' => false
                                    ]
                                );
                        ?>
                    </li>
                </ul>
            </li>
            <?php 
                if($this->request->Session()->read('Auth.User.id') == 1) {
            ?>
                <li class="treeview">
                    <?php
                        echo $this->Html->link(
                            '<i class="fa fa-dashboard" aria-hidden="true"></i> <span>' . __('Manage SubAdmins') . '</span>',
                            ['controller' => 'admins', 'action' => 'list-sub-admins'],
                            [
                                'escape' => false
                            ]
                        );
                    ?>
                </li>
            <?php
                }
            ?>
            <li class="treeview">
                <?php
                    echo $this->Html->link(
                            '<i class="fa fa-gear" aria-hidden="true"></i> <span>' . __('Manage New Reports') . '</span>',
                            'javascript:void(0)',
                            [
                                'escape' => false
                            ]
                        );
                ?>
                <ul class="treeview-menu <?php echo (in_array($this->request->params['controller'], ['Settings', 'Reports', 'ReportTemplates', 'Licenses'])) ? 'menu-open' : ''?>">
                    <li class="<?php echo (($this->request->params['controller'] == 'Settings') && ($this->request->params['action'] == 'reportCategory')) ? 'active' : '' ?>">
                        <?php
                            echo $this->Html->link(
                                    '<i class="fa fa-list"></i> <span>' . __('Manage Report') . '</span>',
                                    [
                                        'controller' => 'new-reports',
                                        'action' => 'index',
                                        'prefix'    => 'admin'
                                    ],
                                    [
                                        'escape' => false
                                    ]
                                );
                        ?>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</aside>