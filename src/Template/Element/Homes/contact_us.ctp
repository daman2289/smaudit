<div class="contact-section" id='contact'>
    <div class="container">
        <h2 class="text-center"><?= __('Keep In Touch') ?></h2>
        <div class="col-sm-6 col-sm-offset-3">
            <?php
                echo 
                $this->Form->create('null',[
                    'type' =>'post',
                    'url' => ['controller' => 'Homes', 'action' =>'contact'],
                    'id' => 'contact-form'
                ]);
            ?>
                <div class="form-group">
                    <label><?= __('Name') ?></label>
                    <input type="text" name='name' class="form-control"  placeholder="Type Your Name Here" >
                </div>
                <div class="form-group">
                    <label><?= __('Email Address') ?></label>
                    <input type="email" name='email' class="form-control"  placeholder="Type Your Email Address">
                </div>
                <div class="form-group">
                    <label ><?= __('Question') ?></label>
                    <textarea class="form-control" rows="6" name='question'></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-orange" type='submit'><?= __('Submit') ?></button>
                </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<?php 
    echo $this->Html->script([
            'Contact/add'
        ],
        [
            'block' => 'scriptBottom'
    ]);
?>
<style type="text/css">
    #email-error {
    color: red;
}
#name-error {
    color:red;
}
#question-error {
    color : red;
}
</style>