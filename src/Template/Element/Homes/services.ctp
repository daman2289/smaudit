<?php
    use Cake\Core\Configure;
?>
<?php
    if(!$services->isEmpty()) {
?>
<div class="container our-services">
    <h2 class="text-center"><?= __('Our Strengths') ?></h2>
   <!--  <p class="text-center service-text">
        <?= __('The modular pricing plan enables you to select only what you really need.') ?>
    </p> -->
    <div class="service-inner clearfix">        
        <?php
            foreach ($services as $key => $value) {
        ?>
            <div class="col-sm-3 hovereffect">
                <div class="z-text">
                    <i class="<?= $value['icon_class'] ?>" aria-hidden="true"></i>
                    <p>
                        <?php if($this->request->session()->read('Config.language') == 'en_GER') { ?>
                            <?= __($value['description_german']) ?>
                        <?php } else { ?>
                            <?= __($value['description_eng']) ?>
                        <?php   } ?>
                    </p>
                </div>
                <div class="z-img">
                    <div class="service-border">
                        <i class="<?= $value['icon_class'] ?>" aria-hidden="true"></i>
                    </div>
                    <div class="text-center service-title">
                    <?php if($this->request->session()->read('Config.language') == 'en_GER') { ?>
                            <?= __($value['title_german']) ?>
                        <?php } else { ?>
                           <?= __($value['title_eng']) ?>
                        <?php   } ?>
                    </div>
                </div>
            </div>
        <?php
            } 
        ?>
    </div>
</div>
<?php 
    }
?>