<div class="pricing" id='pricing'>
    <div class="container">
        <h2 class="text-center pricing-title"><?= __('Pricing') ?></h2>
        <p class="text-center pricing-text">
            <?= 
                __('The modular pricing plan enables you to select only what you really need.') 
            ?>         
        </p>
        <div class="price-container table-responsive">
            <table class="table">
                <tbody>
                    <tr>
                        <td></td>
                    <?php
                        if(!empty($plans)) {
                            foreach ($plans as $key => $value) {
                                $ids[] = $value['id'];
                    ?>
                            <td class="text-center">
                                <?= __($value->title) ?> </br>
                                <!-- <?php
                                    if($value['price']) {                                        
                                        echo 'CHF ' .$value['price']/100;
                                        echo ($value['id'] == 2)?'+':'';
                                    }else {
                                        echo 'Free';
                                    }
                                ?> -->
                                </br>

                                <?= 
                                    ($value->additional_user_charge) 
                                    ?"+ $value->additional_user_charge for each additional user " 
                                    :''

                                ?>     </br>
                                <?php
                                    $details = explode(',', $value->additional_detail) ;
                                    if(!empty($details)){
                                        foreach ($details as $i=> $j) {
                                            echo "<h5>$j</h5>";
                                        }
                                    }
                                ?>
                            </td>
                    <?php
                            }
                        }
                    ?>
                    </tr>
                    <?php

                        $features = $this->Plans->formattPlan($planFeatures->toArray());
                        if(!empty($features)){                            
                            foreach ($features as $key => $value) {
                    ?>

                        <tr>
                            <td> 
                                <?= __($value['title']) ?>
                            </td>
                            <?php
                                if(in_array(1, $ids)) {?>
                            <td> 
                                <?php 
                                    echo ($value['1']) ? 

                                    '<i class="fa fa-check" aria-hidden="true"></i>'

                                    :'' ;
                                ?>
                            </td>

                                <?php    }

                                ?>
                                <?php 
                                    if(in_array(2, $ids)) { 
                                ?>
                            <td>

                                <?php 
                                    echo ($value['2']) ? 

                                    '<i class="fa fa-check" aria-hidden="true"></i>'

                                    :'' ;

                                ?>
                            </td>
                                <?php 
                                    } 
                                ?> 
                                <?php 
                                    if(in_array(3, $ids)) {
                                ?>
                            <td>
                                <?php  

                                    echo ($value['3']) ? 
                                    '<i class="fa fa-check" aria-hidden="true"></i>'

                                    :'' ;

                                ?> 
                            </td>

                                <?php   

                                    }

                                ?> 
                        </tr>
                    <?php
                            }
                        }
                    ?>

                </tbody>

            </table>

        </div>

    </div>

</div>