<?php
    if(!empty($faqs->toArray())) {
?>
<div class="faq" id='help'>
    <div class="container">
        <h2 class="text-center"><?= __('Frequently Asked Questions') ?></h2>
        <?php
            foreach ($faqs as $key => $value) {
        ?>
            <?php
                if($key%2 == 0) {
                    echo '<div class="row margin-faq">';
                }
            ?>       
                <div class="col-sm-6">
                    <div class="dotted-border">
                        <h4> 
                            <i class="fa fa-question-circle" aria-hidden="true"></i>
                            <span>                                  
                                <?php if($this->request->session()->read('Config.language') == 'en_GER') { ?>
                                   <?= __($value['ques_german']) ?>
                                <?php } else { ?>
                                   <?= __($value['ques_eng']) ?>
                                <?php   } ?>
                            </span>
                        </h4>
                        <p>
                            <?php if($this->request->session()->read('Config.language') == 'en_GER') { ?>
                                 <?= __($value['ans_german']) ?>
                            <?php } else { ?>
                                 <?= __($value['ans_eng']) ?>
                            <?php   } ?>
                        </p>
                    </div>
                </div>
            <?php
                if($key%2 != 0) {
                    echo '</div>';
                }
            ?>
        <?php
            }
        ?>
            <div class="text-center view-more">
                <button class="btn btn-orange"><?= __('View More') ?></button>
            </div>
        <?php        
            }
        ?>
    </div>
</div>