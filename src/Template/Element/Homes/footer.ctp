<?php 
    $this->Html->addCrumb(__('Dashboard'), ['controller' => 'Users', 'action' => 'dashboard']);
    $this->Html->addCrumb(__('Reports'), ['controller' => 'new-reports']);
    $this->Html->addCrumb(__('Edit Report'));
?>
<div class="col-sm-12 breadcrumb-no-bg">
    <div class="row">
       <?php
            echo $this->Html->getCrumbList(
                [
                    'firstClass' => false,
                    'lastClass' => 'active',
                    'class' => 'breadcrumb'
                ]
            );
        ?>
    </div>
</div>
<section class="content">
    <!-- <?=
        $this->Form->create(null,['type' => 'get']);
    ?> 
        <div class="col-sm-3 pull-right">
            <div class="input-group date pull-right" id="searchDate">
                <div class="input text">
                    <?= 
                        $this->Form->input('lan',[
                            'default' => $language,
                            'class' => 'form-control, input-lg',
                            'id'    => 'select-lang',
                            'aria-describedby'  => 'basic-addon1',
                            'options' => array(
                                'en'  =>  'English',
                                'er'   =>'German',
                                'fr'   => 'French'
                            ),
                            'label' => false
                        ]);
                    ?>
                </div>        
            </div> 
        </div>
    <?= $this->Form->end() ?> -->
    <div class="col-lg-12">                     
        <div id="emails">
        <?= $this->Form->create($footer) ?>
            
            <div class="form-group">
                <?php  
                    echo $this->Form->input('content', [
                        'type'  => 'text',
                        'class' =>'form-control', 
                        'label' => __('Footer Text')
                    ]);
                ?>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <?php echo $this->Form->label('Set font family for Footer'); ?>
                    <?php  
                        echo $this->Form->select('footer_font_family', $family, [
                            'type'  => 'text',
                            'class' =>'form-control', 
                            'label' => __('Select Font for footer')
                        ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?php echo $this->Form->label('Set font size for Footer'); ?>
                    <?php  
                        echo $this->Form->select('footer_font_size', $size, [
                            'type'  => 'text',
                            'class' =>'form-control', 
                            'label' => __('Select Font size for footer')
                        ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?php echo $this->Form->label('Set font weight for Footer'); ?>
                    <?php  
                        echo $this->Form->select('footer_font_weight', $weight,[
                            'type'  => 'text',
                            'class' =>'form-control', 
                            'label' => __('Select Font weight for footer')
                        ]);
                    ?>
                </div>
            </div>
            <div class="row">
                
                <div class="form-group col-md-4">
                    <?php echo $this->Form->label('Set font style for Footer'); ?>
                    <?php  
                        echo $this->Form->select('footer_font_style', $style,[
                            'type'  => 'text',
                            'class' =>'form-control', 
                            'label' => __('Select Font weight for footer')
                        ]);
                    ?>
                </div>
            </div>
            <div class="col-lg-8">
                <?= $this->Form->button(__('Save'), ['class' => "btn btn-primary submit-info submit_black", 'type' => 'submit']); ?>
            </div>
        <?= $this->Form->end() ?>
        </div>
    </div>
</section>
<?php 
    echo $this->Html->script([
            'Dashboard/report-editor'
        ], 
        [
            'block' => 'scriptBottom'
        ]
    ); 
?>
