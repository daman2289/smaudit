<?php
    if(!$specialize->isEmpty()) {
?>
<div class="container special-services" id ="specialize-section">
    <h2 class="text-center"><?= __('Our Goal') ?></h2>
    <?php
        foreach ($specialize as $key => $value) {
    ?>
        <div class="col-sm-4">
            <div class="specialize-border">
                <?=
                    $this->Html->image($this->Common->specializesIcon('specializes','icon', $value['icon_dir'], 'square_', $value['icon']));
                ?>
                <div class="specialize-heading">
                <?php if($this->request->session()->read('Config.language') == 'en_GER') { ?>
                    <?= __($value['title_german']) ?>
                <?php } else { ?>
                    <?= __($value['title_eng']) ?>
                <?php   } ?>
                </div>
                <p>
                    <?php if($this->request->session()->read('Config.language') == 'en_GER') { ?>
                    <?= __($value['description_german']) ?>
                <?php } else { ?>
                    <?= __($value['description_eng']) ?>
                <?php   } ?>
                </p>
            </div>
        </div>
    <?php } ?>
</div>
<?php  
    }
?>