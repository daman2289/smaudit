<div id='question-block' class='hide'>
	<div class="form-group">
		<?=  
			$this->Form->control('new_questions.0.new_question_type_id', [
					'options' => $questionTypes,
					'onChange' => 'placeOptionForQuestion(this.value)',
					'class' => 'form-control'
				]
			);
		?>
	</div>
	<div class="form-group" id='question-input'>
		<?= $this->Form->control('new_questions.0.question_en', ['class' => 'form-control', 'required']); ?>
	</div>
	<!-- <div class="form-group">
		<?=  $this->Form->control('new_questions.0.question_er', ['class' => 'form-control', 'required']); ?>
	</div>
	<div class="form-group">
		<?= $this->Form->control('new_questions.0.question_fr', ['class' => 'form-control']); ?>
	</div> -->
</div>