<div id='normal-block' class='hide'>    
	<div class="form-group">
    	<?= $this->Form->input('content_en', ['label' => 'Replacement text in english', 'class' => 'form-control']) ?>
	</div>    
	<div class="form-group">		
    	<?= $this->Form->input('content_er', ['label' => 'Replacement text in german', 'class' => 'form-control']) ?>
	</div>    
	<div class="form-group">		
    	<?= $this->Form->input('content_fr', ['label' => 'Replacement text in french', 'class' => 'form-control']) ?>
	</div>
</div>