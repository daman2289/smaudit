<div id='legal-form-block' class='hide'>  
    <?php 
        if(!empty($formTypes->toArray()) ):
            foreach ($formTypes as $key => $value):
            echo $this->Form->hidden("legal_form_replacements.$key.legal_form_type_id",[
                    'value' => $key
                ]);
    ?>
    <div class="row" style="">
        <h3><strong><?= $value ?></strong></h3>
        <div class="form-group col-md-4">
            <?php 
                echo $this->Form->input("legal_form_replacements.$key.content_en", [
                        'type'  => 'textarea',
                        'rows'  => 4,
                        'label' => 'Replacement text in ENGLISH', 
                        'class' => 'form-control'
                    ]) 
            ?>
        </div>    
        <div class="form-group col-md-4">        
            <?php 
                echo $this->Form->input("legal_form_replacements.$key.content_er", [
                        'type'  => 'textarea',
                        'rows'  => 4,
                        'label' => 'Replacement text in GERMAN', 
                        'class' => 'form-control'
                    ]) 
            ?>
        </div>    
        <div class="form-group col-md-4">        
            <?php 
                echo $this->Form->input("legal_form_replacements.$key.content_fr", [
                        'type'  => 'textarea',
                        'rows'  => 4,
                        'label' => 'Replacement text in FRENCH', 
                        'class' => 'form-control'
                    ]) 
            ?>
        </div>        
    </div>
    <?php
            endforeach;
        endif;
    ?>
</div>