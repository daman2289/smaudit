<?php
	$type = $placeholder->new_placeholder_type_id;
?>
<div id='legal-form-block' class='<?= $type == 6?"" :"hide"?>'>
	<?php 
        if($type == 6 && !empty($placeholder->legal_form_replacements)):
            foreach ($placeholder->legal_form_replacements as $key => $value) :
            	echo $this->Form->hidden("legal_form_replacements.$key.id", [
            		'value' => $value->id
            	]);
    ?>
		<div class="row" style="">
	        <h3><strong><?= $value->legal_form_type->name ?></strong></h3>
	        <div class="form-group col-md-4">
	            <?php 
	                echo $this->Form->input("legal_form_replacements.$key.content_en", [
	                        'type'  => 'textarea',
	                        'rows'  => 4,
	                        'label' => 'Replacement text in ENGLISH', 
	                        'class' => 'form-control',
	                        'value'	=> $value->content_en
	                    ]) 
	            ?>
	        </div>    
	        <div class="form-group col-md-4">        
	            <?php 
	                echo $this->Form->input("legal_form_replacements.$key.content_er", [
	                        'type'  => 'textarea',
	                        'rows'  => 4,
	                        'label' => 'Replacement text in GERMAN', 
	                        'class' => 'form-control',
	                        'value'	=> $value->content_er
	                    ]) 
	            ?>
	        </div>    
	        <div class="form-group col-md-4">        
	            <?php 
	                echo $this->Form->input("legal_form_replacements.$key.content_fr", [
	                        'type'  => 'textarea',
	                        'rows'  => 4,
	                        'label' => 'Replacement text in FRENCH', 
	                        'class' => 'form-control',
	                        'value'	=> $value->content_fr
	                    ]) 
	            ?>
	        </div>        
	    </div>
	<?php
			endforeach;
		endif;	
	?>
</div>