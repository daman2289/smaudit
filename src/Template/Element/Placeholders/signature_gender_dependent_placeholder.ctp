<div id='signature-gender-form-block' class='hide'>  
    <div>
    	<?= $this->Form->label('Depends Upon Signature') ?>
    	<?= 
    		$this->Form->select('signature',[
    			'[FirstSignature]' 	=> '[FirstSignature]',
    			'[SecondSignature]' => '[SecondSignature]'
    		],[
    			'class' => 'form-control'
    		])
    	?>
    </div>
    <div class="row" style="">
        <h3><strong>Male</strong></h3>
        <div class="form-group col-md-4">
        	<?php 
                echo $this->Form->hidden("signature_gender_replacements.0.gender", [
                        'value'	=> 1
                    ]) 
            ?>
            <?php 
                echo $this->Form->input("signature_gender_replacements.0.content_en", [
                        'type'  => 'textarea',
                        'rows'  => 4,
                        'label' => 'Replacement text in ENGLISH', 
                        'class' => 'form-control'
                    ]) 
            ?>
        </div>    
        <div class="form-group col-md-4">        
            <?php 
                echo $this->Form->input("signature_gender_replacements.0.content_er", [
                        'type'  => 'textarea',
                        'rows'  => 4,
                        'label' => 'Replacement text in GERMAN', 
                        'class' => 'form-control'
                    ]) 
            ?>
        </div>    
        <div class="form-group col-md-4">        
            <?php 
                echo $this->Form->input("signature_gender_replacements.0.content_fr", [
                        'type'  => 'textarea',
                        'rows'  => 4,
                        'label' => 'Replacement text in FRENCH', 
                        'class' => 'form-control'
                    ]) 
            ?>
        </div>        
    </div>
    <div class="row" style="">
        <h3><strong>Female</strong></h3>
        <div class="form-group col-md-4">
            <?php 
            	echo $this->Form->hidden("signature_gender_replacements.1.gender", [
                        'value'	=> 2
                    ]) ;
                echo $this->Form->input("signature_gender_replacements.1.content_en", [
                        'type'  => 'textarea',
                        'rows'  => 4,
                        'label' => 'Replacement text in ENGLISH', 
                        'class' => 'form-control'
                    ]) 
            ?>
        </div>    
        <div class="form-group col-md-4">        
            <?php 
                echo $this->Form->input("signature_gender_replacements.1.content_er", [
                        'type'  => 'textarea',
                        'rows'  => 4,
                        'label' => 'Replacement text in GERMAN', 
                        'class' => 'form-control'
                    ]) 
            ?>
        </div>    
        <div class="form-group col-md-4">        
            <?php 
                echo $this->Form->input("signature_gender_replacements.1.content_fr", [
                        'type'  => 'textarea',
                        'rows'  => 4,
                        'label' => 'Replacement text in FRENCH', 
                        'class' => 'form-control'
                    ]) 
            ?>
        </div>        
    </div>
</div>