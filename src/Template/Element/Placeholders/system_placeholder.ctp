<div id='system-block' class='hide'>
    <div class="form-group">        
        <?= $this->Form->select('content_en', [
                'Organization' => [
                    'organization_name' => 'Name',
                    'organization_website' => 'Website',
                    'organization_phone' => 'Phone',
                    'organization_street' => 'Street',
                    'organization_place' => 'Place',
                ],
                'Company' => [
                    'company_name' => 'Name',
                    'company_phone' => 'Phone',
                    'company_street' => 'Street',
                    'company_place' => 'Place',
                    'company_place_of_incorporation' => 'Place Incorporation',
                ],
                'Subsidiary' => [
                    'subsidiary_name' => 'Name',
                    'subsidiary_phone' => 'Phone',
                    'subsidiary_street' => 'Street',
                    'subsidiary_street_number' => 'Street No',
                    'subsidiary_place' => 'Place',
                    'subsidiary_fax' => 'Fax',
                    'subsidiary_zip' => 'Zip'
                ]
            ], ['empty' => 'Choose option', 'class' => 'form-control']) 
        ?>
    </div>    
</div>