<?php use Cake\Core\Configure; ?>
<nav class="navbar navbar-default navbar-fixed-top top-nav-collapse">
    <?php echo $this->Flash->render(); ?>
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <?php
                echo $this->Form->button(
                        '<span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>',
                        [
                            'class' => 'navbar-toggle collapsed',
                            'data-toggle' => 'collapse',
                            'data-target' => '#bs-example-navbar-collapse-1',
                            'aria-expanded' => false,
                            'type' => 'button'
                        ]
                    );
            ?>
            <?php
                echo $this->Html->link(
                    $this->Html->image('mini-logo.png', ['class' => 'img-responsive', 'alt' => 'logo']),
                    '#page-top',
                    [
                        'class' => 'logo-mini page-scroll text-right  hidden-md hidden-sm hidden-lg',
                        'escape' => false
                    ]
                );
            ?>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="hidden">
                    <?php echo $this->Html->link('',
                                '#page-top',
                                [
                                    'class' => 'page-scroll'
                                ]);
                    ?>
                </li>
                <li class="hidden-xs">
                    <?php
                        echo $this->Html->link(
                            $this->Html->image('mini-logo.png', ['class' => 'img-responsive', 'alt' => 'logo']),
                            '#page-top',
                            [
                                'class' => 'logo-mini page-scroll text-right',
                                'escape' => false
                            ]
                        );
                    ?>
                </li>
                <li>
                    <?php echo $this->Html->link(__('How It Works'),
                                '#how-it-works',
                                [
                                    'class' => 'page-scroll'
                                ]);
                    ?>
                </li>
                <li>
                    <?php echo $this->Html->link(__('Why Use STASH'),
                                '#why-use-stash',
                                [
                                    'class' => 'page-scroll'
                                ]);
                    ?>
                </li>
                <li>
                    <?php echo $this->Html->link(__('Rent Out Your Space'),
                                '#rent-out-space',
                                [
                                    'class' => 'page-scroll'
                                ]);
                    ?>
                </li>
                <li class="english">
                    <?php
                        echo $this->Html->link(
                            $this->Html->image('english.png', ['alt' => 'English']) . ' &nbsp; <span>' . __('English') . '</span> ',
                            '#page-top',
                            [
                                'class' => 'page-scroll',
                                'escape' => false
                            ]
                        );
                    ?>
                </li>
                <li class="english">
                    <?php
                        echo $this->Html->link(
                            $this->Html->image('japan.png', ['alt' => 'English']) . ' &nbsp; <span>' . __('Japanese') . '</span> ',
                            '#page-top',
                            [
                                'class' => 'page-scroll',
                                'escape' => false
                            ]
                        );
                    ?>
                </li>
                <li class="dropdown login-popover">
                    <?php
                        echo $this->Html->link(
                            __('LOGIN') . '<b class="caret"></b>',
                            'javascript:void(0)',
                            [
                                'class' => 'dropdown-toggle',
                                'escape' => false,
                                'data-toggle' => 'dropdown',
                                'aria-expanded' => true
                            ]
                        );
                    ?>
                    <ul class="dropdown-menu dropdown-menu-left" id="login-form" style="padding: 15px;min-width: 250px;">
                        <li class="login-area">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php echo $this->Form->create(null, [
                                                'url' => [
                                                    'controller' => 'Users',
                                                    'action' => 'login'
                                                ]
                                            ]);
                                    ?>
                                        <div class="form-group">
                                            <?php
                                                echo $this->Html->link(
                                                    '<i class="fa fa-facebook-square" aria-hidden="true"></i> ' . __('Continue with Facebook'),
                                                    [
                                                        'controller' => 'Users',
                                                        'action' => 'login',
                                                        '?' => [
                                                            'provider' => 'Facebook'
                                                        ]
                                                    ],
                                                    [
                                                        'class' => 'btn btn-fb',
                                                        'escape' => false,
                                                    ]
                                                );
                                            ?>
                                        </div>
                                        <div class="form-group">
                                            <?php
                                                echo $this->Html->link(
                                                    '<i class="fa fa-google-plus-square" aria-hidden="true"></i> ' . __('Continue with Gmail'),
                                                    [
                                                        'controller' => 'Users',
                                                        'action' => 'login',
                                                        '?' => [
                                                            'provider' => 'Google'
                                                        ]
                                                    ],
                                                    [
                                                        'class' => 'btn btn-google',
                                                        'escape' => false,
                                                    ]
                                                );
                                            ?>
                                        </div>
                                        <span class="or-sep"><span class="or-sep-inner"><?php echo __('or'); ?></span></span>
                                        <div class="form-group">                                        
                                            <?php
                                                echo $this->Form->input('email', [
                                                        'class' => 'form-control',
                                                        'placeholder' => __('Email Address'),
                                                        'label' => false
                                                    ]);
                                            ?>
                                        </div>
                                        <div class="form-group">                                        
                                            <?php
                                                echo $this->Form->password('password', [
                                                        'class' => 'form-control',
                                                        'placeholder' => __('Password')
                                                    ]);
                                            ?>
                                        </div>
                                        <div class="forgot-pass text-center form-group">
                                            <?php
                                                echo $this->Html->link(__('Forgot password?'),
                                                    'javascript:void(0)'
                                                );
                                            ?>
                                        </div>
                                        <div class="form-group">
                                            <?php
                                                echo $this->Form->button(__('Log in'), [
                                                        'class' => 'btn btn-green-login btn-block',
                                                        'type' => 'submit'
                                                    ]);
                                            ?>
                                        </div>
                                    <?php echo $this->Form->end(); ?>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li>
                    <?php echo $this->Html->link(__('SIGN UP'),
                                '/Users/signup'
                            );
                    ?>
                </li>
                <li>
                    <?php echo $this->Html->link(__('ABOUT'),
                                '/about-us'
                            );
                    ?>
                </li>
                <li class="dropdown login-popover contact-wraper contact-img">
                    <?php echo $this->Html->link(
                                $this->Html->image('contact-icon.png') . ' ' . __('Contact Us'),
                                'javascript:void(0)',
                                [
                                    'class' => 'dropdown-toggle',
                                    'data-toggle' => 'dropdown',
                                    'aria-expanded' => true,
                                    'escape' => false
                                ]
                            );
                    ?>
                    <ul class="dropdown-menu dropdown-menu-right" id="login-forms" style="padding: 15px;min-width: 250px;">
                        <li class="login-area">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php echo $this->Form->create(null); ?>
                                        <div class="form-group">
                                            <?php
                                                echo $this->Form->button(
                                                        '<i class="fa fa-envelope-o" aria-hidden="true"></i><span>' . __('Email Us') . '</span>',
                                                        [
                                                            'class' => 'btn btn-email button',
                                                            'type' => 'button',
                                                            'data-hover' => 'park@stashmyride.com',
                                                            'data-active' => 'park@stashmyride.com'
                                                        ]
                                                    );
                                            ?>
                                        </div>
                                        <div class="form-group">
                                            <?php
                                                echo $this->Form->button(
                                                        '<i class="fa fa-phone" aria-hidden="true"></i><span>' . __('Call Us') . '</span>',
                                                        [
                                                            'class' => 'btn btn-call button',
                                                            'type' => 'button',
                                                            'data-hover' => '+1 (808) 753-5233',
                                                            'data-active' => '+1 (808) 753-5233'
                                                        ]
                                                    );
                                            ?>
                                        </div>
                                        <div class="form-group">
                                            <?php
                                                echo $this->Form->button(
                                                        '<i class="fa fa-facebook-square" aria-hidden="true"></i> ' . __('Connect on Facebook'),
                                                        [
                                                            'class' => 'btn btn-fb',
                                                            'type' => 'button'
                                                        ]
                                                    );
                                            ?>
                                        </div>
                                        <div class="form-group">
                                            <?php
                                                echo $this->Form->button(
                                                        '<i class="fa fa-twitter-square" aria-hidden="true"></i> ' . __('Connect on Twitter'),
                                                        [
                                                            'class' => 'btn btn-twitter',
                                                            'type' => 'button'
                                                        ]
                                                    );
                                            ?>
                                        </div>
                                        <div class="form-group">
                                            <?php
                                                echo $this->Form->button(
                                                        '<i class="fa fa-instagram" aria-hidden="true"></i> ' . __('Connect on Instagram'),
                                                        [
                                                            'class' => 'btn btn-instagram',
                                                            'type' => 'button'
                                                        ]
                                                    );
                                            ?>
                                        </div>
                                        <div class="form-group">
                                            <?php
                                                echo $this->Form->button(
                                                        '<i class="fa fa-youtube" aria-hidden="true"></i> ' . __('Connect on Youtube'),
                                                        [
                                                            'class' => 'btn btn-youtube',
                                                            'type' => 'button'
                                                        ]
                                                    );
                                            ?>
                                        </div>
                                    <?php echo $this->Form->end(); ?>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
<section id="intro" class="intro-section">
    <div class="top-image">
        <div class="container">
            <div class="text-section">
                <?php
                    echo $this->Html->link(
                        $this->Html->image('logo.png', ['class' => 'img-responsive', 'alt' => 'logo']),
                        'javascript:void(0)',
                        [
                            'class' => 'logo cursor-default',
                            'escape' => false
                        ]
                    );
                ?>
                <div class="border-thick">
                    <div class="main-heading-banner">
                        <h1><?php echo __('The simple way to Park'); ?></h1>
                        <p><?php echo __('Connecting drivers with parking space owners'); ?></p>
                    </div>
                    <div class="rent-find col-sm-10 col-sm-offset-1">
                        <div class="col-sm-12 col-lg-4 border-left-start">
                            <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                            <?php
                                echo $this->Form->input('search', [
                                        'class' => 'form-control',
                                        'placeholder' => __('Find Your Parking Spot'),
                                        'label' => false
                                    ]);
                            ?>
                        </div>
                        <div class="col-sm-12 col-lg-3 border-left ">
                            <?php
                                echo $this->Form->button(__('Find Now'), [
                                        'class' => 'btn btn-orange btn-block',
                                        'type' => 'button'
                                    ]);
                            ?>
                        </div>
                        <div class="col-sm-12 col-lg-5 border-left-start-left">
                            <i class="map-marker"> </i>
                            <?php
                                echo $this->Form->button(__('Find Spots Nearby'), [
                                        'class' => 'btn btn-green btn-block',
                                        'type' => 'button'
                                    ]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- About Section -->
<div class="bg-works">
    <section id="how-it-works" class="how-it-works">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-center wow zoomIn"><?php echo __('How It Works'); ?></h2>
                    <p class="how-text">
                        <?php echo __('This is Photoshop\'s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus'); ?>
                    </p>
                    <div class="col-sm-12 col-lg-7 desktop-img wow slideInLeft">
                        <?php echo $this->Html->image('how-it-computer.png', ['class' => 'img-responsive']); ?>
                    </div>
                    <div class="col-sm-12 col-lg-5 how-right-section">
                        <div class="row list-right-section wow slideInRight">
                            <div class="col-sm-4">
                                <?php echo $this->Html->image('find-it.png'); ?>
                            </div>
                            <div class="col-sm-8 text-right-how">
                                <h3><?php echo __('Find It !'); ?></h3>
                                <p><?php echo __('You can easily find where you want to park through the map.'); ?></p>
                            </div>
                        </div>
                        <div class="row list-right-section wow slideInRight">
                            <div class="col-sm-4">
                                <?php echo $this->Html->image('book.png'); ?>
                            </div>
                            <div class="col-sm-8 text-right-how">
                                <h3><?php echo __('Book It !'); ?></h3>
                                <p><?php echo __('Just find and pay and book your parking spot.'); ?></p>
                            </div>
                        </div>
                        <div class="row list-right-section wow slideInRight">
                            <div class="col-sm-4">
                                <?php echo $this->Html->image('rent.png'); ?>
                            </div>
                            <div class="col-sm-8 text-right-how">
                                <h3><?php echo __('Rent It !'); ?></h3>
                                <p><?php echo __('You can easily find you where you want to park.'); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<section id="why-use-stash" class="why-use-stash">
    <div class="stash-top-img">
        <?php echo $this->Html->image('home-img.png'); ?>
    </div>
    <div class="container">
        <div class="col-lg-12 stash-img">
            <h2 class="text-center wow slideInLeft"><?php echo __('Why Use Stash'); ?></h2>
            <div class="col-sm-4 wow slideInLeft">
                <?php echo $this->Html->image('park-in-peace.png', ['alt' => 'park']); ?>
                <h3><?php echo __('Park In Peace'); ?></h3>
                <p><?php echo __('This is Photoshop\'s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.'); ?></p>
            </div>
            <div class="col-sm-4 wow slideInUp">
                <?php echo $this->Html->image('save-money.png', ['alt' => 'park']); ?>
                <h3><?php echo __('Save Money'); ?></h3>
                <p><?php echo __('This is Photoshop\'s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.'); ?></p>
            </div>
            <div class="col-sm-4 wow slideInRight">
                <?php echo $this->Html->image('save-time.png', ['alt' => 'park']); ?>
                <h3><?php echo __('Save Time'); ?></h3>
                <p><?php echo __('This is Photoshop\'s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.'); ?></p>
            </div>
        </div>
    </div>
</section>

<!-- About Section -->
<div class="rent-bg">
    <section id="rent-out-space" class="rent-out-space">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-center"><?php echo __('Rent Out Your Space'); ?></h2>
                    <p class="how-text">
                        <?php echo __('This is Photoshop\'s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio.'); ?>
                    </p>
                    <div class="col-sm-12 col-lg-5 how-right-section">
                        <div class="row list-right-section wow slideInLeft">
                            <div class="col-sm-4">
                                <?php echo $this->Html->image('list-space.png'); ?>
                            </div>
                            <div class="col-sm-8 text-right-how">
                                <h3><?php echo __('List your Space'); ?></h3>
                                <p><?php echo __('Fill out the listing form with your parking space details.'); ?></p>
                            </div>
                        </div>
                        <div class="row list-right-section wow slideInLeft">
                            <div class="col-sm-4">
                                <?php echo $this->Html->image('control-availability.png'); ?>
                            </div>
                            <div class="col-sm-8 text-right-how">
                                <h3><?php echo __('Control Availability'); ?></h3>
                                <p><?php echo __('Choose on which days your space will be available for booking.'); ?></p>
                            </div>
                        </div>
                        <div class="row list-right-section wow slideInLeft">
                            <div class="col-sm-4">
                                <?php echo $this->Html->image('earn-money.png'); ?>
                            </div>
                            <div class="col-sm-8 text-right-how">
                                <h3><?php echo __('Earn'); ?></h3>
                                <p><?php echo __('Receive a monthly check or deposit for an easy way to make extra money.'); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-7 desktop-img">
                        <?php echo $this->Html->image('how-it-computer.png', ['class' => 'img-responsive']); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>