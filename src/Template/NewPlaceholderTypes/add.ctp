<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List New Placeholder Types'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List New Placeholders'), ['controller' => 'NewPlaceholders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New New Placeholder'), ['controller' => 'NewPlaceholders', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="newPlaceholderTypes form large-9 medium-8 columns content">
    <?= $this->Form->create($newPlaceholderType) ?>
    <fieldset>
        <legend><?= __('Add New Placeholder Type') ?></legend>
        <?php
            echo $this->Form->control('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
