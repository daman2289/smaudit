<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit New Placeholder Type'), ['action' => 'edit', $newPlaceholderType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete New Placeholder Type'), ['action' => 'delete', $newPlaceholderType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $newPlaceholderType->id)]) ?> </li>
        <li><?= $this->Html->link(__('List New Placeholder Types'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New New Placeholder Type'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List New Placeholders'), ['controller' => 'NewPlaceholders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New New Placeholder'), ['controller' => 'NewPlaceholders', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="newPlaceholderTypes view large-9 medium-8 columns content">
    <h3><?= h($newPlaceholderType->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($newPlaceholderType->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($newPlaceholderType->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($newPlaceholderType->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($newPlaceholderType->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related New Placeholders') ?></h4>
        <?php if (!empty($newPlaceholderType->new_placeholders)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('New Report Id') ?></th>
                <th scope="col"><?= __('New Placeholder Type Id') ?></th>
                <th scope="col"><?= __('Uid') ?></th>
                <th scope="col"><?= __('Lft') ?></th>
                <th scope="col"><?= __('Rght') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Content En') ?></th>
                <th scope="col"><?= __('Content Er') ?></th>
                <th scope="col"><?= __('Content Fr') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($newPlaceholderType->new_placeholders as $newPlaceholders): ?>
            <tr>
                <td><?= h($newPlaceholders->id) ?></td>
                <td><?= h($newPlaceholders->new_report_id) ?></td>
                <td><?= h($newPlaceholders->new_placeholder_type_id) ?></td>
                <td><?= h($newPlaceholders->uid) ?></td>
                <td><?= h($newPlaceholders->lft) ?></td>
                <td><?= h($newPlaceholders->rght) ?></td>
                <td><?= h($newPlaceholders->name) ?></td>
                <td><?= h($newPlaceholders->content_en) ?></td>
                <td><?= h($newPlaceholders->content_er) ?></td>
                <td><?= h($newPlaceholders->content_fr) ?></td>
                <td><?= h($newPlaceholders->created) ?></td>
                <td><?= h($newPlaceholders->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'NewPlaceholders', 'action' => 'view', $newPlaceholders->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'NewPlaceholders', 'action' => 'edit', $newPlaceholders->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'NewPlaceholders', 'action' => 'delete', $newPlaceholders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $newPlaceholders->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
