<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New New Placeholder Type'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List New Placeholders'), ['controller' => 'NewPlaceholders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New New Placeholder'), ['controller' => 'NewPlaceholders', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="newPlaceholderTypes index large-9 medium-8 columns content">
    <h3><?= __('New Placeholder Types') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($newPlaceholderTypes as $newPlaceholderType): ?>
            <tr>
                <td><?= $this->Number->format($newPlaceholderType->id) ?></td>
                <td><?= h($newPlaceholderType->name) ?></td>
                <td><?= h($newPlaceholderType->created) ?></td>
                <td><?= h($newPlaceholderType->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $newPlaceholderType->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $newPlaceholderType->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $newPlaceholderType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $newPlaceholderType->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
