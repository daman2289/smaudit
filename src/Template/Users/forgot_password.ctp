 <div class="container">
    <div class="col-lg-6 sign-up-outer  col-sm-8 col-sm-offset-2 col-lg-offset-3">
        <div class="login-wraper clearfix">
            <h3 class="text-center"><?php echo __('Forgot Password'); ?></h3>
            <p class="text-center"><?php echo __('Enter your E-Mail address and we will send you a reset password link'); ?> </p>
            <?php
                    echo $this->Form->create(null,
                            array(
                                    'type' => 'post',
                                    'url' => array(
                                            'controller' => 'Users',
                                            'action' => 'forgotPassword'
                                        ),
                                    'id' => 'forgot-password'
                                    
                                )
                        );
                ?>
                <div class="form-group right-inner-addon">
                    <label><?php __('Email Address'); ?></label>
                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                    <?php
                        echo $this->Form->input('email',
                                array(
                                        'class' => 'form-control',
                                        'placeholder' => __('Enter your registered Email Address'),
                                        'div' => false,
                                        'label' => false,
                                        'required' => true,
                                        'type' => 'email'
                                    )
                            );
                    ?>
                </div>
                <div class=" text-center form-group">
                    <?php
                        echo $this->Form->button(__('Submit'),[
                                'class' => 'btn btn-blue btn-block',
                                'type' => 'submit'
                            ]);
                    ?>
                </div>
                <div class="col-sm-12 login-bottom-text text-center">
                     <?php echo $this->Html->link(__('Back to Sign in'),
                        '/Users/login'
                    );?>
                </div>
            </form>
        </div>
    </div>
</div>
<?php echo $this->Html->script('Frontend/Users/login_register', ['block' => 'scriptBottom']); ?>