<div class="container">
    <div class="col-lg-6 sign-up-outer  col-sm-8 col-sm-offset-2 col-lg-offset-3">
        <div class="login-wraper clearfix">
            <h3 class="text-center"><?php echo __('Login Now'); ?></h3>
                <?php
                    echo $this->Form->create(null,
                            array(
                                    'type' => 'post',
                                    'id' => 'login'
                                    
                                )
                        );
                ?>
                <div class="form-group right-inner-addon">
                    <label><?php echo __('Email address'); ?></label>
                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                    <?php
                        echo $this->Form->input('email',
                                array(
                                        'class' => 'form-control',
                                        'placeholder' => __('Email Address'),
                                        'div' => false,
                                        'label' => false,
                                        'required' => true,
                                        'type' => 'email'
                                    )
                            );
                    ?>
                </div>
                <div class="form-group right-inner-addon">
                    <label><?= __('Password') ?></label>
                    <i class="fa fa-key" aria-hidden="true"></i>
                    <?php
                        echo $this->Form->input('password',
                                array(
                                        'class' => 'form-control',
                                        'placeholder' => __('Password'),
                                        'div' => false,
                                        'label' => false,
                                        'required' => true

                                    )
                            );
                    ?>
                </div>
                <div class="form-group clearfix">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="checkbox remember-text">
                                <label>
                                <input type="checkbox"> <?= __('Remember Me') ?>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12 text-right forget-text">
                            <a href=""></a>
                            <?php echo $this->Html->link(__('Forgot Password?'),
                                array(
                                    'controller' => 'users',
                                    'action'    => 'forgot-password'
                                )
                            );?>
                        </div>
                    </div>
                </div>
                <div class=" text-center form-group">
                    <?php
                        echo $this->Form->button(__('Login Now'),[
                                'class' => 'btn btn-blue btn-block',
                                'type' => 'submit'
                            ]);
                    ?>
                </div>
            </form>
        </div>
    </div>
</div>
<?php 
    echo $this->Html->script(
        'Frontend/Users/login_register', 
        ['block' => 'scriptBottom']); 
?>