 <div class="container">
    <div class="col-lg-6 sign-up-outer  col-sm-8 col-sm-offset-2 col-lg-offset-3">
        <div class="login-wraper clearfix">
            <h3 class="text-center"><?php echo __('Recover Password'); ?></h3>
            <?php
                    echo $this->Form->create(null,
                            array(
                                    'type' => 'post',
                                    'id' => 'recover-password'
                                    
                                )
                        );
                ?>
                <div class="form-group right-inner-addon">
                    <label><?php __('New Password'); ?></label>
                    <i class="fa fa-key" aria-hidden="true"></i>
                    <?php
                        echo $this->Form->input('password',
                                array(
                                        'class' => 'form-control',
                                        'placeholder' => __('Enter your new password'),
                                        'div' => false,
                                        'label' => false,
                                        'required' => true,
                                        'type' => 'password'
                                    )
                            );
                    ?>
                </div>
                <div class="form-group right-inner-addon">
                    <label><?php __('Confirm Password'); ?></label>
                    <i class="fa fa-key" aria-hidden="true"></i>
                    <?php
                        echo $this->Form->input('new_password',
                                array(
                                        'class' => 'form-control',
                                        'placeholder' => __('Enter Confirm password'),
                                        'div' => false,
                                        'label' => false,
                                        'required' => true,
                                        'type' => 'password'
                                    )
                            );
                    ?>
                </div>
                <div class=" text-center form-group">
                    <?php
                        echo $this->Form->button(__('Submit'),[
                                'class' => 'btn btn-blue btn-block',
                                'type' => 'submit'
                            ]);
                    ?>
                </div>
            </form>
        </div>
    </div>
</div>
<?php echo $this->Html->script('Frontend/Users/login_register', ['block' => 'scriptBottom']); ?>