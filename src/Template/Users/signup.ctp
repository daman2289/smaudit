<?php use Cake\Core\Configure; ?>
<div class="container">
    <div class="col-lg-6 sign-up-outer  col-sm-8 col-sm-offset-2 col-lg-offset-3">
        <div class="login-wraper clearfix">
            <h3 class="text-center"><?php echo __('Registration'); ?></h3>
            <p class="text-center registrion-text">
                <?php echo __('Sign up to join SMAUDIT,S trusted community');?>
            </p>
                <?php
                    echo $this->Form->create(null,
                            array(
                                    'type' => 'post',
                                    'url' => array(
                                            'controller' => 'Users',
                                            'action' => 'signup'
                                        ),
                                    'id' => 'signup'
                                    
                                )
                        );
                ?>
                <div class="form-group right-inner-addon">
                    <label><?php echo __('Email address'); ?></label>
                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                    <?php
                        echo $this->Form->input('email',
                                array(
                                        'class' => 'form-control',
                                        'placeholder' => __('Email Address'),
                                        'div' => false,
                                        'label' => false,
                                        'required' => true,
                                        'type' => 'email'
                                    )
                            );
                    ?>
                </div>
                <div class="form-group right-inner-addon">
                    <label><?php echo __('Name of Organization');?></label>
                    <i class="fa fa-university" aria-hidden="true"></i>
                    <?php
                        echo $this->Form->input('organization.name',
                                array(
                                        'class' => 'form-control',
                                        'placeholder' => __('Organization Name'),
                                        'div' => false,
                                        'label' => false,
                                        'required' => true                                   
                                    )
                            );
                    ?>
                </div>
                <div class="form-group right-inner-addon">
                    <label><?php echo __('Phone Number');?></label>
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <?php
                        echo $this->Form->input('organization.phone',
                                array(
                                        'class' => 'form-control',
                                        'placeholder' => __('Type Your Phone Number'),
                                        'div' => false,
                                        'label' => false,
                                        'required' => true,
                                        'id' => 'register-phone'                                
                                    )
                            );
                    ?>
                </div>
                <div class="form-group">
                    <label><?php echo __('Select Langauge'); ?></label>
                    <div class="custom-select">
                        <?php
                        echo $this->Form->input('language',
                                array(
                                        'class' => 'form-control',
                                        'placeholder' => __('Languages'),
                                        'div' => false,
                                        'label' => false,
                                        'required' => true,
                                        'options' => Configure::read('Languages')

                                    )
                            );
                        ?>
                    </div>
                </div>
                <div class="form-group right-inner-addon">
                    <label><?php echo __('Password'); ?></label>
                    <i class="fa fa-key" aria-hidden="true"></i>
                    <?php
                        echo $this->Form->input('password',
                                array(
                                        'class' => 'form-control',
                                        'placeholder' => __('Password'),
                                        'div' => false,
                                        'label' => false,
                                        'required' => true

                                    )
                            );
                    ?>
                </div>
                <div class="form-group right-inner-addon">
                    <label><?php echo __('Confirm Password'); ?></label>
                    <i class="fa fa-key" aria-hidden="true"></i>
                    <?php
                        echo $this->Form->input('confirm_password',
                            array(
                                'class' => 'form-control',
                                'placeholder' => __('Confirm Password'),
                                'div' => false,
                                'label' => false,
                                'required' => true,
                                'type' => 'password'
                            )
                        );
                    ?>
                </div>
                <div class="form-group clearfix">
                    <div class="checkbox remember-text">
                        <label>
                        <input name = "terms" type="checkbox"><?php echo __('I agree to'); ?>
                           
                            <?php
                                echo $this->Html->link(
                                    __('Term and Conditions'),
                                    [
                                        'controller' => 'homes',
                                        'action'    =>'terms',
                                        base64_encode(1)
                                    ],
                                    [
                                        'target'=> '_blank'
                                    ]
                                );
                            ?>
                        </label>
                    </div>
                </div>
                <div class=" text-center form-group">
                    <?php
                        echo $this->Form->button(__('Register Now'),[
                                'class' => 'btn btn-blue btn-block',
                                'type' => 'submit'
                            ]);
                    ?>
                </div>
                <div class=" login-bottom-text text-center"><?= __('Already have an Account') ?>? 
                    
                    <?php echo $this->Html->link(__('Login'),
                        '/Users/login'
                    );?>
                </div>
            </form>
        </div>
    </div>
</div>
<?php echo $this->Html->script('Frontend/Users/login_register', ['block' => 'scriptBottom']); ?>