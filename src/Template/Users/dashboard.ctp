<?php
    use Cake\Filesystem\File;
    use Cake\Core\Configure;
?>

    <div class="dashboard-top clearfix">
        <div class="col-xs-12 col-sm-6 col-lg-3">
            <div class="outer-box-blue clearfix">
                <div class="col-xs-5 left-blue-box text-center">
                    <i class="fa fa-users" aria-hidden="true"></i>
                </div>
                <div class="col-xs-7 blue-box">
                    <b><?php echo count($users->toArray())?></b>
                    <div><?= __('Users') ?></div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg-3">
            <div class="outer-box-blue clearfix">
                <div class="col-xs-5 left-blue-box text-center">
                    <i class="fa fa-sitemap" aria-hidden="true"></i>
                </div>
                <div class="col-xs-7 blue-box">
                    <b><?php echo count($companies->toArray())?></b>
                    <div><?= __('Companies') ?></div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg-3">
            <div class="outer-box-blue clearfix">
                <div class="col-xs-5 left-blue-box text-center">
                    <i class="fa fa-building" aria-hidden="true"></i>
                </div>
                <div class="col-xs-7 blue-box">
                    <b><?php echo count($subsidiary->toArray());?></b>
                    <div>Subsidaries</div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg-3">
            <div class="outer-box-blue clearfix">
                <div class="col-xs-5 left-blue-box text-center">
                    <i class="fa fa-flag-o" aria-hidden="true"></i>
                </div>
                <div class="col-xs-7 blue-box">
                    <b> <?= $reports ?> </b>
                    <div><?= __('Reports')?></div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="organization-detail col-sm-12">
        <div class="col-sm-12">
            <div class="top-title"> Organization Detail
            </div>
        </div>
        <div class="col-sm-6 text-right">
            <a href="#" data-toggle="modal" data-target="#addSubsidiary" class="btn btn-blue">Add Subsidiary</a>
            </div>
        <?php //pr($queryToGetusers);?>
        <div class="col-lg-2">
            <div class="logo-organization">
                <?php 
                	$profile = $queryToGetusers['organization']->logo;
                    $profilePicPath = 'auditA_front_logo.jpg';
                    if(!empty($profile)) {
                        $profilePic = new File(Configure::read('LogoPicPathToCheck') . $queryToGetusers['organization']->logo_dir . DS . 'square_' .$profile, true, 0644);
                        if ($profilePic->exists()) {
                            $profilePicPath = Configure::read('LogoPicPath') . $queryToGetusers['organization']->logo_dir . DS . 'square_' . $profile;
                        }
                    }
                    echo $this->Html->image($profilePicPath, [
                                'class' => 'user-image', 'alt' => 'User Image'
                            ]);
                    ?>
            </div>
        </div>
        <div class="col-lg-4 col-lg-offset-1">
            <h2><?php echo $queryToGetusers['organization']->name?></h2>
            <div class="text-organization"><i class="fa fa-phone-square" aria-hidden="true"></i> <?php echo $queryToGetusers['organization']->phone?></div>
            <div class="text-organization"><i class="fa fa-fax" aria-hidden="true"></i> 
            <?php echo (!empty($queryToGetusers['organization']->fax)) ? $queryToGetusers['organization']->fax : "no number" ;?></div>
            <div class="text-organization"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php 
            echo (!empty($queryToGetusers['organization']->street_number)) ? ($queryToGetusers['organization']->street_number.','.$queryToGetusers['organization']->street.','.$queryToGetusers['organization']->place) : "No address" ;?>
            </div>
            <div class="text-organization"><?php echo $queryToGetusers->email ;?></div>
        </div>
        <div class="col-lg-3">
            <div class="current-plan"> Current Plan:
            </div>
        </div>
    </div> -->
    <!--div class="col-lg-12">
        <div id="container"  class="box-shadows" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div-->
    <?php //echo $this->Html->script('Dashboard/piechart', ['block' => 'scriptBottom']); ?>
    <script type="text/javascript">
        var graphData = '<?php echo json_encode(array_values($userGraph)) ?>';
        var graphKey  = '<?php echo json_encode(array_keys($userGraph)) ?>';
        console.log(graphKey);
    </script>
