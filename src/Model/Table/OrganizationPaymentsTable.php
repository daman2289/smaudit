<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrganizationPayments Model
 *
 * @property \Cake\ORM\Association\BelongsTo $OrganizationSubscriptionPlans
 * @property \Cake\ORM\Association\BelongsTo $OrganizationAdditionalUserSubscriptionPlans
 *
 * @method \App\Model\Entity\OrganizationPayment get($primaryKey, $options = [])
 * @method \App\Model\Entity\OrganizationPayment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OrganizationPayment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OrganizationPayment|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrganizationPayment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OrganizationPayment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OrganizationPayment findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrganizationPaymentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('organization_payments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('OrganizationSubscriptionPlans', [
            'foreignKey' => 'organization_subscription_plan_id'
        ]);
        $this->belongsTo('OrganizationAdditionalUserSubscriptionPlans', [
            'foreignKey' => 'organization_additional_user_subscription_plan_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->decimal('amount')
            ->requirePresence('amount', 'create')
            ->notEmpty('amount');

        $validator
            ->integer('payment_status')
            ->allowEmpty('payment_status');

        $validator
            ->allowEmpty('message');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['organization_subscription_plan_id'], 'OrganizationSubscriptionPlans'));
        $rules->add($rules->existsIn(['organization_additional_user_subscription_plan_id'], 'OrganizationAdditionalUserSubscriptionPlans'));

        return $rules;
    }
}
