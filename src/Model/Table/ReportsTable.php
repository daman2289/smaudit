<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
/**
 * Reports Model
 *
 * @property \Cake\ORM\Association\BelongsTo $OrganizationCompanies
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $ReportTypes
 * @property \Cake\ORM\Association\BelongsTo $OpinionTypes
 * @property \Cake\ORM\Association\BelongsTo $ModifiedOpinions
 * @property \Cake\ORM\Association\BelongsTo $FirstSignatureUsers
 * @property \Cake\ORM\Association\BelongsTo $SecondSignatureUsers
 *
 * @method \App\Model\Entity\Report get($primaryKey, $options = [])
 * @method \App\Model\Entity\Report newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Report[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Report|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Report patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Report[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Report findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ReportsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('reports');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('OrganizationCompanies', [
            'foreignKey' => 'organization_company_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('ReportTypes', [
            'foreignKey' => 'report_type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('AccountingFrameworkTypes', [
            'foreignKey' => 'accounting_framework_type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('OpinionTypes', [
            'foreignKey' => 'opinion_type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ModifiedOpinions', [
            'foreignKey' => 'modified_opinion_id',
            'joinType' => 'INNER'
        ]);
        // $this->belongsTo('FirstSignatureUsers', [
        //     'foreignKey' => 'first_signature_user_id'
        // ]);
        // $this->belongsTo('SecondSignatureUsers', [
        //     'foreignKey' => 'second_signature_user_id'
        // ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        // $validator
        //     ->dateTime('report_date')
        //     ->requirePresence('report_date', 'create')
        //     ->notEmpty('report_date');

        // $validator
        //     ->date('financial_year_from')
        //     ->requirePresence('financial_year_from', 'create')
        //     ->notEmpty('financial_year_from');

        // $validator
        //     ->date('financial_year_to')
        //     ->requirePresence('financial_year_to', 'create')
        //     ->notEmpty('financial_year_to');

        // $validator
        //     ->requirePresence('financial_year', 'create')
        //     ->notEmpty('financial_year');

        // $validator
        //     ->boolean('short_long')
        //     ->requirePresence('short_long', 'create')
        //     ->notEmpty('short_long');

        // // $validator
        // //     ->boolean('interim_statement')
        // //     ->requirePresence('interim_statement', 'create')
        // //     ->notEmpty('interim_statement');

        // $validator
        //     ->boolean('first_year_audit')
        //     ->requirePresence('first_year_audit', 'create')
        //     ->notEmpty('first_year_audit');

        // $validator
        //     ->boolean('opting_out')
        //     ->allowEmpty('opting_out');

        // $validator
        //     ->date('date_prior_audit_report')
        //     ->allowEmpty('date_prior_audit_report');

        // $validator
        //     ->boolean('qualified_opinion')
        //     ->allowEmpty('qualified_opinion');

        // $validator
        //     ->allowEmpty('qualification');

        // $validator
        //     ->boolean('assistance_book_keeping')
        //     ->requirePresence('assistance_book_keeping', 'create')
        //     ->notEmpty('assistance_book_keeping');

        // $validator
        //     ->boolean('appropriation_available_earning')
        //     ->requirePresence('appropriation_available_earning', 'create')
        //     ->notEmpty('appropriation_available_earning');

        // $validator
        //     ->boolean('audit_terminated_later_than_report')
        //     ->requirePresence('audit_terminated_later_than_report', 'create')
        //     ->notEmpty('audit_terminated_later_than_report');

        // $validator
        //     ->date('audit_terminated_date')
        //     ->allowEmpty('audit_terminated_date');

        // $validator
        //     ->requirePresence('emphasis_of_matter_ids', 'create')
        //     ->notEmpty('emphasis_of_matter_ids');

        // $validator
        //     ->decimal('subordination_amount')
        //     ->allowEmpty('subordination_amount');

        // $validator
        //     ->integer('going_concern_note_no')
        //     ->allowEmpty('going_concern_note_no');

        // $validator
        //     ->decimal('amount_shareholder_loan')
        //     ->allowEmpty('amount_shareholder_loan');

        // $validator
        //     ->requirePresence('basis_for_modification', 'create')
        //     ->notEmpty('basis_for_modification');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['organization_company_id'], 'OrganizationCompanies'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['report_type_id'], 'ReportTypes'));
        $rules->add($rules->existsIn(['opinion_type_id'], 'OpinionTypes'));
        $rules->add($rules->existsIn(['modified_opinion_id'], 'ModifiedOpinions'));
        // $rules->add($rules->existsIn(['first_signature_user_id'], 'FirstSignatureUsers'));
        // $rules->add($rules->existsIn(['second_signature_user_id'], 'SecondSignatureUsers'));

        return $rules;
    }

    public function getUserReports($user_id) {

        $reportTable = TableRegistry::get('Reports');
        $reportTable = $reportTable->find()
            ->where(['Reports.user_id' => $user_id]);
        return $reportTable;
    }

    public function getReport($report_id) {
        $reportTable = TableRegistry::get('Reports');
        $report = $reportTable->find()
        ->where(['Reports.id'    => base64_decode($report_id)])
        ->contain([
            'OrganizationCompanies' => [
                'Organizations' , 
                'LegalFormTypes' => [
                    'fields' => ['id', 'name']
                ] 
            ],
            'AccountingFrameworkTypes'=> [
                'fields' => ['id', 'name']
            ] ,
            'ReportTypes',
            'OpinionTypes'  => [
                'fields' => ['id', 'name', 'content_eng', 'content_fra', 'content_ger', 'content_ita']
            ] ,
            'ModifiedOpinions'  => [
                'fields' => ['id', 'name','content_eng', 'content_fra', 'content_ger', 'content_ita']
            ] ,
        ])
        ->first();
        return $report;
    }

    public function beforeSave($event, $entity, $options) {
        // if ($entity->has('subordination_amount') && !empty($entity->subordination_amount)) {
        //     $entity->subordination_amount = preg_replace(
        //         "/[^0-9,.'\]/", 
        //         "", 
        //         $entity->subordination_amount 
        //     );
           
        // }
        // if ($entity->has('amount_shareholder_loan') && !empty($entity->amount_shareholder_loan)) {
        //     $entity->amount_shareholder_loan = preg_replace(
        //         "/[^0-9,.]/", 
        //         "", 
        //         $entity->amount_shareholder_loan 
        //     );
        // }
        // if ($entity->has('amount_dividend') && !empty($entity->amount_dividend)) {
        //     $entity->amount_dividend = preg_replace(
        //         "/[^0-9,.CHF]/", 
        //         "", 
        //         $entity->amount_dividend 
        //     );
        // }
    }
}
