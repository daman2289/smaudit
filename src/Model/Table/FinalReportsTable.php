<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

/**
 * FinalReports Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Reports
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\FinalReport get($primaryKey, $options = [])
 * @method \App\Model\Entity\FinalReport newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FinalReport[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FinalReport|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FinalReport patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FinalReport[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FinalReport findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FinalReportsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('final_reports');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Reports', [
            'foreignKey' => 'report_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('body', 'create')
            ->notEmpty('body');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['report_id'], 'Reports'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function addFinalReport($data) {
        $reportTable = TableRegistry::get('FinalReports');
        $report = $reportTable->newEntity($data);
        if ($reportTable->save($report)) {
            // The foreign key value was set automatically.
            return true;
        } 
        return false;

    }

    public function getFinalReport($report_id, $lang) {        
        $reportTable = TableRegistry::get('FinalReports');
        $report = $reportTable->find()
            ->where([
                'FinalReports.report_id' => $report_id,
                'FinalReports.lang'     => $lang
            ])->last();
       return $report;
    }
}
