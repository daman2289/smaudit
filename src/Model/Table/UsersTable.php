<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Event\EventManager;
use Cake\Event\Event;


/**
 * Users Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Roles
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER'
        ]);
        $this->hasOne('Organizations');
        $this->hasOne('OrganizationUsers',[
            'dependent' => true
            ]);

        $this->hasOne('ADmad/HybridAuth.SocialProfiles');
        $this->addBehavior('Proffer.Proffer', [
            'profile_pic' => [    // The name of your upload field
                'root' => WWW_ROOT . 'files', // Customise the root upload folder here, or omit to use the default
                'dir' => 'profile_pic_dir',   // The name of the field to store the folder
                'thumbnailSizes' => [ // Declare your thumbnails
                    'square' => [   // Define the prefix of your thumbnail
                        'w' => 45, // Width
                        'h' => 45, // Height
                        'fit'  => true
                    ]
                ],
                'thumbnailMethod' => 'gd'   // Options are Imagick or Gd
            ]
        ]);
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            //->requirePresence('first_name', 'create')
            ->allowEmpty('first_name', __('First name must be provided.'))
            ->add('first_name', 
                    [
                        'length' => [
                            'rule' => ['minLength', 3],
                            'message' => __('First name must be three characters long atleast.')
                        ]
                    ]
                );

        $validator
            //->requirePresence('last_name', 'create')
            ->allowEmpty('last_name', __('Last name must be provided.'))
            ->add('last_name', 
                    [
                        'length' => [
                            'rule' => ['minLength', 3],
                            'message' => __('Last name must be three characters long atleast.')
                        ]
                    ]
                );

        $validator
            ->add('email', 'validFormat', [
                'rule' => 'email',
                'message' => 'E-mail must be valid'
            ])
            ->requirePresence('email', 'create')
            ->notEmpty('email', __('Email must be provided.'))
            ->add('email', 'unique', [
                'rule' => 'validateUnique',
                'provider' => 'table',
                'message' => __('Email already exists.')
            ]);

        $validator
            ->notEmpty('old_password', __('Old password must be provided'))
            ->add('old_password', 'custom', [
                    'rule' => 
                        function($value, $context) {
                            $data = $this->get($context['data']['id']);
                            return (new DefaultPasswordHasher)->check($value, $data['password']);
                        },
                        'message' => __('Old password does not match!')
                ]);
        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password', __('Password must be provided'));
            // ->add('password', 
            //         [
            //             'length' => [
            //                 'rule' => ['minLength', 9],
            //                 'message' => __('Please enter password of nine characters atleast.')
            //             ]
            //         ]
            //     );
            // ->add('password', 'validFormat',[
            //         'rule' => [$this, 'strongPassowrd'],
            //         'message' => 'Password must contain atleast one alphabet, one number and one special character.'
            // ]);
        $validator
            ->notEmpty('confirm_password', __('Confirm Password must be provided'))
            ->add('confirm_password', 'no-misspelling', 
                    [
                        'rule' => [
                            'compareWith', 'password'
                        ],
                        'message' => __('Confirm password does not match.'),
                    ]
                );

        $validator->add('accept', 'myRule', [
            'rule' => function ($data, $provider) {
                if ($data == 1) {
                    return true;
                }
                return __('You must accept the User agreement and privacy policy.');
            }
        ]);
        $validator
            ->allowEmpty('profile_pic')
            ->add('profile_pic', 'file', [
                'rule' => ['mimeType', ['image/jpeg', 'image/png', 'image/gif']],
                'message' => __('Please select a picture with file extensions png, jpeg, jpg or gif.')
            ]);
            // ->add('profile_pic', 'proffer', [
            //     'rule' => ['dimensions', [
            //         'min' => ['w' => 45, 'h' => 45],
            //         'max' => ['w' => 500, 'h' => 500]
            //     ]],
            //     'message' => __('Image has not the correct dimensions. Correct dimensions are 45*45 minimum and 500 * 500 maximum.'),
            //     'provider' => 'proffer'
            // ]);
        
        return $validator;
    }

    /**
     * Method strongPassowrd to validate user password
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return bool
     */
    function strongPassowrd($value, $context) {
        if (!preg_match('/(?=.*\d)(?=.*\W+)(?![.\n])(?=.*[a-zA-Z]).*$/',$value)) {
            return false;
        }

        return true;
    }

     public function validationOnlyCheck(Validator $validator) {
        $validator = $this->validationDefault($validator);
        $validator->remove('email');
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email'], 'The email should be unique'));
        $rules->add($rules->existsIn(['role_id'], 'Roles'));

        return $rules;
    }
}
