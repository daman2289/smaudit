<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ReportTemplates Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ReportTypes
 *
 * @method \App\Model\Entity\ReportTemplate get($primaryKey, $options = [])
 * @method \App\Model\Entity\ReportTemplate newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ReportTemplate[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ReportTemplate|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ReportTemplate patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ReportTemplate[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ReportTemplate findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ReportTemplatesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('report_templates');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ReportTypes', [
            'foreignKey' => 'report_type_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('template_eng', 'create')
            ->notEmpty('template_eng');

        $validator
            ->requirePresence('template_french', 'create')
            ->notEmpty('template_french');

        $validator
            ->requirePresence('template_german', 'create')
            ->notEmpty('template_german');

        $validator
            ->requirePresence('template_italian', 'create')
            ->notEmpty('template_italian');

        $validator
            ->boolean('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['report_type_id'], 'ReportTypes'));

        return $rules;
    }
}
