<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\NotFoundException;

/**
 * Organizations Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $OrganizationCompanies
 * @property \Cake\ORM\Association\HasMany $OrganizationSubsidiaries
 * @property \Cake\ORM\Association\HasMany $OrganizationUsers
 *
 * @method \App\Model\Entity\Organization get($primaryKey, $options = [])
 * @method \App\Model\Entity\Organization newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Organization[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Organization|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Organization patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Organization[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Organization findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrganizationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('organizations');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('OrganizationCompanies', [
            'foreignKey' => 'organization_id'
        ]);
        $this->hasMany('OrganizationSubsidiaries', [
            'foreignKey' => 'organization_id'
        ]);
        $this->hasMany('OrganizationUsers', [
            'foreignKey' => 'organization_id'
        ]);
        $this->addBehavior('Proffer.Proffer', [
            'logo' => [    // The name of your upload field
                'root' => WWW_ROOT . 'logo', // Customise the root upload folder here, or omit to use the default
                'dir' => 'logo_dir',   // The name of the field to store the folder
                'thumbnailSizes' => [ // Declare your thumbnails
                    'square' => [   // Define the prefix of your thumbnail
                        'w' => 350, // Width
                        'h' => 107, // Height
                        'fit'  => true
                    ],
                    'word' => [   // Define the prefix of your thumbnail
                        'w' => 350, // Width
                        'h' => 107, // Height
                        'fit'  => false
                    ]
                ],
                'thumbnailMethod' => 'gd'   // Options are Imagick or Gd
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            // ->requirePresence('logo', 'create')
            ->allowEmpty('logo');

        $validator
            ->requirePresence('phone', 'create')
            ->notEmpty('phone');


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function resetTrial($id) {
        $organizationTable = TableRegistry::get('Organizations');        
        try {
            $organization = $organizationTable->get($id); // Return article with id 12
            $organization->trial_start = date('Y-m-d');
            $organizationTable->save($organization);    
            return true;                            
        } catch (NotFoundException $e) {
            // redirecting to Last page if request page doesn't exist
            return false;
           
        }
    }

    public function deleteOrganization($id) {
        $organizationTable = TableRegistry::get('Organizations');        
        try {
            $organization = $organizationTable->get($id); // Return article with id 12
            $organization->is_deleted = 1;
            $organizationTable->save($organization);    
            return true;                            
        } catch (NotFoundException $e) {
            // redirecting to Last page if request page doesn't exist
            return false;
           
        }
    }
}
