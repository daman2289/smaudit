<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * NewQuestions Model
 *
 * @property \Cake\ORM\Association\BelongsTo $NewPlaceholders
 * @property \Cake\ORM\Association\BelongsTo $NewQuestionTypes
 * @property \Cake\ORM\Association\HasMany $NewQuestionOptions
 *
 * @method \App\Model\Entity\NewQuestion get($primaryKey, $options = [])
 * @method \App\Model\Entity\NewQuestion newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\NewQuestion[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\NewQuestion|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\NewQuestion patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\NewQuestion[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\NewQuestion findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class NewQuestionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('new_questions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('NewPlaceholders', [
            'foreignKey' => 'new_placeholder_id',
            'joinType' => 'INNER'
        ]);
        
        $this->belongsTo('NewQuestionTypes', [
            'foreignKey' => 'new_question_type_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('NewQuestionOptions', [
            'foreignKey' => 'new_question_id',
            'dependent' => true,
            'cascadeCallbacks'  => true
            // 'saveStrategy'  => 'replace'
        ]);
        
        $this->belongsToMany('ChildQuestions', [
            'className' => 'NewQuestions',
            'through' => 'NewQuestionsQuestions',
            'foreignKey' => 'parent_question_id',
            'saveStrategy' => 'append'
        ]);

        $this->belongsToMany('ParentQuestions', [
            'className' => 'NewQuestions',
            'through' => 'NewQuestionsQuestions',
            'foreignKey' => 'child_question_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('question_en', 'create')
            ->notEmpty('question_en');

        $validator
            ->allowEmpty('question_er');

        $validator
            ->allowEmpty('question_fr');

        $validator
            ->integer('is_parent')
            ->allowEmpty('is_parent');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['new_placeholder_id'], 'NewPlaceholders'));
        $rules->add($rules->existsIn(['new_question_type_id'], 'NewQuestionTypes'));

        return $rules;
    }

    public function deletePlaceholderQuestion($placeholder_id) {
        $question = $this->find()
            ->where([
                'new_placeholder_id'    => $placeholder_id 
            ])
            ->first();
        if ($this->delete($question)) {
            return true;
        }
        return false;

    }
}
