<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;


/**
 * WorkInProgresses Model
 *
 * @property \Cake\ORM\Association\HasMany $WorkInProgressFeatures
 *
 * @method \App\Model\Entity\WorkInProgress get($primaryKey, $options = [])
 * @method \App\Model\Entity\WorkInProgress newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\WorkInProgress[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\WorkInProgress|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WorkInProgress patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\WorkInProgress[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\WorkInProgress findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WorkInProgressesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('work_in_progresses');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('WorkInProgressFeatures', [
            'foreignKey' => 'work_in_progress_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('description_eng', 'create')
            ->notEmpty('description_eng');

        $validator
            ->requirePresence('description_ger', 'create')
            ->notEmpty('description_ger');

        return $validator;
    }

    public function customSave($data) {
        $placeholder = TableRegistry::get('WorkInProgresses');
        $entities = $placeholder->newEntity($data);
        $placeholder->save($entities);
    }
}
