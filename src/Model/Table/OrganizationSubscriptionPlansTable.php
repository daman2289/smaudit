<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrganizationSubscriptionPlans Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Organizations
 * @property \Cake\ORM\Association\BelongsTo $SubscriptionPlans
 * @property \Cake\ORM\Association\BelongsTo $StripeCustomers
 * @property \Cake\ORM\Association\BelongsTo $StripeSubscriptions
 * @property \Cake\ORM\Association\HasMany $OrganizationPayments
 *
 * @method \App\Model\Entity\OrganizationSubscriptionPlan get($primaryKey, $options = [])
 * @method \App\Model\Entity\OrganizationSubscriptionPlan newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OrganizationSubscriptionPlan[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OrganizationSubscriptionPlan|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrganizationSubscriptionPlan patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OrganizationSubscriptionPlan[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OrganizationSubscriptionPlan findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrganizationSubscriptionPlansTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('organization_subscription_plans');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SubscriptionPlans', [
            'foreignKey' => 'subscription_plan_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('StripeSubscriptions', [
            'foreignKey' => 'stripe_subscription_id'
        ]);
        $this->hasMany('OrganizationPayments', [
            'foreignKey' => 'organization_subscription_plan_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('expire_date')
            ->allowEmpty('expire_date');

        $validator
            ->integer('is_deleted')
            ->allowEmpty('is_deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));
        $rules->add($rules->existsIn(['subscription_plan_id'], 'SubscriptionPlans'));
        return $rules;
    }

    public function getOrganizationPlan($organization_id) {
        $query = $this->find()
        ->where([
            'OR' => [
                'OrganizationSubscriptionPlans.organization_id' => $organization_id,
                'OrganizationSubscriptionPlans.stripe_customer_id' => $organization_id
            ],
            'SubscriptionPlans.is_deleted' => (int)false,
        ])->contain(['SubscriptionPlans']);
        return $query->first();
    }
}
