<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * NewQuestionOptions Model
 *
 * @property \Cake\ORM\Association\BelongsTo $NewQuestions
 *
 * @method \App\Model\Entity\NewQuestionOption get($primaryKey, $options = [])
 * @method \App\Model\Entity\NewQuestionOption newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\NewQuestionOption[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\NewQuestionOption|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\NewQuestionOption patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\NewQuestionOption[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\NewQuestionOption findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class NewQuestionOptionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('new_question_options');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('NewQuestions', [
            'foreignKey' => 'new_question_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name_en');

        $validator
            ->allowEmpty('name_er');

        $validator
            ->allowEmpty('name_fr');

        $validator
            ->allowEmpty('content_en');

        $validator
            ->allowEmpty('content_er');

        $validator
            ->allowEmpty('question_fr');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['new_question_id'], 'NewQuestions'));

        return $rules;
    }
}
