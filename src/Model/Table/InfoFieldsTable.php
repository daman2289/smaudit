<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * InfoFields Model
 *
 * @property \Cake\ORM\Association\HasMany $ReportHeaders
 *
 * @method \App\Model\Entity\InfoField get($primaryKey, $options = [])
 * @method \App\Model\Entity\InfoField newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\InfoField[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\InfoField|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InfoField patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\InfoField[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\InfoField findOrCreate($search, callable $callback = null, $options = [])
 */
class InfoFieldsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('info_fields');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('ReportHeaders', [
            'foreignKey' => 'info_field_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('field');

        $validator
            ->allowEmpty('title_en');

        $validator
            ->allowEmpty('title_er');

        $validator
            ->allowEmpty('title_fr');

        return $validator;
    }
}
