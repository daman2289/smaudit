<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Answers Model
 *
 * @property \Cake\ORM\Association\BelongsTo $NewPlaceholders
 * @property \Cake\ORM\Association\BelongsTo $OrganizationUsers
 * @property \Cake\ORM\Association\BelongsTo $NewQuestions
 * @property \Cake\ORM\Association\BelongsTo $NewQuestionOptions
 *
 * @method \App\Model\Entity\Answer get($primaryKey, $options = [])
 * @method \App\Model\Entity\Answer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Answer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Answer|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Answer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Answer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Answer findOrCreate($search, callable $callback = null, $options = [])
 */
class AnswersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('answers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');


        $this->addBehavior('Timestamp');
        
        $this->belongsTo('NewPlaceholders', [
            'foreignKey' => 'new_placeholder_id'
        ]);
        $this->belongsTo('OrganizationUsers', [
            'foreignKey' => 'organization_user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('NewQuestions', [
            'foreignKey' => 'new_question_id'
        ]);
        $this->belongsTo('NewQuestionOptions', [
            'foreignKey' => 'new_question_option_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('answer');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['new_placeholder_id'], 'NewPlaceholders'));
        // $rules->add($rules->existsIn(['organization_user_id'], 'OrganizationUsers'));
        $rules->add($rules->existsIn(['new_question_id'], 'NewQuestions'));
        $rules->add($rules->existsIn(['new_question_option_id'], 'NewQuestionOptions'));

        return $rules;
    }

    public function beforeMarshal($event, $data,  $options) {
        if(isset($data['answer']) && is_array($data['answer'])) {
            $data['answer'] = implode(',', $data['answer']);
        }
    }
}
