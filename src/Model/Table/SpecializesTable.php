<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
/**
 * Specializes Model
 *
 * @method \App\Model\Entity\Specialize get($primaryKey, $options = [])
 * @method \App\Model\Entity\Specialize newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Specialize[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Specialize|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Specialize patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Specialize[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Specialize findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SpecializesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('specializes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Proffer.Proffer', [
            'icon' => [    // The name of your upload field
                'root' => WWW_ROOT . 'files', // Customise the root upload folder here, or omit to use the default
                'dir' => 'icon_dir',   // The name of the field to store the folder
                'thumbnailSizes' => [ // Declare your thumbnails
                    'square' => [   // Define the prefix of your thumbnail
                        'w' => 78, // Width
                        'h' => 78, // Height
                        // 'fit'  => true
                    ]
                ],
                'thumbnailMethod' => 'gd'   // Options are Imagick or Gd
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('title_eng', 'create')
            ->notEmpty('title_eng');

        $validator
            ->requirePresence('title_german', 'create')
            ->notEmpty('title_german');

        $validator
            ->requirePresence('description_eng', 'create')
            ->notEmpty('description_eng');

        $validator
            ->requirePresence('description_german', 'create')
            ->notEmpty('description_german');

        $validator
            ->boolean('is_activated')
            ->allowEmpty('is_activated');

        $validator
            ->boolean('is_deleted')
            ->allowEmpty('is_deleted');

        return $validator;
    }

    public function getSpecializes() {
        $specializeTable = TableRegistry::get('Specializes');
        $specializes = $specializeTable->find()
            ->where([
                'Specializes.is_activated' => 1,
                'Specializes.is_deleted' => 0
            ]);
        return $specializes;
    }
}
