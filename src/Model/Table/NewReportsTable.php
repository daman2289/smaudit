<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * NewReports Model
 *
 * @property \Cake\ORM\Association\HasMany $NewPlaceholders
 * @property \Cake\ORM\Association\HasMany $NewReportPlaceholders
 *
 * @method \App\Model\Entity\NewReport get($primaryKey, $options = [])
 * @method \App\Model\Entity\NewReport newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\NewReport[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\NewReport|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\NewReport patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\NewReport[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\NewReport findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class NewReportsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('new_reports');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('NewPlaceholders', [
            'foreignKey' => 'new_report_id'
        ]);
        $this->hasMany('NewReportPlaceholders', [
            'foreignKey' => 'new_report_id'
        ]);
        $this->hasMany('NewFinalReports', [
            'foreignKey' => 'new_report_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('uid', 'create')
            ->notEmpty('uid');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->integer('is_activated')
            ->allowEmpty('is_activated');

        $validator
            ->allowEmpty('content_en');

        $validator
            ->allowEmpty('content_er');

        $validator
            ->allowEmpty('content_fr');

        return $validator;
    }
}
