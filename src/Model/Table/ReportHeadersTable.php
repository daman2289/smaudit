<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ReportHeaders Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Organizations
 * @property \Cake\ORM\Association\BelongsTo $InfoFields
 *
 * @method \App\Model\Entity\ReportHeader get($primaryKey, $options = [])
 * @method \App\Model\Entity\ReportHeader newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ReportHeader[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ReportHeader|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ReportHeader patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ReportHeader[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ReportHeader findOrCreate($search, callable $callback = null, $options = [])
 */
class ReportHeadersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('report_headers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id'
        ]);
        $this->belongsTo('InfoFields', [
            'foreignKey' => 'info_field_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    // public function buildRules(RulesChecker $rules)
    // {
    //     $rules->add($rules->existsIn(['organization_id'], 'Organizations'));
    //     $rules->add($rules->existsIn(['info_field_id'], 'InfoFields'));

    //     return $rules;
    // }

    public function getOrganizationHeader($organization_id) {
        return $this->find()   
            ->where(['organization_id' => $organization_id])
            ->contain('InfoFields');
    }
}
