<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

/**
 * AdditionalUserSubscriptionPlans Model
 *
 * @property \Cake\ORM\Association\BelongsTo $StripePlans
 * @property \Cake\ORM\Association\HasMany $OrganizationAdditionalUserSubscriptionPlans
 *
 * @method \App\Model\Entity\AdditionalUserSubscriptionPlan get($primaryKey, $options = [])
 * @method \App\Model\Entity\AdditionalUserSubscriptionPlan newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AdditionalUserSubscriptionPlan[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AdditionalUserSubscriptionPlan|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AdditionalUserSubscriptionPlan patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AdditionalUserSubscriptionPlan[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AdditionalUserSubscriptionPlan findOrCreate($search, callable $callback = null, $options = [])
 */
class AdditionalUserSubscriptionPlansTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('additional_user_subscription_plans');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');        
        $this->hasMany('OrganizationAdditionalUserSubscriptionPlans', [
            'foreignKey' => 'additional_user_subscription_plan_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->decimal('price')
            ->requirePresence('price', 'create')
            ->notEmpty('price');

        $validator
            ->allowEmpty('frequency');

        $validator
            ->integer('interval_count')
            ->allowEmpty('interval_count');

        $validator
            ->allowEmpty('currency');

        $validator
            ->allowEmpty('additional_detail');

        $validator
            ->boolean('is_activated')
            ->allowEmpty('is_activated');

        $validator
            ->boolean('is_deleted')
            ->allowEmpty('is_deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    // public function buildRules(RulesChecker $rules)
    // {
    //     $rules->add($rules->existsIn(['stripe_plan_id'], 'StripePlans'));

    //     return $rules;
    // }

    public function getAdditionalPlanByOrganization($organization_id) {
        $planTable = TableRegistry::get('AdditionalUserSubscriptionPlans');
        $plan = $planTable->find()
            ->matching(
                'OrganizationAdditionalUserSubscriptionPlans', function ($q)use ($organization_id) {
                    return $q->where([
                            'OrganizationAdditionalUserSubscriptionPlans.organization_id' => $organization_id]
                        );
                }
            )->first();
        return $plan;
    }

    public function getOrganizationPlanByStripeId($stripe_id) {
        $planTable = TableRegistry::get('AdditionalUserSubscriptionPlans');
        $plan = $planTable->find()
            ->where([
                'AdditionalUserSubscriptionPlans.stripe_plan_id' => $stripe_id
            ])
            ->first();
        return $plan;
    }
}
