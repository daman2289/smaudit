<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
/**
 * SubscriptionPlans Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $PlanFeatures
 *
 * @method \App\Model\Entity\SubscriptionPlan get($primaryKey, $options = [])
 * @method \App\Model\Entity\SubscriptionPlan newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SubscriptionPlan[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SubscriptionPlan|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SubscriptionPlan patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SubscriptionPlan[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SubscriptionPlan findOrCreate($search, callable $callback = null, $options = [])
 */
class SubscriptionPlansTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('subscription_plans');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->belongsToMany('PlanFeatures', [
            'foreignKey' => 'subscription_plan_id',
            'targetForeignKey' => 'plan_feature_id',
            'joinTable' => 'subscription_plans_plan_features'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->decimal('price')
            ->requirePresence('price', 'create')
            ->notEmpty('amount');

        $validator
            ->boolean('is_activated')
            // ->requirePresence('is_activated', 'create')
            ->notEmpty('is_activated');

        $validator
            ->boolean('is_deleted')
            ->allowEmpty('is_deleted');

        return $validator;
    }

    public function getOrganizatonPlan($plan_id) {
        $planTable = TableRegistry::get('SubscriptionPlans');
        $plan = $planTable->get($plan_id);
        return $plan;
    }

    public function getSubscriptionPlan($stripe_plan_id) {
        $planTable = TableRegistry::get('SubscriptionPlans');
        $plan = $planTable->find()
            ->where(['SubscriptionPlans.stripe_plan_id' => $stripe_plan_id])
            ->first();
        return $plan;
    }

    public function getAllPlan() {
        $planTable = TableRegistry::get('SubscriptionPlans');
        $plans = $planTable->find()
        ->where(['SubscriptionPlans.is_activated' => 1, 'SubscriptionPlans.is_deleted' => 0])
        ->contain(['PlanFeatures']);
        return $plans;
    }

    public function getOrganizationSubscription($organization_id) {
        $planTable = TableRegistry::get('OrganizationSubscriptionPlans');
        $plans = $planTable->find()
        ->where([
            'OrganizationSubscriptionPlans.organization_id' => $organization_id
        ])
        ->first();
        return $plans;
    }

    public function getPlanByOrganization($organization_id) {
        $planTable = TableRegistry::get('SubscriptionPlans');
        $plan = $planTable->find()
            ->where();
        return $plan;
    }
}
