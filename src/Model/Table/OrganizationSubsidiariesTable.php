<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
/**
 * OrganizationSubsidiaries Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Organizations
 * @property \Cake\ORM\Association\HasMany $OrganizationUsers
 *
 * @method \App\Model\Entity\OrganizationSubsidiary get($primaryKey, $options = [])
 * @method \App\Model\Entity\OrganizationSubsidiary newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OrganizationSubsidiary[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OrganizationSubsidiary|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrganizationSubsidiary patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OrganizationSubsidiary[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OrganizationSubsidiary findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrganizationSubsidiariesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('organization_subsidiaries');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id'
        ]);
        $this->hasMany('OrganizationUsers', [
            'foreignKey' => 'organization_subsidiary_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('phone', 'create')
            ->notEmpty('phone');

        $validator
            ->requirePresence('fax', 'create')
            ->notEmpty('fax');

        $validator
            ->requirePresence('street', 'create')
            ->notEmpty('street');

        $validator
            ->requirePresence('street_number', 'create')
            ->notEmpty('street_number');

        $validator
            ->requirePresence('place', 'create')
            ->notEmpty('place');

        $validator
            ->requirePresence('zip', 'create')
            ->notEmpty('zip');


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));

        return $rules;
    }

    public function setSubsidiaryToPrimary($id) {
        $subsidiaryTable = TableRegistry::get('OrganizationSubsidiaries');
        $subsidiary     = $subsidiaryTable->find()
            ->where(['OrganizationSubsidiaries.id' => $id])
            ->first();
       
        if(!empty($subsidiary->toArray())) {
            $subsidiaryTable->updateAll(
                ['is_primary' => 0],
                [
                    'organization_id' => $subsidiary->organization_id,
                    'id <>' => $subsidiary->id
                ]
            );
            $query  = $subsidiaryTable->query();
            $query->update()
                ->set(['is_primary' => 1])
                ->where(['id'   => $subsidiary->id])
                ->execute();
            return true;
        }
        return false;
    }

    public function getUserSubsidiary($user_id = null,$organization_id =null) {
        if($organization_id) {
            $subsidiaryTable = TableRegistry::get('OrganizationSubsidiaries');
            $subsidiary     = $subsidiaryTable->find()
                ->where([
                    'OrganizationSubsidiaries.organization_id' => $organization_id,
                    'OrganizationSubsidiaries.is_primary' => true,
                ])
                ->first();
            if(empty($subsidiary)) {
                $subsidiary     = $subsidiaryTable->find()
                ->where([
                    'OrganizationSubsidiaries.organization_id' => $organization_id,
                ])
                ->first();
            }
        }
        if($user_id) {
            $userTable = TableRegistry::get('OrganizationUsers');
            $user = $userTable->getOrganizaionUser($user_id);
            $subsidiaryTable = TableRegistry::get('OrganizationSubsidiaries');
            $subsidiary     = $subsidiaryTable->find()
                ->where([
                    'OrganizationSubsidiaries.id' => $user->organization_subsidiary_id,
                ])
                ->first();
            if(empty($subsidiary->toArray())) {
                $subsidiary     = $subsidiaryTable->find()
                    ->where([
                        'OrganizationSubsidiaries.id' => $organization_id,
                    ])
                    ->first();
            }
        }
        return $subsidiary;
    }

    public function beforeSave($event, $entity, $options) {       

        foreach($entity as $k => $v) {
            pr($entity[$k] );die;
          $entity[$k] = Sanitize::clean($v);
        }
    }
}
