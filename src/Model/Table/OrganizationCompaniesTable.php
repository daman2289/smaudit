<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrganizationCompanies Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Organizations
 * @property \Cake\ORM\Association\BelongsTo $LegalFormTypes
 * @property \Cake\ORM\Association\BelongsTo $AccountingFrameworkTypes
 * @property \Cake\ORM\Association\HasMany $Reports
 *
 * @method \App\Model\Entity\OrganizationCompany get($primaryKey, $options = [])
 * @method \App\Model\Entity\OrganizationCompany newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OrganizationCompany[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OrganizationCompany|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrganizationCompany patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OrganizationCompany[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OrganizationCompany findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrganizationCompaniesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('organization_companies');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id'
        ]);
        $this->belongsTo('LegalFormTypes', [
            'foreignKey' => 'legal_form_type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('AccountingFrameworkTypes', [
            'foreignKey' => 'accounting_framework_type_id',
        ]);
        $this->hasMany('Reports', [
            'foreignKey' => 'organization_company_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('company_name', 'create')
            ->notEmpty('company_name');

        $validator
            ->requirePresence('street', 'create')
            ->notEmpty('street');

        $validator
            ->requirePresence('street_number', 'create')
            ->notEmpty('street_number');

        $validator
            ->requirePresence('place', 'create')
            ->notEmpty('place');

        $validator
            ->requirePresence('zip', 'create')
            ->notEmpty('zip');

        $validator
            ->requirePresence('place_of_incorporation', 'create')
            ->notEmpty('place_of_incorporation');


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));
        $rules->add($rules->existsIn(['legal_form_type_id'], 'LegalFormTypes'));
        $rules->add($rules->existsIn(['accounting_framework_type_id'], 'AccountingFrameworkTypes'));

        return $rules;
    }
}
