<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LegalFormValues Model
 *
 * @property \Cake\ORM\Association\BelongsTo $LegalFormTypes
 *
 * @method \App\Model\Entity\LegalFormValue get($primaryKey, $options = [])
 * @method \App\Model\Entity\LegalFormValue newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\LegalFormValue[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\LegalFormValue|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LegalFormValue patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\LegalFormValue[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\LegalFormValue findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SignatureGenderReplacementsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('signature_gender_replacements');
        $this->setDisplayField('gender');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    // public function buildRules(RulesChecker $rules)
    // {
    //     $rules->add($rules->existsIn(['legal_form_type_id'], 'LegalFormTypes'));

    //     return $rules;
    // }
}
