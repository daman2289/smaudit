<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AccountingFrameworkValues Model
 *
 * @property \Cake\ORM\Association\BelongsTo $AccountingFrameworkTypes
 *
 * @method \App\Model\Entity\AccountingFrameworkValue get($primaryKey, $options = [])
 * @method \App\Model\Entity\AccountingFrameworkValue newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AccountingFrameworkValue[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AccountingFrameworkValue|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AccountingFrameworkValue patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AccountingFrameworkValue[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AccountingFrameworkValue findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AccountingFrameworkValuesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('accounting_framework_values');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('AccountingFrameworkTypes', [
            'foreignKey' => 'accounting_framework_type_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('eng_value', 'create')
            ->notEmpty('eng_value');

        $validator
            ->requirePresence('ger_value', 'create')
            ->notEmpty('ger_value');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['accounting_framework_type_id'], 'AccountingFrameworkTypes'));

        return $rules;
    }
}
