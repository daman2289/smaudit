<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ReportFooters Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Organizations
 *
 * @method \App\Model\Entity\ReportFooter get($primaryKey, $options = [])
 * @method \App\Model\Entity\ReportFooter newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ReportFooter[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ReportFooter|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ReportFooter patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ReportFooter[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ReportFooter findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ReportFootersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('report_footers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('content');

        $validator
            ->allowEmpty('footer_font_family');

        $validator
            ->allowEmpty('footer_font_size');

        $validator
            ->allowEmpty('footer_font_weight');

        $validator
            ->allowEmpty('footer_font_style');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    // public function buildRules(RulesChecker $rules)
    // {
    //     $rules->add($rules->existsIn(['organization_id'], 'Organizations'));

    //     return $rules;
    // }

    public function getOrganizationFooter($organization_id) {
        return $this->find()
                ->where(['organization_id'  => $organization_id])
                ->first();
    }
}
