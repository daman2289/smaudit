<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * NewPlaceholders Model
 *
 * @property \Cake\ORM\Association\BelongsTo $NewReports
 * @property \Cake\ORM\Association\BelongsTo $NewPlaceholderTypes
 * @property \Cake\ORM\Association\HasMany $NewQuestions
 *
 * @method \App\Model\Entity\NewPlaceholder get($primaryKey, $options = [])
 * @method \App\Model\Entity\NewPlaceholder newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\NewPlaceholder[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\NewPlaceholder|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\NewPlaceholder patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\NewPlaceholder[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\NewPlaceholder findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class NewPlaceholdersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('new_placeholders');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('NewReports', [
            'foreignKey' => 'new_report_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('NewPlaceholderTypes', [
            'foreignKey' => 'new_placeholder_type_id'
        ]);

        $this->hasMany('NewQuestions', [
            'foreignKey' => 'new_placeholder_id',
            'dependent'  => true,
            'cascadeCallbacks'  => true
        ]);

        $this->hasMany('LegalFormReplacements', [
            'foreignKey' => 'new_placeholder_id',
            'dependent'  => true,
            'cascadeCallbacks'  => true
        ]);

        $this->hasMany('SignatureGenderReplacements', [
            'foreignKey' => 'new_placeholder_id',
            'dependent'  => true,
            'cascadeCallbacks'  => true
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('uid');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->allowEmpty('content_en');

        $validator
            ->allowEmpty('content_er');

        $validator
            ->allowEmpty('content_fr');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['new_report_id'], 'NewReports'));
        $rules->add($rules->existsIn(['new_placeholder_type_id'], 'NewPlaceholderTypes'));

        return $rules;
    }

    public function beforeSave($event, $entity, $options)  {
        if(!$entity->isNew()){
            $originalPlaceholderType = $entity->getOriginal('new_placeholder_type_id');
            if($originalPlaceholderType == 3 && $entity->new_placeholder_type_id !=3) {
                $this->NewQuestions->deletePlaceholderQuestion($entity->id);
            }
        }
    }

    public function saveSingleRecord($data) {
        $newPlaceholder = $this->newEntity($data,[
                'associated' => [
                    'NewQuestions' => ['associated' => 'NewQuestionOptions'],
                    'LegalFormReplacements',
                    'SignatureGenderReplacements'
                ]
            ]);
        if ($this->save($newPlaceholder)) {            
            return true;
        }
        return false;
    }

    public function saveMultipleRecord($data) {
        $newPlaceholders = $this->newEntities($data,[
                'associated' => [
                    'NewQuestions' => [
                        'associated' =>[ 
                            'NewQuestionOptions', 
                            'ChildQuestions' =>[
                                'associated' => 'NewPlaceholders'
                            ]
                        ]
                    ]
                ]
            ]);
        // pr($newPlaceholders);die;
        if($this->saveMany($newPlaceholders)){
            return true;
        }else {
            return false;
        }
    }
}
