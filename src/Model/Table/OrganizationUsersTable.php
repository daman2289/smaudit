<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;


/**
 * OrganizationUsers Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Organizations
 * @property \Cake\ORM\Association\BelongsTo $Diplomas
 * @property \Cake\ORM\Association\BelongsTo $OrganizationSubsidiaries
 *
 * @method \App\Model\Entity\OrganizationUser get($primaryKey, $options = [])
 * @method \App\Model\Entity\OrganizationUser newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OrganizationUser[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OrganizationUser|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrganizationUser patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OrganizationUser[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OrganizationUser findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrganizationUsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('organization_users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id'
        ]);
        $this->belongsTo('Licenses', [
            'foreignKey' => 'license_id'
        ]);
        $this->belongsTo('OrganizationSubsidiaries', [
            'foreignKey' => 'organization_subsidiary_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));
        $rules->add($rules->existsIn(['license_id'], 'Licenses'));
        $rules->add($rules->existsIn(['organization_subsidiary_id'], 'OrganizationSubsidiaries'));

        return $rules;
    }

    public function getOrganizaionUser($user_id) {
        $userTable = TableRegistry::get('OrganizationUsers');
        $user     = $userTable->find()
            ->where(['OrganizationUsers.user_id' => $user_id])
            ->contain(['Users','Licenses'])
            ->first();
        return $user;
    }

    public function getOrganizaionAllUsers($organization_id) {
        $userTable = TableRegistry::get('OrganizationUsers');
        $users     = $userTable->find()
            ->where(['OrganizationUsers.organization_id' => $organization_id])
            ->contain(['Users','Licenses']);
        return $users;
    }

    public function getOrganizationUsersForGender($organization_id) {
        $userTable = TableRegistry::get('OrganizationUsers');
        $users     = $userTable->find('list', [
                'keyField'  => function ($userTable) {
                        return $userTable->user->get('FullName');
                    },
                'valueField'    => 'gender'
            ])
            ->where(['OrganizationUsers.organization_id' => $organization_id])
            ->contain(['Users']);
        return $users;
    }

    public function getOrganizationUsersLicence($organization_id, $field) {
        $userTable = TableRegistry::get('OrganizationUsers');
        $users     = $userTable->find('list', [
                'keyField'  => function ($userTable) {
                    return $userTable->user->get('FullName');
                },
                'valueField'    => function ($userTable) use ($field) {
                    return $userTable->license[$field];
                },
            ])
            ->where(['OrganizationUsers.organization_id' => $organization_id])
            ->contain(['Users', 'Licenses']);
        return $users;
    }
}
