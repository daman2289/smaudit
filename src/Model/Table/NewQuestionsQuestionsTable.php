<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * NewQuestionsQuestions Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ParentQuestions
 * @property \Cake\ORM\Association\BelongsTo $ChildQuestions
 *
 * @method \App\Model\Entity\NewQuestionsQuestion get($primaryKey, $options = [])
 * @method \App\Model\Entity\NewQuestionsQuestion newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\NewQuestionsQuestion[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\NewQuestionsQuestion|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\NewQuestionsQuestion patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\NewQuestionsQuestion[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\NewQuestionsQuestion findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class NewQuestionsQuestionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('new_questions_questions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ParentQuestions', [            
            'className' => 'NewQuestions',
            'foreignKey' => 'parent_question_id',
            
        ]);
        $this->belongsTo('ChildQuestions', [
            'className' => 'Questions',
            'foreignKey' => 'child_question_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('condition_for_child', 'create')
            ->notEmpty('condition_for_child');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['parent_question_id'], 'ParentQuestions'));
        $rules->add($rules->existsIn(['child_question_id'], 'ChildQuestions'));

        return $rules;
    }
}
