<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CmsPages Model
 *
 * @method \App\Model\Entity\CmsPage get($primaryKey, $options = [])
 * @method \App\Model\Entity\CmsPage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CmsPage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CmsPage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CmsPage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CmsPage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CmsPage findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CmsPagesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('cms_pages');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('meta_title', 'create')
            ->notEmpty('meta_title', __('Meta title must be provided.'));

        $validator
            ->requirePresence('meta_description', 'create')
            ->notEmpty('meta_description', __('Meta description must be provided.'));

        $validator
            ->requirePresence('meta_keyword', 'create')
            ->notEmpty('meta_keyword', __('Meta keyword must be provided.'));

        $validator
            ->requirePresence('page_title', 'create')
            ->notEmpty('page_title', __('Page title must be provided.'));

        $validator
            ->requirePresence('page_name', 'create')
            ->notEmpty('page_name', __('Page name must be provided.'));

        return $validator;
    }
}
