<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
/**
 * WorkInProgressFeatures Model
 *
 * @property \Cake\ORM\Association\BelongsTo $WorkInProgresses
 *
 * @method \App\Model\Entity\WorkInProgressFeature get($primaryKey, $options = [])
 * @method \App\Model\Entity\WorkInProgressFeature newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\WorkInProgressFeature[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\WorkInProgressFeature|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WorkInProgressFeature patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\WorkInProgressFeature[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\WorkInProgressFeature findOrCreate($search, callable $callback = null, $options = [])
 */
class WorkInProgressFeaturesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('work_in_progress_features');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('WorkInProgresses');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        $validator
            ->requirePresence('release_number', 'create')
            ->notEmpty('release_number');

        $validator
            ->requirePresence('features', 'create')
            ->notEmpty('features');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['work_in_progress_id'], 'WorkInProgresses'));

        return $rules;
    }
}
