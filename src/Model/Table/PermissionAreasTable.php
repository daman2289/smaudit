<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
/**
 * PermissionAreas Model
 *
 * @property \Cake\ORM\Association\HasMany $OrganizationUserPermissions
 *
 * @method \App\Model\Entity\PermissionArea get($primaryKey, $options = [])
 * @method \App\Model\Entity\PermissionArea newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PermissionArea[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PermissionArea|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PermissionArea patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PermissionArea[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PermissionArea findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PermissionAreasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('permission_areas');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('OrganizationUserPermissions', [
            'foreignKey' => 'permission_area_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('controller', 'create')
            ->notEmpty('controller');

        $validator
            ->requirePresence('action', 'create')
            ->notEmpty('action');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        return $validator;
    }

    public function getPremissionAreas() {
        $table = TableRegistry::get('PermissionAreas');
        $data = $table->find('list');
        return $data;
    }
}
