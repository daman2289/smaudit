<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * NewFinalReports Model
 *
 * @property \Cake\ORM\Association\BelongsTo $NewReports
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\NewFinalReport get($primaryKey, $options = [])
 * @method \App\Model\Entity\NewFinalReport newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\NewFinalReport[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\NewFinalReport|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\NewFinalReport patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\NewFinalReport[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\NewFinalReport findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class NewFinalReportsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('new_final_reports');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('NewReports', [
            'foreignKey' => 'new_report_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        // $validator
        //     ->requirePresence('body', 'create')
        //     ->notEmpty('body');

        $validator
            ->allowEmpty('lang');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['new_report_id'], 'NewReports'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function saveFinalReport($data) {
        $newEntity = $this->newEntity();
        $newEntity = $this->patchEntity($newEntity, $data);
        if($this->save($newEntity)) {
            return true;
        }
        return false;
    }
}
