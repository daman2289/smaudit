<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
/**
 * OrganizationUserPermissions Model
 *
 * @property \Cake\ORM\Association\BelongsTo $PermissionAreas
 * @property \Cake\ORM\Association\BelongsTo $Organizations
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\OrganizationUserPermission get($primaryKey, $options = [])
 * @method \App\Model\Entity\OrganizationUserPermission newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OrganizationUserPermission[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OrganizationUserPermission|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrganizationUserPermission patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OrganizationUserPermission[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OrganizationUserPermission findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrganizationUserPermissionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('organization_user_permissions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('PermissionAreas', [
            'foreignKey' => 'permission_area_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->boolean('is_allowed')
            ->allowEmpty('is_allowed');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['permission_area_id'], 'PermissionAreas'));
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function getOrganizationUserPermissions($organization_id, $user_id) {
        $table  = TableRegistry::get('OrganizationUserPermissions');
        $data   = $table->find()
                    ->where([
                        'OrganizationUserPermissions.organization_id' => $organization_id,
                        'OrganizationUserPermissions.user_id' => $user_id,
                    ])
                    ->contain([
                        'PermissionAreas'
                    ]);
        return $data;
    }

    public function setUserDefaultPermission($organization_id, $user_id) {
        $table = TableRegistry::get('PermissionAreas');
        $permissionAreas = $table->getPremissionAreas();
        foreach ($permissionAreas as $key => $value) {
            $data[]   = [
                'permission_area_id' => $key,
                'organization_id'   => $organization_id,
                'user_id'   => $user_id,
                'is_allowed'    => 0
            ];
        }
        if(!empty($data)) {
            $tableUserPermission  = TableRegistry::get('OrganizationUserPermissions');
            $entities = $tableUserPermission->newEntities($data);
            if($tableUserPermission->saveMany($entities)){
                return true;
            }
        }
        return false;
    }
}
