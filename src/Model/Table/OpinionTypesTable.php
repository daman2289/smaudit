<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
/**
 * OpinionTypes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ReportTypes
 * @property \Cake\ORM\Association\HasMany $Reports
 *
 * @method \App\Model\Entity\OpinionType get($primaryKey, $options = [])
 * @method \App\Model\Entity\OpinionType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OpinionType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OpinionType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OpinionType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OpinionType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OpinionType findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OpinionTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('opinion_types');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ReportTypes', [
            'foreignKey' => 'report_type_id'
        ]);
        $this->hasMany('Reports', [
            'foreignKey' => 'opinion_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        

        $validator
            ->requirePresence('content_eng', 'create')
            ->notEmpty('content_eng');

        

        $validator
            ->requirePresence('content_ger', 'create')
            ->notEmpty('content_ger');

      

      

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['report_type_id'], 'ReportTypes'));

        return $rules;
    }
    public function getOpinionList($language) {
        $opinionTable = TableRegistry::get('OpinionTypes');
        if ($language == 'en_GER') {    
            $field = 'name_ger';
        }
        else{
            $field = 'name';
        }
        $opinion = $opinionTable->find('list', 
            ['keyField' => 'id',
            'valueField' => $field,'limit' => 200])
        ->where([
            'OpinionTypes.id <>' => 1
        ]);
        return $opinion;
    }
}
