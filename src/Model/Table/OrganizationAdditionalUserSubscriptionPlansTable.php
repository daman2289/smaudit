<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

/**
 * OrganizationAdditionalUserSubscriptionPlans Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Organizations
 * @property \Cake\ORM\Association\BelongsTo $AdditionalUserSubscriptionPlans
 * @property \Cake\ORM\Association\BelongsTo $StripeCustomers
 * @property \Cake\ORM\Association\BelongsTo $StripeSubscriptions
 *
 * @method \App\Model\Entity\OrganizationAdditionalUserSubscriptionPlan get($primaryKey, $options = [])
 * @method \App\Model\Entity\OrganizationAdditionalUserSubscriptionPlan newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OrganizationAdditionalUserSubscriptionPlan[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OrganizationAdditionalUserSubscriptionPlan|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrganizationAdditionalUserSubscriptionPlan patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OrganizationAdditionalUserSubscriptionPlan[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OrganizationAdditionalUserSubscriptionPlan findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrganizationAdditionalUserSubscriptionPlansTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('organization_additional_user_subscription_plans');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('AdditionalUserSubscriptionPlans', [
            'foreignKey' => 'additional_user_subscription_plan_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('OrganizationPayments', [
            'foreignKey' => 'organization_additional_user_subscription_plan_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('expire_date')
            ->allowEmpty('expire_date');

        $validator
            ->integer('is_deleted')
            ->allowEmpty('is_deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));
        $rules->add($rules->existsIn(['additional_user_subscription_plan_id'], 'AdditionalUserSubscriptionPlans'));
        return $rules;
    }

    public function getOrganizationPlan($organization_id) {
        $query = $this->find()
        ->where([
            'OR' => [
                'OrganizationAdditionalUserSubscriptionPlans.organization_id' => $organization_id,
                'OrganizationAdditionalUserSubscriptionPlans.stripe_customer_id' => $organization_id
            ],
        ])
        ->contain(['AdditionalUserSubscriptionPlans']);
        return $query->first();
    }

    public function updateOrganizationPlan($id, $plan_id) {
        $planTable = TableRegistry::get('OrganizationAdditionalUserSubscriptionPlans');
        $plan = $planTable->get($id); // Return article with id 12
        $plan->additional_user_subscription_plan_id = $plan_id;
        $planTable->save($plan);
    }
}
