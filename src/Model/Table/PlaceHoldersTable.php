<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
/**
 * PlaceHolders Model
 *
 * @method \App\Model\Entity\PlaceHolder get($primaryKey, $options = [])
 * @method \App\Model\Entity\PlaceHolder newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PlaceHolder[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PlaceHolder|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PlaceHolder patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PlaceHolder[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PlaceHolder findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PlaceHoldersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('place_holders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->belongsTo('LegalFormTypes');
        $this->belongsTo('AccountingFrameworkTypes');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('selection_type')
            ->requirePresence('selection_type', 'create')
            ->notEmpty('selection_type');


        $validator
            ->integer('place_holder_number')
            ->requirePresence('place_holder_number', 'create')
            ->notEmpty('place_holder_number');

        $validator
            ->requirePresence('eng_value', 'create')
            ->notEmpty('eng_value');

        $validator
            ->requirePresence('fra_value', 'create')
            ->notEmpty('fra_value');

        $validator
            ->requirePresence('ger_value', 'create')
            ->notEmpty('ger_value');

        $validator
            ->requirePresence('ita_value', 'create')
            ->notEmpty('ita_value');


        return $validator;
    }
    public function customSave($data) {
        $placeholder = TableRegistry::get('PlaceHolders');
        $entities = $placeholder->newEntities($data);
        $placeholder->savemany($entities);
    }
}
