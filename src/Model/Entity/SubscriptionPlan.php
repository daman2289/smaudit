<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SubscriptionPlan Entity
 *
 * @property int $id
 * @property string $title
 * @property float $amount
 * @property bool $is_activated
 * @property bool $is_deleted
 *
 * @property \App\Model\Entity\PlanFeature[] $plan_features
 */
class SubscriptionPlan extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        // 'id' => false
    ];
}
