<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OrganizationAdditionalUserSubscriptionPlan Entity
 *
 * @property int $id
 * @property int $organization_id
 * @property int $additional_user_subscription_plan_id
 * @property string $stripe_customer_id
 * @property string $stripe_subscription_id
 * @property \Cake\I18n\Time $expire_date
 * @property int $is_deleted
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Organization $organization
 * @property \App\Model\Entity\AdditionalUserSubscriptionPlan $additional_user_subscription_plan
 * @property \App\Model\Entity\StripeCustomer $stripe_customer
 * @property \App\Model\Entity\StripeSubscription $stripe_subscription
 */
class OrganizationAdditionalUserSubscriptionPlan extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
