<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OrganizationPayment Entity
 *
 * @property int $id
 * @property int $organization_subscription_plan_id
 * @property int $organization_additional_user_subscription_plan_id
 * @property float $amount
 * @property int $payment_status
 * @property string $message
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\OrganizationSubscriptionPlan $organization_subscription_plan
 * @property \App\Model\Entity\OrganizationAdditionalUserSubscriptionPlan $organization_additional_user_subscription_plan
 */
class OrganizationPayment extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
