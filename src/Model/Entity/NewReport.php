<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * NewReport Entity
 *
 * @property int $id
 * @property string $uid
 * @property string $name
 * @property int $is_activated
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property string $content_en
 * @property string $content_er
 * @property string $content_fr
 *
 * @property \App\Model\Entity\NewPlaceholder[] $new_placeholders
 * @property \App\Model\Entity\NewReportPlaceholder[] $new_report_placeholders
 */
class NewReport extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
