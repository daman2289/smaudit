<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ReportFooter Entity
 *
 * @property int $id
 * @property int $organization_id
 * @property string $content
 * @property string $footer_font_family
 * @property string $footer_font_size
 * @property string $footer_font_weight
 * @property string $footer_font_style
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Organization $organization
 */
class ReportFooter extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
