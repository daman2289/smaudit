<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OrganizationCompany Entity
 *
 * @property int $id
 * @property int $organization_id
 * @property string $company_name
 * @property string $street
 * @property string $street_number
 * @property string $place
 * @property string $zip
 * @property string $place_of_incorporation
 * @property int $legal_form_type_id
 * @property int $accounting_framework_type_id
 * @property bool $status
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Organization $organization
 * @property \App\Model\Entity\LegalFormType $legal_form_type
 * @property \App\Model\Entity\AccountingFrameworkType $accounting_framework_type
 * @property \App\Model\Entity\Report[] $reports
 */
class OrganizationCompany extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
