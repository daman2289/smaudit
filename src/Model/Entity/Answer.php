<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Answer Entity
 *
 * @property int $id
 * @property int $new_placeholder_id
 * @property int $organization_user_id
 * @property int $new_question_id
 * @property int $new_question_option_id
 * @property string $answer
 *
 * @property \App\Model\Entity\NewPlaceholder $new_placeholder
 * @property \App\Model\Entity\OrganizationUser $organization_user
 * @property \App\Model\Entity\NewQuestion $new_question
 * @property \App\Model\Entity\NewQuestionOption $new_question_option
 */
class Answer extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
