<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WorkInProgressFeature Entity
 *
 * @property int $id
 * @property int $work_in_progress_id
 * @property \Cake\I18n\Time $date
 * @property string $release_number
 * @property string $features
 *
 * @property \App\Model\Entity\WorkInProgress $work_in_progress
 */
class WorkInProgressFeature extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
