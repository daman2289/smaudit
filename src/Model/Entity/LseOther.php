<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LseOther Entity
 *
 * @property int $id
 * @property string $placeholder_number
 * @property int $value
 * @property string $selection_name
 * @property string $eng_value
 * @property string $ger_value
 * @property int $status
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class LseOther extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
