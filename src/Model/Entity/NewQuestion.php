<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * NewQuestion Entity
 *
 * @property int $id
 * @property int $new_placeholder_id
 * @property int $new_question_type_id
 * @property string $question_en
 * @property string $question_er
 * @property string $question_fr
 * @property int $is_parent
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\NewPlaceholder $new_placeholder
 * @property \App\Model\Entity\NewQuestionType $new_question_type
 * @property \App\Model\Entity\NewQuestionOption[] $new_question_options
 */
class NewQuestion extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
