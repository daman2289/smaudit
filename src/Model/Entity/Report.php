<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Report Entity
 *
 * @property int $id
 * @property int $organization_company_id
 * @property int $user_id
 * @property int $report_type_id
 * @property \Cake\I18n\Time $report_date
 * @property \Cake\I18n\Time $financial_year_from
 * @property \Cake\I18n\Time $financial_year_to
 * @property string $financial_year
 * @property bool $short_long
 * @property bool $interim_statement
 * @property bool $first_year_audit
 * @property bool $opting_out
 * @property \Cake\I18n\Time $date_prior_audit_report
 * @property int $opinion_type_id
 * @property bool $qualified_opinion
 * @property string $qualification
 * @property bool $assistance_book_keeping
 * @property bool $appropriation_available_earning
 * @property bool $audit_terminated_later_than_report
 * @property \Cake\I18n\Time $audit_terminated_date
 * @property string $emphasis_of_matter_ids
 * @property float $subordination_amount
 * @property int $going_concern_note_no
 * @property float $amount_shareholder_loan
 * @property int $modified_opinion_id
 * @property string $basis_for_modification
 * @property int $first_signature_user_id
 * @property int $second_signature_user_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\OrganizationCompany $organization_company
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\ReportType $report_type
 * @property \App\Model\Entity\OpinionType $opinion_type
 * @property \App\Model\Entity\ModifiedOpinion $modified_opinion
 * @property \App\Model\Entity\FirstSignatureUser $first_signature_user
 * @property \App\Model\Entity\SecondSignatureUser $second_signature_user
 */
class Report extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
