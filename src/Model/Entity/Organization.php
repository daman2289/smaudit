<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Organization Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $website
 * @property string $phone
 * @property string $fax
 * @property string $street
 * @property string $street_number
 * @property string $place
 * @property string $zip
 * @property string $logo
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\OrganizationCompany[] $organization_companies
 * @property \App\Model\Entity\OrganizationSubsidiary[] $organization_subsidiaries
 * @property \App\Model\Entity\OrganizationUser[] $organization_users
 */
class Organization extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
