<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * NewPlaceholder Entity
 *
 * @property int $id
 * @property int $new_report_id
 * @property int $new_placeholder_type_id
 * @property string $uid
 * @property int $lft
 * @property int $rght
 * @property string $name
 * @property string $content_en
 * @property string $content_er
 * @property string $content_fr
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\NewReport $new_report
 * @property \App\Model\Entity\NewPlaceholderType $new_placeholder_type
 * @property \App\Model\Entity\NewQuestion[] $new_questions
 */
class NewPlaceholder extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
