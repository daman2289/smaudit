<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * EmphasisOfMatter Entity
 *
 * @property int $id
 * @property int $report_type_id
 * @property string $name
 * @property int $place_holder_number
 * @property string $content_eng
 * @property string $content_fra
 * @property string $content_ger
 * @property string $content_ita
 * @property bool $status
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\ReportType $report_type
 */
class EmphasisOfMatter extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
