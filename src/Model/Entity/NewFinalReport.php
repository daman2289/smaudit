<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * NewFinalReport Entity
 *
 * @property int $id
 * @property int $new_report_id
 * @property int $user_id
 * @property string $body
 * @property string $lang
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\NewReport $new_report
 * @property \App\Model\Entity\User $user
 */
class NewFinalReport extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
