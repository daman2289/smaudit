<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * NewQuestionsQuestion Entity
 *
 * @property int $id
 * @property int $parent_question_id
 * @property int $child_question_id
 * @property string $condition_for_child
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\ParentQuestion $parent_question
 * @property \App\Model\Entity\ChildQuestion $child_question
 */
class NewQuestionsQuestion extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
