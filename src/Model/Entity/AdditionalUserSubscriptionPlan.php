<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AdditionalUserSubscriptionPlan Entity
 *
 * @property int $id
 * @property string $title
 * @property string $stripe_plan_id
 * @property float $price
 * @property string $frequency
 * @property int $interval_count
 * @property string $currency
 * @property string $additional_detail
 * @property bool $is_activated
 * @property bool $is_deleted
 *
 * @property \App\Model\Entity\StripePlan $stripe_plan
 * @property \App\Model\Entity\OrganizationAdditionalUserSubscriptionPlan[] $organization_additional_user_subscription_plans
 */
class AdditionalUserSubscriptionPlan extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        // 'id' => false
    ];
}
