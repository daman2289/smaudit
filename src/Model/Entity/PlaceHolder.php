<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PlaceHolder Entity
 *
 * @property int $id
 * @property int $selection_type
 * @property string $selection_name
 * @property int $place_holder_number
 * @property string $eng_value
 * @property string $fra_value
 * @property string $ger_value
 * @property string $ita_value
 * @property bool $status
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class PlaceHolder extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
