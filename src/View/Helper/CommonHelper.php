<?php

namespace App\View\Helper;

use Cake\View\Helper;

class CommonHelper extends Helper
{
    public function userImage ($table, $field, $dir, $prefix, $image) {
        $filePath = 'files' . DS . $table . DS . $field . DS . $dir . DS . $prefix . $image;

        if (file_exists ( WWW_ROOT.$filePath )) {
            return DS . $filePath;
        }else {
            return DS . 'img' . DS . 'batman.png';
        }
    } 
    public function specializesIcon ($table = 'specializes', $field ='icon', $dir, $prefix = 'square_', $image) {
        $filePath = 'files' . DS . $table . DS . $field . DS . $dir . DS . $prefix . $image;
        if (file_exists ( WWW_ROOT.$filePath )) {
            return DS . $filePath;
        }else {
            return DS . 'img' . DS . 'intiutive.png';
        }
    }
}