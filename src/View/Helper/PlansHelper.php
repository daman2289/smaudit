<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\Utility\Hash;
class PlansHelper extends Helper
{
    public function formattPlan($data) {
        $result = Hash::map($data, "{n}", [$this, 'noop']);
        return $result;
    }
    public function noop(array $array)
    {   $lang='title';
        if($this->request->session()->read('Config.language') == 'en_GER') {
            $lang   = 'title_ger';
        }
        $result['title'] = $array[$lang];
        foreach ($array['subscription_plans'] as $key => $value) {
            $result[$value['id']] = $value['_joinData']['is_available'];
        }
        return $result;
    }
    
}


