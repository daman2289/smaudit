<?php
namespace App\Event;

use App\Controller\AppController;
use Cake\Event\EventListenerInterface;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

class UserAddListener extends AppController implements EventListenerInterface
{

    public function implementedEvents()
    {
        return [
            'Model.User.afterAdd' => 'sendEmailToUser',
        ];
    }

    /**
     * Method sendEmailToUser to notify about their account creation
     *
     * @param $event object
     * @param $user array containing all the info of newly created user
     * @return bool
     */
    public function sendEmailToUser($event, $user)
    {
        $emailTemplatesTable = TableRegistry::get('EmailTemplates');
        $UserAddEmailTemplate = $emailTemplatesTable->find()
                                                        ->where([
                                                            'EmailTemplates.template_used_for' => 'add_user'
                                                        ])
                                                        ->first();

        $url = Router::url('/Users/activateAccount/' . $user['token'],true);
        $UserAddEmailTemplate->mail_body = str_replace(
            ['#NAME', '#EMAIL', '#PASSWORD', '#LINK'],
            [
                $user['first_name'] . ' ' . $user['last_name'],
                $user['email'],
                $user['password'],
                $url
            ],
            $UserAddEmailTemplate->mail_body
        );
        if ($this->_sendEmailMessage($user['email'], $UserAddEmailTemplate->mail_body, $UserAddEmailTemplate->subject)) {
            return true;
        }
        
        return false;
    }
}