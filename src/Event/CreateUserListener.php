<?php
namespace App\Event;

use Cake\Event\EventListenerInterface;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Utility\Text;

class CreateUserListener implements EventListenerInterface
{

    public function implementedEvents()
    {
        return [
            'HybridAuth.newUser' => 'createUser',
        ];
    }

    /**
     * createUser method to create user while social login if user record does not exists in users table
     *
     * @param $event
     * @return $user
     */
    public function createUser($event)
    {
        $usersTable = TableRegistry::get('Users');
        $profile = $event->data()['profile'];
        $user = $usersTable->newEntity([
                'email' => $profile->email,
                'first_name' => $profile->first_name,
                'last_name' => $profile->last_name,
                'role_id' => Configure::read('UserRoles.User'),
                'token' => Text::uuid(),
                'is_activated' => Configure::read('UserActivated.True'),
                'password' => Text::uuid()
            ]);
        // pr($user);die;
        $isUserSaved = $usersTable->save($user);
        
        if (!$isUserSaved) {
            throw new \RuntimeException('Unable to save new user');
        }

        return $user;
    }
}