<?php
namespace App\Event;

use App\Controller\AppController;
use Cake\Event\EventListenerInterface;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

class SubAdminAddListener extends AppController implements EventListenerInterface
{

    public function implementedEvents()
    {
        return [
            'Model.User.afterAdd' => 'sendEmailToSubAdmin',
        ];
    }

/**
 * Method sendEmailToSubAdmin to notify about their account creation
 *
 * @param $event object
 * @param $user array containing all the info of newly created sub admin user
 * @return bool
 */
    public function sendEmailToSubAdmin($event, $user)
    {
        $emailTemplatesTable = TableRegistry::get('EmailTemplates');
        $SubAdminAddEmailTemplate = $emailTemplatesTable->find()
                                                        ->where([
                                                            'EmailTemplates.template_used_for' => 'New Sub Admin'
                                                        ])
                                                        ->first();

        $url = Router::url('/admin',true);
        $SubAdminAddEmailTemplate->mail_body = str_replace(
            ['#NAME', '#EMAIL', '#PASSWORD', '#LINK'],
            [
                $user['first_name'] . ' ' . $user['last_name'],
                $user['email'],
                $user['password'],
                $url
            ],
            $SubAdminAddEmailTemplate->mail_body
        );
        if ($this->_sendEmailMessage($user['email'], $SubAdminAddEmailTemplate->mail_body, $SubAdminAddEmailTemplate->subject)) {
            return true;
        }
        
        return false;
    }
}