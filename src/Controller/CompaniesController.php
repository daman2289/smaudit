<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Text;
use App\Event\UserAddListener;
use Cake\Event\EventManager;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class CompaniesController extends AppController
{
	public function indexCompany() {
        if(!$this->checkPermission()){            
            $this->Flash->error(__('You do not have permission to access that location'));
            return $this->redirect($this->referer());
        }
		$this->viewBuilder()->layout('dashboard');
		$this->loadModel('LegalFormTypes');
		$legal = $this->LegalFormTypes->find('list');
		$this->loadModel('AccountingFrameworkTypes');
		$accounting = $this->AccountingFrameworkTypes->find('list');
		try {
            $companyTable = TableRegistry::get('OrganizationCompanies');
            $queryToGetcompanies = $companyTable->find()
            							->where(['OrganizationCompanies.organization_id' => $this->Auth->user('organization.id')])
            							->contain(['LegalFormTypes','AccountingFrameworkTypes']);
                                            
              // pr($queryToGetcompanies->toArray());die;                              
            $this->paginate = [
                    'sortWhitelist' => [
                        'name',
                        'place'
                    ],
                    'limit' => 25
                ];
            $companies = $this->paginate($queryToGetcompanies);
        } catch (NotFoundException $e) {
            // redirecting to Last page if request page doesn't exist
           
        }
        $this->set(compact('companies','legal','accounting'));
	}
	public function add() {
        if(!$this->checkPermission()){            
            $this->Flash->error(__('You do not have permission to access that location'));
            return $this->redirect($this->referer());
        }
		$this->viewBuilder()->layout('dashboard');
		$this->loadModel('OrganizationCompanies');
		$company = $this->OrganizationCompanies->newEntity();
	       if($this->request->is('post') ) {
	       	$this->request->data['organization_id'] = $this->Auth->user('organization.id');
	        $company = $this->OrganizationCompanies->patchEntity($company, $this->request->data);
	           if ($this->OrganizationCompanies->save($company)) {
	               $this->Flash->success(__('New Client-Company has been saved.'));
	               return $this->redirect($this->referer());
	           }
	       }
	       $this->set(compact('company'));
	}
	public function edit($id) {
        if(!$this->checkPermission()){            
            $this->Flash->error(__('You do not have permission to access that location'));
            return $this->redirect($this->referer());
        }
		$this->loadModel('OrganizationCompanies');
		$this->loadModel('LegalFormTypes');
		$legal = $this->LegalFormTypes->find('list');
		$this->loadModel('AccountingFrameworkTypes');
		$accounting = $this->AccountingFrameworkTypes->find('list');
		try {
            if ($id != null) {                
               $company = $this->OrganizationCompanies->find()
               ->where(
                   [
                       'OrganizationCompanies.id' => base64_decode($id)
                   ]
               )
               ->first();
             } 
           
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('company','legal','accounting'));
                $this->render('/Element/Companies/edit');
            }            
        } catch (RecordNotFoundException $e) {
            $this->Flash->success('Organization Company not found please try agian');
            return $this->redirect($this->referer());
        }

        if($this->request->is('post') ) {
            $company = $this->OrganizationCompanies->newEntity();
            $company->id = $this->request->data['id'];
            $this->request->data['organization_id'] = $this->Auth->user('organization.id');
            $company = $this->OrganizationCompanies->patchEntity($company, $this->request->data);
            if ($this->OrganizationCompanies->save($company)) {
                $this->Flash->success(__('
                    Organization company has been updated successfully.'));
                return $this->redirect(['action' => 'index-company']);
            } else {  	
                $this->Flash->error(__('Unable to update Company'));
            }
       }
       $this->set(compact('company'));
	}

	public function deactivate($id) {
       $company_id = base64_decode($id);
       $this->viewBuilder()->layout(false);
       $this->autoRender = false;
       $this->loadModel('OrganizationCompanies'); 
       $company = $this->OrganizationCompanies->find()
       				->where(['OrganizationCompanies.id' => $company_id ])
       				->first();
        $company->status = ($company->status) ? 0: 1;
      	$this->OrganizationCompanies->save($company);
        echo $company->status;
    }

    public function companyInfo() {
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('OrganizationSubsidiaries');
        $this->loadModel('OrganizationUsers');
        $this->loadModel('OrganizationCompanies');
        $this->loadModel('Reports');
        $this->loadModel('OrganizationSubscriptionPlans');
        // echo $this->Auth->user('id');die;
        $reports = $this->Reports->getUserReports($this->Auth->user('id'))->count();
        $subsidiary = $this->OrganizationSubsidiaries->find()
                    ->where(['OrganizationSubsidiaries.organization_id' => $this->Auth->user('organization.id')]);
        $users = $this->OrganizationUsers->find()
                    ->where(['OrganizationUsers.organization_id' => $this->Auth->user('organization.id')]);
        $companies = $this->OrganizationCompanies->find()
                    ->where(['OrganizationCompanies.organization_id' => $this->Auth->user('organization.id')]);
        $organizationPlan = $this->OrganizationSubscriptionPlans->getOrganizationPlan($this->Auth->user('organization.id'));

        $this->loadModel('OrganizationUsers');
        $userGraph = $this->OrganizationUsers->find('list',[
                    'keyField' => 'day',
                    'valueField' => 'userCount'
                ])
                ->select([
                    'userCount' => 'count(*)',
                    'day' => "str_to_date(concat(yearweek(OrganizationUsers.created),' monday'),'%X%V %W')"                    
                    ])
                ->where([
                    'OrganizationUsers.organization_id' => $this->Auth->user('organization.id')
                ])
                ->group(['yearweek(OrganizationUsers.created)'])
                ->toArray();

        try {
            $UsersTable = TableRegistry::get('Users');
            $queryToGetusers = $UsersTable->find()
                ->where(['Users.id' => $this->Auth->user('organization.user_id')])
                ->contain(['Organizations'])
                ->first();
                                            
        } catch (NotFoundException $e) {
            // redirecting to Last page if request page doesn't exist
           
        }
        $this->set(compact('queryToGetusers','subsidiary','users','companies', 'userGraph', 'reports','organizationPlan'));
    }

    public function uploadCompanies() {
        $this->loadModel('LegalFormTypes');
        $forms = $this->LegalFormTypes->find('list')->toArray();

        $this->loadModel('Organizations');
        $organization = $this->Organizations->find()
            ->where(['id' => $this->Auth->user('organization.id')])
            ->contain('OrganizationCompanies')
            ->first();
        $this->viewBuilder()->layout('dashboard');
        if($this->request->is('post')) {
            $file = fopen($this->request->data['user']['tmp_name'], 'r');
            while (($data = fgetcsv($file)) !== FALSE) {
              //$line is an array of the csv elements
                $rowData[] = $data ;
            }
            if(!empty($rowData)) {
                $keys = $this->noop($rowData[0]);
                $forms = array_flip($forms);
                foreach ($rowData as $key => $value) {
                    if($key != 0) {
                        $companies[$key] = array_combine ($keys , $value );
                        if(!empty($forms[$companies[$key]['legal_form_type_id'] ])) {
                            $companies[$key]['legal_form_type_id'] = $forms[$companies[$key]['legal_form_type_id'] ];
                        }else {
                             $companies[$key]['legal_form_type_id'] = 1;
                        }
                    }
                }
                if(empty($companies)) {
                    $this->Flash->error('Nothing to save.');
                    return $this->redirect($this->referer());
                }
                $organization = $this->Organizations->patchEntity($organization, ['organization_companies' => $companies]);
                // pr($organization);die;
                if($this->Organizations->save($organization)) {
                    $this->Flash->success('Companies uploaded successfully.');
                    return $this->redirect($this->referer());
                } else {
                    $this->Flash->error('Companies could not be uploaded please try again.');
                    return $this->redirect($this->referer());

                }
            }
        }
    }

    public function noop($array)
    { 
        foreach ($array as $key => $value) {
            if($value == 'Legal Form Type') {
                $array[$key] = 'legal_form_type_id';
            }else {
                $array[$key] = str_replace(' ', '_', strtolower($value));
            }
        }
        return $array;
    }

    public function download() {
        $file['path'] = WWW_ROOT .'sample'. DS . 'company.csv';
        $response = $this->response->withFile(
           $file['path'],
           ['download' => true, 'name' => 'companies.csv']
        );
        return $response;
    }
    
}