<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Response;
use PhpOffice\PhpWord\Exception\InvalidImageException;
use Cake\Core\Configure;
use Cake\Routing\Router;
/**
 * NewFinalReports Controller
 *
 * @property \App\Model\Table\NewFinalReportsTable $NewFinalReports
 *
 * @method \App\Model\Entity\NewFinalReport[] paginate($object = null, array $settings = [])
 */
class NewFinalReportsController extends AppController
{

    public $helpers = ['AkkaCKEditor.CKEditor'];

    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->layout('dashboard');
    }


    public function index($id)
    {        
        $this->viewBuilder()->layout(false);
        // pr($headers);die;
        $this->loadModel('NewFinalReports');
        try{
            $report = $this->NewFinalReports->find()
            ->where(['id' => base64_decode($id)])
            ->first();
        } catch(RecordNotFoundException $e) {

        }
        
        $language = $this->request->getQuery('language');
        $this->loadModel('ReportFooters');
        $this->loadModel('ReportHeaders');
        $footer = $this->ReportFooters->getOrganizationFooter($this->Auth->user('organization.id'));
        $headers = $this->ReportHeaders->getOrganizationHeader($this->Auth->user('organization.id'));
        $headers = $this->__getHeaderContent($headers, $report->company, $language);

        $head =     $this->pdfHeader($headers);
        $data =     $report->body_en;
        if($language) {
            ($language == 'en')? $data =  $report->body_en:'';
            ($language == 'er')? $data =  $report->body_er:'';
            ($language == 'fr')? $data =  $report->body_fr:'';
        }
        
        if($this->request->getParam('_ext') == 'pdf') {
            Configure::write('CakePdf.engine.options', [
                //'print-media-type' => false,
                'outline'          => true,
                'dpi'              => 50,
                'footer-html'     => 'https://smaudit.ch/homes/prepare-organization-pdf-footer/'.base64_encode($this->Auth->user('organization.id')),
            ]);
            $data = explode('<div style="page-break-after: always"><span style="display:none">&nbsp;</span></div>', $data);
            $newData = '';
            foreach ($data as $key => $value) {
                $newData = $newData . $head .'<br /><br /><br />'. $value . '<div style="page-break-after: always"><span style="display:none">&nbsp;</span></div>';
            }
            $data = $newData;
            $this->set(compact(['data']));
            $this->viewBuilder()->options([
                'pdfConfig' => [
                    'orientation' => 'portrait',
                    'filename' => 'lsereport.pdf'
                ],
            ]);
            $this->render('/Reports/pdf/report_pdf');
        }

        if($this->request->getParam('_ext') == 'doc') {
            // divide the data by pages so we can create pages in Word document
            $data = explode('<div style="page-break-after: always"><span style="display:none">&nbsp;</span></div>', $data);

            $phpWord = new \PhpOffice\PhpWord\PhpWord();
            // Adding Text element with font customized using named font style...
                $fontStyleName = 'footerStyle';
                $headerFontStyle = 'headerStyle';
                $phpWord->addFontStyle(
                    $fontStyleName,
                    $this->setReportFont($footer)
                );
                $phpWord->addFontStyle(
                    $headerFontStyle,
                    ['name' => 'Trebuchet MS', 'size' => '8']
                );
            foreach ($data as $key => $value) {
                // $value = str_replace('</p>', '</p><p>&nbsp;</p>', $value);
                $value = '<br/><br/><br/>'.$value;

                $section = $phpWord->addSection(array());
                $paragraphStyleName = 'pStyle';
                $phpWord->addParagraphStyle($paragraphStyleName, array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER, 'spaceAfter' => 100));

                if(!empty($headers)) {

                    $this->prepareHeader($section, $headers, $headerFontStyle);
                }

                if(!empty($footer)) {
                    $section->addFooter()->addText($footer->content, $fontStyleName);
                }
                // \PhpOffice\PhpWord\Shared\Html::addHtml($section, '<p style="margin-top: 240pt;">Ordered (numbered) list:</p>', false, false);
                try {
                    \PhpOffice\PhpWord\Shared\Html::addHtml($section, $value, false, false);
                    $section->addPageBreak();
                }catch (InvalidImageException $e) {
                    $this->Flash->error(__('Unable generate report due to : '. $e->getMessage()));
                    return $this->redirect($this->referer());
                }
            }

            // Saving the document as OOXML file...
            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');//die;
            $file = WWW_ROOT.DS.'files'.DS.base64_decode($id).'.docx';
            $objWriter->save($file);
            $response = $this->response->withFile($file);
            return $response;
        }
    }

    public function prepareHeader(&$section, $info, $style, $image='https://smaudit.ch/img/logo.png') {

        $header = $section->addHeader();
        $table = $header->addTable('Colspan Rowspan');
        $imagestyle = array(
                'width'            => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(2.1),
                'height'           => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(0.9),
                'positioning'      => \PhpOffice\PhpWord\Style\Image::POSITION_ABSOLUTE,
                'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_LEFT,
                'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_PAGE,
                // 'posVertical'      => \PhpOffice\PhpWord\Style\Image::POSITION_VERTICAL_TOP,
                'posVerticalRel'   => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_PAGE,
                //'posVerticalRel'   => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_LINE,
                'marginTop'        => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(0),
            );
        $row = $table->addRow();        
        $row->addCell(5500)->addImage($this->getCompanyLogo(),
            $imagestyle
        );
        $cell1 = $row->addCell(3200)->addTextRun();
        $cell2 = $row->addCell(3200)->addTextRun();
        if(!empty($info[0])) {
            $cell1->addText($info[0], $style);
            $cell1->addTextBreak();
        }
        if(!empty($info[1])) {
            $cell2->addText($info[1], $style);
            $cell2->addTextBreak();
        }

        if(!empty($info[2])) {
            $cell1->addText($info[2], $style);
            $cell1->addTextBreak();
        }
        if(!empty($info[3])) {            
            $cell2->addText($info[3], $style);
            $cell2->addTextBreak();
        }

        if(!empty($info[4])) {
            $cell1->addText($info[4], $style);
            $cell1->addTextBreak();
        }
        if(!empty($info[5])) {
            $cell2->addText($info[5], $style);
            $cell2->addTextBreak();
        }
    }

    function setReportFont($report) {
         $font['bold'] = false;
         $font['italic'] = false;
        if(!empty($report->footer_font_weight) && $report->footer_font_weight == 'Bold') {
            $font['bold'] = true;
        }
        if(!empty($report->footer_font_family)) {
            $font['name'] = $report->footer_font_family;
        }
        if(!empty($report->footer_font_size)) {
            $font['size'] = $report->footer_font_size;
        }
        if(!empty($report->footer_font_color)) {
            $font['color'] = $report->footer_font_color;
        }
        if(!empty($report->footer_font_style) && $report->footer_font_style == 'Italic') {
            $font['italic'] = true;
        }
        return $font;
    }

    protected function __joinTextImage(&$section, $images, $template) {
        foreach ($images as $key => $value) {
            // exploding template by image tag
            $parts = explode($value['html'], $template);
            // setting text if exists any befor image
            (!empty($parts[0])) ? $section->addText($parts[0]) : null;
            //setting image
            if(!empty($value['attr']) && !empty($value['attr']['src']) ) {
                $section->addImage($value['attr']['src']);   
            }
            // this is to handle the case of last text for last image
            if($key+1 == count($value) && !empty($parts[1]) ) {
                $section->addText($parts[1]);
            }
            //Updating template to second part of template so we can process for rest of images
            $template = $parts[1];
        }
    }

    protected function __getImage($data, $get_attrs = FALSE) {
        $post_images = array();
    
        // Get all images
        preg_match_all('/<img (.+)>/', $data, $image_matches, PREG_SET_ORDER);
//        // Loop the images and add the raw img html tag to $post_images
        foreach ($image_matches as $image_match)
        {
            $post_image['html'] = $image_match[0];            
            // If attributes have been requested get them and add them to the $post_image
            if ($get_attrs == TRUE)
            {
                preg_match_all('/\s+?(.+)="([^"]*)"/U', $image_match[0], $image_attr_matches, PREG_SET_ORDER);

                foreach ($image_attr_matches as $image_attr)
                {
                    $post_image['attr'][$image_attr[1]] = $image_attr[2];
                }
            }
            
            $post_images[] = $post_image;
        }
        return $post_images;
    }

    public function generateWord() {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        // New section
        $section = $phpWord->addSection();
        // Add Line elements
        // See Element/Line.php for all options
        $section->addText('Horizontal Line (Inline style):');
        $section->addLine(
            array(
                'width'       => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(4),
                'height'      => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(0),
                'positioning' => 'absolute',
            )
        );
        $section->addText('Vertical Line (Inline style):');
        $section->addLine(
            array(
                'width'       => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(0),
                'height'      => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(1),
                'positioning' => 'absolute',
            )
        );
        // Two text break
        $section->addTextBreak(1);
        $section->addText('Positioned Line (red):');
        $section->addLine(
            array(
                'width'            => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(4),
                'height'           => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(1),
                'positioning'      => 'absolute',
                'posHorizontalRel' => 'page',
                'posVerticalRel'   => 'page',
                'marginLeft'       => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(10),
                'marginTop'        => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(8),
                'wrappingStyle'    => \PhpOffice\PhpWord\Style\Image::WRAPPING_STYLE_SQUARE,
                'color'            => 'red',
            )
        );
        $section->addText('Horizontal Formatted Line');
        $section->addLine(
            array(
                'width'       => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(15),
                'height'      => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(0),
                'positioning' => 'absolute',
                'beginArrow'  => \PhpOffice\PhpWord\Style\Line::ARROW_STYLE_BLOCK,
                'endArrow'    => \PhpOffice\PhpWord\Style\Line::ARROW_STYLE_OVAL,
                'dash'        => \PhpOffice\PhpWord\Style\Line::DASH_STYLE_LONG_DASH_DOT_DOT,
                'weight'      => 10,
            )
        );
        // Save file
        // Saving the document as OOXML file...
            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
            $objWriter->save(WWW_ROOT.DS.'files'.DS.'hello.docx');die;

    }

    /**
     * View method
     *
     * @param string|null $id New Final Report id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $newFinalReport = $this->NewFinalReports->get($id, [
            'contain' => ['NewReports', 'Users']
        ]);

        $this->set('newFinalReport', $newFinalReport);
        $this->set('_serialize', ['newFinalReport']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $newFinalReport = $this->NewFinalReports->newEntity();
        if ($this->request->is('post')) {
            $newFinalReport = $this->NewFinalReports->patchEntity($newFinalReport, $this->request->getData());
            if ($this->NewFinalReports->save($newFinalReport)) {
                $this->Flash->success(__('The new final report has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The new final report could not be saved. Please, try again.'));
        }
        $newReports = $this->NewFinalReports->NewReports->find('list', ['limit' => 200]);
        $users = $this->NewFinalReports->Users->find('list', ['limit' => 200]);
        $this->set(compact('newFinalReport', 'newReports', 'users'));
        $this->set('_serialize', ['newFinalReport']);
    }

    /**
     * Edit method
     *
     * @param string|null $id New Final Report id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $newFinalReport = $this->NewFinalReports->get(base64_decode($id), [
            'contain' => []
        ]);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $newFinalReport = $this->NewFinalReports->patchEntity($newFinalReport, $this->request->getData());
            if ($this->NewFinalReports->save($newFinalReport)) {
                $this->Flash->success(__('The new final report has been saved.'));

                return $this->redirect(['controller' => 'new-reports', 'action' => 'index']);
            }
            $this->Flash->error(__('The new final report could not be saved. Please, try again.'));
        }
        $newReports = $this->NewFinalReports->NewReports->find('list', ['limit' => 200]);
        $users = $this->NewFinalReports->Users->find('list', ['limit' => 200]);
        $language = $this->request->getQuery('lan');
        $this->set(compact(
                'newFinalReport', 
                'newReports', 
                'users', 
                'language'
            )
        );
        $this->set('_serialize', ['newFinalReport', 'language']);
    }

    /**
     * Delete method
     *
     * @param string|null $id New Final Report id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $newFinalReport = $this->NewFinalReports->get(base64_decode($id));
        if ($this->NewFinalReports->delete($newFinalReport)) {
            $this->Flash->success(__('The new final report has been deleted.'));
        } else {
            $this->Flash->error(__('The new final report could not be deleted. Please, try again.'));
        }

        return $this->redirect($this->referer());
    }

    public function __getHeaderContent($infos, $company, $language) {
        $this->loadModel('NewPlaceholders');
        $this->loadModel('OrganizationUsers');
        $this->loadModel('OrganizationSubsidiaries');
        $placeholders= [];
        // Setting organization detail for replacement
        $data['organization'] = $this->Auth->user('organization');
        // Setting Subsidiary details
        $data['subsidiary'] = $this->__getSubsidiaryDetail();
        // get list of system placeholders
        $data['company'] = $this->__getCompanyDetail($company);
        // get list of system placeholders

        foreach ($infos as $key => $value) {
            if(!empty($value->info_field)) {

                // getting model and field part from _ seprated string
                $match = explode('_', $value->info_field->field);
                if(in_array($match[0], ['company', 'organization', 'subsidiary'])) {
                    $model = $match[0];
                } else {
                    $model = 'organization';
                }
                unset($match[0]);
                $field = implode('_', $match);
                if($model == 'company' && $field == 'name') {
                    $field = 'company_name';
                }
                // setting placeholder replacement corresponding to their type company, orgni.., subsidi..
                if(!empty($data[$model]) && !empty($data[$model][$field])) {
                    if($field == 'zip' || $field == 'place') {
                        $placeholders[$key] = $data[$model]['zip'].' '. $data[$model]['place'];
                    }elseif($field == 'street' || $field == 'street_number') {
                        $placeholders[$key] = $data[$model]['street'].' '. $data[$model]['street_number'];
                    }elseif($field == 'phone') {
                        $label = 'Phone: ';
                        if($language == 'er'){$label =  'Tel.: ';} 
                        if($language == 'fr'){$label =  'Tél.: ';} 
                        $placeholders[$key] = $label . $data[$model][$field];
                    }elseif($field == 'fax') {
                        $label = 'Fax: ';
                        $placeholders[$key] = $label . $data[$model][$field];
                    }
                    else {
                        $placeholders[$key] = $data[$model][$field];
                    }
                } else {
                    // if not found set default to organization
                    $placeholders[$key] = $this->Auth->user('organization.'.$field);
                }
            }
        }
        return $placeholders;
    }

    protected function __getSubsidiaryDetail() {
        if($this->Auth->user('organization_user')) {
            $userSubsidiary = $this->OrganizationSubsidiaries->get($this->Auth->user('organization_user.organization_subsidiary_id')); 
            if(!empty($userSubsidiary)){
                return $userSubsidiary->toArray();
            }
            return [];

        }else {
            $organizationDefaultSubsidiary = $this->OrganizationSubsidiaries->find()
                ->where([
                    'OrganizationSubsidiaries.organization_id' => $this->Auth->user('organization.id'),
                    'OrganizationSubsidiaries.is_primary' => 1
                ])
                ->first();
            if(empty($organizationDefaultSubsidiary)) {
                $organizationDefaultSubsidiary = $this->OrganizationSubsidiaries->find()
                ->where([
                    'OrganizationSubsidiaries.organization_id' => $this->Auth->user('organization.id')
                ])
                ->first();
            } 
            if(!empty($organizationDefaultSubsidiary)) {
                return $organizationDefaultSubsidiary->toArray();
            }
            return [];
        }
    }

    protected function __getCompanyDetail($company_name) {
        $this->loadModel('OrganizationCompanies');
        $organizationCompany = $this->OrganizationCompanies->find()
        ->where([
            'OrganizationCompanies.organization_id' => $this->Auth->user('organization.id'),
            'OrganizationCompanies.company_name' => $company_name
        ])
        ->first(); 
        if(empty($organizationCompany)) {
            $organizationCompany = $this->OrganizationCompanies->find()
                ->where([
                    'OrganizationCompanies.organization_id' => $this->Auth->user('organization.id'),
                    'OrganizationCompanies.status' => 1
                ])
                ->first(); 
        }
        if(!empty($organizationCompany)) {
            return $organizationCompany->toArray();
        }
        return [];
    }

    protected function getCompanyLogo() {
        $image = $this->__companylogo(
            'organizations', 
            'logo', 
            $this->Auth->user('organization.logo_dir'), 
            'word_', 
            $this->Auth->user('organization.logo')
        );
        $image = Router::url($image,true);
        return  $image;
    }



    protected function __companylogo ($table, $field, $dir, $prefix, $image) {
        $filePath = 'logo' . DS . $table . DS . $field . DS . $dir . DS . $prefix . $image;
        if (file_exists ( WWW_ROOT.$filePath )) {
            return DS . $filePath;
        } else {
            return DS . 'img' . DS . 'logo.png';
        }
    } 

    protected function pdfHeader($headers) {
        $str = '<table style="text-align: left;"><tr>';
        $image = $this->getCompanyLogo();
        $str = $str.'<th width="450"><img src="'.$image.'" style="height:45px; width:110px;"></th>';
        $col1 = '<th style="font-weight:normal; font-size:11px;"  width="200">';
            if(!empty($headers[0])) {
                $col1 = $col1. $headers[0] .'<br>';
            }
            if(!empty($headers[2])) {
                $col1 = $col1. $headers[2] .'<br>';
                
            }
            if(!empty($headers[4])) {            
                $col1 = $col1. $headers[4] .'<br>';           
            }
        $col1 = $col1.'</th>';
        $col2  = '<th style="font-weight:normal; font-size:11px;" width="200">';
            if(!empty($headers[1])) {
                $col2 = $col2.$headers[1] .'<br>';
            }

            if(!empty($headers[3])) {
                $col2 = $col2.$headers[3] .'<br>';
            }

            if(!empty($headers[5])) {
                $col2 = $col2.$headers[5] .'<br>';
            }
        $col2 = $col2.'</th>';

        return $str . $col1. $col2.'</tr></table>';
    }
}
