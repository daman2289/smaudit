<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Text;
// use App\Event\UserAddListener;
// use Cake\Event\EventManager;
// use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['signup', 'forgotPassword','recoverPassword']);
    }

    /**
     * signup method
     *
     * @return \Cake\Network\Response|null
     */
    public function signup()
    {
        $this->viewBuilder()->layout('home');
        $user = $this->Users->newEntity();
        $error = false;
        if ($this->request->is('post')) {
            $this->request->data['token'] = Text::uuid();
            $this->request->data['role_id'] = Configure::read('UserRoles.Organization Manager');
            $this->request->data['organization']['trial_start'] = date('Y-m-d');
            // pr($this->request->data);die;
            $user = $this->Users->patchEntity($user, $this->request->data,[
                    'associated' => ['Organizations']]);
            if ($this->Users->save($user)) {
               //setting default user permission
                $this->loadModel('EmailTemplates');
                //Fake Template : in future we can replace with our template
                $temp = $this->EmailTemplates->find()->where(['EmailTemplates.id' =>1])
                    -> first();
                   
                $link = Router::url(
                        array('controller' => 'Users', 'action' => 'activate',$this->request->data['token']),
                        true
                    );
                $lang =  'mail_body_ger';
                if($this->request->data['language'] == 'eng') {
                    $lang = 'mail_body_eng';
                }
                $temp[$lang] = str_replace(
                    array('#NAME','#LINK'),
                    array(
                        $this->request->data['organization']['name'],
                        $link
                    ), 
                    $temp[$lang]
                );
                $this->_sendEmailMessage($this->request->data['email'], $temp[$lang], $temp['subject']);
                 $this->Flash->success(__('Yeah, your Account has been successfully created at smaudit.ch. To activate your account, click on the activation link which has been sent to your E-Mail Adress.'));
                 $this->redirect(['action' => 'login']);
            } else {
                $error = true;
                $errors = $this->_setValidationError($user->errors());
                $this->Flash->error(__('Your account could not be created due to the below errors. Please, try again.' . $errors));
            }
        }
        $this->set(compact('user', 'error'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Add __sendSubAdminCreateEmail to send email to sub admin user after account creation by super admin
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    private function __sendUserCreateEmail() {
        $UserAddListener = new UserAddListener();
        EventManager::instance()->attach($UserAddListener);

        $event = new Event('Model.User.afterAdd', $this, [
            'user' => $this->request->data
        ]);
        $this->eventManager()->dispatch($event);
        return true;
    }

    /**
    * activateAccount method to activate newly created account
    *
    * @param $token string token to match user record
    * @return void
    */
    public function activate($token) {
        $user = $this->Users->find()
                            ->where([
                                'Users.token' => $token
                            ]);

        if ($user->isEmpty()) {
            $this->Flash->error(__('This link has been expired. You have already used this link.'));
            return $this->redirect('/');
        }
        $user = $user->first();
        $user->is_activated = Configure::read('UserActivated.True');
        $user->token = Text::uuid();
        
        if ($this->Users->save($user)) {
            $this->Flash->success(__('Your account has been successfully activated. Please log-in and enjoy.'));
            return $this->redirect(['action' => 'login']);
        }
        $this->Flash->error(__('Some errors occurred while activating your account. Please try again.'));
        return $this->redirect(['action' => 'login']);
    }

    public function forgotPassword() {
        if($this->Auth->user()) {
            // pr('fdfdfd');die;
            return $this->redirect([
                    'controller' => 'Reports',
                    'action' => 'reportListing'
                ]
            );
        }
        $this->viewBuilder()->layout('home');
        $this->loadModel('Users');
        if($this->request->is('post')){
            if (empty($this->request->data)) {
                throw new NotFoundException;
            }
            $data = $this->request->data;
            if(!empty($data['email'])){
                $user = $this->Users->find()->where(['Users.email' => $data['email']])
                    ->contain(['Organizations'])
                    ->first();
                if(empty($user)){
                   $this->Flash->error(__('Email does not exist. Please provide correct email or signup')
                 ); 
                   $this->redirect($this->referer());
                }               
                    
                    $key = $user['token'];
                           $link = Router::url(array(
                           'controller' => 'Users',
                           'action' => 'recoverPassword',
                           $key,
                            ),
                           true
                        );
                        $this->loadModel('EmailTemplates');
                        $temp = $this->EmailTemplates->find()->where(['EmailTemplates.id' => 2])
                                -> first();
                        $lang =  'mail_body_ger';          
                        if($this->request->session()->read('Config.language') == 'en_US') {
                            $lang =  'mail_body_eng'; 
                        }
                          $temp[$lang] = str_replace(
                            array('#NAME','#LINK','#EMAIL'),
                            array($user['organization']['name'],
                                $link,
                                $user['email']), 
                            $temp[$lang]);
                          // pr($temp['EmailTemplate']['mail_body']);die;
                          $this->_sendEmailMessage($data['email'], $temp[$lang], $temp['subject']);
                          $this->Flash->success(__('Forgot password link has been sent to your registered email account.'
                            ));
                        $this->redirect(['action'=>'login']);
                                     
                }
                $this->redirect(['action'=>'login']);
            } 
    }
    public function recoverPassword($token) {
        $this->viewBuilder()->layout('home');
        $this->loadModel('Users');
        if($this->request->is('post')){
                $user = $this->Users->find()
                        -> where(['Users.token' => $token])
                        ->first();
                if(empty($user)){
                    $this->Flash->error(__('Link has been expired please Try again.')
                 );
                    return $this->redirect($this->referer());
                }
            
            if(!empty($this->request->data['password']) && $this->request->data['password'] == $this->request->data['new_password']){
                $this->request->data['token'] = Text::uuid();
                $user = $this->Users->patchEntity($user, $this->request->data);
                $user->id = $user['id'];
                if( $this->Users->save($user) ) {
                    $this->Flash->success(__('Password has been changed successfully. Login with your new password')
                 );
                    $redirectUrl = array('controller'=> 'Users','action'=>'login');
                    $this->redirect($redirectUrl);
                }
            }else{

            $this->Flash->error(__('Make sure that both passwords match.')
                 );
            }
            $redirectUrl = array('controller'=> 'Users','action'=>'login');
                    $this->redirect($redirectUrl);
        }
    }
    /**
    * dashboard method to show user dashboard after successfully loged in
    *
    * @return void
    */
    public function dashboard() {
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('OrganizationSubsidiaries');
        $this->loadModel('OrganizationUsers');
        $this->loadModel('OrganizationCompanies');
        $this->loadModel('Reports');
        $reports = $this->Reports->getUserReports($this->Auth->user('id'))->count();
        $subsidiary = $this->OrganizationSubsidiaries->find()
                    ->where(['OrganizationSubsidiaries.organization_id' => $this->Auth->user('organization.id')]);
        $users = $this->OrganizationUsers->find()
                    ->where(['OrganizationUsers.organization_id' => $this->Auth->user('organization.id')]);
        $companies = $this->OrganizationCompanies->find()
                    ->where(['OrganizationCompanies.organization_id' => $this->Auth->user('organization.id')]);


        $this->loadModel('OrganizationUsers');
        $userGraph = $this->OrganizationUsers->find('list',[
	                'keyField' => 'day',
	                'valueField' => 'userCount'
	            ])
	            ->select([
	                'userCount' => 'count(*)',
	                'day' => "str_to_date(concat(yearweek(OrganizationUsers.created),' monday'),'%X%V %W')"                    
	                ])
	            ->group(['yearweek(OrganizationUsers.created)'])
	            ->toArray();

        try {
            $UsersTable = TableRegistry::get('Users');
            $queryToGetusers = $UsersTable->find()
                            ->where(['Users.id' => $this->Auth->user('id')])
                            ->contain(['Organizations'])
                            ->first();
                                            
        } catch (NotFoundException $e) {
            // redirecting to Last page if request page doesn't exist
           
        }
        $this->set(compact('queryToGetusers','subsidiary','users','companies', 'userGraph', 'reports'));

    }

     /**
    * login method to login users
    *
    * @return void
    */
    public function login() {
        $this->viewBuilder()->layout('home');
        if($this->Auth->user()) {
            return $this->redirect([
                    'controller' => 'new-reports'
                ]
            );
        }
        if ($this->request->is('post') || $this->request->query('provider')) {
            $user = $this->Auth->identify();
            $isUserAllowedToLogin = true;
            if (!$user) {
                $this->Flash->error(__('Invalid username or password! Try again.'));
                $isUserAllowedToLogin = false;
            } elseif($user['organization']['is_deleted']){
                 $this->Flash->error(__('Organization has been deleted.'));
                 $isUserAllowedToLogin = false;
            }elseif (!in_array($user['role_id'], [Configure::read('UserRoles.Organization Manager'),Configure::read('UserRoles.Organization User')])) {
                $this->Flash->error(__('Prankster, you are not allowed to login here.'));
                $isUserAllowedToLogin = false;
            } elseif (!$user['is_activated']) {
                $this->Flash->error(__('Your account is deactivated. Please contact us.'));
                $isUserAllowedToLogin = false;
            }elseif ($user['is_admin_activated'] == 0) {
                $this->Flash->error(__('Your account has been deactivated. Please contact administrator.'));
                $isUserAllowedToLogin = false;
            }elseif ($user['is_deleted'] == 1) {
                $this->Flash->error(__('Your account has been deleted. Please contact administrator.'));
                $isUserAllowedToLogin = false;
            }
            if ($isUserAllowedToLogin) {
                if(empty($user['organization']) && !empty($user['organization_user']['organization'])) {
                    $user['organization'] = $user['organization_user']['organization'];
                    // pr($user);die;
                }
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
        }
    }

    /**
     * To logout the admin user
     *
     * @return void|\Cake\Network\Response
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
    
    public function changePassword() {
        if ($this->request->is('post')) {
            // pr($this->request->data);die;
            $usersTable = TableRegistry::get('Users');
            $userId = $this->Auth->user('id');
            $adminUser = $usersTable->get($userId);
            $adminUser = $usersTable->patchEntity($adminUser, $this->request->data);
            if ($usersTable->save($adminUser)) {
                $this->Flash->success(__('Your password has been updated successfully.'));
                return $this->redirect($this->referer());
            }

            $errors = $this->_setValidationError($adminUser->errors());
            $this->Flash->error(__('Oops, Password update not completed due to the following errors: - ' . $errors));
            return $this->redirect($this->referer());
        }
    }
    
    public function profile($id)
    {
        if(!$this->checkPermission()){            
            if(!$this->request->is('ajax')) {
                $this->Flash->error(__('You do not have permission to access that location'));
                return $this->redirect($this->referer());
            }
        }
        $this->loadModel('Users');
        try {
           if ($id != null) {                
               $user = $this->Users->find()
                   ->where(
                       [
                           'Users.id' => base64_decode($id)
                       ]
                   )
                   ->contain(['Organizations'])
                   ->first();
           } 
           // pr($user);die;
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('user'));
                $this->render('/Element/Frontend/Dashboard/profile');
            }            
       } catch (RecordNotFoundException $e) {
           $this->Flash->success('Organization not found please try agian');
           return $this->redirect($this->referer());
       }
       if($this->request->is(['patch', 'post', 'put']) ) {
            $this->request->data['id'] = $user->id;
            $this->request->data['organization']['id'] = $user['organization']->id;
            $user = $this->Users->patchEntity($user, $this->request->data,['associated' => ['Organizations']]);
            if ($this->Users->save($user)) {
                if($this->Auth->user('role_id')  == 2 ) {
                    $this->Auth->setUser($user->toArray());
                }else {
                    $client = $this->Auth->user();
                    $client['organization'] = $user->organization;
                }
               $this->Flash->success(__('
                   Organization has been updated successfully.'));
               return $this->redirect(['controller' => 'Companies', 'action' => 'CompanyInfo']);
            }
           $this->Flash->error(__('Unable to update Organization'));
           return $this->redirect($this->referer());
       }
       $this->set(compact('user'));
    }
    public function support() {
        if($this->request->is('post')) {
            $this->loadModel('EmailTemplates');
            //Fake Template : in future we can replace with our template
            $temp = $this->EmailTemplates->find()->where(['EmailTemplates.id' =>6])
                -> first();  
            $lang =  'mail_body_ger';             
            if($this->Auth->user('language') == 'eng') {
                        $lang =  'mail_body_eng';
                    }
            $temp[$lang] = str_replace(
                array('#NAME','#EMAIL','#ORGANIZATION','#QUESTION'),
                array(
                    $this->Auth->user('first_name'),
                    $this->Auth->user('email'),
                    $this->Auth->user('organization.name'),
                    $this->request->data['message'],
                ), 
                $temp[$lang]
            );
            // pr($temp['mail_body']);die;
            $this->_sendEmailMessage(Configure::read('ContactUsInfo.Email'), $temp[$lang], $temp['subject']);
             $this->Flash->success(__('Your query has submitted successfully.Admin will contact you as soon as possible'));
             $this->redirect($this->referer());
        }
    }

    public function deleteOrganization() {
        $organizationTable = TableRegistry::get('Organizations');      
        if($organizationTable->deleteOrganization($this->Auth->user('organization.id'))) {
            $this->Flash->success(__('Organization has been deleted successfully.'));
            return $this->redirect($this->Auth->logout());
        }
        $this->Flash->error(__('Unable to delete Organization'));
        return $this->redirect($this->referer());
    }
}
