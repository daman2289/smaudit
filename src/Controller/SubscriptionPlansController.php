<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
/**
 * SubscriptionPlans Controller
 *
 * @property \App\Model\Table\SubscriptionPlansTable $SubscriptionPlans
 */
class SubscriptionPlansController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('OrganizationSubscriptionPlans');
        $this->loadModel('AdditionalUserSubscriptionPlans');
        $this->loadModel('OrganizationUsers');
        $packages = $this->SubscriptionPlans->find()
            ->where(['SubscriptionPlans.is_activated' => 1,'SubscriptionPlans.is_deleted' => 0])
            ->contain('PlanFeatures');

        $organizationPlan = $this->OrganizationSubscriptionPlans->getOrganizationPlan($this->Auth->user('organization.id'));
        $additionalUserSubscriptionPlans = $this->AdditionalUserSubscriptionPlans->find()
            ->where(['AdditionalUserSubscriptionPlans.is_activated' => 1]);
        $this->loadModel('Organizations');
        $organizationAdditinalPlan   =  $this->AdditionalUserSubscriptionPlans->getAdditionalPlanByOrganization($this->Auth->user('organization.id'));
        $users = $this->OrganizationUsers->getOrganizaionAllUsers($this->Auth->user('organization.id'))->count();
        $this->set(compact(['header', 'footer', 'packages', 'organizationPlan','additionalUserSubscriptionPlans', 'organizationAdditinalPlan', 'users']));       
        $this->set('_serialize', ['packages']);
    }

    private function formattPlanData($data) {       
        $formattedData['id'] = 'Plan-'.rand(99,1000);
        $formattedData['name'] = $data['title']; 
        $formattedData['amount'] = $data['price']*100;
        $formattedData['interval'] = $data['frequency'];        
        $formattedData['currency'] = 'usd';
        return $formattedData;
    }

    protected function _stripeError($e){
        $body = $e->getJsonBody();
        $this->Flash->error($body['error']['message']);
        return false;
    }

    private function createStripePlan($data) {        
        if(empty($data)) {
            return false;
        }
        $data = $this->formattPlanData($data);
        \Stripe\Stripe::setApiKey(Configure::read('Stripe.secret_key'));
          
        try {
            $plans = \Stripe\Plan::create($data);
            // pr($plans->__toArray(true));die;
            $this->request->data['stripe_plan_id'] = $plans->__toArray(true)['id'];
            // pr($this->request->data);die;
        } catch(\Stripe\Error\Card $e) {
          // Since it's a decline, \Stripe\Error\Card will be caught
            return $this->_stripeError($e);
        } catch (\Stripe\Error\RateLimit $e) {
            return $this->_stripeError($e);
        } catch (\Stripe\Error\InvalidRequest $e) {
            return $this->_stripeError($e);
        } catch (\Stripe\Error\Authentication $e) {
            return $this->_stripeError($e);
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            return $this->_stripeError($e);
        } catch (\Stripe\Error\Base $e) {
          // Display a very generic error to the user, and maybe send
            return $this->_stripeError($e);
        } catch (Exception $e) {
          // Something else happened, completely unrelated to Stripe
            return $this->_stripeError($e);
        }
        return true;      
    }
    

    public function updateStripePlan() {
        \Stripe\Stripe::setApiKey(Configure::read('Stripe.secret_key'));
        $plans = \Stripe\Plan::all(array("limit" => 9)); 
            $this->loadModel('AdditionalUserSubscriptionPlans');
        foreach ($plans->data as $key => $value) { 
            $package = $this->AdditionalUserSubscriptionPlans->find()
                ->where([
                        'stripe_plan_id' => $value->id
                    ]
                )->first(); 
                
            if(!empty($package)) {
                $data[$key]['id'] = $package->id;
                $data[$key]['price'] = $value->amount ;
                $data[$key]['frequency'] = $value->interval;
                $data[$key]['title'] = $value->name;
                $data[$key]['interval_count'] = $value->interval_count;
                $data[$key]['currency'] = $value->currency;                
            }
        } 
        // pr($data);die;
        if(!empty($data)) {
            $packages = $this->AdditionalUserSubscriptionPlans->newEntities($data); 
            foreach ($packages as $package) {  
                $this->AdditionalUserSubscriptionPlans->save($package);
            }
        }       
    }

    public function stripeCallback() {
        $status = $this->checkOrganizationPlan($this->request->data);

        \Stripe\Stripe::setApiKey(Configure::read('Stripe.secret_key'));
        try {
            $customer =\Stripe\Customer::create(array(
                "source" => $this->request->data['stripeToken'],
                "plan" => $this->request->data['plan_id'],
                "email" => $this->request->data['stripeEmail']
            ));            
            $customerDetail = $customer->__toArray(true);
            $userId = $this->Auth->User('id');
            
            //Get the planes to which custoer has to be subscribed.
            $subscriptionPlan = $this->SubscriptionPlans->getSubscriptionPlan($customerDetail['subscriptions']['data'][0]['plan']['id']);

            $data = [
                'subscription_plan_id' => $subscriptionPlan['id'],
                'expire_date' => new Time($customerDetail['subscriptions']['data'][0]['current_period_end']),
                'amount' => $customerDetail['subscriptions']['data'][0]['plan']['amount'],
                'message' => __('You are successfully subscribe for '.$customerDetail['subscriptions']['data'][0]['plan']['name'].'.'),
                'stripe_customer_id' => $customerDetail['id'],
                'stripe_subscription_id' => $customerDetail['subscriptions']['data'][0]['id'],
            ];

            $this->saveOrganizationPlan($data, $this->Auth->User('organization.id'), false);

        } catch(\Stripe\Error\Card $e) {
          // Since it's a decline, \Stripe\Error\Card will be caught
          $error = $this->_stripeError($e);
        } catch (\Stripe\Error\RateLimit $e) {
            $error = $this->_stripeError($e);
        } catch (\Stripe\Error\InvalidRequest $e) {
            $error = $this->_stripeError($e);
        } catch (\Stripe\Error\Authentication $e) {
            $error = $this->_stripeError($e);
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $error = $this->_stripeError($e);
        } catch (\Stripe\Error\Base $e) {
          // Display a very generic error to the user, and maybe send
            $error = $this->_stripeError($e);
        } catch (Exception $e) {
          // Something else happened, completely unrelated to Stripe
            $this->_stripeError($e);
        }
        // $this->Flash->success(__('Something went wrong please try again!'));        
        return $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
    }

    // save deatil in user package in user packages table
    protected function saveOrganizationPlan($data, $organization_id, $webhook) {
        //var_dump($webhook); die;
        $this->loadModel('OrganizationSubscriptionPlans');
        // get user  trail package
        $organizationSubscriptionPlan = $this->OrganizationSubscriptionPlans->newEntity();
        $userPack = $this->OrganizationSubscriptionPlans->getOrganizationPlan($organization_id);
        if (!empty($userPack)){
            $organizationSubscriptionPlan = $this->OrganizationSubscriptionPlans->get($userPack['id']);
        }

        $userPackeges = [
            'organization_id' => $organization_id,
            'stripe_customer_id' => $data['stripe_customer_id'],
            'stripe_subscription_id' => $data['stripe_subscription_id'],
            'subscription_plan_id' => $data['subscription_plan_id'],
            'expire_date' => $data['expire_date']
        ];
        
        $userPackeges['organization_payments'][]= [
            'amount' => $data['amount'],
            'message' => $data['message']
        ];

        // pr($userPackeges);die;
        $organizationSubscriptionPlan = $this->OrganizationSubscriptionPlans->patchEntity($organizationSubscriptionPlan, $userPackeges, [
                'associated' => ['OrganizationPayments'],
        ]);
        
        if ($this->OrganizationSubscriptionPlans->save($organizationSubscriptionPlan)) {
            if ($webhook) {
                $this->response->body('ok');
                return $this->response;
            }    
            $this->Flash->success(__($data['message']));        
            return $this->redirect(['action' => 'index']);
        }
        $this->Flash->success(__('Something went wrong please try again!'));        
        return $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
    }

    protected function checkOrganizationPlan($data){      
    // pr($data);die;  
        $this->loadModel('OrganizationSubscriptionPlans');
        $organizationPackageExist = $this->OrganizationSubscriptionPlans->getOrganizationPlan($this->Auth->User('organization.id'));
        
        if (empty($organizationPackageExist)) {
            return true;
        }

        if ($organizationPackageExist['OrganizationSubscriptionPlans']['stripe_subscription_id'] ==  $data['plan_id']) {            
            $this->Flash->Message(__('You are already enrolled with this plan'));
            return $this->redirect($this->referer());
        }
        try{
            $subscriptionId = $organizationPackageExist['stripe_subscription_id'];
            \Stripe\Stripe::setApiKey(Configure::read('Stripe.secret_key'));
            $subscription = \Stripe\Subscription::retrieve($subscriptionId);
            
            $subscription->plan = $data['plan_id'];

            if ($subscription->save()) {
                $subscriptionChange = $subscription->__toArray(true);

                $this->loadModel('SubscriptionPlans');
                $packageDetail = $this->SubscriptionPlans->getSubscriptionPlan($subscriptionChange['plan']['id']);
                $data = [
                    'subscription_plan_id' => $packageDetail['id'],
                    'expire_date' => new Time($subscriptionChange['current_period_end']),
                    'amount' => $subscriptionChange['plan']['amount'],
                    'message' => __('You  subscription plan ('.$subscriptionChange['plan']['name'].') has been changed.'),
                    'stripe_customer_id' => $subscriptionChange['customer'],
                    'stripe_subscription_id' => $subscriptionChange['id'],
                ];
            
                $status = $this->saveOrganizationPlan($data, $this->Auth->User('organization.id'), false);
                if ($status == null);
                    return true;
            }
        } catch (Exception $e) {
          // Something else happened, completely unrelated to Stripe
            $this->_stripeError($e);
        }
    }
}
