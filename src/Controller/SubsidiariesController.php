<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Text;
use App\Event\UserAddListener;
use Cake\Event\EventManager;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class SubsidiariesController extends AppController
{
	public function index() {
		$this->viewBuilder()->layout('dashboard');        
		try {
            $subsidiaryTable = TableRegistry::get('OrganizationSubsidiaries');
            $queryToGetsubsidiaries = $subsidiaryTable->find()
    			->where([
                    'OrganizationSubsidiaries.organization_id' => $this->Auth->user('organization.id')
                    ]
                );                                            
                                            
            $this->paginate = [
                    'sortWhitelist' => [
                        'name',
                        'place'
                    ],
                    'limit' => 25
                ];
            $subsidiaries = $this->paginate($queryToGetsubsidiaries);
        } catch (NotFoundException $e) {
            // redirecting to Last page if request page doesn't exist
           
        }
        $this->set(compact('subsidiaries'));
	}

	public function add() {
        if(!$this->checkPermission()) {            
            $this->Flash->error(__('You do not have permission to access that location'));
            return $this->redirect($this->referer());
        }
		$this->viewBuilder()->layout('dashboard');
		$this->loadModel('OrganizationSubsidiaries');
		$subsidiary = $this->OrganizationSubsidiaries->newEntity();
        if($this->request->is('post') ) {
           	$this->request->data['organization_id'] = $this->Auth->user('organization.id');
            $subsidiary = $this->OrganizationSubsidiaries->patchEntity($subsidiary, $this->request->data);
            if ($this->OrganizationSubsidiaries->save($subsidiary)) {
               $this->Flash->success(__('New Subsidiary has been saved.'));
               return $this->redirect($this->referer());
            }
        }
        $this->set(compact('subsidiary'));
	}

	public function deactivate($id) {
       $subid = base64_decode($id);
       $this->viewBuilder()->layout(false);
       $this->autoRender = false;
       $this->loadModel('OrganizationSubsidiaries'); 
       $subsidiary = $this->OrganizationSubsidiaries->find()
       				->where(['OrganizationSubsidiaries.id' => $subid ])
       				->first();
        $subsidiary->status = ($subsidiary->status) ? 0: 1;
      	$this->OrganizationSubsidiaries->save($subsidiary);
       
    }
    
    public function edit($id) {
        if(!$this->checkPermission()){            
            $this->Flash->error(__('You do not have permission to access that location'));
            return $this->redirect($this->referer());
        }
    	$this->loadModel('OrganizationSubsidiaries');
		try {
           if ($id != null) {                
               $subsidiary = $this->OrganizationSubsidiaries->find()
                   ->where(
                       [
                           'OrganizationSubsidiaries.id' => base64_decode($id)
                       ]
                   )
                   ->first();
           } 
           
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('subsidiary'));
                $this->render('/Element/Subsidiaries/edit');
            }            
       } catch (RecordNotFoundException $e) {
           $this->Flash->success('Organization subsidiary not found please try agian');
           return $this->redirect($this->referer());
       }
       if($this->request->is('post') ) {
           $subsidiary = $this->OrganizationSubsidiaries->newEntity();
           $subsidiary->id = $this->request->data['id'];
           $subsidiary = $this->OrganizationSubsidiaries->patchEntity($subsidiary, $this->request->data);
           if ($this->OrganizationSubsidiaries->save($subsidiary)) {
               $this->Flash->success(__('
                  Subsidiary has been updated successfully.'));
               return $this->redirect(['action' => 'index']);
           }else{  	
           $this->Flash->error(__('Unable to update Subsidiary'));
           }
       }
       $this->set(compact('subsidiary'));
    }

    public function organizationUser() {
        if(!$this->checkPermission()){            
            $this->Flash->error(__('You do not have permission to access that location'));
            return $this->redirect($this->referer());
        }
        if(!$this->checkApplicapability()){
            $upgradeRequired = true;
        }
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('Licenses');
        $this->loadModel('OrganizationSubsidiaries');

        $license = $this->Licenses->find('list')
                ->toArray();
        $subsidiary = $this->OrganizationSubsidiaries->find('list')
                    ->where(['OrganizationSubsidiaries.organization_id' => $this->Auth->user('organization.id')])
                    ->toArray();
        try {
            $organizationUserTable = TableRegistry::get('OrganizationUsers');
            $queryToGetorganizationUsers = $organizationUserTable->find()
                ->where(['OrganizationUsers.organization_id'=> $this->Auth->user('organization.id')])
                ->contain(['Users','Licenses','OrganizationSubsidiaries']);
                                            
            $this->paginate = [
                    'sortWhitelist' => [
                        'first_name',
                        'last_name'
                    ],
                    'limit' => 25
                ];
            $users = $this->paginate($queryToGetorganizationUsers);
            // pr($users->toArray());die;
        } catch (NotFoundException $e) {
            // redirecting to Last page if request page doesn't exist
           
        }
        $this->set(compact('license','subsidiary','users', 'permissionAreas', 'upgradeRequired'));

    }
    
    public function addUser() {
        if(!$this->checkPermission()){            
            $this->Flash->error(__('You do not have permission to access that location'));
            return $this->redirect($this->referer());
        }

        $inTrial = $this->__isUnderTrial();

        if(!$inTrial && !$this->checkApplicapability()){
            return $this->redirect([
                    'controller' => 'additional-user-subscription-plans',
                    'action'    => 'index'
                ]
            );
        }

        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('Users');
        $user = $this->Users->newEntity();
            if($this->request->is('post') ) {
                $this->request->data['role_id'] = 3;
                $this->request->data['is_activated'] = ($inTrial)?0:1;
                $this->request->data['token'] = Text::uuid();
                $this->request->data['organization_user']['organization_id'] = $this->Auth->user('organization.id');
                $user = $this->Users->patchEntity($user, $this->request->data,['associated' => ['OrganizationUsers']]);
                if ($this->Users->save($user)) {
                //set user default permission
                    $user_id = $user->id;
                    $table  = TableRegistry::get('OrganizationUserPermissions');
                    $table->setUserDefaultPermission($this->Auth->user('organization.id'),$user_id);

                    $this->loadModel('EmailTemplates');
                    //Fake Template : in future we can replace with our template
                    $temp = $this->EmailTemplates->find()->where(['EmailTemplates.id' =>3])
                    -> first();
                       
                    $link = Router::url('/', true);
                    $lang =  'mail_body_ger';
                    if($this->Auth->user('language') == 'eng') {
                        $lang =  'mail_body_eng';
                    }
                    $temp[$lang] = str_replace(
                            array('#NAME','#ORGANIZATION','#LINK','#EMAIL','#PASSWORD'),
                            array(
                                $this->request->data['first_name'].' '.$this->request->data['last_name'],
                                $this->Auth->user('organization.name'),
                                $link,
                                $this->request->data['email'],
                                $this->request->data['password']
                            ), 
                        $temp[$lang]
                    );
                    $this->_sendEmailMessage($this->request->data['email'], $temp[$lang], $temp['subject']);
                    $this->Flash->success(__('New User has been saved and activation email has been sent'));
                    return $this->redirect($this->referer());
                }else {
                    if(!empty($user->errors())) {
                        $this->Flash->error(__($this->_setValidationError($user->errors())));
                        return $this->redirect($this->referer());
                    }
                }
            }
            $this->set(compact('user'));
    }

    private function checkApplicapability() {
        $OrganizationuserTable = TableRegistry::get('OrganizationUsers');
        $users     = $OrganizationuserTable->getOrganizaionAllUsers($this->Auth->user('organization.id'));
        $organizationPlanTable = TableRegistry::get('AdditionalUserSubscriptionPlans');
        $organizationPlan   =  $organizationPlanTable->getAdditionalPlanByOrganization($this->Auth->user('organization.id'));
        if((!empty($organizationPlan) && ($users->count() < $organizationPlan->additional_users)))
        {
            return true;
        }else {
            $this->Flash->error('To add new users please create or upgrade your plan.');
            return false;
        }
    }

    public function editUser($id) {
        if(!$this->checkPermission()){            
            $this->Flash->error(__('You do not have permission to access that location'));
            return $this->redirect($this->referer());
        }
        $user_id = base64_decode($id);
        $this->loadModel('Users');
        $this->loadModel('Licenses');
        $this->loadModel('OrganizationSubsidiaries');
        $license = $this->Licenses->find('list')
                ->toArray();
        $subsidiary = $this->OrganizationSubsidiaries->find('list')
                    ->toArray();
        try {
           if ($id != null) {                
               $user = $this->Users->find()
                   ->where(
                       [
                           'Users.id' => base64_decode($id)
                       ]
                   )
                   ->contain(['OrganizationUsers'=>['Licenses','OrganizationSubsidiaries']])
                   ->first();
           } 
           
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('user','license','subsidiary'));
                $this->render('/Element/Subsidiaries/edit_user');
            }            
        } catch (RecordNotFoundException $e) {
           $this->Flash->success('Organization User not found please try agian');
           return $this->redirect($this->referer());
        }
        if($this->request->is(['post','put','patch']) ) {
           $this->request->data['organization_user']['user_id'] = $user['organization_user']->id;
           $this->request->data['organization_user']['organization_id'] = $this->Auth->user('organization.id');
           $this->request->data['id'] = $user_id;
           
           $user = $this->Users->patchEntity($user, $this->request->data,['associated' => ['OrganizationUsers']]);
           if ($this->Users->save($user)) {
               $this->Flash->success(__('
                  User has been updated successfully.'));
               return $this->redirect(['action' => 'organizationUser']);
           }else{   
                $this->Flash->error(__('Unable to update User'));
                return $this->redirect(['action' => 'organizationUser']);
           }
        }
       $this->set(compact('user'));
    }

    public function deleteUser($id) {
        $this->loadModel('Users');   
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get(base64_decode($id));
        if ($this->Users->delete($user)) {
            $this->Flash->success(__(
                   'User has been deleted'
               )
           );
           return $this->redirect($this->referer());
        }
    }
    
    public function download() {
        $file['path'] = WWW_ROOT .'sample'. DS . 'userupload.csv';
        $response = $this->response->withFile(
           $file['path'],
           ['download' => true, 'name' => 'userupload.csv']
        );
        return $response;
    }
    
    public function userUpload() {
       $this->viewBuilder()->layout('dashboard');
       $this->loadModel('Users');
       $this->loadModel('Licenses');
       $this->loadModel('OrganizationSubsidiaries');
       $subsidiary = $this->OrganizationSubsidiaries->find('list')
                    ->toArray();
       $license = $this->Licenses->find('list')
                    ->toArray();
       if ($this->request->is('post')) {  
       $file = fopen($this->request->data['user']['tmp_name'], 'r');
            while (($data = fgetcsv($file)) !== FALSE) {
              //$line is an array of the csv elements
                $users[] = $data ;
            }
            foreach ($users as $key => $user) {
                if($key != 0) {
                    $user_data = [
                    'first_name' => $user[0],
                    'last_name' => $user[1],
                    'email' => $user[2],
                    'password' => $user[5],
                    'role_id' => 3,
                    'is_activated' => 1,
                    'token' => Text::uuid(),
                    'organization_user' => [
                        'license_id' => array_search($user[3],$license),
                        'organization_subsidiary_id' => array_search($user[4], $subsidiary),
                        'organization_id' => $this->Auth->user('organization.id')
                        ]
                    ];
                    $user = $this->Users->newEntity(); 
                    $user = $this->Users->patchEntity($user, $user_data,['associated' => ['OrganizationUsers']]);
                    if ($this->Users->save($user)) {
                        return $this->redirect(['action' => 'organizationUser']);
                    }
                }
            }
        }
    }

    public function setPrimary($id) {
        if($this->request->is('post')) {
            $subsidiaryTable = TableRegistry::get('OrganizationSubsidiaries');
            if($subsidiaryTable->setSubsidiaryToPrimary(base64_decode($id))){
                $this->Flash->success(__(
                       'Primary subsidiary has been changed successfully'
                   )
                );
                return $this->redirect($this->referer());
            }
            $this->Flash->error(__(
                   'Primary subsidiary could not be changed. Please Try again!'
               )
            );
            return $this->redirect($this->referer());
        }
    }

    public function userPermissions($user_id) {
        if($this->checkPermission()){            
            $table  = TableRegistry::get('OrganizationUserPermissions');
            $userPermissions = $table->getOrganizationUserPermissions($this->Auth->user('organization.id'),base64_decode($user_id));
        }
        
        if($this->request->is('post')) {
            $table  = TableRegistry::get('OrganizationUserPermissions');
            $permission = $table->get($this->request->data['id']); // Return article with id 12
            $permission->is_allowed = $this->request->data['is_allowed'];
            if($table->save($permission)) {
                $res = array(
                        'status' => '200'
                    );

            }else {
                $res = array(
                    'status' => '400'
                );
            }
            $this->set(compact('res'));
            $this->set('_serialize', ['res']);
        }
        if($this->request->is('ajax')) {

            $this->set(compact('userPermissions'));
            $this->viewBuilder()->layout(false);
            $this->render('/Element/UserPermissions/set_permission');
        }
    }

    public function setDefaultPermisson($user_id) {
        $table  = TableRegistry::get('OrganizationUserPermissions');
        $table->setUserDefaultPermission($this->Auth->user('organization.id'),$user_id);
    }
}
