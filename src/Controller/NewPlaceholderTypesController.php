<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * NewPlaceholderTypes Controller
 *
 * @property \App\Model\Table\NewPlaceholderTypesTable $NewPlaceholderTypes
 *
 * @method \App\Model\Entity\NewPlaceholderType[] paginate($object = null, array $settings = [])
 */
class NewPlaceholderTypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $newPlaceholderTypes = $this->paginate($this->NewPlaceholderTypes);

        $this->set(compact('newPlaceholderTypes'));
        $this->set('_serialize', ['newPlaceholderTypes']);
    }

    /**
     * View method
     *
     * @param string|null $id New Placeholder Type id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $newPlaceholderType = $this->NewPlaceholderTypes->get($id, [
            'contain' => ['NewPlaceholders']
        ]);

        $this->set('newPlaceholderType', $newPlaceholderType);
        $this->set('_serialize', ['newPlaceholderType']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $newPlaceholderType = $this->NewPlaceholderTypes->newEntity();
        if ($this->request->is('post')) {
            $newPlaceholderType = $this->NewPlaceholderTypes->patchEntity($newPlaceholderType, $this->request->getData());
            if ($this->NewPlaceholderTypes->save($newPlaceholderType)) {
                $this->Flash->success(__('The new placeholder type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The new placeholder type could not be saved. Please, try again.'));
        }
        $this->set(compact('newPlaceholderType'));
        $this->set('_serialize', ['newPlaceholderType']);
    }

    /**
     * Edit method
     *
     * @param string|null $id New Placeholder Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $newPlaceholderType = $this->NewPlaceholderTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $newPlaceholderType = $this->NewPlaceholderTypes->patchEntity($newPlaceholderType, $this->request->getData());
            if ($this->NewPlaceholderTypes->save($newPlaceholderType)) {
                $this->Flash->success(__('The new placeholder type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The new placeholder type could not be saved. Please, try again.'));
        }
        $this->set(compact('newPlaceholderType'));
        $this->set('_serialize', ['newPlaceholderType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id New Placeholder Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $newPlaceholderType = $this->NewPlaceholderTypes->get($id);
        if ($this->NewPlaceholderTypes->delete($newPlaceholderType)) {
            $this->Flash->success(__('The new placeholder type has been deleted.'));
        } else {
            $this->Flash->error(__('The new placeholder type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
