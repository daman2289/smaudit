<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Utility\Hash;
/**

/**
 * Homes Controller
 *
 * @property \App\Model\Table\HomesTable $Homes
 */
class HomesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['index', 'terms', 'manuCollapse', 'uniqueEmail', 
            'prepareOrganizationPdfFooter']);
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->viewBuilder()->layout('home');
        $planTable  = TableRegistry::get('SubscriptionPlans');
        $plans = $planTable->find()
        ->where(['SubscriptionPlans.is_activated' => 1])
        ->contain(['PlanFeatures']);
        $planFeaturesTable  = TableRegistry::get('PlanFeatures');
        $planFeatures      = $planFeaturesTable->find()->contain([
            'SubscriptionPlans'
            ])
            ->hydrate(false);
        $faqTable   = TableRegistry::get('Faqs');
        $faqs       = $faqTable->find();        
        $specializeTable = TableRegistry::get('Specializes');
        $specialize = $specializeTable->getSpecializes();        
        $serviceTable = TableRegistry::get('Services');
        $services = $serviceTable->find('all')
                    -> where(['Services.is_activated' => 1]);
        $this->loadModel('WorkInProgresses');
        $work = $this->WorkInProgresses->find()
                ->contain(['WorkInProgressFeatures'])
                ->last();
        $this->set(compact('plans', 'faqs', 'specialize', 'services', 'planFeatures','work'));

    }

    public function terms($id) {
        $this->viewBuilder()->layout('home');
        $emailTemplatesTable = TableRegistry::get('CmsPages');
        try {
            $terms = $emailTemplatesTable->get(base64_decode($id));
            // pr($terms);die;
        }
        catch (RecordNotFoundException $e) {
            return $this->redirect($this->referer());
        }
        $this->set(compact('terms'));
    }

    public function uniqueEmail (){
        if($this->request->getQuery('email')) {
            $this->loadModel('Users');
            $user = $this->Users->findByEmail($this->request->getQuery('email'))->toArray();
            if(!empty($user)) {
                $response = false;
            }else {
                $response = true;
            }
            $this->set(compact('response'));
            $this->set('_serialize','response');
        }
    }

    public function footer() {

        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('FontProperties');
        $this->loadModel('ReportFooters');
        $footer = $this->ReportFooters->find()
            ->where([
                'ReportFooters.organization_id' => $this->Auth->user('organization.id')
            ])
            ->first();
        $footer = (!empty($footer)) 
                    ? $footer 
                    : $this->ReportFooters->newEntity();
        $family = $this->FontProperties->find('list', ['keyField'   => 'value', 'valueField' => 'value'])
            ->where(['type'  => 1]);
        $size = $this->FontProperties->find('list', ['keyField'   => 'value', 'valueField' => 'value'])
            ->where(['type'  => 2]);
        $weight = $this->FontProperties->find('list', ['keyField'   => 'value', 'valueField' => 'value'])
            ->where(['type'  => 3]);
        $style = $this->FontProperties->find('list', ['keyField'   => 'value', 'valueField' => 'value'])
            ->where(['type'  => 4]);

        if($this->request->is('post')) {
            $footer = $this->ReportFooters->patchEntity($footer, $this->request->data);
        // pr($footer);die;
            $footer->organization_id = $this->Auth->user('organization.id');
            // pr($footer);die;
            if($this->ReportFooters->save($footer)) {
                $this->Flash->success(__('Footer updated successfully.'));
                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Footer could not be updated.'));
            return $this->redirect($this->referer());
        }

        $this->set(compact(
                'newFinalReport', 
                'newReports', 
                'users', 
                'language', 
                'family', 
                'size', 
                'weight', 
                'style',
                'footer'
            )
        );
        $this->set('_serialize', ['newFinalReport', 'language']);
    }


    public function header() {
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('InfoFields');
        $this->loadModel('ReportHeaders');
        $fields = $this->InfoFields->find('list', [
            'keyField'  => 'id',
            'valueField'    => 'field'
        ]);
        $headers = $this->ReportHeaders->find()
            ->where(['organization_id' => $this->Auth->user('organization.id')]);

        if($this->request->is('post')) {
            if(empty($this->request->data)) {
                $this->Flash->error(__('Nothing to save.'));
                return $this->redirect($this->referer());
            }
            $data = Hash::insert($this->request->data, '{n}.organization_id', $this->Auth->user('organization.id'));
            $headers = $this->ReportHeaders->patchEntities($headers, $data);
            // pr($headers);die;
            if($this->ReportHeaders->saveMany($headers)) {
                $this->Flash->success(__('Header updated successfully.'));
                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Header could not be updated.'));
            return $this->redirect($this->referer());
        }
        $data['organization'] = $this->Auth->user('organization');
        // Setting Subsidiary details
        $data['subsidiary'] = $this->__getSubsidiaryDetail();
        // get list of system placeholders
        $data['company'] = $this->__getCompanyDetail();
        $this->set(compact('fields', 'headers', 'data'));
    }




    protected function __getSubsidiaryDetail() {
        $this->loadModel('OrganizationSubsidiaries');
        if($this->Auth->user('organization_user')) {
            $userSubsidiary = $this->OrganizationSubsidiaries->get($this->Auth->user('organization_user.organization_subsidiary_id')); 
            if(!empty($userSubsidiary)){
                return $userSubsidiary->toArray();
            }
            return [];

        }else {
            $organizationDefaultSubsidiary = $this->OrganizationSubsidiaries->find()
                ->where([
                    'OrganizationSubsidiaries.organization_id' => $this->Auth->user('organization.id'),
                    'OrganizationSubsidiaries.is_primary' => 1
                ])
                ->first(); 
            if(!empty($organizationDefaultSubsidiary)) {
                return $organizationDefaultSubsidiary->toArray();
            }
            return [];
        }
    }

    protected function __getCompanyDetail() {
        $this->loadModel('OrganizationCompanies');
            $organizationCompany = $this->OrganizationCompanies->find()
                ->where([
                    'OrganizationCompanies.organization_id' => $this->Auth->user('organization.id'),
                    'OrganizationCompanies.status' => 1
                ])
                ->first(); 
        if(!empty($organizationCompany)) {
            return $organizationCompany->toArray();
        }
        return [];
    }

    public function prepareOrganizationPdfFooter($organization_id) {
        $this->viewBuilder()->layout(false);
        $this->loadModel('ReportFooters');
        $footer = $this->ReportFooters->getOrganizationFooter(base64_decode($organization_id));
        $this->set(compact('footer'));
    }
    
}
