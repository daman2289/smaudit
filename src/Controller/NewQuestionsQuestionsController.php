<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * NewQuestionsQuestions Controller
 *
 * @property \App\Model\Table\NewQuestionsQuestionsTable $NewQuestionsQuestions
 *
 * @method \App\Model\Entity\NewQuestionsQuestion[] paginate($object = null, array $settings = [])
 */
class NewQuestionsQuestionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ParentQuestions', 'ChildQuestions']
        ];
        $newQuestionsQuestions = $this->paginate($this->NewQuestionsQuestions);

        $this->set(compact('newQuestionsQuestions'));
        $this->set('_serialize', ['newQuestionsQuestions']);
    }

    /**
     * View method
     *
     * @param string|null $id New Questions Question id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $newQuestionsQuestion = $this->NewQuestionsQuestions->get($id, [
            'contain' => ['ParentQuestions', 'ChildQuestions']
        ]);

        $this->set('newQuestionsQuestion', $newQuestionsQuestion);
        $this->set('_serialize', ['newQuestionsQuestion']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $newQuestionsQuestion = $this->NewQuestionsQuestions->newEntity();
        if ($this->request->is('post')) {
            $newQuestionsQuestion = $this->NewQuestionsQuestions->patchEntity($newQuestionsQuestion, $this->request->getData());
            if ($this->NewQuestionsQuestions->save($newQuestionsQuestion)) {
                $this->Flash->success(__('The new questions question has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The new questions question could not be saved. Please, try again.'));
        }
        $parentQuestions = $this->NewQuestionsQuestions->ParentQuestions->find('list', ['limit' => 200]);
        $childQuestions = $this->NewQuestionsQuestions->ChildQuestions->find('list', ['limit' => 200]);
        $this->set(compact('newQuestionsQuestion', 'parentQuestions', 'childQuestions'));
        $this->set('_serialize', ['newQuestionsQuestion']);
    }

    /**
     * Edit method
     *
     * @param string|null $id New Questions Question id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $newQuestionsQuestion = $this->NewQuestionsQuestions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $newQuestionsQuestion = $this->NewQuestionsQuestions->patchEntity($newQuestionsQuestion, $this->request->getData());
            if ($this->NewQuestionsQuestions->save($newQuestionsQuestion)) {
                $this->Flash->success(__('The new questions question has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The new questions question could not be saved. Please, try again.'));
        }
        $parentQuestions = $this->NewQuestionsQuestions->ParentQuestions->find('list', ['limit' => 200]);
        $childQuestions = $this->NewQuestionsQuestions->ChildQuestions->find('list', ['limit' => 200]);
        $this->set(compact('newQuestionsQuestion', 'parentQuestions', 'childQuestions'));
        $this->set('_serialize', ['newQuestionsQuestion']);
    }

    /**
     * Delete method
     *
     * @param string|null $id New Questions Question id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $newQuestionsQuestion = $this->NewQuestionsQuestions->get($id);
        if ($this->NewQuestionsQuestions->delete($newQuestionsQuestion)) {
            $this->Flash->success(__('The new questions question has been deleted.'));
        } else {
            $this->Flash->error(__('The new questions question could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
