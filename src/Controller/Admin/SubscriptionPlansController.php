<?php
namespace App\Controller\Admin;
use Cake\Core\Configure;

use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use App\Controller\AppController;

/**
 * SubscriptionPlans Controller
 *
 * @property \App\Model\Table\SubscriptionPlansTable $SubscriptionPlans
 */
class SubscriptionPlansController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->viewBuilder()->layout('admin');

        $subscriptionPlans = $this->paginate($this->SubscriptionPlans);
        $this->set(compact('subscriptionPlans'));
        $this->set('_serialize', ['subscriptionPlans']);
    }

    /**
     * View method
     *
     * @param string|null $id Subscription Plan id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $subscriptionPlan = $this->SubscriptionPlans->get($id, [
            'contain' => ['PlanFeatures']
        ]);

        $this->set('subscriptionPlan', $subscriptionPlan);
        $this->set('_serialize', ['subscriptionPlan']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $subscriptionPlan = $this->SubscriptionPlans->newEntity();
        if ($this->request->is('post')) {
            $subscriptionPlan = $this->SubscriptionPlans->patchEntity($subscriptionPlan, $this->request->getData());
            if ($this->SubscriptionPlans->save($subscriptionPlan)) {
                $this->Flash->success(__('Your subscription has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Your subscription could not be saved. Please, try again.'));
        }
        $planFeatures = $this->SubscriptionPlans->PlanFeatures->find('list', ['limit' => 200]);
        $this->set(compact('subscriptionPlan', 'planFeatures'));
        $this->set('_serialize', ['subscriptionPlan']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Subscription Plan id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $subscriptionPlan = $this->SubscriptionPlans->get($id, [
            'contain' => ['PlanFeatures']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $subscriptionPlan = $this->SubscriptionPlans->patchEntity($subscriptionPlan, $this->request->getData());
            if ($this->SubscriptionPlans->save($subscriptionPlan)) {
                $this->Flash->success(__('The subscription plan has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The subscription plan could not be saved. Please, try again.'));
        }
        $planFeatures = $this->SubscriptionPlans->PlanFeatures->find('list', ['limit' => 200]);
        $this->set(compact('subscriptionPlan', 'planFeatures'));
        $this->set('_serialize', ['subscriptionPlan']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Subscription Plan id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $subscriptionPlan = $this->SubscriptionPlans->get($id);
        if ($this->SubscriptionPlans->delete($subscriptionPlan)) {
            $this->Flash->success(__('Your subscription has been deleted.'));
        } else {
            $this->Flash->error(__('Your subscription could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function editPlanFeatures($id) {        
        try {
            $subscriptionPlan = $this->SubscriptionPlans->find()
               ->where(
                   [
                       'SubscriptionPlans.id' => base64_decode($id)
                   ]
               )
               ->contain(['PlanFeatures'])->first();
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('subscriptionPlan'));
                $this->render('/Element/SubscriptionPlans/edit_features');
            }            
        } catch (RecordNotFoundException $e) {
           $this->Flash->success(__('Specialize not found please try agian'));
           return $this->redirect($this->referer());
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $subscriptionPlan = $this->SubscriptionPlans->patchEntity($subscriptionPlan, $this->request->getData());
            if ($this->SubscriptionPlans->save($subscriptionPlan)) {
                $this->Flash->success(__('The subscription plan has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The subscription plan could not be saved. Please, try again.'));
        }
        $planFeatures = $this->SubscriptionPlans->PlanFeatures->find('list', ['limit' => 200]);
        $this->set(compact('subscriptionPlan', 'planFeatures'));
        $this->set('_serialize', ['subscriptionPlan']);
    }

    public function updatePlanFeatures($plan_id, $feature_id, $status) {
        $this->loadModel('SubscriptionPlansPlanFeatures');
        try {
            $planFeature = $this->SubscriptionPlansPlanFeatures->find()
               ->where(
                   [
                       'SubscriptionPlansPlanFeatures.subscription_plan_id' => base64_decode($plan_id),
                       'SubscriptionPlansPlanFeatures.plan_feature_id' => base64_decode($feature_id)
                   ]
               )->first();
                

            $data['is_available'] = base64_decode($status);
            
            $subscriptionPlan = $this->SubscriptionPlansPlanFeatures->patchEntity($planFeature, $data);
            if ($this->SubscriptionPlansPlanFeatures->save($subscriptionPlan)) {
            }
            if($this->request->is('ajax')){
                $this->autoRender = false;
                // $this->set(compact('subscriptionPlan'));
                // $this->render('/Element/SubscriptionPlans/edit_features');
            }            
        } catch (RecordNotFoundException $e) {
           $this->Flash->success(__('Specialize not found please try agian'));
           return $this->redirect($this->referer());
        }
    }
    public function deactivate($id) {
        $plan_id = base64_decode($id);
        $this->viewBuilder()->layout(false);
        $this->autoRender = false;
        $this->loadModel('SubscriptionPlans'); 
        $plan = $this->SubscriptionPlans->find()
                    ->where(['SubscriptionPlans.id' => $plan_id])
                    ->first();
        $plan->is_activated = ($plan->is_activated) ? 0: 1;
        $this->SubscriptionPlans->save($plan);
        echo $plan->is_activated;
    }

    public function editPlan($id = null)
    {
        try {
            $plan = $this->SubscriptionPlans->find()
                ->where(
                   [
                       'SubscriptionPlans.id' => base64_decode($id)
                   ]
                )
               ->first();
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('plan'));
                $this->render('/Element/SubscriptionPlans/edit_plan');
            }            
        } catch (RecordNotFoundException $e) {
           $this->Flash->success(__('Subscription Plan not found please try agian'));
           return $this->redirect($this->referer());
        }
        if ($this->request->is(['patch', 'post', 'put'])) {

            $plan = $this->SubscriptionPlans->patchEntity($plan, $this->request->getData());
            $status = $this->updateTrial($plan->stripe_plan_id,$this->request->data['title'],$this->request->data['trial_days']);
            if($status) {
                if ($this->SubscriptionPlans->save($plan)) {
                    $this->Flash->success(__('The plan has been updated.'));
                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('The plan could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('plan'));
        $this->set('_serialize', ['plan']);
    }

    public function updateTrial($plan_id, $plan_title, $plan_trial){
        try{
            \Stripe\Stripe::setApiKey(Configure::read('Stripe.secret_key'));
            $plan = \Stripe\Plan::retrieve($plan_id);
            // pr($plan_trial);die;
            $plan->name = $plan_title;
            $plan->trial_period_days = $plan_trial;
            if ($plan->save()) {               
                return true;
            }
        } catch (Exception $e) {
          // Something else happened, completely unrelated to Stripe
            $this->_stripeError($e);
        }
    }
}
