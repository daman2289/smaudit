<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Utility\Text;
use App\Event\UserAddListener;
use Cake\Event\EventManager;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class PlaceholdersController extends AppController
{
    public $helpers = ['AkkaCKEditor.CKEditor'];
	public function index() {
		$this->viewBuilder()->layout('admin');
        
       	try {
            $workTable = TableRegistry::get('WorkInProgresses');
            $queryToGetwork = $workTable->find()
                                            ->contain(['WorkInProgressFeatures']);
            $this->paginate = [
                    'sortWhitelist' => [
                        'date',
                        'release_number'
                    ],
                    'limit' => 25
                ];
            $works = $this->paginate($queryToGetwork);
        } catch (NotFoundException $e) {
            // redirecting to Last page if request page doesn't exist
           
        }
        
        $this->set(compact('works')); 
	}
	public function add() {
		$this->viewBuilder()->layout('admin');
		$this->loadModel('WorkInProgresses');
        if($this->request->is('post') ) {
       	   $data = $this->format_array($this->request->data);
       	   $this->WorkInProgresses->customSave($data);
               $this->Flash->success(__('New Work in Progress Blog has been saved.'));
               return $this->redirect(['action' => 'index']);
       }
	}
	public function format_array($datas) {
		foreach ($datas['work_in_progress_features']['date'] as $key => $data) {
            $formatdata[$key]['date'] = $data;
            $formatdata[$key]['release_number'] = $datas['work_in_progress_features']['release_number'][$key];
			$formatdata[$key]['features'] = $datas['work_in_progress_features']['features'][$key];
		}
        $datas['work_in_progress_features']= $formatdata;
		return $datas;
	}
	public function edit($id) {
		$this->viewBuilder()->layout('admin');
		$workId = base64_decode($id);
        $workTable = TableRegistry::get('WorkInProgresses');
        $work = $workTable->find()
                    ->where(['WorkInProgresses.id' => $workId])
                    ->contain(['WorkInProgressFeatures']);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $work = $workTable->patchEntity($work, $this->request->data);
            if ($workTable->save($work)) {
                $this->Flash->success(__('The Work in Progress blog has been Updated.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The The Work in Progress blog could not be Updated. Please, try again.'));
            }
        }

        $this->set(compact('work'));
	}
    public function delete($id) {
        $this->loadModel('WorkInProgresses');   
        $this->request->allowMethod(['post', 'delete']);
        $work = $this->WorkInProgresses->get(base64_decode($id));
        
       if ($this->WorkInProgresses->delete($work)) {
           $this->Flash->success(__(
                   'The Blog has been deleted'
               )
           );
           return $this->redirect($this->referer());
       }
    }
}