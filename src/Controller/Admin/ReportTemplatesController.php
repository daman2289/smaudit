<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\Datasource\Exception\RecordNotFoundException;
/**
 * ReportTemplates Controller
 *
 * @property \App\Model\Table\ReportTemplatesTable $ReportTemplates
 */
class ReportTemplatesController extends AppController
{
    public $helpers = ['AkkaCKEditor.CKEditor'];
    public $paginate = [
       'limit' => 20
    ];
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {   
        $this->viewBuilder()->layout('admin');
        $title = __('Report Templates');
        $this->paginate = [
            'contain' => ['ReportTypes']
        ];
        $reportTemplates = $this->paginate($this->ReportTemplates);
        $this->set(compact('reportTemplates', 'title'));
        $this->set('_serialize', ['reportTemplates']);
    }

    /**
     * View method
     *
     * @param string|null $id Report Template id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {

            $reportTemplate = $this->ReportTemplates->get($id, [
                'contain' => ['ReportTypes']
            ]);
      

        $this->set('reportTemplate', $reportTemplate);
        $this->set('_serialize', ['reportTemplate']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $reportTemplate = $this->ReportTemplates->newEntity();
        if ($this->request->is('post')) {
            $reportTemplate = $this->ReportTemplates->patchEntity($reportTemplate, $this->request->getData());
            if ($this->ReportTemplates->save($reportTemplate)) {
                $this->Flash->success(__('The report template has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The report template could not be saved. Please, try again.'));
        }
        $reportTypes = $this->ReportTemplates->ReportTypes->find('list', ['limit' => 200]);
        $this->set(compact('reportTemplate', 'reportTypes'));
        $this->set('_serialize', ['reportTemplate']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Report Template id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {   $this->viewBuilder()->layout('admin');
        $title = __('Edit Report Template');
        try {
            $reportTemplate = $this->ReportTemplates->get(base64_decode($id), [
                'contain' => []
            ]);
        } catch (RecordNotFoundException $e) {
            $this->Flash->error(__('Template not found.Please try again!'));
            return $this->redirect($this->referer());
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $reportTemplate = $this->ReportTemplates->patchEntity($reportTemplate, $this->request->getData());
            if ($this->ReportTemplates->save($reportTemplate)) {
                $this->Flash->success(__('The report template has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            pr($reportTemplate->errors());die;
            $this->Flash->error(__('The report template could not be saved. Please, try again.'));
        }
        $reportTypes = $this->ReportTemplates->ReportTypes->find('list', ['limit' => 200]);
        $this->set(compact('reportTemplate', 'reportTypes','title'));
        $this->set('_serialize', ['reportTemplate']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Report Template id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $reportTemplate = $this->ReportTemplates->get($id);
        if ($this->ReportTemplates->delete($reportTemplate)) {
            $this->Flash->success(__('The report template has been deleted.'));
        } else {
            $this->Flash->error(__('The report template could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
