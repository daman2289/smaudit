<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Utility\Text;
use Cake\Utility\Hash;
/**
 * NewReports Controller
 *
 * @property \App\Model\Table\NewReportsTable $NewReports
 *
 * @method \App\Model\Entity\NewReport[] paginate($object = null, array $settings = [])
 */
class NewReportsController extends AppController
{   
    public $helpers = ['AkkaCKEditor.CKEditor'];
    public $dateFormatt = 'j. F Y';
    public $germanMonth = [
        'January'   =>     'Januar',
        'February'  =>    'Februar',
        'March' =>   'März',
        'April' =>   'April',
        'May'   =>     'Mai',
        'June'  =>    'Juni',
        'July'  =>    'Juli',
        'August'    =>  'August',
        'September' =>   'September',
        'October'   =>     'Oktober',
        'November'  =>    'November',
        'December'  =>    'Dezember'
    ];
    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->layout('admin');
    }

    function translateMonth($date,$lang) {
        if($lang == 'template_german') {
            foreach ($this->germanMonth as $key => $value) {
                if (strstr($date,  $key))  {               
                    $str = str_replace(
                        array($key),
                        array(
                            $value
                        ),
                        $date
                    );
                    return $str;
                }
            }
        }
        return $date;
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->loadModel('NewPlaceholders');
        $this->NewReports->NewPlaceholders->hasOne('NewQuestions');
       
        $report = $this->NewReports->find()
            ->contain([
                'NewPlaceholders' => [
                    'NewQuestions', 
                    'NewPlaceholderTypes',
                    'sort' => ['NewQuestions.sort_order' => 'ASC']
                ],
                'NewFinalReports' => function($q) {
                    return $q->where(['NewFinalReports.user_id' => $this->Auth->user('id')]);
                }
            ]);
        $reports = $this->paginate($report);
        $this->set(compact('reports'));
        $this->set('_serialize', ['reports']);
    }

     /**
     * View method
     *
     * @param string|null $id New Report id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $newReport = $this->NewReports->get($id, [
            'contain' => ['NewPlaceholders', 'NewReportPlaceholders']
        ]);

        $this->set('newReport', $newReport);
        $this->set('_serialize', ['newReport']);
    }

    protected function __addReportPlaceholders($data) {
        // pr($data);die;
        $res = [];
        foreach ($data as $key => $value) {
            $res[] = ['name' => $value, 'uid' => '111'];
        }
        return $res;
    }

    protected function __getTextPattren($pattren, $text) {
        preg_match_all($pattren, $text, $match);
        return $match[0];
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $newReport = $this->NewReports->newEntity();
        if ($this->request->is('post')) {
            $text       = $this->__getTextPattren('#\[(.*?)\]#', $this->request->data['content_en']);
            $germanText = $this->__getTextPattren('#\[(.*?)\]#', $this->request->data['content_er']);
            $frenchText = $this->__getTextPattren('#\[(.*?)\]#', $this->request->data['content_fr']);

            $headerPlaceholders = $this->__getTextPattren('#\[(.*?)\]#', $this->request->data['header']);
            
            $footerPlacholders  = $this->__getTextPattren('#\[(.*?)\]#', $this->request->data['footer']);
            
            $allPlaceholders = array_merge($text, $germanText, $frenchText, $headerPlaceholders, $footerPlacholders);

            $this->request->data['new_placeholders'] = $this->__addReportPlaceholders(array_unique($allPlaceholders));
            $this->request->data['uid'] = Text::uuid();
            $this->request->data['content_en'] = str_replace('{{LOGO}}&nbsp;', '{{LOGO}}', $this->request->data['content_en']); 
            $this->request->data['content_er'] = str_replace('{{LOGO}}&nbsp;', '{{LOGO}}', $this->request->data['content_er']); 
            $this->request->data['content_fr'] = str_replace('{{LOGO}}&nbsp;', '{{LOGO}}', $this->request->data['content_fr']); 
            $newReport = $this->NewReports->patchEntity($newReport, $this->request->getData());
            if ($this->NewReports->save($newReport)) {
                $this->Flash->success(__('The new report has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The new report could not be saved. Please, try again.'));
        }
        $this->set(compact('newReport'));
        $this->set('_serialize', ['newReport']);
    }

    /**
     * Edit method
     *
     * @param string|null $id New Report id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $newReport = $this->NewReports->get($id, [
            'contain' => ['NewPlaceholders']
        ]);
        // pr($newReport->toArray());die;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $text       = $this->__getTextPattren('#\[(.*?)\]#', $this->request->data['content_en']);
            $germanText = $this->__getTextPattren('#\[(.*?)\]#', $this->request->data['content_er']);
            $frenchText = $this->__getTextPattren('#\[(.*?)\]#', $this->request->data['content_fr']);
            
            $headerPlaceholders = $this->__getTextPattren('#\[(.*?)\]#', $this->request->data['header']);
            $footerPlacholders  = $this->__getTextPattren('#\[(.*?)\]#', $this->request->data['footer']);
            
            $allPlaceholders = array_merge($text, $germanText, $frenchText, $headerPlaceholders, $footerPlacholders);
            
            $this->request->data['new_placeholders'] = $this->__addReportPlaceholders(array_unique($allPlaceholders));

            $this->loadModel('NewPlaceholders');
            $placeholders = $this->NewPlaceholders->find('list')
                ->where(['NewPlaceholders.new_report_id' => $newReport->id]);

            $p = Hash::extract($this->request->data['new_placeholders'], '{n}.name');           

            // $this->deletePlacholder($placeholders->toArray(), $p);
            $this->request->data['new_placeholders'] = $this->updateRequest($this->request->data['new_placeholders'], $placeholders->toArray());
            if(empty($this->request->data['new_placeholders'])) {
                unset($this->request->data['new_placeholders']);
            }
            // pr($this->request->data);die;
            $this->request->data['content_en'] = str_replace('{{LOGO}}&nbsp;', '{{LOGO}}', $this->request->data['content_en']); 
            $this->request->data['content_er'] = str_replace('{{LOGO}}&nbsp;', '{{LOGO}}', $this->request->data['content_er']); 
            $this->request->data['content_fr'] = str_replace('{{LOGO}}&nbsp;', '{{LOGO}}', $this->request->data['content_fr']); 
            $newReport = $this->NewReports->get($id, [
                'contain' => ['NewPlaceholders']
            ]);

            $newReport = $this->NewReports->patchEntity($newReport, $this->request->getData());
            $this->loadModel('NewPlaceholders');
            if ($this->NewReports->save($newReport)) {
                $this->Flash->success(__('The new report has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The new report could not be saved. Please, try again.'));
        }
        $this->set(compact('newReport'));
        $this->set('_serialize', ['newReport']);
    }


    public function deletePlacholder($saved, $new) {
        foreach ($saved as $key => $value) {
            $this->loadModel('NewPlaceholders');
            if(!in_array($value, $new)) {
                $newPlaceholder = $this->NewPlaceholders->get($key);
                $this->NewPlaceholders->delete($newPlaceholder);
            }
        }
    }

    public function updateRequest($request, $saved) {
        foreach ($request as $key => $value) {
            if(in_array($value['name'], $saved)) {
                unset($request[$key]);
            }
        }
        return $request;
    }

    /**
     * Delete method
     *
     * @param string|null $id New Report id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $newReport = $this->NewReports->get($id);
        if ($this->NewReports->delete($newReport)) {
            $this->Flash->success(__('The new report has been deleted.'));
        } else {
            $this->Flash->error(__('The new report could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function report($id) {
        $newReport = $this->NewReports->get($id);
        $placeholders = [];
        // get system placeholder
        $systemPlaceholders = $this->__getValueOfSystemPlacholders($id);
        $placeholders = array_merge($placeholders, $systemPlaceholders);
        // getting normal placeholder
        $normalPlaceholder = $this->__getValueOfNormalPlacehoders($id);
        $placeholders = array_merge($placeholders, $normalPlaceholder);

        $this->loadModel('Answers');
        $answers    = $this->Answers->find()
            ->where(['Answers.organization_user_id' => $this->Auth->user('id')])
            ->contain(['NewPlaceholders', 'NewQuestions', 'NewQuestionOptions'])
            ->order(['Answers.created' => 'ASC']);
        // pr($answers->limit(5)->toArray());die;
        // getting placeholders of questions
        $answers    = $this->__getValueOfPlaceholders($answers);
        $placeholders   = array_merge($placeholders, $answers);
        $keys   = array_keys($placeholders);
        $values     = array_values($placeholders);

        $report     = $str  = str_replace(
            $keys,                
            $values,
            $newReport->content_en
        );
        $report     = $this->__recursivelyReplacePlaceholders($report, $placeholders);
        $this->set(compact('report'));
    }


    protected function __getValueOfPlaceholders($data) {
        // pr($data->toArray());die;
        $placeholders = [];
        foreach ($data as $key => $value) {
            // pr($value->toArray());
            if($value->has('new_question_option')) {
                $placeholders[$value->new_placeholder->name] = $value->new_question_option->content_en;
            }
            if(!empty($value->answer)) {               
                $placeholders[$value->new_placeholder->name] = $value->answer;
            }        
        }
        return $placeholders;
    }

    protected function __getValueOfSystemPlacholders($id) {
        $this->loadModel('NewPlaceholders');
        $this->loadModel('OrganizationUsers');
        $this->loadModel('OrganizationSubsidiaries');
        $placeholders = [];
        // Setting organization detail for replacement
        $data['organization'] = $this->Auth->user('organization');
        // Setting Subsidiary details
        $data['subsidiary'] = $this->__getSubsidiaryDetail();
        // get list of system placeholders
        $data['company'] = $this->__getCompanyDetail();
        // get list of system placeholders
        $systemPlaceholder = $this->NewPlaceholders->find('list', [
                'keyField' => 'name',
                'valueField' => 'content_en'
            ])
            ->where([
                'NewPlaceholders.new_placeholder_type_id' => 1,
                'NewPlaceholders.new_report_id' => $id
            ]);

        foreach ($systemPlaceholder as $key => $value) {
            // getting model and field part from _ seprated string
            $match = explode('_', $value);
            $field = $match[1];

            if(in_array($match[0], ['company', 'organization', 'subsidiary'])) {
                $model = $match[0];
            } else {
                $model = 'organization';
            }
            // setting placeholder replacement corresponding to their type company, orgni.., subsidi..
            if(!empty($data[$model]) && !empty($data[$model][$field])) {
                $placeholders[$key] = $data[$model][$field];
            } else {
                // if not found set default to organization
                $placeholders[$key] = $this->Auth->user('organization.'.$field);
            }
        }
        return $placeholders;
    }

    protected function __getValueOfNormalPlacehoders($id) {
        $this->loadModel('NewPlaceholders');
        $placeholders = [];
        $normalPlaceholder = $this->NewPlaceholders->find('list', [
            'keyField' => 'name',
            'valueField' => 'content_en'
        ])
        ->where([
            'NewPlaceholders.new_placeholder_type_id' => 2,
            'NewPlaceholders.new_report_id' => $id
        ]);

        foreach ($normalPlaceholder as $key => $value) {
            $placeholders[$key] = $value;
        }
        return $placeholders;
    }

    protected function __getSubsidiaryDetail() {
        $this->loadModel('OrganizationSubsidiaries');
        if($this->Auth->user('organization_user')) {
            $userSubsidiary = $this->OrganizationSubsidiaries->get($this->Auth->user('organization_user.organization_subsidiary_id')); 
            return $userSubsidiary->toArray();
        }else {
            try {                
                $organizationDefaultSubsidiary = $this->OrganizationSubsidiaries->find()
                ->where([
                    'OrganizationSubsidiaries.organization_id' => $this->Auth->user('organization.id'),
                    'OrganizationSubsidiaries.is_primary' => 1
                ])
                ->first(); 
                if($organizationDefaultSubsidiary) {
                    return $organizationDefaultSubsidiary->toArray();
                }
                return '';
            } catch (Exception $e) {                
                return '';
            }
        }
    }

    protected function __getCompanyDetail() {
        $this->loadModel('OrganizationCompanies');
        $organizationCompany = $this->OrganizationCompanies->find()
            ->where([
                'OrganizationCompanies.organization_id' => $this->Auth->user('organization.id'),
                'OrganizationCompanies.status' => 1
            ])
            ->first(); 
        if($organizationCompany) {
            return $organizationCompany->toArray();
        }
        return '';
    }

    protected function __recursivelyReplacePlaceholders($template, $allPlaceholders) {
        $placeholders = $this->__getTemplatePlacholder($template);
        if(!empty($placeholders)) {
            $template   =   $this->__replacePlaceholders($template, $placeholders, $allPlaceholders);
            $template   =   $this->__recursivelyReplacePlaceholders($template, $allPlaceholders);
            return $template;
        }
        return $template;
    }

    protected function __getTemplatePlacholder($temp) {
        preg_match_all('#\[(.*?)\]#', $temp, $match);
        return $match[0];
    }

    protected function __replacePlaceholders($template, $placeholders, $allPlaceholders) {
        foreach ($placeholders as $key => $value) {
            $template = str_replace(
                $value,                
                $allPlaceholders[$value],
                $template
            );
        }
        return $template;         
    }

    public function setQuestionOrder() {
        $data = [];
        $order = 1;
        $this->loadModel('NewQuestions');
        foreach ($this->request->getData() as $key => $value) {
            if(!empty($value['question_id'])){
                echo $value['question_id'];
                $question = $this->NewQuestions->find()
                    ->where(['id' => $value['question_id']])
                    ->first();
                $question->sort_order = $order;
                if(!empty($question)) {
                    $this->NewQuestions->save($question);
                }
                $order ++;
            }
        }
        echo true;die;
    }
}
