<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Reports Controller
 *
 * @property \App\Model\Table\ReportsTable $Reports
 */
class ReportsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['OrganizationCompanies', 'Users', 'ReportTypes', 'AccountingFrameworkTypes', 'OpinionTypes', 'ModifiedOpinions']
        ];
        $reports = $this->paginate($this->Reports);

        $this->set(compact('reports'));
        $this->set('_serialize', ['reports']);
    }

    /**
     * View method
     *
     * @param string|null $id Report id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $report = $this->Reports->get($id, [
            'contain' => ['OrganizationCompanies', 'Users', 'ReportTypes', 'AccountingFrameworkTypes', 'OpinionTypes', 'ModifiedOpinions']
        ]);

        $this->set('report', $report);
        $this->set('_serialize', ['report']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $report = $this->Reports->newEntity();
        if ($this->request->is('post')) {
            $report = $this->Reports->patchEntity($report, $this->request->getData());
            if ($this->Reports->save($report)) {
                $this->Flash->success(__('The report has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The report could not be saved. Please, try again.'));
        }
        $organizationCompanies = $this->Reports->OrganizationCompanies->find('list', ['limit' => 200]);
        $users = $this->Reports->Users->find('list', ['limit' => 200]);
        $reportTypes = $this->Reports->ReportTypes->find('list', ['limit' => 200]);
        $accountingFrameworkTypes = $this->Reports->AccountingFrameworkTypes->find('list', ['limit' => 200]);
        $opinionTypes = $this->Reports->OpinionTypes->find('list', ['limit' => 200]);
        $modifiedOpinions = $this->Reports->ModifiedOpinions->find('list', ['limit' => 200]);
        $this->set(compact('report', 'organizationCompanies', 'users', 'reportTypes', 'accountingFrameworkTypes', 'opinionTypes', 'modifiedOpinions'));
        $this->set('_serialize', ['report']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Report id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $report = $this->Reports->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $report = $this->Reports->patchEntity($report, $this->request->getData());
            if ($this->Reports->save($report)) {
                $this->Flash->success(__('The report has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The report could not be saved. Please, try again.'));
        }
        $organizationCompanies = $this->Reports->OrganizationCompanies->find('list', ['limit' => 200]);
        $users = $this->Reports->Users->find('list', ['limit' => 200]);
        $reportTypes = $this->Reports->ReportTypes->find('list', ['limit' => 200]);
        $accountingFrameworkTypes = $this->Reports->AccountingFrameworkTypes->find('list', ['limit' => 200]);
        $opinionTypes = $this->Reports->OpinionTypes->find('list', ['limit' => 200]);
        $modifiedOpinions = $this->Reports->ModifiedOpinions->find('list', ['limit' => 200]);
        $this->set(compact('report', 'organizationCompanies', 'users', 'reportTypes', 'accountingFrameworkTypes', 'opinionTypes', 'modifiedOpinions'));
        $this->set('_serialize', ['report']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Report id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $report = $this->Reports->get($id);
        if ($this->Reports->delete($report)) {
            $this->Flash->success(__('The report has been deleted.'));
        } else {
            $this->Flash->error(__('The report could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function placeholder() {
        $this->viewBuilder()->layout('admin');
        $this->loadModel('ReportReferences');
        $this->loadModel('ModifiedOpinions');
        $this->loadModel('EmphasisOfMatters');
        $this->loadModel('OpinionTypes');
        $this->loadModel('LegalFormValues');
        $this->loadModel('AccountingFrameworkValues');
        $this->loadModel('LseOthers');
        $references = $this->ReportReferences->find();
        $modified = $this->ModifiedOpinions->find();
        $emphasis = $this->EmphasisOfMatters->find();
        $opinions = $this->OpinionTypes->find();
        $legals = $this->LegalFormValues->find()
                ->contain(['LegalFormTypes']);
        $accountings = $this->AccountingFrameworkValues->find()
                    ->contain(['AccountingFrameworkTypes']);
        $others = $this->LseOthers->find();
        $this->set(compact('references','modified','emphasis','opinions','legals','accountings','others'));
    }
    public function editModified($id) {
        $this->loadModel('ModifiedOpinions');
        try {
           if ($id != null) {                
               $modified = $this->ModifiedOpinions->find()
                   ->where(
                       [
                           'ModifiedOpinions.id' => base64_decode($id)
                       ]
                   )
                   ->first();
           } 
           
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('modified'));
                $this->render('/Element/Reports/edit_modified');
            }            
       } catch (RecordNotFoundException $e) {
           $this->Flash->success('Modified Opinion not found please try agian');
           return $this->redirect($this->referer());
       }
       if($this->request->is('post') ) {
           $modified = $this->ModifiedOpinions->newEntity();
           $modified->id = $this->request->data['id'];
           $modified = $this->ModifiedOpinions->patchEntity($modified, $this->request->data);
           if ($this->ModifiedOpinions->save($modified)) {
               $this->Flash->success(__('
                   Modified Opinion has been updated successfully.'));
               return $this->redirect($this->referer());
           }
           $this->Flash->error(__('Unable to update Modified Opinion'));
       }
    }
    public function editReference($id) {
        $this->loadModel('ReportReferences');
        try {
           if ($id != null) {                
               $reference = $this->ReportReferences->find()
                   ->where(
                       [
                           'ReportReferences.id' => base64_decode($id)
                       ]
                   )
                   ->first();
           } 
           
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('reference'));
                $this->render('/Element/Reports/edit_reference');
            }            
       } catch (RecordNotFoundException $e) {
           $this->Flash->success('Reference not found please try agian');
           return $this->redirect($this->referer());
       }
       if($this->request->is('post') ) {
           $reference = $this->ReportReferences->newEntity();
           $reference->id = $this->request->data['id'];
           $reference = $this->ReportReferences->patchEntity($reference, $this->request->data);
           if ($this->ReportReferences->save($reference)) {
               $this->Flash->success(__('
                   Reference has been updated successfully.'));
               return $this->redirect($this->referer());
           }
           $this->Flash->error(__('Unable to update Reference'));
       }
    }
    public function editEmphasis($id) {
        $this->loadModel('EmphasisOfMatters');
        try {
           if ($id != null) {                
               $emphasis = $this->EmphasisOfMatters->find()
                   ->where(
                       [
                           'EmphasisOfMatters.id' => base64_decode($id)
                       ]
                   )
                   ->first();
           } 
           
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('emphasis'));
                $this->render('/Element/Reports/edit_emphasis');
            }            
       } catch (RecordNotFoundException $e) {
           $this->Flash->success('emphasis not found please try agian');
           return $this->redirect($this->referer());
       }
       if($this->request->is('post') ) {
           $emphasis = $this->EmphasisOfMatters->newEntity();
           $emphasis->id = $this->request->data['id'];
           $emphasis = $this->EmphasisOfMatters->patchEntity($emphasis, $this->request->data);
           if ($this->EmphasisOfMatters->save($emphasis)) {
               $this->Flash->success(__('
                   Emphasis Of Matter has been updated successfully.'));
               return $this->redirect($this->referer());
           }
           $this->Flash->error(__('Unable to update Emphasis Of Matter'));
       }
    }
    public function editOpinion($id) {
        $this->loadModel('OpinionTypes');
        try {
           if ($id != null) {                
               $opinion = $this->OpinionTypes->find()
                   ->where(
                       [
                           'OpinionTypes.id' => base64_decode($id)
                       ]
                   )
                   ->first();
           } 
           
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('opinion'));
                $this->render('/Element/Reports/edit_opinion');
            }            
       } catch (RecordNotFoundException $e) {
           $this->Flash->success('opinion not found please try agian');
           return $this->redirect($this->referer());
       }
       if($this->request->is('post') ) {
           $opinion = $this->OpinionTypes->newEntity();
           $opinion->id = $this->request->data['id'];
           $opinion = $this->OpinionTypes->patchEntity($opinion, $this->request->data);
           if ($this->OpinionTypes->save($opinion)) {
               $this->Flash->success(__('
                   Opinion Type has been updated successfully.'));
               return $this->redirect($this->referer());
           }
           $this->Flash->error(__('Unable to update Opinion Type'));
       }
    }
    public function editLegal($id) {
        $this->loadModel('LegalFormValues');
        try {
           if ($id != null) {                
               $legal = $this->LegalFormValues->find()
                   ->where(
                       [
                           'LegalFormValues.id' => base64_decode($id)
                       ]
                   )
                   ->contain(['LegalFormTypes'])
                   ->first();
           } 
           
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('legal'));
                $this->render('/Element/Reports/edit_legal');
            }            
       } catch (RecordNotFoundException $e) {
           $this->Flash->success('Legal Form Value not found please try agian');
           return $this->redirect($this->referer());
       }
       if($this->request->is('post') ) {
           $legal = $this->LegalFormValues->newEntity();
           $legal->id = $this->request->data['id'];
           $legal = $this->LegalFormValues->patchEntity($legal, $this->request->data);
           if ($this->LegalFormValues->save($legal)) {
               $this->Flash->success(__('
                   Legal Form Value has been updated successfully.'));
               return $this->redirect($this->referer());
           }
           $this->Flash->error(__('Unable to update Legal Form Value'));
       }
    }
    public function editAccounting($id) {
        $this->loadModel('AccountingFrameworkValues');
        try {
           if ($id != null) {                
               $accounting = $this->AccountingFrameworkValues->find()
                   ->where(
                       [
                           'AccountingFrameworkValues.id' => base64_decode($id)
                       ]
                   )
                   ->contain(['AccountingFrameworkTypes'])
                   ->first();
           } 
           
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('accounting'));
                $this->render('/Element/Reports/edit_accounting');
            }            
       } catch (RecordNotFoundException $e) {
           $this->Flash->success('Accounting Form Value not found please try agian');
           return $this->redirect($this->referer());
       }
       if($this->request->is('post') ) {
           $accounting = $this->AccountingFrameworkValues->newEntity();
           $accounting->id = $this->request->data['id'];
           $accounting = $this->AccountingFrameworkValues->patchEntity($accounting, $this->request->data);
           if ($this->AccountingFrameworkValues->save($accounting)) {
               $this->Flash->success(__('
                   Accounting Form Value has been updated successfully.'));
               return $this->redirect($this->referer());
           }
           $this->Flash->error(__('Unable to update Accounting Form Value'));
       }
    }
    public function editOther($id) {
        $this->loadModel('LseOthers');
        try {
           if ($id != null) {                
               $other = $this->LseOthers->find()
                   ->where(
                       [
                           'LseOthers.id' => base64_decode($id)
                       ]
                   )
                   ->first();
           } 
           
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('other'));
                $this->render('/Element/Reports/edit_other');
            }            
       } catch (RecordNotFoundException $e) {
           $this->Flash->success('Lse Other Value not found please try agian');
           return $this->redirect($this->referer());
       }
       if($this->request->is('post') ) {
           $other = $this->LseOthers->newEntity();
           $other->id = $this->request->data['id'];
           $other = $this->LseOthers->patchEntity($other, $this->request->data);
           if ($this->LseOthers->save($other)) {
               $this->Flash->success(__('
                   Lse Other Value has been updated successfully.'));
               return $this->redirect($this->referer());
           }
           $this->Flash->error(__('Unable to update Lse Other Value'));
       }
    }
}
