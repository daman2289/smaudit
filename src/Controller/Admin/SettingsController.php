<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;

/**
 * Settings Controller
 *
 */
class SettingsController extends AppController
{
    public $helpers = ['AkkaCKEditor.CKEditor'];
    public $paginate = [
       'limit' => 20
    ];

    /**
     * emailTemplateList method
     *
     * @return \Cake\Network\Response|null
     */
    public function emailTemplateList()
    {
        $this->viewBuilder()->layout('admin');
        $title = __('Email Templates');
        try {
            $emailTemplatesTable = TableRegistry::get('EmailTemplates');
            $emailTemplates = $emailTemplatesTable->find('all');
            $emailTemplates = $this->paginate($emailTemplates);
        } catch (NotFoundException $e) {
            // redirecting to Last page if request page doesn't exist
            $this->request->query['page'] = $this->request->query['page'] - 1;
            return $this->redirect([
                   'controller' => $this->request->params['controller'],
                   'action' => $this->request->params['action'],
                   '?' => $this->request->query
                ]
            );
        }
        $this->set(compact('emailTemplates', 'title'));
    }

    /**
     * editEmailTemplate method
     *
     * @return \Cake\Network\Response|null
     */
    public function editEmailTemplate($emailTemplateId = null) {
        $this->viewBuilder()->layout('admin');
        $title = __('Edit Email Templates');
        $emailTemplateId = base64_decode($emailTemplateId);
        $emailTemplatesTable = TableRegistry::get('EmailTemplates');
        $emailTemplate = $emailTemplatesTable->get($emailTemplateId);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $emailTemplate = $emailTemplatesTable->patchEntity($emailTemplate, $this->request->data);
            if ($emailTemplatesTable->save($emailTemplate)) {
                $this->Flash->success(__('The email template has been saved.'));
                return $this->redirect(['action' => 'emailTemplateList']);
            } else {
                $this->Flash->error(__('The email template could not be saved. Please, try again.'));
            }
        }

        $this->set(compact('emailTemplate', 'title'));
    }

    /**
     * cmsList method
     *
     * @return \Cake\Network\Response|null
     */
    public function cmsList() {
        $this->viewBuilder()->layout('admin');
        $title = __('CMS Pages');
        try {
            $cmspagesTable = TableRegistry::get('CmsPages');
            $cmspages = $cmspagesTable->find('all');
            $cmspages = $this->paginate($cmspages);
        } catch (NotFoundException $e) {
            // redirecting to Last page if request page doesn't exist
            $this->request->query['page'] = $this->request->query['page'] - 1;
            return $this->redirect([
                   'controller' => $this->request->params['controller'],
                   'action' => $this->request->params['action'],
                   '?' => $this->request->query
                ]
            );
        }
        $this->set(compact('cmspages', 'title'));
    }

    /**
     * editCms method
     *
     * @return \Cake\Network\Response|null
     */
    public function editCms($cmsId = null) {
        $this->viewBuilder()->layout('admin');
        $title = __('Edit CMS Page');
        $cmsId = base64_decode($cmsId);
        $cmspagesTable = TableRegistry::get('CmsPages');
        $cms = $cmspagesTable->get($cmsId);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $cms = $cmspagesTable->patchEntity($cms, $this->request->data);
            if ($cmspagesTable->save($cms)) {
                $this->Flash->success(__('The Page has been saved.'));
                return $this->redirect(['action' => 'cmsList']);
            } else {
                $this->Flash->error(__('The cms could not be saved. Please, try again.'));
            }
        }

        $this->set(compact('cms', 'title'));
    }
    public function reportCategory() {
       $this->viewBuilder()->layout('admin');
       try {
            if (!isset($this->request->query['search']) ) {
                $this->request->query['search'] = null;
            }
            $categoryTable = TableRegistry::get('Categories');
            $queryToGetCategories = $categoryTable->find()
                                            ->where(function ($exp) {
                                                $orConditions = $exp->or_(['Categories.name LIKE' => '%' . $this->request->query['search'] . '%']);
                                                return $exp
                                                    ->add($orConditions);
                                            });
                                            
            $this->paginate = [
                    'sortWhitelist' => [
                        'name'
                    ],
                    'limit' => 25
                ];
            $categories = $this->paginate($queryToGetCategories);
        } catch (NotFoundException $e) {
            // redirecting to Last page if request page doesn't exist
           
        }
        
        $this->set(compact('categories')); 
    }
    /**
    * addAdmin method to add new admin     *
    * @return \Cake\Network\Response
    */
    public function addCategory () {
       $this->loadModel('Categories');
       $category = $this->Categories->newEntity();
       if($this->request->is('post') ) {
           $category = $this->Categories->patchEntity($category, $this->request->data);
           if ($this->Categories->save($category)) {
               $this->Flash->success(__('New Report Category has been saved.'));
               return $this->redirect($this->referer());
           }
       }
       $this->set(compact('category'));
    }   
    public function editCategory ($id = null) {
        $this->loadModel('Categories');
       try {
           if ($id != null) {                
               $category = $this->Categories->find()
                   ->where(
                       [
                           'Categories.id' => base64_decode($id)
                       ]
                   )
                   ->first();
           } 
           
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('category'));
                $this->render('/Element/Administration/edit_category');
            }            
       } catch (RecordNotFoundException $e) {
           $this->Flash->success('Report Category not found please try agian');
           return $this->redirect($this->referer());
       }
       if($this->request->is('post') ) {
           $category = $this->Categories->newEntity();
           $category->id = $this->request->data['id'];
           $category = $this->Categories->patchEntity($category, $this->request->data);
           if ($this->Categories->save($category)) {
               $this->Flash->success(__('
                   Report category has been updated successfully.'));
               return $this->redirect(['action' => 'reportCategory']);
           }
           $this->Flash->error(__('Unable to add the report category.'));
       }
       $this->set(compact('category'));
    }  

      /**
    * deleteUser method to delete user *
    * @return \Cake\Network\Response
    */
    public function deleteCategory ($id) {   
        $this->loadModel('Categories');   
        $this->request->allowMethod(['post', 'delete']);
        $category = $this->Categories->get(base64_decode($id));
        
       if ($this->Categories->delete($category)) {
           $this->Flash->success(__(
                   'The Report Category has been deleted'
               )
           );
           return $this->redirect($this->referer());
       }
   }
}