<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Specializes Controller
 *
 * @property \App\Model\Table\SpecializesTable $Specializes
 */
class SpecializesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->viewBuilder()->layout('admin');
        $specializes = $this->paginate($this->Specializes);
        $this->set(compact('specializes'));
        $this->set('_serialize', ['specializes']);
    }

    /**
     * View method
     *
     * @param string|null $id Specialize id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $specialize = $this->Specializes->get($id, [
            'contain' => []
        ]);

        $this->set('specialize', $specialize);
        $this->set('_serialize', ['specialize']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $specialize = $this->Specializes->newEntity();
        if ($this->request->is('post')) {
            // pr($this->request->getData());die;
            $specialize = $this->Specializes->patchEntity($specialize, $this->request->getData());
            if ($this->Specializes->save($specialize)) {
                $this->Flash->success(__('The Specialization has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The Specialization could not be saved. Please, try again.'));
        }
        $this->set(compact('specialize'));
        $this->set('_serialize', ['specialize']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Specialize id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {        
        try {
            $specialize = $this->Specializes->find()
                ->where(
                   [
                       'Specializes.id' => base64_decode($id)
                   ]
                )
               ->first();
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('specialize'));
                $this->render('/Element/Specializes/edit');
            }            
        } catch (RecordNotFoundException $e) {
           $this->Flash->success(__('Specialize not found please try agian'));
           return $this->redirect($this->referer());
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $specialize = $this->Specializes->patchEntity($specialize, $this->request->getData());
            if ($this->Specializes->save($specialize)) {
                $this->Flash->success(__('The specialize has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The specialize could not be saved. Please, try again.'));
        }
        $this->set(compact('specialize'));
        $this->set('_serialize', ['specialize']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Specialize id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $specialize = $this->Specializes->get($id);
        if ($this->Specializes->delete($specialize)) {
            $this->Flash->success(__('The Specialization has been deleted.'));
        } else {
            $this->Flash->error(__('The Specialization could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
