<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Services Controller
 *
 * @property \App\Model\Table\ServicesTable $Services
 */
class ServicesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->viewBuilder()->layout('admin');
        $services = $this->paginate($this->Services);

        $this->set(compact('services'));
        $this->set('_serialize', ['services']);
    }

    /**
     * View method
     *
     * @param string|null $id Service id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $service = $this->Services->get($id, [
            'contain' => []
        ]);

        $this->set('service', $service);
        $this->set('_serialize', ['service']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $service = $this->Services->newEntity();
        if ($this->request->is('post')) {
            $service = $this->Services->patchEntity($service, $this->request->getData());
            if ($this->Services->save($service)) {
                $this->Flash->success(__('The service has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The service could not be saved. Please, try again.'));
        }
        $this->set(compact('service'));
        $this->set('_serialize', ['service']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Service id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        try {
            $service = $this->Services->find()
                ->where(
                   [
                       'Services.id' => base64_decode($id)
                   ]
                )
               ->first();
        // pr($service->toArray());die;
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('service'));
                $this->render('/Element/Services/edit');
            }            
        } catch (RecordNotFoundException $e) {
           $this->Flash->success(__('Service not found please try agian'));
           return $this->redirect($this->referer());
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $service = $this->Services->patchEntity($service, $this->request->getData());
            if ($this->Services->save($service)) {
                $this->Flash->success(__('The service has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The service could not be saved. Please, try again.'));
        }
        $this->set(compact('service'));
        $this->set('_serialize', ['service']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Service id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    
    public function deactivate($id) {
        $service_id = base64_decode($id);
        $this->viewBuilder()->layout(false);
        $this->autoRender = false;
        $this->loadModel('Services'); 
        $service = $this->Services->find()
                    ->where(['Services.id' => $service_id])
                    ->first();
        $service->is_activated = ($service->is_activated) ? 0: 1;
        $this->Services->save($service);
        echo $service->is_activated;
    }
    public function delete ($id) {   
        $this->loadModel('Services');   
        $this->request->allowMethod(['post', 'delete']);
        $service = $this->Services->get(base64_decode($id));
        
       if ($this->Services->delete($service)) {
           $this->Flash->success(__(
                   'The Service has been deleted'
               )
           );
           return $this->redirect($this->referer());
       }
    }
}
