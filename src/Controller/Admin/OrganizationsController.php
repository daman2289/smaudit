<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Utility\Text;
use App\Event\UserAddListener;
use Cake\Event\EventManager;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class OrganizationsController extends AppController
{
	public function index() {
		$this->viewBuilder()->layout('admin');
		try {
            $organizationTable = TableRegistry::get('Users');
            $queryToGetusers = $organizationTable->find()
				->where(['Users.role_id' => 2])
                ->contain([ 'Organizations' => [
                        'OrganizationUsers'
                    ]
                ])
                ->matching('Organizations', function ($q) {
                    return $q->where(['Organizations.is_deleted' => 0]);
                });           
                                            
            $this->paginate = [
                    'sortWhitelist' => [
                        'name'
                    ],
                    'limit' => 25
                ];
            // pr($queryToGetusers->toArray());die;
            $users = $this->paginate($queryToGetusers);
        } catch (NotFoundException $e) {
            // redirecting to Last page if request page doesn't exist
           
        }
        $this->set(compact('users'));
	}

    public function users($organization_id) {
        $this->loadModel('OrganizationUsers');
        $this->viewBuilder()->layout('admin');
        try {
            $this->Organizations->belongsToMany('Users', [
                'joinTable' => 'organization_users',
            ]);
            $queryToGetusers = $this->Organizations->find()
                ->where(['Organizations.id' => base64_decode($organization_id)])
                ->contain([
                    'Users' => function ($q) {
                        return $q->where(['Users.is_deleted' => 0]);
                    }
                ])
                ->first();           
                                            
            $this->paginate = [
                    'sortWhitelist' => [
                        'name'
                    ],
                    'limit' => 25
                ];
            // pr($queryToGetusers->toArray());die;
            $users = $queryToGetusers;
        } catch (NotFoundException $e) {
            // redirecting to Last page if request page doesn't exist
           
        }
        $this->set(compact('users'));
    }

	public function deactivate($id) {
       $user_id = base64_decode($id);
       $this->viewBuilder()->layout(false);
       $this->autoRender = false;
       $this->loadModel('Users'); 
       	$user = $this->Users->find()
       				->where(['Users.id' => $user_id])
       				->first();
        $user->is_admin_activated = ($user->is_admin_activated) ? 0: 1;
      	$this->Users->save($user);
       	echo $user->is_admin_activated;
    }

    public function resetTrial($id) {
        $organizationTable = TableRegistry::get('Organizations');
        if($organizationTable->resetTrial(base64_decode($id))) {
            $this->Flash->success(__('Trail has been reset successfully.'));
            return $this->redirect($this->referer());
        }
        $this->Flash->error(__('Something went wrong try again.'));
        return $this->redirect($this->referer());
    }

    public function deleteOrganization($id) {
        $organizationTable = TableRegistry::get('Organizations');      
        if($organizationTable->deleteOrganization(base64_decode($id))) {
            $this->Flash->success(__('Organization has been deleted successfully.'));
            if($this->Auth->user('role_id') == Configure::read('UserRoles.Super Admin')) {
                return $this->redirect($this->referer());
            }
            return $this->redirect($this->Auth->logout());
        }
        $this->Flash->error(__('Unable to delete Organization'));
        return $this->redirect($this->referer());
    }

    public function deleteUser($id) {
        $this->loadModel('Users');
        $user = $this->Users->find()
            ->where(['Users.id' => base64_decode($id)])
            ->first();
        if(empty($user)) {
            $this->Flash->error(__('User not found'));
            return $this->redirect($this->referer());
        }
        $user->is_deleted =1 ;
        if($this->Users->save($user)) {
            $this->Flash->success(__('User deleted successfully.'));
            return $this->redirect($this->referer());
        }
        $this->Flash->error(__('Unable to delete user'));
        return $this->redirect($this->referer());
    }

    public function addUser($organization_id) {
        $this->viewBuilder()->layout('admin');
        
        $this->loadModel('Users');
        $this->loadModel('Licenses');
        $this->loadModel('OrganizationSubsidiaries');
        $license = $this->Licenses->find('list')
                ->toArray();

        $subsidiary = $this->OrganizationSubsidiaries->find('list')
                    ->where(['OrganizationSubsidiaries.organization_id' => base64_decode($organization_id) ])
                    ->toArray();
        $user = $this->Users->newEntity();
        if($this->request->is('post')) {
            $this->request->data['role_id'] = 3;
            $this->request->data['is_activated'] = 1;
            $this->request->data['is_admin_activated'] = 1;
            $this->request->data['token'] = Text::uuid();
            $this->request->data['organization_user']['organization_id'] = base64_decode($organization_id);
            $user = $this->Users->patchEntity($user, $this->request->data);

            if($this->Users->save($user)) {
                $user_id = $user->id;
                $table  = TableRegistry::get('OrganizationUserPermissions');
                $table->setUserDefaultPermission(base64_decode($organization_id), $user_id);

                $this->Flash->success(__('User has been added successfully.'));
                return $this->redirect(['action' => 'users' , $organization_id]);
            }
            $this->Flash->error(__('User can not be added'));
            return $this->redirect($this->referer());
        }
        $this->set(compact('user', 'organization_id', 'license', 'subsidiary'));
    }

    public function editUser($user_id, $organization_id) {
        $this->viewBuilder()->layout('admin');
        
        $this->loadModel('Users');
        $this->loadModel('Licenses');
        $this->loadModel('OrganizationSubsidiaries');
        $license = $this->Licenses->find('list')
                ->toArray();

        $subsidiary = $this->OrganizationSubsidiaries->find('list')
                    ->where(['OrganizationSubsidiaries.organization_id' => base64_decode($organization_id) ])
                    ->toArray();
        $user = $this->Users->find()
            ->where(['Users.id' => base64_decode($user_id)])
            ->contain(['OrganizationUsers'])
            ->first();

        if($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data, [
                'associated' => [
                    'OrganizationUsers'
                ]
            ]);
            if($this->Users->save($user)) {
                $user_id = $user->id;
                $table  = TableRegistry::get('OrganizationUserPermissions');
                $table->setUserDefaultPermission(base64_decode($organization_id), $user_id);

                $this->Flash->success(__('User has been added successfully.'));
                return $this->redirect(['action' => 'users' , $organization_id]);
            }
            $this->Flash->error(__($this->_setValidationError($user->errors())));
            return $this->redirect($this->referer());
        }
        $this->set(compact('user', 'organization_id', 'license', 'subsidiary', 'user_id'));
    }

}