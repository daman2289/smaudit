<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;
use Cake\Network\Exception\NotFoundException;
use App\Event\SubAdminAddListener;
use Cake\Event\EventManager;
use Cake\Event\Event;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class FaqsController extends AppController
{
	public function index() {
		$this->viewBuilder()->layout('admin');
		try {
            $placeholderTable = TableRegistry::get('Faqs');
            $queryToGetFaqs = $placeholderTable->find();
                                            
                                            
            $this->paginate = [
                    'sortWhitelist' => [
                        'name'
                    ],
                    'limit' => 25
                ];
            $faqs = $this->paginate($queryToGetFaqs);
        } catch (NotFoundException $e) {
            // redirecting to Last page if request page doesn't exist
           
        }
        $this->set(compact('faqs'));
	}
	public function add() {
		$this->viewBuilder()->layout('admin');
		$faq = $this->Faqs->newEntity();
	       if($this->request->is('post') ) {
	           $faq = $this->Faqs->patchEntity($faq, $this->request->data);
	           if ($this->Faqs->save($faq)) {
	               $this->Flash->success(__('New Question has been saved.'));
	               return $this->redirect($this->referer());
	           }
	       }
	       $this->set(compact('faq'));
	}
	public function edit($id) {
		$this->loadModel('Faqs');
		try {
           if ($id != null) {                
               $faq = $this->Faqs->find()
                   ->where(
                       [
                           'Faqs.id' => base64_decode($id)
                       ]
                   )
                   ->first();
           } 
           
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('faq'));
                $this->render('/Element/Faq/edit_faq');
            }            
       } catch (RecordNotFoundException $e) {
           $this->Flash->success(__('FAQ not found please try again'));
           return $this->redirect($this->referer());
       }
       if($this->request->is('post') ) {
            $faq = $this->Faqs->newEntity();
            $faq->id = base64_decode($id);
            $faq = $this->Faqs->patchEntity($faq, $this->request->data);
            if ($this->Faqs->save($faq)) {
                $this->Flash->success(__('
                   FAQ has been updated successfully.'));
                return $this->redirect(['action' => 'index']);
            }
           $this->Flash->error(__('Unable to update License'));
       }
	}
    public function delete ($id) {   
        $this->loadModel('Faqs');   
        $this->request->allowMethod(['post', 'delete']);
        $faq = $this->Faqs->get(base64_decode($id));
        
       if ($this->Faqs->delete($faq)) {
           $this->Flash->success(__(
                   'The FAQ has been deleted'
               )
           );
           return $this->redirect($this->referer());
       }
   }
}