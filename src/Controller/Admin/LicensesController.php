<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Utility\Text;
use App\Event\UserAddListener;
use Cake\Event\EventManager;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class LicensesController extends AppController
{
	public function index() {
		$this->viewBuilder()->layout('admin');
		try {
            $placeholderTable = TableRegistry::get('Licenses');
            $queryToGetdiplomas = $placeholderTable->find();
                                            
                                            
            $this->paginate = [
                    'sortWhitelist' => [
                        'name'
                    ],
                    'limit' => 25
                ];
            $licenses = $this->paginate($queryToGetdiplomas);
        } catch (NotFoundException $e) {
            // redirecting to Last page if request page doesn't exist
           
        }
        $this->set(compact('licenses'));
	}
	public function add() {
		$this->viewBuilder()->layout('admin');
		$license = $this->Licenses->newEntity();
	       if($this->request->is('post') ) {
	           $license = $this->Licenses->patchEntity($license, $this->request->data);
	           if ($this->Licenses->save($license)) {
	               $this->Flash->success(__('New Licenses has been saved.'));
	               return $this->redirect($this->referer());
	           }
	       }
	       $this->set(compact('license'));
	}
	public function editDiploma($id) {
		$this->loadModel('Licenses');
		try {
           if ($id != null) {                
               $license = $this->Licenses->find()
                   ->where(
                       [
                           'Licenses.id' => base64_decode($id)
                       ]
                   )
                   ->first();
           } 
           
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('license'));
                $this->render('/Element/Diploma/edit_diploma');
            }            
       } catch (RecordNotFoundException $e) {
           $this->Flash->success('License not found please try agian');
           return $this->redirect($this->referer());
       }
       if($this->request->is('post') ) {
           $license = $this->Licenses->newEntity();
           $license->id = $this->request->data['id'];
           $license = $this->Licenses->patchEntity($license, $this->request->data);
           if ($this->Licenses->save($license)) {
               $this->Flash->success(__('
                   License has been updated successfully.'));
               return $this->redirect(['action' => 'index']);
           }
           $this->Flash->error(__('Unable to update License'));
       }
       $this->set(compact('license'));
	}
}