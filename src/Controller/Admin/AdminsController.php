<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;
use Cake\Network\Exception\NotFoundException;
use App\Event\SubAdminAddListener;
use Cake\Event\EventManager;
use Cake\Event\Event;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class AdminsController extends AppController
{

    public $paginate = [
        'limit' => 25,
        'order' => [
            'Users.created' => 'desc'
        ]
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    /**
     * Displays the admin login view
     *
     * @return void|\Cake\Network\Response
     */
    public function login() {
        $this->viewBuilder()->layout('admin_login');
        if ($this->request->is('post')) {
        	$user = $this->Auth->identify();
            $isAdminUserAllowedToLogin = true;
            if (!$user) {
                $this->Flash->error(__('Invalid username or password! Try again.'));
                $isAdminUserAllowedToLogin = false;
            } elseif (!in_array($user['role_id'], [Configure::read('UserRoles.Super Admin')])) {
                $this->Flash->error(__('You are not allowed to login to super admin panel.'));
                $isAdminUserAllowedToLogin = false;
            } elseif (!$user['is_activated']) {
                $this->Flash->error(__('Your account is deactivated. Kindly contact administration.'));
                $isAdminUserAllowedToLogin = false;
            }

            if ($isAdminUserAllowedToLogin) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
        }
    }

    /**
     * Displays the admin dashboard after succesfully logging in
     *
     * @return void|\Cake\Network\Response
     */
    public function dashboard() {
    	$this->viewBuilder()->layout('admin');
    	$title = __('Admin Dashboard');
    	$this->set(compact('title'));
        $this->loadModel('Users');
        $Organizations = $this->Users->find()
                        ->where(['Users.role_id' => 2,'Users.is_admin_activated' =>1,'Users.is_activated' => 1]);
        $this->loadModel('Reports');
        $reports = $this->Reports->find('all');
        $this->set(compact('Organizations','reports'));
    }

    /**
     * To logout the admin user
     *
     * @return void|\Cake\Network\Response
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    /**
     * To change the admin user passowrd
     *
     * @return void|\Cake\Network\Response
     */
    public function changePassword() {
        if ($this->request->is('post')) {
            // pr($this->request->data);die;
            $usersTable = TableRegistry::get('Users');
            $userId = $this->Auth->user('id');
            $adminUser = $usersTable->get($userId);
            $adminUser = $usersTable->patchEntity($adminUser, $this->request->data);
            if ($usersTable->save($adminUser)) {
                $this->Flash->success(__('Your passwords has been updated succesfully.'));
                return $this->redirect($this->referer());
            }

            $errors = $this->_setValidationError($adminUser->errors());
            $this->Flash->error(__('Password update request not completed due to the following errors: - ' . $errors));
            return $this->redirect($this->referer());
        }
    }

    /**
     * editProfile method
     *
     * @param none
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function editProfile()
    {
        $users = TableRegistry::get('Users');
        $user = $users->get($this->Auth->user('id'));
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $users->patchEntity($user, $this->request->data);
            if ($users->save($user)) {
                $this->Auth->setUser($user->toArray());
                $this->Flash->success(__('Your profile information has been updated.'));
                return $this->redirect($this->referer());
            }
            $errors = $this->_setValidationError($user->errors());
            $this->Flash->error(__('The user could not be updated due to the following errors: - ' . $errors));
            return $this->redirect($this->referer());
        }
    }

    /**
     * list all sub admins method
     *
     * @param none
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function listSubAdmins() {
        $this->viewBuilder()->layout('admin');
        try {
            if (!isset($this->request->query['search']) ) {
                $this->request->query['search'] = null;
            }
            $usersTable = TableRegistry::get('Users');
            $queryToGetSubAdmins = $usersTable->find()
                                            ->where(function ($exp) {
                                                $orConditions = $exp->or_(['Users.first_name LIKE' => '%' . $this->request->query['search'] . '%'])
                                                                    ->like('Users.last_name', '%' . $this->request->query['search'] . '%')
                                                                    ->like('Users.email', '%' . $this->request->query['search'] . '%');
                                                return $exp
                                                    ->add($orConditions)
                                                    ->eq('Users.role_id', Configure::read('UserRoles.Super Admin'));
                                            })->andWhere(['Users.email <>' => 'admin@smaudit.com']);
            $this->paginate = [
                    'sortWhitelist' => [
                        'first_name', 'last_name', 'email' 
                    ],
                    'limit' => 25
                ];
            $subAdmins = $this->paginate($queryToGetSubAdmins);
        } catch (NotFoundException $e) {
            // redirecting to Last page if request page doesn't exist
            return $this->redirect(DS . 'admin' . DS . $this->request->params['controller'] . DS . $this->request->params['action'] . '?page=' . $this->request->getParam('paging')['Users']['page']);
        }
        $title = __('Sub Admin');
        $this->set(compact('subAdmins', 'title'));
    }

    /**
     * Add method to add new sub admin
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $usersTable = TableRegistry::get('Users');
        $user = $usersTable->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['role_id'] = Configure::read('UserRoles.Super Admin');
            $this->request->data['is_activated'] = Configure::read('UserActivated.True');
            $this->request->data['is_admin_activated'] = Configure::read('UserActivated.True');
            $this->request->data['token'] = Text::uuid();
            $user = $usersTable->patchEntity($user, $this->request->data);
            if ($usersTable->save($user)) {
                $this->__sendSubAdminCreateEmail();
                $this->Flash->success(__('New Sub-Admin has been added successfully.'));
                return $this->redirect($this->referer());
            }
            $errors = $this->_setValidationError($user->errors());
            $this->Flash->error(__('The record could not be added due to the following errors: - ' . $errors));
            return $this->redirect($this->referer());
        }
        $this->Flash->error(__('Some errors occurred. Please try again!'));
        return $this->redirect($this->referer());
    }

    /**
     * Add method to add new sub admin
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function edit($id)
    {
        $usersTable = TableRegistry::get('Users');
        $user = $usersTable->find()
            ->where(['Users.id' => base64_decode($id)])
            ->first();
        if ($this->request->is('post')) {
            if(empty($this->request->data['password'])) {
                unset($this->request->data['password']);
                unset($this->request->data['confirm_password']);
            }
            $this->request->data['role_id'] = Configure::read('UserRoles.Super Admin');
            $this->request->data['is_activated'] = Configure::read('UserActivated.True');
            $this->request->data['is_admin_activated'] = Configure::read('UserActivated.True');
            $this->request->data['token'] = Text::uuid();
            $user = $usersTable->patchEntity($user, $this->request->data,['validate' => 'OnlyCheck']);
            if ($usersTable->save($user)) {
                // $this->__sendSubAdminCreateEmail();
                $this->Flash->success(__('New Sub-Admin has been added successfully.'));
                return $this->redirect($this->referer());
            }
            $errors = $this->_setValidationError($user->errors());
            $this->Flash->error(__('The record could not be added due to the following errors: - ' . $errors));
            return $this->redirect($this->referer());
        }
        if($this->request->is('ajax')){
            $this->autoRender = false;
            $this->set(compact('user'));
            $this->render('/Element/Administration/edit_subadmin');
        }  
        
    }

    /**
     * Add __sendSubAdminCreateEmail to send email to sub admin user after account creation by super admin
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    private function __sendSubAdminCreateEmail() {
        $SubAdminAddListener = new SubAdminAddListener();
        EventManager::instance()->attach($SubAdminAddListener);

        $event = new Event('Model.User.afterAdd', $this, [
            'user' => $this->request->data
        ]);
        $this->eventManager()->dispatch($event);
        return true;
    }

    public function manuCollapse($state) {
        $this->request->session()->write('collapse', $state);
    }

    // public function addSubadmin() {
    //     if($this->request->is('post')) {
    //         $this->loadModel('User');
    //         if($this->User->save()) {

    //         }
    //     }
    // }
}