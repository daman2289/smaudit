<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * NewPlaceholders Controller
 *
 * @property \App\Model\Table\NewPlaceholdersTable $NewPlaceholders
 *
 * @method \App\Model\Entity\NewPlaceholder[] paginate($object = null, array $settings = [])
 */
class NewPlaceholdersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->layout('admin');
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['NewReports', 'NewPlaceholderTypes']
        ];
        $newPlaceholders = $this->paginate($this->NewPlaceholders);

        $this->set(compact('newPlaceholders'));
        $this->set('_serialize', ['newPlaceholders']);
    }

    /**
     * View method
     *
     * @param string|null $id New Placeholder id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {   
        $placeholder = $this->NewPlaceholders->get($id, [
            'contain' => ['NewQuestions']
        ]);
        $this->loadModel('NewPlaceholderTypes');
        $types = $this->NewPlaceholderTypes->find('list')
            ->where(['is_primary' => 1]);
        $this->loadModel('NewQuestionTypes');
        $questionTypes = $this->NewQuestionTypes->find('list')
            ->where(['id <' => 9]);

        $this->loadModel('LegalFormTypes');
        $formTypes = $this->LegalFormTypes->find('list');

        $this->set(compact('placeholder', 'types', 'questionTypes', 'formTypes'));
        $this->set('_serialize', ['newPlaceholder']);
    }

    protected function __preProcessRequest($data) {
        $newData = [];
        if($data['new_placeholder_type_id'] != 3) {
            if($data['new_placeholder_type_id'] == 6) {
                foreach ($data['legal_form_replacements'] as $key => $value) {
                    $data['legal_form_replacements'][$key]['new_report_id'] = $data['new_report_id'];
                }
            }
            if($data['new_placeholder_type_id'] == 7) {
                foreach ($data['signature_gender_replacements'] as $key => $value) {
                    $data['signature_gender_replacements'][$key]['new_report_id'] = $data['new_report_id'];
                    $data['signature_gender_replacements'][$key]['signature'] = $data['signature'];
                }
            }
            return $this->NewPlaceholders->saveSingleRecord($data);
        }
        if(!empty($data['new_questions'])) {
            if($data['new_questions'][0]['new_question_type_id'] == 10) {
                $newData = $this->__addSignaturePlaceholder($data);
                $newData = $this->__addLicencePlaceholder($newData, $data);
                return $this->NewPlaceholders->saveMultipleRecord($newData);

            } elseif($data['new_questions'][0]['new_question_type_id'] == 11) {
                $newData = $this->__addFinancialYearPlaceholder($data);
                return $this->NewPlaceholders->saveMultipleRecord($newData);

            } elseif($data['new_questions'][0]['new_question_type_id'] == 9) {
                $newData = $this->__addClientCompanyPlaceholder($data);
                return $this->NewPlaceholders->saveMultipleRecord($newData);

            } else {
                return $this->NewPlaceholders->saveSingleRecord($data);
            }
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $newPlaceholder = $this->NewPlaceholders->newEntity();
        if ($this->request->is('post')) {
            // pr($this->request->data);die;
            if($this->__preProcessRequest($this->request->data)) {
                $this->Flash->success(__('The new placeholder has been saved.'));
                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The new placeholder could not be saved. Please, try again.'));
        }
        $newReports = $this->NewPlaceholders->NewReports->find('list', ['limit' => 200]);
        $newPlaceholderTypes = $this->NewPlaceholders->NewPlaceholderTypes->find('list', ['limit' => 200])
            ->where(['is_primary' => 1]);

        $this->loadModel('NewQuestionTypes');
        $this->loadModel('LegalFormTypes');
        $formTypes = $this->LegalFormTypes->find('list');
        $questionTypes = $this->NewQuestionTypes->find('list');

        $this->set(compact('newPlaceholder', 'newReports', 'newPlaceholderTypes', 'questionTypes', 'formTypes'));
        $this->set('_serialize', ['newPlaceholder']);
    }

    /**
     * Edit method
     *
     * @param string|null $id New Placeholder id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        
        $newPlaceholder = $this->NewPlaceholders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            // pr($this->request->data);die;
             if(!empty($this->request->data['legal_form_replacements'])) {
                foreach ($this->request->data['legal_form_replacements'] as $key => $value) {
                    $this->request->data['legal_form_replacements'][$key]['new_report_id'] = $newPlaceholder->new_report_id;
                }
            }
            // pr($this->request->data);die;
            $newPlaceholder = $this->NewPlaceholders->patchEntity($newPlaceholder, $this->request->getData(),[
                'associated' => [
                    'NewQuestions' => ['associated' => 'NewQuestionOptions'],
                    'LegalFormReplacements'
                ]
            ]);
            // pr($newPlaceholder->errors());die;
            if ($this->NewPlaceholders->save($newPlaceholder)) {
                $this->Flash->success(__('The new placeholder has been saved.'));
                return $this->redirect(['controller' => 'new-reports', 'action' => 'index']);
            }
            $this->Flash->error(__('The new placeholder could not be saved. Please, try again.'));
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id New Placeholder id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $newPlaceholder = $this->NewPlaceholders->get(base64_decode($id));
        if ($this->NewPlaceholders->delete($newPlaceholder)) {
            $this->Flash->success(__('Placeholder has been deleted.'));
        } else {
            $this->Flash->error(__('Placeholder could not be deleted. Please, try again.'));
        }

        return $this->redirect($this->referer());
    }

    /**
     * Edit method
     *
     * @param string|null $id New Placeholder id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function editPlaceholder($id = null)
    {
        $placeholder = $this->NewPlaceholders->get(base64_decode($id), [
            'contain' => [
                'NewQuestions' => ['NewQuestionOptions'],
                'LegalFormReplacements' => ['LegalFormTypes']
            ]
        ]);
        // pr($placeholder->toArray());die;
        // pr($placeholder->toArray());
        if ($this->request->is(['patch', 'post', 'put'])) {
            $newPlaceholder = $this->NewPlaceholders->patchEntity($placeholder, $this->request->getData(),[
                'associated' => [
                    'NewQuestions' => ['associated' => 'NewQuestionOptions'],
                    'LegalFormReplacements'
                ]
            ]);
            // pr($newPlaceholder);die;
            // $newPlaceholder  = $this->__deleteQuestion($placeholder, $newPlaceholder);
            if ($this->NewPlaceholders->save($newPlaceholder)) {
                $this->Flash->success(__('The new placeholder has been saved.'));
                return $this->redirect(['controller' => 'new-reports', 'action' => 'index']);
            }
            $this->Flash->error(__('The new placeholder could not be saved. Please, try again.'));
        }

        $this->loadModel('NewPlaceholderTypes');
        $types = $this->NewPlaceholderTypes->find('list')
            ->where(['is_primary' => 1]);
        $this->loadModel('NewQuestionTypes');
        $questionTypes = $this->NewQuestionTypes->find('list');

        $this->set(compact('placeholder', 'types', 'questionTypes'));
        $this->set('_serialize', ['newPlaceholder']);
    }

    protected function __deleteQuestion($placeholder, $newentity) {
        //changing placeholder form question to system
        // remove placeholder question and options
        $this->loadModel('NewQuestions');
        $this->loadModel('NewQuestionOptions');
        if($placeholder->new_placeholder_type_id == 3 && $newentity->new_placeholder_type_id == 1) {
            try {
                $question = $this->NewQuestions->find()
                    ->where(['NewQuestions.new_placeholder_id' => $placeholder->id])->first();
                if($question) {
                    $this->NewQuestions->delete($question);
                }
            }   catch (RecordNotFoundException $e) {
            }   catch (NotFoundException $e) {
            }
        } else if($placeholder->new_placeholder_type_id == 3 && $newentity->new_placeholder_type_id == 3) {
            $question = $this->NewQuestions->find()
                    ->where(['NewQuestions.new_placeholder_id' => $placeholder->id])->first();
                if($question) {
                    $this->NewQuestionOptions->deleteAll(['new_question_id' => $question->id ]);
                }
        }else { // changing from system placehoder to qustion empty the content en field
            $newentity->content_en = '';            
        }

        return $newentity;
    }

    public function viewPlaceholder($id) {
        $placeholder = $this->NewPlaceholders->get($id, [
            'contain' => [
                'NewQuestions' => [
                    'NewQuestionOptions',
                    'ParentQuestions'
                ],
                'NewPlaceholderTypes',
                'LegalFormReplacements' => ['LegalFormTypes']
            ]
        ]);
        // pr($placeholder);die;
        $this->loadModel('NewPlaceholderTypes');
        $types = $this->NewPlaceholderTypes->find('list');
        $this->loadModel('NewQuestionTypes');
        $questionTypes = $this->NewQuestionTypes->find('list');

        $this->set(compact('placeholder', 'types', 'questionTypes'));
        $this->set('_serialize', ['newPlaceholder']);
    }

    public function addDependentQuestion($question_id) {
        $this->loadModel('NewQuestions');
        $newPlaceholder = $this->NewPlaceholders->newEntity();
        
        $parentQuestion = $this->NewQuestions->find()
            ->where(['NewQuestions.id' => base64_decode($question_id)])
            ->contain(['NewPlaceholders', 'NewQuestionOptions', ])
            ->first();
        if ($this->request->is('post')) {
            // pr($this->request->data);die;
            $this->request->data['child_questions'][0]['_joinData']['condition_for_child'] = implode(',', $this->request->data['child_questions'][0]['_joinData']['condition_for_child']);
            $this->request->data['child_questions'][0]['new_placeholder']['new_report_id']  = $parentQuestion->new_placeholder->new_report_id;
            $this->request->data['child_questions'][0]['is_parent'] = 0;
            $question = $this->NewQuestions->patchEntity($parentQuestion, $this->request->data, [
                'associated' => [
                        'ChildQuestions._joinData',
                        'ChildQuestions.NewPlaceholders',
                        'ChildQuestions.NewQuestionOptions'
                    ]
            ]);
            if($this->NewQuestions->save($question)) {
                $this->Flash->success(__('The new placeholder has been saved.'));
                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The new placeholder could not be saved. Please, try again.'));
        }
        $newReports = $this->NewPlaceholders->NewReports->find('list', ['limit' => 200]);
        
        $this->loadModel('NewQuestionTypes');
        $questionTypes = $this->NewQuestionTypes->find('list');
        $this->set(compact('newPlaceholder', 'newReports', 'questionTypes', 'parentQuestion'));
        $this->set('_serialize', ['newPlaceholder']);
    }

    public function addQuestion() {
        $this->loadModel('NewQuestions');
        $data = [
                'new_question_type_id' => 1,
                'question_en' => 'Dividend',
                'new_placeholder' => [
                    'new_report_id' => 7,
                    'new_placeholder_type_id'   => 3,
                    'name'  => '[d]'
                ]
        ];
        $newPlaceholder = $this->NewQuestions->newEntity($data);
        // pr($newPlaceholder);
        $this->NewQuestions->save($newPlaceholder);die;

    }

    

    protected function __addLicencePlaceholder($newData, $data) {
        $newData[] = [
            'name' => '[FirstLicence]',
            'new_report_id' => $data['new_report_id'],
            'new_placeholder_type_id' => 8
        ];
        $newData[] = [
            'name' => '[SecondLicence]',
            'new_report_id' => $data['new_report_id'],
            'new_placeholder_type_id' => 8
        ];
        return $newData;
    }

    protected function __addClientCompanyPlaceholder($data) {
        $data['name'] = '[CCName]';
        $newData[] = $data;
        $newData[] = [
            'name' => '[CCAdress]',
            'new_report_id' => $data['new_report_id'],
            'new_placeholder_type_id' => 4,
            'content_en' => 'company_street'
        ];
        $newData[] = [
            'name' => '[CCAdressNo]',
            'new_report_id' => $data['new_report_id'],
            'new_placeholder_type_id' => 4,
            'content_en' => 'company_street_number'
        ];
        $newData[] = [
            'name' => '[CCPlace]',
            'new_report_id' => $data['new_report_id'],
            'new_placeholder_type_id' => 4,
            'content_en' => 'company_place'
        ];
        $newData[] = [
            'name' => '[CCZip]',
            'new_report_id' => $data['new_report_id'],
            'new_placeholder_type_id' => 4,
            'content_en' => 'company_zip'
        ];
        $newData[] = [
            'name' => '[CCPlaceIncorp]',
            'new_report_id' => $data['new_report_id'],
            'new_placeholder_type_id' => 4,
            'content_en' => 'company_place_of_incorporation'
        ];
        $newData[] = [
            'name' => '[CCLegalForm]',
            'new_report_id' => $data['new_report_id'],
            'new_placeholder_type_id' => 4,
            'content_en' => 'company_legal_form_type_id'
        ];
        $newData[] = [
            'name' => '[CCAccFrame]',
            'new_report_id' => $data['new_report_id'],
            'new_placeholder_type_id' => 4,
            'content_en' => 'company_accounting_framework_type_id'
        ]; 

        return $newData;  
    }

    protected function __addFinancialYearPlaceholder($data) {
        $newData[] = [
            'name' => '[FinYearFrom]',
            'new_report_id' => $data['new_report_id'],
            'new_placeholder_type_id' => $data['new_placeholder_type_id'],
            'new_questions' => [
                [
                    'new_question_type_id' => 8,
                    'question_en' => 'Financial year from'
                ]
            ]
        ];
        $newData[] = [
            'name' => '[FinYearTo]',
            'new_report_id' => $data['new_report_id'],
            'new_placeholder_type_id' => $data['new_placeholder_type_id'],
            'new_questions' => [
                [
                    'new_question_type_id' => 8,
                    'question_en' => 'Financial year to'
                ]
            ]
        ];
        $newData[] = [
            'name' => '[FinYear]',
            'new_report_id' => $data['new_report_id'],
            'new_placeholder_type_id' => $data['new_placeholder_type_id'],
            'new_questions' => [
                [
                    'new_question_type_id' => 1,
                    'question_en' => 'Financial year'
                ]
            ]
        ];
        return $newData;
    }

    protected function __addSignaturePlaceholder($data) {
        $newData[] = [
            'name' => '[FirstSignature]',
            'new_report_id' => $data['new_report_id'],
            'new_placeholder_type_id' => $data['new_placeholder_type_id'],
            'new_questions' => [
                [
                    'new_question_type_id' => 10,
                    'question_en' => 'First Signature',
                ]
            ]
        ];
        $newData[] = [
            'name' => '[SecondSignature]',
            'new_report_id' => $data['new_report_id'],
            'new_placeholder_type_id' => $data['new_placeholder_type_id'],
            'new_questions' => [
                [
                    'new_question_type_id' => 10,
                    'question_en' => 'Second Signature'
                ]
            ]
        ];
        if(!empty($data['auditor_in_charge']) && $data['auditor_in_charge'] == 'on') {
            $firstIncharge = [
                'new_question_type_id' => 4,
                'question_en'   => 'Auditor Incharge',
                'is_parent' => 0,
                'new_placeholder'   => [
                    'new_report_id' => $data['new_report_id'],
                    'new_placeholder_type_id'   => 3,
                    'name'  => '[FirstInCharge]'
                ]
            ];
            $newData[0]['new_questions'][0]['child_questions'][0] = $firstIncharge;

            $secondIncharge = [
                'new_question_type_id' => 4,
                'question_en'   => 'Auditor Incharge',
                'is_parent' => 0,
                'new_placeholder'   => [
                    'new_report_id' => $data['new_report_id'],
                    'new_placeholder_type_id'   => 3,
                    'name'  => '[SecondInCharge]'
                ]
            ];
            $newData[1]['new_questions'][0]['child_questions'][0] = $secondIncharge;
        }
        return $newData;
    }
}
