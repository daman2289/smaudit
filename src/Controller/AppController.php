<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Utility\Hash;
use Cake\Mailer\Email;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Routing\Router;
use Cake\I18n\I18n;
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $userController = 'new-reports';
        $userAction = 'index';
        $logoutController = 'Users';
        $logoutAction = 'login';
        $prefix = null;
        if ($this->request->prefix == 'admin') {
            $userController = 'Admins';
            $userAction = 'dashboard';
            $logoutController = 'Admins';
            $logoutAction = 'login';
            $prefix = 'admin';
        }

        $this->loadComponent('TinyAuth.Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email', 'password' => 'password'],
                    'contain' => ['Organizations', 'OrganizationUsers.Organizations']
                ],
                'ADmad/HybridAuth.HybridAuth' => [
                    // All keys shown below are defaults
                    'fields' => [
                        'provider' => 'provider',
                        'openid_identifier' => 'openid_identifier',
                        'email' => 'email'
                    ],

                    'profileModel' => 'ADmad/HybridAuth.SocialProfiles',
                    'profileModelFkField' => 'user_id',

                    'userModel' => 'Users',
                    'hauth_return_to' => null
                ]
            ],
            'loginRedirect' => [
                'controller' => $userController,
                'action' => $userAction,
                'prefix' => $prefix,
                'plugin' => false
            ],
            'logoutRedirect' => [
                'controller' => $logoutController,
                'action' => $logoutAction,
                'prefix' => $prefix
            ],
            'loginAction' => array(
                'controller' => $logoutController,
                'action' => $logoutAction,
                'prefix' => $prefix
            ),
            'unauthorizedRedirect' => false,
            'autoClearCache' => true
        ]);

        if ($this->request->session()->check('Config.language')) {
            I18n::locale($this->request->session()->read('Config.language'));
        } else {
            $this->request->session()->write('Config.language', 'en_GER');
            I18n::locale($this->request->session()->read('Config.language'));
        }
        $this->Auth->allow('activate','forgotPassword');
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        if($this->Auth->user('role_id') && $this->Auth->user('role_id') != 1 ) {
            if(!empty($this->request->params['prefix']) && $this->request->params['prefix'] == 'admin') {
                $this->Auth->logout();
                return $this->redirect(['controller' => 'admins', 'action' => 'login', 'prefix' => 'admin']);
            }
        }
        if(!$this->__checkFreeTrialPeriod()){
            if(!empty($this->request->params['prefix']) && $this->request->params['prefix'] != 'admin'  && !in_array($this->request->params['controller'], ['Homes']) && !in_array($this->request->params['action'], ['dashboard', 'logout']) ) {
                return $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
            }
        }
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    /**
     * Method _setValidationErrorForInvoice to set validation errors for invoice
     *
     * @param $errors array containing the error list
     * @return $str string containg the list of errors as string
     */
    protected function _setValidationError($errors = array()) {
        $allErrors = Hash::flatten($errors);
        $allErrors = array_unique($allErrors);
        $str = null;
        if (!empty($allErrors)) {
            $str = '<ul>';
            foreach ($allErrors as $key => $val):
                $str.= '<li>'.$val.'</li>';
            endforeach;
            $str .= '</ul>';
        }
        return $str;
    }

    /**
     * Method _sendEmailMessage to send email from website
     *
     * @param $to string contain the receiver's email address
     * @param $email_body string the body of the email to send
     * @param $subject string subject of the email
     * @param $attachments array containg the array of attachments if any
     * @return bool
     */
    protected function _sendEmailMessage($to = null, $email_body = null, $subject = null, $attachments = [])
    {
        $email = new Email('default');
        $email->from([Configure::read('EmailFrom') => Configure::read('EmailFromName')])
            ->to($to)
            ->emailFormat('html')
            ->subject($subject);
        if (!empty($attachments)) {
            $email->attachments($attachments);
        }
        if ($email->send($email_body)) {
            return true;
        }
        return false;
    }

    public function checkPermission($user_id = null, $organization_id=null) {
        if($this->Auth->user('role_id') != 2){            
            $permissionTable  = TableRegistry::get('OrganizationUserPermissions'); 
            $permission = $permissionTable->find()
                ->where(['OrganizationUserPermissions.user_id' => $this->Auth->user('id')])
                ->matching('PermissionAreas', function ($q) {
                        return $q->where([
                            'PermissionAreas.controller' => $this->request->params['controller'],
                            'PermissionAreas.action'    => $this->request->params['action']
                        ]
                    );
                });
            if(!$permission->isEmpty()) {
                if($permission->first()->is_allowed) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }
    /*
     *  
     */    
    public function __checkFreeTrialPeriod() {
        if ($this->Auth->user()) {            
            $planTable = TableRegistry::get('SubscriptionPlans');
            $organizationPlan = $planTable->getOrganizationSubscription($this->Auth->user('organization.id'));
            if(empty($organizationPlan)) {
                $organizationCreated = new Time($this->Auth->user('organization.created')); 
                if(!empty($this->Auth->user('organization.trial_start'))) {
                    $organizationCreated = new Time($this->Auth->user('organization.trial_start')); 
                }
                $organizationCreated->modify('+10 days');
                $datetime1 = date_create(date('Y-m-d'));
                $datetime2 = date_create(date('Y-m-d',strtotime($organizationCreated)));
                if ($datetime2 > $datetime1) {
                    $interval = date_diff($datetime1, $datetime2);
                    $notification = "Your free trial is ending within ".$interval->format(' %a days'). '. Please upgrade your plan.';
                    $response = true;//false;
                } else  {
                    $notification = "Your free trial has been expired. Please upgrade your plan to continue service.";
                    $response = false;
                }
                if($this->Auth->user('role_id') != Configure::read('UserRoles.Organization Manager')) {
                    $additionalText = ' Please contact your Manager.';
                    $notification = $notification . $additionalText;
                }else {
                    $link = Router::url(
                        array('controller' => 'subscription-plans'),
                        true
                    );
                    $linkString = "<a href=$link>Buy a plan now</a>";
                    $notification = $notification . $linkString;
                }
                $this->set('notification',$notification);
                return $response;
            }
        }
        //Let Auth handle all permission as user is not login currently
        return true;
    }

    public function __isUnderTrial() {
        $organizationCreated = new Time($this->Auth->user('organization.created')); 
        if(!empty($this->Auth->user('organization.trial_start'))) {
            $organizationCreated = new Time($this->Auth->user('organization.trial_start')); 
        }
        $organizationCreated->modify('+10 days');
        $datetime1 = date_create(date('Y-m-d'));
        $datetime2 = date_create(date('Y-m-d',strtotime($organizationCreated)));
        if ($datetime2 > $datetime1) {
            return true;
        } else  {
            return false;
        }
    }
}
