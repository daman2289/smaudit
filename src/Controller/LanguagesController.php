<?php
namespace App\Controller;

use Cake\Core\Configure;
/*
 * ***********************************************************************************
 * Languages Controller
 * Functionality		 :	All features on front including home page
 * including super administrator,client user,QA type of users,admin type of users
 * ***********************************************************************************
 */

class LanguagesController extends AppController {

    public $name = 'Languages';
    public $uses = array('Specialty');
    public $helpers = array('Html', 'Form', 'Session', 'Js', 'Paginator', 'Common', 'Time');
	
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow('changeLanguage');
    }
	
	public function changeLanguage($lang){
        if(!empty($lang)){
            if($lang == 'ar'){
                $this->request->session()->write('Config.language', 'en_GER');
            } 
            elseif($lang == 'en'){
                $this->request->session()->write('Config.language', 'en_US');
            } 
            //in order to redirect the user to the page from which it was called
            return $this->redirect($this->referer());
        }
    }
 }
