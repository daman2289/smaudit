<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Text;
use Cake\Utility\Hash;
use Cake\Routing\Router;
/**
 * NewReports Controller
 *
 * @property \App\Model\Table\NewReportsTable $NewReports
 *
 * @method \App\Model\Entity\NewReport[] paginate($object = null, array $settings = [])
 */
class NewReportsController extends AppController
{
    public $helpers = ['AkkaCKEditor.CKEditor'];
    public $language = 'content_en';

    public $dateFormatt = 'j F Y';
    public $germanMonth = [
        'January'   =>     'Januar',
        'February'  =>    'Februar',
        'March' =>   'März',
        'April' =>   'April',
        'May'   =>     'Mai',
        'June'  =>    'Juni',
        'July'  =>    'Juli',
        'August'    =>  'August',
        'September' =>   'September',
        'October'   =>     'Oktober',
        'November'  =>    'November',
        'December'  =>    'Dezember'
    ];
    public $frenchMonth = [
        'January'   =>     'Janvier',
        'February'  =>    'Février',
        'March' =>   'Mars',
        'April' =>   'Avril',
        'May'   =>     'Mai',
        'June'  =>    'Juin',
        'July'  =>    'Juillet',
        'August'    =>  'Août',
        'September' =>   'Septembre',
        'October'   =>     'Octobre',
        'November'  =>    'Novembre',
        'December'  =>    'Décembre'
    ];
    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->layout('dashboard');
    }



    function translateMonth($date,$lang) {
        if($lang == 'content_er') {
            foreach ($this->germanMonth as $key => $value) {
                if (strstr($date,  $key))  {               
                    $str = str_replace(
                        array($key),
                        array(
                            $value
                        ),
                        $date
                    );
                    return $str;
                }
            }
        }elseif($lang == 'content_fr') {
            foreach ($this->frenchMonth as $key => $value) {
                if (strstr($date,  $key))  {               
                    $str = str_replace(
                        array($key),
                        array(
                            $value
                        ),
                        $date
                    );
                    return $str;
                }
            }
        }
        return $date;
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $reports = $this->NewReports->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])
        ->where(['is_published' => 1]);
        $this->loadModel('NewFinalReports');
        $whereCondition = [
            'NewFinalReports.user_id' => $this->Auth->user('id')
        ];
        if (!empty($this->request->query['search']) ) {
            $search = [
                'or' => [
                    'NewFinalReports.company LIKE ' => '%'. $this->request->query['search'] .'%',   
                ]
            ];
            $whereCondition = array_merge($search, $whereCondition);
        }

        $savedReports = $this->NewFinalReports->find()
            ->where($whereCondition)
            ->contain(['NewReports']);
        $this->paginate = [
            'limit' => 20,
            'order' => [
                'NewFinalReports.created' => 'desc'
            ]
        ];
        $savedReports = $this->paginate($savedReports);
        $this->set(compact('reports', 'savedReports'));
        $this->set('_serialize', ['reports', 'savedReports']);
    }

    /**
     * View method
     *
     * @param string|null $id New Report id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $newReport = $this->NewReports->get($id, [
            'contain' => ['NewPlaceholders', 'NewReportPlaceholders']
        ]);

        $this->set('newReport', $newReport);
        $this->set('_serialize', ['newReport']);
    }

    protected function __addReportPlaceholders($data) {
        $res = [];
        foreach ($data as $key => $value) {
            $res[] = ['name' => $value, 'uid' => '111'];
        }
        return $res;
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $newReport = $this->NewReports->newEntity();
        if ($this->request->is('post')) {
            $text = $this->request->data['content_en'];
            preg_match_all('#\[(.*?)\]#', $text, $match);
            $this->request->data['new_placeholders'] = $this->__addReportPlaceholders($match[0]);
            $this->request->data['uid'] = Text::uuid();
            $newReport = $this->NewReports->patchEntity($newReport, $this->request->getData());
            if ($this->NewReports->save($newReport)) {
                $this->Flash->success(__('The new report has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The new report could not be saved. Please, try again.'));
        }
        $this->set(compact('newReport'));
        $this->set('_serialize', ['newReport']);
    }

    /**
     * Edit method
     *
     * @param string|null $id New Report id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $newReport = $this->NewReports->get($id, [
            'contain' => ['NewPlaceholders']
        ]);
        // pr($newReport->toArray());die;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $text = $this->request->data['content_en'];
            preg_match_all('#\[(.*?)\]#', $text, $match);
            $this->request->data['new_placeholders'] = $this->__addReportPlaceholders($match[0]);

            $this->loadModel('NewPlaceholders');
            $placeholders = $this->NewPlaceholders->find('list')
                ->where(['NewPlaceholders.new_report_id' => $newReport->id]);

            $p = Hash::extract($this->request->data['new_placeholders'], '{n}.name');           

            // $this->deletePlacholder($placeholders->toArray(), $p);
            $this->request->data['new_placeholders'] = $this->updateRequest($this->request->data['new_placeholders'], $placeholders->toArray());
            if(empty($this->request->data['new_placeholders'])) {
                unset($this->request->data['new_placeholders']);
            }
            // pr($this->request->data);die;
            $newReport = $this->NewReports->get($id, [
                'contain' => ['NewPlaceholders']
            ]);

            $newReport = $this->NewReports->patchEntity($newReport, $this->request->getData());
            $this->loadModel('NewPlaceholders');
            if ($this->NewReports->save($newReport)) {
                $this->Flash->success(__('The new report has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The new report could not be saved. Please, try again.'));
        }
        $this->set(compact('newReport'));
        $this->set('_serialize', ['newReport']);
    }


    public function deletePlacholder($saved, $new) {
        foreach ($saved as $key => $value) {
            $this->loadModel('NewPlaceholders');
            if(!in_array($value, $new)) {
                $newPlaceholder = $this->NewPlaceholders->get($key);
                $this->NewPlaceholders->delete($newPlaceholder);
            }
        }
    }

    public function updateRequest($request, $saved) {
        foreach ($request as $key => $value) {
            if(in_array($value['name'], $saved)) {
                unset($request[$key]);
            }
        }
        return $request;
    }

    /**
     * Delete method
     *
     * @param string|null $id New Report id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $newReport = $this->NewReports->get($id);
        if ($this->NewReports->delete($newReport)) {
            $this->Flash->success(__('The new report has been deleted.'));
        } else {
            $this->Flash->error(__('The new report could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function report($id) {

        $newReport = $this->NewReports->get($id);
        // pr($newReport);die;
        $placeholders = [];
        // getting the last answer to get the report uuid
        $this->loadModel('Answers');
        $lastAnswer = $this->Answers->find()
            ->where(['Answers.organization_user_id' => $this->Auth->user('id')])
            ->order(['id' => 'DESC'])
            ->first();
        // get system placeholder
        $systemPlaceholders = $this->__getValueOfSystemPlacholders($id);
        $placeholders = array_merge($placeholders, $systemPlaceholders);
        // getting normal placeholder
        $normalPlaceholder = $this->__getValueOfNormalPlacehoders($id);
        $placeholders = array_merge($placeholders, $normalPlaceholder);

        // getting text only placeholder
        $textOnlyPlaceholder = $this->__getValueOfTextOnlyPlacehoders($id);
        $placeholders = array_merge($placeholders, $textOnlyPlaceholder);
        
        // getting licence only placeholder
        $licencePlaceholder = $this->__getValueOfLicencePlacehoders($id, $lastAnswer->report_uuid);
        $placeholders = array_merge($placeholders, $licencePlaceholder);

        // getting Legal form type placeholder
        $LegaFormPlaceholder = $this->__getLegaFormTypePlacehoders($id, $lastAnswer->report_uuid);
        $placeholders = array_merge($placeholders, $LegaFormPlaceholder);
        // getting Legal form type placeholder
        $signatureGenderDependentPlaceholder = $this->__getSignatureDependentPlacehoders($id, $lastAnswer->report_uuid);
        $placeholders = array_merge($placeholders, $signatureGenderDependentPlaceholder);

        $answers    = $this->Answers->find()
            ->where([
                'Answers.organization_user_id' => $this->Auth->user('id'),
                'Answers.report_uuid'   => $lastAnswer->report_uuid
            ])
            ->innerJoinWith('NewPlaceholders', function($q) use ($id) {
                return $q->where(['NewPlaceholders.new_report_id' => $id]);
            })
            ->innerJoinWith('NewQuestions', function($q) use ($id) {
                return $q->where(['NewQuestions.new_question_type_id <> ' => 3]);
            })
            ->contain([
                'NewPlaceholders', 
                'NewQuestions',
                'NewQuestionOptions'
            ])
            ->order(['Answers.created' => 'ASC']);

        $multiselectanswers    = $this->Answers->find()
            ->where([
                'Answers.organization_user_id' => $this->Auth->user('id'),
                'Answers.report_uuid'   => $lastAnswer->report_uuid
            ])
            ->innerJoinWith('NewPlaceholders', function($q) use ($id) {
                return $q->where(['NewPlaceholders.new_report_id' => $id]);
            })
            ->innerJoinWith('NewQuestions', function($q) {
                return $q->where(['NewQuestions.new_question_type_id' => 3]);
            })
            ->contain([
                'NewPlaceholders', 
                'NewQuestions',
                'NewQuestionOptions'
            ])
            ->order(['Answers.created' => 'ASC']);

        $multiselectanswers = $this->getValueOfMultiseletAnswer($multiselectanswers);
        $placeholders   = array_merge($placeholders, $multiselectanswers);
        // getting placeholders of questions
        $answers    = $this->__getValueOfPlaceholders($answers);
        $placeholders   = array_merge($placeholders, $answers);
        // getting cc placeholder
        $ccPlaceholder = $this->__getValueOfCcPlaceholder($id);
        $placeholders = array_merge($placeholders, $ccPlaceholder);
        
        $keys   = array_keys($placeholders);
        $values     = array_values($placeholders);
        // getting body with replaced placeholder value
        $report     = $str  = str_replace(
            $keys,                
            $values,
            $newReport[$this->language]
        );
        $report     = $this->__recursivelyReplacePlaceholders($report, $placeholders);
        $report = str_replace(['{{LOGO}}'], [$this->getOrganizatonLogo()], $report);
        // Getting header with replaced placeholder value
        $header     = str_replace(
            $keys,                
            $values,
            $newReport['header']
        );
        $header     = $this->__recursivelyReplacePlaceholders($header, $placeholders);
        $header     = str_replace(['{{LOGO}}'], [$this->getOrganizatonLogo()], $header);
        // gettinf footer with replace value
        $footer     = str_replace(
            $keys,                
            $values,
            $newReport['footer']
        );
        $footer     = $this->__recursivelyReplacePlaceholders($footer, $placeholders);
        $footer = str_replace(['{{LOGO}}'], [$this->getOrganizatonLogo()], $footer);

        return [
            'header'    => $header,
            'footer'    => $footer,
            'report'    => $report ,
            'company'   => (!empty($ccPlaceholder) && $ccPlaceholder['[CCName]'])
                ? $ccPlaceholder['[CCName]']
                : ''
        ];
    }

    protected function __getIncharegeReplacement ($data) {
        $this->loadModel('OrganizationUsers');
        $user = $this->OrganizationUsers->getOrganizationUsersForGender($this->Auth->user('organization.id'));
        $user  = $user->toArray();
        $placeholder = $this->NewPlaceholders->find('list', [
                'keyField' => 'id',
                'valueField'    => 'id'
            ])
            ->where([
                'new_report_id' => $data->new_placeholder->new_report_id,
                'name IN' => ['[FirstSignature]', '[SecondSignature]']
            ])
            ->contain([
                'NewQuestions'
            ]);
        if(!empty($placeholder->toArray()) ) {
            // getting the answer from the paceholder
            $answer = $this->Answers->find()
                ->where([
                    'new_placeholder_id IN'    => $placeholder->toArray(),
                    'organization_user_id'  => $this->Auth->user('id'),
                    'report_uuid' => $data->report_uuid
                ])
                ->contain(['NewPlaceholders']);
            $signatures = [];
            if(!empty($answer->toArray())) {
                $this->loadModel('OrganizationUsers');
                foreach ($answer as $key => $value) {                   
                    $signatures[$value->new_placeholder->name] = $user[$value->answer];
                }
            }
        }

        $replementText = '';
        // pr($signatures);die;
        // return empty answer that is incharge is not checked return empty
        if(in_array($data->new_placeholder->name, ['[FirstInCharge]', '[SecondInCharge]']) && empty($data->answer)) {
            $replementText = '';
        // if langauge is german check the gender of corresponding Signature([[FirstInCharge] => '[FirstSignature]'])
        }else if($this->language== 'content_en') {
            $replementText = 'Auditor in Charge';
        }else if($this->language== 'content_fr') {
            $replementText = 'Réviseur responsable';                
        } else if($this->language== 'content_er') {
            if($data->new_placeholder->name == '[FirstInCharge]' && !empty($signatures['[FirstSignature]'])) {
                $replementText = ($signatures['[FirstSignature]'] == 1) ? 'Leitender Revisor' : 'Leitende Revisorin' ;
            } elseif ($data->new_placeholder->name == '[SecondInCharge]' && !empty($signatures['[SecondSignature]'])) {
                $replementText = ($signatures['[SecondSignature]'] == 1) ? 'Leitender Revisor' : 'Leitende Revisorin' ;
            }
        }
        return $replementText;
    }

    protected function __getValueOfPlaceholders($data) {
        $placeholders = [];
        // pr($data->toArray());die;
        foreach ($data as $key => $value) {
            if($value->has('new_question_option')) {
                if(!empty($value->new_placeholder)) {
                    $placeholders[$value->new_placeholder->name] = $value->new_question_option[$this->language];
                }
            }
            if(empty($value->new_question_option_id) && $value->new_question->new_question_type_id == 4) {
                // pr($value);
                $placeholders[$value->new_placeholder->name] = $this->__getIncharegeReplacement($value);
            }elseif(empty($value->new_question_option_id) && $value->new_question->new_question_type_id == 8) {
                // pr($value);
                if($this->language == 'content_er') {
                    $this->dateFormatt = 'j. F Y';
                }else {
                    $this->dateFormatt = 'j F Y';
                }
                
                $placeholders[$value->new_placeholder->name] = $this->translateMonth(date($this->dateFormatt, strtotime(str_replace('/', '-', $value->answer))), $this->language);
            }else if(!empty($value->answer)) { 
                if(!empty($value->new_placeholder)) {
                    $placeholders[$value->new_placeholder->name] = $value->answer;
                }              
            }        
        }
        // pr($placeholders);die;
        return $placeholders;
    }

    function getValueOfMultiseletAnswer($data) {
        $placeholders = [];
        foreach ($data as $key => $value) {
            if(!empty($value->answer) && !empty($value->new_placeholder)) { 
                $options = explode(',', $value->answer);
                $this->loadModel('NewQuestionOptions');
                $options = $this->NewQuestionOptions->find('list', [
                    'valueField' => $this->language
                ])
                ->where(['id IN' => $options])
                ->toArray();

                $options = array_filter($options);
                if(!empty($options)) {
                    $placeholders[$value->new_placeholder->name] = implode('<br/><br/>', $options);
                }
            }        
        }
        return $placeholders;
    }

    protected function __getValueOfCcPlaceholder($id) {
        $this->loadModel('Answers');
        $this->loadModel('NewPlaceholders');
        $placeholders = [];
        $placeholder = $this->NewPlaceholders->find()
            ->where([
                'new_report_id' => $id,
                'name' => '[CCName]'
            ])
            ->contain([
                'NewQuestions'
            ])->first();

        if(!empty($placeholder) && !empty($placeholder->new_questions)) {
            $answer = $this->Answers->find()
                ->where([
                    'new_question_id'       => $placeholder->new_questions[0]->id,
                    'new_placeholder_id'    => $placeholder->id,
                    'organization_user_id'  => $this->Auth->user('id')
                ])
                ->order(['created' => 'DESC'])->first();
            if(!empty($answer)) {
                $placeholders = $this->__getCcValues($answer->answer);
            }
        }
        return $placeholders;
    }

    protected function __getCcValues($company_id) {
        $this->loadModel('OrganizationCompanies');
        $company = $this->OrganizationCompanies->find()
            ->where([
                'OrganizationCompanies.id'  => $company_id
            ])
            ->innerJoinWith('LegalFormTypes')
            ->leftJoinWith('AccountingFrameworkTypes')
            ->contain(['LegalFormTypes', 'AccountingFrameworkTypes'])
            ->first();
        
        if(!empty($company)) {
            return [
                '[CCName]' => $company->company_name,
                '[CCAdress]'    => $company->street,
                '[CCAdressNo]'    => $company->street_number,
                '[CCPlace]'    => $company->place,
                '[CCZip]'    => $company->zip,
                '[CCPlaceIncorp]'    => $company->place_of_incorporation,
                '[CCLegalForm]'    => (!empty($company->legal_form_type))
                    ?   $company->legal_form_type->name
                    :   '',
                '[CCAccFrame]'    => (!empty($company->accounting_framework_type))
                    ?   $company->accounting_framework_type->name
                    :   '',
            ];
        }
        return [];
    }

    protected function __getValueOfSystemPlacholders($id) {
        $this->loadModel('NewPlaceholders');
        $this->loadModel('OrganizationUsers');
        $this->loadModel('OrganizationSubsidiaries');
        $placeholders = [];
        // Setting organization detail for replacement
        $data['organization'] = $this->Auth->user('organization');
        // Setting Subsidiary details
        $data['subsidiary'] = $this->__getSubsidiaryDetail();
        // get list of system placeholders
        $data['company'] = $this->__getCompanyDetail();
        // get list of system placeholders
        $systemPlaceholder = $this->NewPlaceholders->find('list', [
                'keyField' => 'name',
                'valueField' => 'content_en'
            ])
            ->where([
                'NewPlaceholders.new_placeholder_type_id' => 1,
                'NewPlaceholders.new_report_id' => $id
            ]);

        foreach ($systemPlaceholder as $key => $value) {
            // getting model and field part from _ seprated string
            $match = explode('_', $value);
            if(in_array($match[0], ['company', 'organization', 'subsidiary'])) {
                $model = $match[0];
            } else {
                $model = 'organization';
            }
            unset($match[0]);
            $field = implode('_', $match);
            // setting placeholder replacement corresponding to their type company, orgni.., subsidi..
            if(!empty($data[$model]) && !empty($data[$model][$field])) {
                $placeholders[$key] = $data[$model][$field];
            } else {
                // if not found set default to organization
                $placeholders[$key] = $this->Auth->user('organization.'.$field);
            }
        }
        return $placeholders;
    }

    protected function __getValueOfNormalPlacehoders($id) {
        $this->loadModel('NewPlaceholders');
        $placeholders = [];
        $normalPlaceholder = $this->NewPlaceholders->find('list', [
            'keyField' => 'name',
            'valueField' => $this->language
        ])
        ->where([
            'NewPlaceholders.new_placeholder_type_id' => 2,
            'NewPlaceholders.new_report_id' => $id
        ]);
        foreach ($normalPlaceholder as $key => $value) {
            $placeholders[$key] = $value;
        }
        return $placeholders;
    }

    protected function __getValueOfTextOnlyPlacehoders($id) {
        $this->loadModel('NewPlaceholders');
        $placeholders = [];
        $textOnlyPlaceholder = $this->NewPlaceholders->find('list', [
            'keyField' => 'name',
            'valueField' => $this->language
        ])
        ->where([
            'NewPlaceholders.new_placeholder_type_id' => 5,
            'NewPlaceholders.new_report_id' => $id
        ]);
        foreach ($textOnlyPlaceholder as $key => $value) {
            $placeholders[$key] = $value;
        }
        return $placeholders;
    }

    protected function __getLegaFormTypePlacehoders($id, $report_uuid) {
        $this->loadModel('NewPlaceholders');
        $placeholders = [];
        // getting company paceholder for report
        $placeholder = $this->NewPlaceholders->find()
            ->where([
                'new_report_id' => $id,
                'name' => '[CCName]'
            ])
            ->contain([
                'NewQuestions'
            ])->first();

        if(!empty($placeholder) ) {
            // getting the company from the Ccname paceholder
            $answer = $this->Answers->find()
                ->where([
                    'new_question_id'       => $placeholder->new_questions[0]->id,
                    'new_placeholder_id'    => $placeholder->id,
                    'organization_user_id'  => $this->Auth->user('id'),
                    'report_uuid' => $report_uuid
                ])
                ->order(['created' => 'DESC'])->first();

            if(!empty($answer)) {
                $company = $this->OrganizationCompanies->find()
                ->where([
                    'OrganizationCompanies.id'  => $answer->answer
                ])
                ->innerJoinWith('LegalFormTypes')
                ->leftJoinWith('AccountingFrameworkTypes')
                ->contain(['LegalFormTypes', 'AccountingFrameworkTypes'])
                ->first();
            }
        }
        if(!empty($company)) {
            $formType = $company->legal_form_type_id;
        }else {
           $formType = 1; 
        }
        // getting all the Legalform dependent placeholder of report.
        $legalFormPlaceholders = $this->NewPlaceholders->find('list', [
            'keyField' => 'name',
            'valueField' => 'id'
        ])
        ->where([
            'NewPlaceholders.new_placeholder_type_id' => 6,
            'NewPlaceholders.new_report_id' => $id
        ]);
        // getting replement of placeholders for a report
        $this->loadModel('LegalFormReplacements');
        // pr($legalFormPlaceholders->toArray());die;
        foreach ($legalFormPlaceholders as $key => $value) {
            $formPlaceholder = $this->LegalFormReplacements->find()
                ->where([
                    'legal_form_type_id'   => $formType,
                    'new_placeholder_id' => $value,
                    'new_report_id' => $id
                ])
                ->first();
            $placeholders[$key] = $formPlaceholder[$this->language];
        }
        // pr($placeholders);die;
        return $placeholders;
    }

    protected function __getSignatureDependentPlacehoders($id, $report_uuid) {
        $this->loadModel('NewPlaceholders');
        $placeholders = [];
        // getting signature paceholder for report
        $placeholder = $this->NewPlaceholders->find('list', [
                'keyField' => 'id',
                'valueField'    => 'id'
            ])
            ->where([
                'new_report_id' => $id,
                'name IN' => ['[FirstSignature]', '[SecondSignature]']
            ])
            ->contain([
                'NewQuestions'
            ]);

        if(!empty($placeholder->toArray()) ) {
            // getting the answer from the paceholder
            $answer = $this->Answers->find()
                ->where([
                    'new_placeholder_id IN'    => $placeholder->toArray(),
                    'organization_user_id'  => $this->Auth->user('id'),
                    'report_uuid' => $report_uuid
                ])
                ->contain(['NewPlaceholders']);
            $signatures = [];
            if(!empty($answer->toArray())) {
                $this->loadModel('OrganizationUsers');
                foreach ($answer as $key => $value) {
                    $user = $this->OrganizationUsers->getOrganizationUsersForGender($this->Auth->user('organization.id'));
                    $user  = $user->toArray();
                    $signatures[$value->new_placeholder->name] = $user[$value->answer];
                }
            }
        }
        // getting all the Legalform dependent placeholder of report.
        $genderDependendPlaceholders = $this->NewPlaceholders->find('list', [
            'keyField' => 'name',
            'valueField' => 'id'
        ])
        ->where([
            'NewPlaceholders.new_placeholder_type_id' => 7,
            'NewPlaceholders.new_report_id' => $id
        ]);
        // getting replement of placeholders for a report
        $this->loadModel('SignatureGenderReplacements');
        // pr($legalFormPlaceholders->toArray());die;
        $formPlaceholder = $this->SignatureGenderReplacements->find('list', [
            'keyField'      => 'new_placeholder_id',
            'valueField'    => $this->language
        ]);
        $is_first = true;
        if(empty($signatures)) {
            return [];
        }
        foreach ($signatures as $key => $value) {
            if($is_first) {
                $formPlaceholder = $formPlaceholder->where([
                    'new_report_id' => $id,
                    'signature'    => $key,
                    'gender'    => $value
                ]);

                $is_first   = false;
            }else {                
                $formPlaceholder = $formPlaceholder->orwhere([
                    'new_report_id' => $id,
                    'signature'    => $key,
                    'gender'    => $value
                ]);
            }
        }

        $formPlaceholder = $formPlaceholder->toArray();
        foreach ($genderDependendPlaceholders as $key => $value) {
            if(!empty($formPlaceholder[$value])) {

                $placeholders[$key] = $formPlaceholder[$value];
            }else{

                $placeholders[$key] = '';
            }
        }
        // pr($placeholders);die;
        return $placeholders;
    }

    protected function __getValueOfLicencePlacehoders($id, $report_uuid) {
        $this->loadModel('NewPlaceholders');
        $placeholders = [];
        // getting signature paceholder for report
        $placeholder = $this->NewPlaceholders->find('list', [
                'keyField' => 'id',
                'valueField'    => 'id'
            ])
            ->where([
                'new_report_id' => $id,
                'name IN' => ['[FirstSignature]', '[SecondSignature]']
            ])
            ->contain([
                'NewQuestions'
            ]);
        if(!empty($placeholder->toArray()) ) {
            // getting the answer from the paceholder
            $answer = $this->Answers->find()
                ->where([
                    'new_placeholder_id IN'    => $placeholder->toArray(),
                    'organization_user_id'  => $this->Auth->user('id'),
                    'report_uuid' => $report_uuid
                ])
                ->contain(['NewPlaceholders']);
            $signatures = [];
            if(!empty($answer->toArray())) {
                $this->loadModel('OrganizationUsers');
                foreach ($answer as $key => $value) {
                    // echo $this->language;die;
                    $user = $this->OrganizationUsers->getOrganizationUsersLicence($this->Auth->user('organization.id'), $this->language);
                    $user  = $user->toArray();
                    $signatures[$value->new_placeholder->name] = $user[$value->answer];
                }
            }
        }

        // getting all the Legalform dependent placeholder of report.
        $licensePlaceholders = $this->NewPlaceholders->find('list', [
            'keyField' => 'name',
            'valueField' => 'id'
        ])
        ->where([
            'NewPlaceholders.new_placeholder_type_id' => 8,
            'NewPlaceholders.new_report_id' => $id
        ]);
        foreach ($licensePlaceholders as $key => $value) {
            if($key == '[FirstLicence]') {
                $placeholders[$key] = (!empty($signatures['[FirstSignature]'])) 
                    ? $signatures['[FirstSignature]']
                    : '';
            }elseif ($key == '[SecondLicence]') {
                $placeholders[$key] = (!empty($signatures['[SecondSignature]'])) 
                    ? $signatures['[SecondSignature]']
                    : '';
            }
        }
        return $placeholders;
    }
    protected function __getSubsidiaryDetail() {
        if($this->Auth->user('organization_user')) {
            $userSubsidiary = $this->OrganizationSubsidiaries->get($this->Auth->user('organization_user.organization_subsidiary_id')); 
            if(!empty($userSubsidiary)){
                return $userSubsidiary->toArray();
            }
            return [];

        }else {
            $organizationDefaultSubsidiary = $this->OrganizationSubsidiaries->find()
                ->where([
                    'OrganizationSubsidiaries.organization_id' => $this->Auth->user('organization.id'),
                    'OrganizationSubsidiaries.is_primary' => 1
                ])
                ->first(); 
            if(!empty($organizationDefaultSubsidiary)) {
                return $organizationDefaultSubsidiary->toArray();
            }
            return [];
        }
    }

    protected function __getCompanyDetail() {
        $this->loadModel('OrganizationCompanies');
        $organizationCompany = $this->OrganizationCompanies->find()
            ->where([
                'OrganizationCompanies.organization_id' => $this->Auth->user('organization.id'),
                'OrganizationCompanies.status' => 1
            ])
            ->first(); 
        if(!empty($organizationCompany)) {
            return $organizationCompany->toArray();
        }
        return [];
    }

    protected function __recursivelyReplacePlaceholders($template, $allPlaceholders) {
        $placeholders = $this->__getTemplatePlacholder($template);
        if(!empty($placeholders)) {
            $template   =   $this->__replacePlaceholders($template, $placeholders, $allPlaceholders);
            $template   =   $this->__recursivelyReplacePlaceholders($template, $allPlaceholders);
            return $template;
        }
        return $template;
    }

    protected function __getTemplatePlacholder($temp) {
        preg_match_all('#\[(.*?)\]#', $temp, $match);
        return $match[0];
    }

    protected function __replacePlaceholders($template, $placeholders, $allPlaceholders) {
        foreach ($placeholders as $key => $value) {
            $template = str_replace(
                $value,                
               ( !empty($allPlaceholders[$value])) ? $allPlaceholders[$value] : '',
                $template
            );
        }
        return $template;         
    }

    public function getOrganizatonLogo() {

        $image = $this->__companylogo(
            'organizations', 
            'logo', 
            $this->Auth->user('organization.logo_dir'), 
            'square_', 
            $this->Auth->user('organization.logo')
        );
        $image = Router::url($image,true);
        //$imageTag = '<img src="'.$image.'" class="logo" />';
        $imageTag = '<img src="'.$image.'" class="logo" style="height:50px; width:200px;" />';
        return $imageTag;
    }

    protected function __companylogo ($table, $field, $dir, $prefix, $image) {
        $filePath = 'logo' . DS . $table . DS . $field . DS . $dir . DS . $prefix . $image;
        if (file_exists ( WWW_ROOT.$filePath )) {
            return DS . $filePath;
        } else {
            return DS . 'img' . DS . 'logo.png';
        }
    } 

     /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function saveAnswers($reportId)
    {   $uuid = Text::uuid();
        if ($this->request->is('post')) {
            // pr($this->request->data);die;
            $this->loadModel('Answers');
            $this->loadModel('NewFinalReports');
            $users = Hash::insert($this->request->data, '{n}.organization_user_id', $this->Auth->user('id'));
            $users = Hash::insert($users, '{n}.report_uuid', $uuid);

            $newQuestion = $this->Answers->newEntity();
            $newQuestion = $this->Answers->patchEntities($newQuestion, $users);
            $this->Answers->saveMany($newQuestion);

            $report = $this->report(base64_decode($reportId));
            $data['uuid'] =   $uuid;
            $data['header'] =   $report['header'];
            $data['footer'] =   $report['footer'];
            $data['body_en'] = $report['report'];
            $data['company'] = $report['company'];
            $data['user_id']    = $this->Auth->user('id');
            $data['new_report_id']    = base64_decode($reportId);

            // Get German content
            $this->language = 'content_er';
            $report = $this->report(base64_decode($reportId));
            $data['body_er'] = $report['report'];

            // Get French content
            $this->language = 'content_fr';
            $report = $this->report(base64_decode($reportId));
            $data['body_fr'] = $report['report'];

            $this->NewFinalReports->saveFinalReport($data);
            $this->Flash->success(__('Report generated successfully.'));
            return $this->redirect(['controller' => 'new-reports', 'action' => 'index']);
        }
    }

    protected function __updateAnswer($data, $reportUuid) {
        $this->loadModel('Answers');
        $changedAnswers = [];
       
        foreach ($data as $key => $value) {            
            $newQuestion = $this->Answers->find()
            ->where(['Answers.id' => $value['id']])
            ->first();
            if(empty($newQuestion) ) {
                $value['organization_user_id'] =  $this->Auth->user('id');
                $value['report_uuid'] =  $reportUuid;
                $newQuestion = $this->Answers->newEntity();
            }
            $newQuestion = $this->Answers->patchEntity($newQuestion, $value);
            $this->Answers->save($newQuestion);
            // if company data found update final report
            if(!empty($value['company'])) {
                $this->loadModel('NewFinalReports');
                $this->NewFinalReports->updateAll(
                    [  // fields
                        'company' => $value['answer'],
                    ],
                    [  // conditions
                        'uuid' => $reportUuid
                    ]
                );
            }
            $changedAnswers[] = $value['id'];
        }
        $this->Answers->deleteAll([
            'id NOT IN'     =>  $changedAnswers,
            'report_uuid'   => $reportUuid 
        ]);
    }

    public function editAnswer($reportId, $final_report_uuid) {
        $this->loadModel('Answers');
        $this->loadModel('OrganizationUsers');
        $this->loadModel('OrganizationCompanies');
        $this->loadModel('NewQuestions');
        $this->loadModel('NewFinalReports');

        $finalReport = $this->NewFinalReports->find()
            ->where(['NewFinalReports.uuid'   =>  $final_report_uuid])
            ->first();
        if($this->request->is('post')) {
            // echo $final_report_uuid;
            $this->__updateAnswer($this->request->getData(), $final_report_uuid);
            $data = $this->__generateReport($reportId);
            $finalReport = $this->NewFinalReports->patchEntity($finalReport, $data);
            $this->NewFinalReports->save($finalReport);
            $this->Flash->success(__('Report updated successfully.'));
            return $this->redirect(['controller' => 'new-reports', 'action' => 'index']);
        }
        $this->NewQuestions->ChildQuestions->hasOne('ChildAnswers', [
            'className'     => 'Answers',
            'foreignKey'    => 'new_question_id',
            'conditions'    => ['ChildAnswers.report_uuid' => $final_report_uuid] 
        ]);

        // $this->NewQuestions->ChildQuestions->ChildAnswers->hasOne('ChildAnswers', [
        //     'className'     => 'Answers',
        //     'foreignKey'    => 'new_question_id',
        //     'conditions'    => ['ChildAnswers.report_uuid' => $final_report_uuid] 
        // ]);

        $this->NewQuestions->ChildQuestions->belongsToMany('SecondChildQuestions', [
            'className' => 'NewQuestions',
            'through' => 'NewQuestionsQuestions',
            'foreignKey' => 'parent_question_id',
            'targetForeignKey'  => 'child_question_id',
            'saveStrategy' => 'append'
        ]);
        $this->NewQuestions->ChildQuestions->SecondChildQuestions->hasOne('SecondChildAnswers', [
            'className'     => 'Answers',
            'foreignKey'    => 'new_question_id',
            'conditions'    => ['SecondChildAnswers.report_uuid' => $final_report_uuid] 
        ]);
        

        $answers = $this->Answers->find()
            ->where(['Answers.report_uuid' => $final_report_uuid])
            ->innerJoinWith('NewQuestions' , function ($q) {
                return $q->where(['NewQuestions.is_parent' => 1]);
            })
            ->contain([
                'NewQuestions' => [
                    'ChildQuestions' => [
                        'NewQuestionTypes', 'NewQuestionOptions', 'ChildAnswers' => 'NewQuestionOptions',
                        'SecondChildQuestions' => [
                            'NewQuestionTypes', 
                            'NewQuestionOptions',
                            'SecondChildAnswers'
                        ] 
                    ], 
                    'NewQuestionTypes', 'NewQuestionOptions' 
                ],
                'NewQuestionOptions'
            ])
            ->order(['NewQuestions.sort_order']);
        // pr($answers->toArray());die;
        $usersList = $this->OrganizationUsers->find('list', [
            'keyField' => 'user.full_name',
            'valueField' => 'user.full_name'
        ])
        ->where([
            'OrganizationUsers.organization_id' => $this->Auth->user('organization.id'),
            'Users.role_id' => 3
        ])
        ->contain(['Users']);
        $companies = $this->OrganizationCompanies->find('list', [
            'keyField'  => 'id',
            'valueField'    => 'company_name'
        ])
        ->where([
            'OrganizationCompanies.organization_id' => $this->Auth->user('organization.id'),
            'OrganizationCompanies.status'  => 1
        ]);
        $this->set(compact('answers', 'usersList', 'reportId', 'finalReport', 'companies'));
    }

    protected function __generateReport($reportId) {

        $data = [];
        $report = $this->report(base64_decode($reportId));
        $data['header'] =   $report['header'];
        $data['footer'] =   $report['footer'];
        $data['body_en'] = $report['report'];
        $data['company'] = $report['company'];
        $data['user_id']    = $this->Auth->user('id');
        $data['new_report_id']    = base64_decode($reportId);

        // Get German content
        $this->language = 'content_er';
        $report = $this->report(base64_decode($reportId));
        $data['body_er'] = $report['report'];

        // Get French content
        $this->language = 'content_fr';
        $report = $this->report(base64_decode($reportId));
        $data['body_fr'] = $report['report'];
        return $data;
    } 

    public function publish($id) {
        $report = $this->NewReports->find()
            ->where(['id'   => base64_decode($id)])
            ->first();
        if(empty($report)) {
            $this->Flash->error(__('Report not found.'));
            return $this->redirect($this->referer());
        }
        $report->is_published = 1;
        $this->NewReports->save($report);
        $this->Flash->success(__('Report is publised now.'));
        return $this->redirect($this->referer());
    }
}
