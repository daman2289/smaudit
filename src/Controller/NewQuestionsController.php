<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Hash;

/**
 * NewQuestions Controller
 *
 * @property \App\Model\Table\NewQuestionsTable $NewQuestions
 *
 * @method \App\Model\Entity\NewQuestion[] paginate($object = null, array $settings = [])
 */
class NewQuestionsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->layout('dashboard');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index($reportId)
    {
        $this->loadModel('NewPlaceholders');
        $this->loadModel('OrganizationUsers');
        $this->loadModel('OrganizationCompanies');

        $this->NewQuestions->ChildQuestions->belongsToMany('SecondChildQuestions', [
            'className' => 'NewQuestions',
            'through' => 'NewQuestionsQuestions',
            'foreignKey' => 'parent_question_id',
            'targetForeignKey'  => 'child_question_id',
            'saveStrategy' => 'append'
        ]);

        $placholders = $this->NewPlaceholders->find('list', [
            'keyField' => 'id',
            'valueField' => 'id'
        ])
        ->where([
            'NewPlaceholders.new_report_id' => base64_decode($reportId) , 
            'NewPlaceholders.new_placeholder_type_id'   => 3
        ]);
        $questions = $this->NewQuestions->find()
            ->where([
                'NewQuestions.new_placeholder_id IN' => $placholders,
                'NewQuestions.is_parent'    => 1
            ])
            ->contain([
                'NewQuestionOptions',
                'NewQuestionTypes',
                'ChildQuestions' => [
                    'NewQuestionTypes', 
                    'NewQuestionOptions',
                    'sort' => ['ChildQuestions.sort_order' => 'ASC'],
                    'SecondChildQuestions' => [
                        'NewQuestionTypes', 
                        'NewQuestionOptions',
                        'sort' => ['SecondChildQuestions.sort_order' => 'ASC']
                    ]
                ]
            ])
            ->order(['sort_order'   => 'ASC']);
        // pr($questions->toArray());die;
        $usersList = $this->OrganizationUsers->find('list', [
            'keyField' => 'user.full_name',
            'valueField' => 'user.full_name'
        ])
        ->where([
            'OrganizationUsers.organization_id' => $this->Auth->user('organization.id'),
            'Users.role_id' => 3
        ])
        ->contain(['Users']);

        if(!empty($questions) && $questions->isEmpty()) {
            $this->Flash->error(__('Report category is not ready for creating report please wait.'));
            return $this->redirect($this->referer());
        }
        $companies = $this->OrganizationCompanies->find('list', [
                'keyField'  => 'id',
                'valueField'    => 'company_name'
            ])
            ->where([
                'OrganizationCompanies.organization_id' => $this->Auth->user('organization.id'),
                'OrganizationCompanies.status'  => 1
            ]);
        $this->set(compact('questions', 'reportId', 'usersList', 'companies'));
        $this->set('_serialize', ['questions', 'reportId', 'usersList', 'companies']);
    }

    /**
     * View method
     *
     * @param string|null $id New Question id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $newQuestion = $this->NewQuestions->get($id, [
            'contain' => ['NewPlaceholders', 'NewQuestionTypes', 'NewQuestionOptions']
        ]);

        $this->set('newQuestion', $newQuestion);
        $this->set('_serialize', ['newQuestion']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $users = Hash::insert($this->request->data, '{n}.organization_user_id', $this->Auth->user('id'));
            $this->loadModel('Answers');
            $newQuestion = $this->Answers->newEntity();
            $newQuestion = $this->Answers->patchEntities($newQuestion, $users);
            foreach ($newQuestion as $entity) {
                if ($this->Answers->save($entity)) {
                }
            }
            $this->Flash->success(__('Report generated successfully.'));
            return $this->redirect(['controller' => 'new-reports', 'action' => 'index']);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id New Question id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $newQuestion = $this->NewQuestions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $newQuestion = $this->NewQuestions->patchEntity($newQuestion, $this->request->getData());
            if ($this->NewQuestions->save($newQuestion)) {
                $this->Flash->success(__('The new question has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The new question could not be saved. Please, try again.'));
        }
        $newPlaceholders = $this->NewQuestions->NewPlaceholders->find('list', ['limit' => 200]);
        $newQuestionTypes = $this->NewQuestions->NewQuestionTypes->find('list', ['limit' => 200]);
        $this->set(compact('newQuestion', 'newPlaceholders', 'newQuestionTypes'));
        $this->set('_serialize', ['newQuestion']);
    }

    /**
     * Delete method
     *
     * @param string|null $id New Question id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $newQuestion = $this->NewQuestions->get($id);
        if ($this->NewQuestions->delete($newQuestion)) {
            $this->Flash->success(__('The new question has been deleted.'));
        } else {
            $this->Flash->error(__('The new question could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
