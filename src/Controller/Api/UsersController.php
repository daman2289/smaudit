<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

/**
 * Admins Controller
 *
 * @property \App\Model\Table\AdminsTable $Admins
 */
class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow([
                                'updateUserStatus'
                            ]);
    }

    /**
     * Method updateUserStatus to update the status of record under any table
     *
     * @param $id int the id of the record
     * @param string the name of the model
     * @param $statusToChange bool
     * @return void
     */
    public function updateUserStatus($id = null, $model = null ,$statusToChange = null) {
        $modelsTable = TableRegistry::get($model);
        $tableData = $modelsTable->get($id);
        $tableData->is_activated = $statusToChange;
        $responseData = [
            'type' => true,
            'status_to_change' => $statusToChange,
            'message' => __('Some errors occurred. Please try again!')
        ];
        if ($modelsTable->save($tableData)) {
            $responseData = [
                'type' => true,
                'status_to_change' => $statusToChange,
                'message' => __('Status has been updated successfully')
            ];
        }
        
        $this->set([
            'responseData' => $responseData,
            '_serialize' => ['responseData']
        ]);
    }
}