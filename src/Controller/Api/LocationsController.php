<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * Businessuser Controller
 *
 * @property \App\Model\Table\BusinessuserTable $Businessuser
 */
class LocationsController extends AppController
{
    
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow([
                                'index',
                                'view',
                                'states',
                                'cities'
                            ]);
    }

	/**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index(){
        $countriesTable = TableRegistry::get('Countries');
        $countries = $countriesTable->find('list')->toArray();
        $this->set([
            'countries' => $countries,
            '_serialize' => ['countries']
        ]);

    }

    public function view($id) {
        $countriesTable = TableRegistry::get('Countries');
        $country = $countriesTable->get($id);
        $this->set([
            'country' => $country,
            '_serialize' => ['country']
        ]);
    }

    public function states($countryId = null) {
        $statesTable = TableRegistry::get('States');
        $states = $statesTable->find('list',['conditions' => ['States.country_id'=> $countryId]])->toArray();
        $this->set([
            'states' => $states,
            '_serialize' => ['states']
        ]);
    }

    public function cities($stateId = null) {
        $citiesTable = TableRegistry::get('Cities');
        $cities = $citiesTable->find('list',['conditions' => ['Cities.state_id'=> $stateId]])->toArray();
        $this->set([
            'cities' => $cities,
            '_serialize' => ['cities']
        ]);
    }

}