<?php
namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * Admins Controller
 *
 * @property \App\Model\Table\AdminsTable $Admins
 */
class SpaceTypesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow([
                                'index'
                            ]);
    }

    /**
     * Method index to get all space types
     *
     * @return void
     */
    public function index() {
        $spaceTypes = $this->SpaceTypes->find('all')
                                    ->toArray();

        $this->set([
            'spaceTypes' => $spaceTypes,
            '_serialize' => ['spaceTypes']
        ]);
    }

    /**
     * Method view to view a single space type
     *
     * @param $id int the id of the space type
     * @return void
     */
    public function view($id) {
        $spaceType = $this->SpaceTypes->get($id);

        $this->set([
            'spaceType' => $spaceType,
            '_serialize' => ['spaceType']
        ]);
    }
    
}