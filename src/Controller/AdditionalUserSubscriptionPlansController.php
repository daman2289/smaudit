<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * OrganizationAdditionalUserSubscriptionPlans Controller
 *
 * @property \App\Model\Table\OrganizationAdditionalUserSubscriptionPlansTable $OrganizationAdditionalUserSubscriptionPlans
 */
class AdditionalUserSubscriptionPlansController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow('paymentWebhook');
    }
    public function index()
    {
        $this->viewBuilder()->layout('dashboard');
        $this->paginate = [
            'contain' => ['Organizations', 'AdditionalUserSubscriptionPlans']
        ];

        $additionalUserSubscriptionPlans = $this->AdditionalUserSubscriptionPlans->find()
            ->where(['AdditionalUserSubscriptionPlans.is_activated' => 1]);
        $this->loadModel('Organizations');
        $organizationPlan   =  $this->AdditionalUserSubscriptionPlans->getAdditionalPlanByOrganization($this->Auth->user('organization.id'));
        $this->set(compact(['additionalUserSubscriptionPlans', 'organizationPlan']));
        $this->set('_serialize', ['additionalUserSubscriptionPlans']);
    }

    protected function _stripeError($e){
        $body = $e->getJsonBody();
        $this->Flash->error($body['error']['message']);
        return false;
    }

     // save deatil in user package in user packages table
    protected function saveOrganizationPlan($data, $organization_id) {
        //var_dump($webhook); die;
        $this->loadModel('OrganizationAdditionalUserSubscriptionPlans');
        // get user  trail package
        $organizationSubscriptionPlan = $this->OrganizationAdditionalUserSubscriptionPlans->newEntity();
        $userPack = $this->OrganizationAdditionalUserSubscriptionPlans->getOrganizationPlan($organization_id);
        if (!empty($userPack)){
            $organizationSubscriptionPlan = $this->OrganizationAdditionalUserSubscriptionPlans->get($userPack['id']);
        }

        $userPackeges = [
            'organization_id' => $organization_id,
            'stripe_customer_id' => $data['stripe_customer_id'],
            'stripe_subscription_id' => $data['stripe_subscription_id'],
            'additional_user_subscription_plan_id' => $data['subscription_plan_id'],
            'expire_date' => $data['expire_date']
        ];
        
        $userPackeges['organization_payments'][]= [
            'amount' => $data['amount'],
            'message' => $data['message']
        ];

        $organizationSubscriptionPlan = $this->OrganizationAdditionalUserSubscriptionPlans->patchEntity($organizationSubscriptionPlan, $userPackeges, [
                'associated' => ['OrganizationPayments'],
        ]);
        
        if ($this->OrganizationAdditionalUserSubscriptionPlans->save($organizationSubscriptionPlan)) {
            $this->Flash->success(__($data['message']));        
            return $this->redirect(['action' => 'index']);
        }
        $this->Flash->success(__('Something went wrong please try again!'));
    }

    public function subscribeOrganization() {
        $this->loadModel('AdditionalUserSubscriptionPlans');
        \Stripe\Stripe::setApiKey(Configure::read('Stripe.secret_key'));
        \Stripe\Stripe::setApiKey(Configure::read('Stripe.secret_key'));
        try {

            $customer =\Stripe\Customer::create(array(
                "source" => $this->request->data['stripeToken'],
                "plan" => $this->request->data['plan_id'],
                "email" => $this->request->data['stripeEmail']
            ));            
            $customerDetail = $customer->__toArray(true);
            $userId = $this->Auth->User('id');
            
            //Get the planes to which custoer has to be subscribed.
            $subscriptionPlan = $this->AdditionalUserSubscriptionPlans->getOrganizationPlanByStripeId($customerDetail['subscriptions']['data'][0]['plan']['id']);

            $data = [
                'subscription_plan_id' => $subscriptionPlan['id'],
                'expire_date' => new Time($customerDetail['subscriptions']['data'][0]['current_period_end']),
                'amount' => $customerDetail['subscriptions']['data'][0]['plan']['amount'],
                'message' => __('You are successfully subscribe for '.$customerDetail['subscriptions']['data'][0]['plan']['name'].'.'),
                'stripe_customer_id' => $customerDetail['id'],
                'stripe_subscription_id' => $customerDetail['subscriptions']['data'][0]['id'],
            ];

            $this->saveOrganizationPlan($data, $this->Auth->User('organization.id'));

        } catch(\Stripe\Error\Card $e) {
          // Since it's a decline, \Stripe\Error\Card will be caught
          $error = $this->_stripeError($e);
        } catch (\Stripe\Error\RateLimit $e) {
            $error = $this->_stripeError($e);
        } catch (\Stripe\Error\InvalidRequest $e) {
            $error = $this->_stripeError($e);
        } catch (\Stripe\Error\Authentication $e) {
            $error = $this->_stripeError($e);
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $error = $this->_stripeError($e);
        } catch (\Stripe\Error\Base $e) {
          // Display a very generic error to the user, and maybe send
            $error = $this->_stripeError($e);
        } catch (Exception $e) {
          // Something else happened, completely unrelated to Stripe
            $this->_stripeError($e);
        }
        // $this->Flash->success(__('Something went wrong please try again!'));        
        return $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
    }

    public function upgradeSubscription($plan_id) {
        $organizationPlan   =  $this->AdditionalUserSubscriptionPlans->getAdditionalPlanByOrganization($this->Auth->user('organization.id'));
        $this->loadModel('AdditionalUserSubscriptionPlans');
        $plan   = $this->AdditionalUserSubscriptionPlans->find()
            ->where([
                'AdditionalUserSubscriptionPlans.id' => base64_decode($plan_id)
            ])
            ->first();
        if(!empty($organizationPlan) && !empty($plan)) {            
            \Stripe\Stripe::setApiKey(Configure::read('Stripe.secret_key'));
            \Stripe\Stripe::setApiKey(Configure::read('Stripe.secret_key'));
            try {
                $subscription = \Stripe\Subscription::retrieve($organizationPlan->_matchingData['OrganizationAdditionalUserSubscriptionPlans']->stripe_subscription_id);
                $subscription->plan = $plan->stripe_plan_id;
                $subscription = $subscription->save();                
                if(!empty($subscription->__toArray(true))) {
                    $this->loadModel('OrganizationAdditionalUserSubscriptionPlans');
                    $this->OrganizationAdditionalUserSubscriptionPlans->updateOrganizationPlan($organizationPlan->_matchingData['OrganizationAdditionalUserSubscriptionPlans']->id,$plan->id);
                    $this->Flash->success(__('Plan upgraded successfully.'));        
                    return $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
                }else {
                    $this->Flash->error(__('Something went wrong please try again!'));        
                    return $this->redirect(['controller' => 'users', 'action' => 'dashboard']); 
                }
            }catch(\Stripe\Error\Card $e) {
              // Since it's a decline, \Stripe\Error\Card will be caught
              $error = $this->_stripeError($e);
            } catch (\Stripe\Error\RateLimit $e) {
                $error = $this->_stripeError($e);
            } catch (\Stripe\Error\InvalidRequest $e) {
                $error = $this->_stripeError($e);
            } catch (\Stripe\Error\Authentication $e) {
                $error = $this->_stripeError($e);
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed
                $error = $this->_stripeError($e);
            } catch (\Stripe\Error\Base $e) {
              // Display a very generic error to the user, and maybe send
                $error = $this->_stripeError($e);
            } catch (Exception $e) {
              // Something else happened, completely unrelated to Stripe
                $this->_stripeError($e);
            }
        }
        $this->Flash->error(__('Something went wrong please try again!'));        
        return $this->redirect(['controller' => 'users', 'action' => 'dashboard']); 
    }

    public function paymentWebhook() {
        $this->autoRender = false;         
        $body = @file_get_contents('php://input');          
        $event_details = json_decode($body, true);                  
        $type = $event_details['type'];   
        if($type == 'invoice.payment_succeeded') {

        }      
        $customerId = (isset($event_details['data']['object']['customer']) ? $event_details['data']['object']['customer'] : '');
        // $plan_id = 
        $this->response->body(200);
        return $this->response;
    }
}
