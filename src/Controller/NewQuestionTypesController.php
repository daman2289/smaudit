<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * NewQuestionTypes Controller
 *
 * @property \App\Model\Table\NewQuestionTypesTable $NewQuestionTypes
 *
 * @method \App\Model\Entity\NewQuestionType[] paginate($object = null, array $settings = [])
 */
class NewQuestionTypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $newQuestionTypes = $this->paginate($this->NewQuestionTypes);

        $this->set(compact('newQuestionTypes'));
        $this->set('_serialize', ['newQuestionTypes']);
    }

    /**
     * View method
     *
     * @param string|null $id New Question Type id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $newQuestionType = $this->NewQuestionTypes->get($id, [
            'contain' => ['NewQuestions']
        ]);

        $this->set('newQuestionType', $newQuestionType);
        $this->set('_serialize', ['newQuestionType']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $newQuestionType = $this->NewQuestionTypes->newEntity();
        if ($this->request->is('post')) {
            $newQuestionType = $this->NewQuestionTypes->patchEntity($newQuestionType, $this->request->getData());
            if ($this->NewQuestionTypes->save($newQuestionType)) {
                $this->Flash->success(__('The new question type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The new question type could not be saved. Please, try again.'));
        }
        $this->set(compact('newQuestionType'));
        $this->set('_serialize', ['newQuestionType']);
    }

    /**
     * Edit method
     *
     * @param string|null $id New Question Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $newQuestionType = $this->NewQuestionTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $newQuestionType = $this->NewQuestionTypes->patchEntity($newQuestionType, $this->request->getData());
            if ($this->NewQuestionTypes->save($newQuestionType)) {
                $this->Flash->success(__('The new question type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The new question type could not be saved. Please, try again.'));
        }
        $this->set(compact('newQuestionType'));
        $this->set('_serialize', ['newQuestionType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id New Question Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $newQuestionType = $this->NewQuestionTypes->get($id);
        if ($this->NewQuestionTypes->delete($newQuestionType)) {
            $this->Flash->success(__('The new question type has been deleted.'));
        } else {
            $this->Flash->error(__('The new question type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
