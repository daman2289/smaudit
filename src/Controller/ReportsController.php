<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Text;
use App\Event\UserAddListener;
use Cake\Event\EventManager;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\NotFoundException;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class ReportsController extends AppController
{
	public $components = array('Paginator', 'ReportPlaceholder');
    public $helpers = ['AkkaCKEditor.CKEditor'];
    public $paginate = [
        // Other keys here.
        'maxLimit' => 10
    ];
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    private function preCondition() {
        $this->loadModel('OrganizationCompanies');
        $this->loadModel('OrganizationSubsidiaries');
        $organizationCompany = $this->OrganizationCompanies->find()
            ->where(['OrganizationCompanies.organization_id' => $this->Auth->user('organization.id')])
            ->count();
        $organizationSubsidiary = $this->OrganizationSubsidiaries->find()
            ->where(['OrganizationSubsidiaries.organization_id' => $this->Auth->user('organization.id')])
            ->count();

        if(!$organizationCompany || !$organizationSubsidiary){
            $this->Flash->error(__('To add reports please add at least one company and a subsidiary. Please add a company and subsidiary (Settings) first.'));
            return $this->redirect($this->referer());
        }
    }

	public function index() {
        if(!$this->__checkFreeTrialPeriod()){            
            return $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
        }
		$this->viewBuilder()->layout('dashboard');
		$this->loadModel('reports');
		$this->loadModel('OpinionTypes');
		$this->loadModel('EmphasisOfMatters');
        $this->preCondition();        
		$emphasis = $this->EmphasisOfMatters->getEmphasisOfMatter($this->request->session()->read('Config.language'));
        $opinion = $this->OpinionTypes->getOpinionList($this->request->session()->read('Config.language'));
        $OrganizationUsers = TableRegistry::get('OrganizationUsers');
        $usersList = $OrganizationUsers->find('list', [
            'keyField' => 'user.id',
            'valueField' => 'user.full_name'
        ])
        ->where([
            'OrganizationUsers.organization_id' => $this->Auth->user('organization.id'),
    		'Users.role_id' => 3
        ])
        ->contain(['Users']);

		$this->loadModel('AccountingFrameworkTypes');
		$accounting = $this->AccountingFrameworkTypes->find('list');
		$this->loadModel('ModifiedOpinions');
		$modified = $this->ModifiedOpinions->getModifiedOpinion($this->request->session()->read('Config.language'));
        $this->loadModel('ReportReferences');
		$reference = $this->ReportReferences->getReference($this->request->session()->read('Config.language'));
		$report = $this->Reports->newEntity();
        $error = false;
        if ($this->request->is('post')) {
            $this->request->data['report_type_id'] = 1;
            $this->request->data['user_id'] = $this->Auth->user('id');

            if(!empty($this->request->data['report_date'])) { 	
            $this->request->data['report_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $this->request->data['report_date'])));
            }
            if(!empty($this->request->data['financial_year_from'])) {
            $this->request->data['financial_year_from'] = date('Y-m-d',strtotime(str_replace('/', '-', $this->request->data['financial_year_from'])));
            }
            if(!empty($this->request->data['financial_year_to'])) {
            $this->request->data['financial_year_to'] = date('Y-m-d',strtotime(str_replace('/', '-', $this->request->data['financial_year_to'])));
            }
            if(!empty($this->request->data['date_prior_audit_report'])) {
            $this->request->data['date_prior_audit_report'] = date('Y-m-d',strtotime(str_replace('/', '-', $this->request->data['date_prior_audit_report'])));
            }
            if(!empty($this->request->data['audit_terminated_date'])) {    	
            	$this->request->data['audit_terminated_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $this->request->data['audit_terminated_date'])));
            }
            if(!empty($this->request->data['emphasis_of_matter_ids']) && is_array($this->request->data['emphasis_of_matter_ids'])) {
            	$this->request->data['emphasis_of_matter_ids'] = implode(',', $this->request->data['emphasis_of_matter_ids']);
            }
             if(!empty($this->request->data['reference_ids']) && is_array($this->request->data['reference_ids'])) {
                $this->request->data['reference_ids'] = implode(',', $this->request->data['reference_ids']);
            }
            $report = $this->Reports->patchEntity($report, $this->request->data);
            if ($this->Reports->save($report)) {
                 $this->Flash->success(__('Report has been created'));
                 $this->redirect(array('action' => 'report-listing'));
            } else {
                $error = true;
                $errors = $this->_setValidationError($report->errors());
                $this->Flash->error(__('Your account could not be created due to the below errors. Please, try again.' . $errors));
            }
        }
        $this->set(compact('opinion','accounting','emphasis','modified','users','report','error', 'usersList','reference'));
        $this->set('_serialize', ['report']);
	}

	public function getCompanies() {
        if($this->request->is('ajax')) {
            $companyTable = TableRegistry::get('OrganizationCompanies');
            $companyData = $companyTable->find()->where([
                'OrganizationCompanies.organization_id' => $this->Auth->user('organization.id'),
                'OrganizationCompanies.company_name COLLATE UTF8_GENERAL_CI LIKE' => '%'.$this->request->query['term'].'%'
            ]);
            $tagsList = [];
            foreach ($companyData as $key => $data) {
                $tagsList[$key]['id'] = $data->id;
                $tagsList[$key]['value'] = $data->company_name;
                $tagsList[$key]['label'] = $data->company_name;
            }
            echo json_encode($tagsList);die;
        }
    }

    public function reportListing() {
        if(!$this->__checkFreeTrialPeriod()){            
            return $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
        }
    	$this->viewBuilder()->layout('dashboard');
    	$reportTable = TableRegistry::get('Reports');
        $whereCondition = $this->makeSearchCondition();
        try {
            $this->paginate ['order']=['Reports.created' => 'DESC'];
        	$reports = $reportTable->find()
        		->where($whereCondition)
        		->contain([
        			'OrganizationCompanies',
        			'AccountingFrameworkTypes',
        			'ReportTypes'
        		]);
        	$reports = $this->paginate($reports);
        } catch (NotFoundException $e) { 
            $this->request->query['page'] = $this->request->query['page'] -1;
            return $this->redirect([
                    'controller' => $this->request->params['controller'],
                    'action' => $this->request->params['action'],
                    '?' => $this->request->query
                ]
            );
        }
    	$this->set(compact('reports'));
    }

    public function viewReport($id) {
        $reportTable = TableRegistry::get('Reports');
        try{
            $report = $reportTable->find()
            ->where(['Reports.id'    => base64_decode($id)])
            ->contain([
                'OrganizationCompanies',
                'AccountingFrameworkTypes',
                'ReportTypes',
                'OpinionTypes',
                'ModifiedOpinions',
            ])->first();
            $this->loadModel('Users');
            $firstSignature = $this->Users->find()
            ->where(['Users.id' => $report->first_signature_user_id])
            ->first();
            $secondSignature = $this->Users->find()
            ->where(['Users.id' => $report->second_signature_user_id])
            ->first();

            $this->loadModel('OpinionTypes');
            $this->loadModel('EmphasisOfMatters');
            $this->preCondition();        
            $emphasis = $this->EmphasisOfMatters->getEmphasisOfMatter($this->request->session()->read('Config.language'));
            $opinion = $this->OpinionTypes->getOpinionList($this->request->session()->read('Config.language'));
            $OrganizationUsers = TableRegistry::get('OrganizationUsers');
            $usersList = $OrganizationUsers->find('list', [
                'keyField' => 'user.id',
                'valueField' => 'user.full_name'
            ])
            ->where([
                'OrganizationUsers.organization_id' => $this->Auth->user('organization.id'),
                'Users.role_id' => 3
            ])
            ->contain(['Users']);

            $this->loadModel('AccountingFrameworkTypes');
            $accounting = $this->AccountingFrameworkTypes->find('list');
            $this->loadModel('ModifiedOpinions');
            $modified = $this->ModifiedOpinions->getModifiedOpinion($this->request->session()->read('Config.language'));
            $this->loadModel('ReportReferences');
            $reference = $this->ReportReferences->getReference($this->request->session()->read('Config.language'));
            
            $this->set(compact('report', 'firstSignature','secondSignature','opinion','accounting','emphasis','modified','usersList','reference'));

            if($this->request->is('ajax')) {
                $this->viewBuilder()->layout(false);
                $this->render('/Element/Reports/report_view');
            }
        } catch(NotFoundException $e) {

        }
    }

    public function reportEditor($report_id) {
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('ReportTemplates');
        $lang='template_eng';
        if($this->request->session()->read('Config.language') == 'en_GER') {
            $lang   = 'template_german';
        }
        
        if(!empty($this->request->query['lang'])) {
            $lang=$this->request->query['lang'];
        }
        $reportTemplate = $this->ReportTemplates->find()
            ->where(['ReportTemplates.id' => 1])
            ->first();
        if($this->Auth->user('role_id') == Configure::read('UserRoles.Organization Manager')) {
            $subsidiaryTable = TableRegistry::get('OrganizationSubsidiaries');
            $subsidiary = $subsidiaryTable->getUserSubsidiary(null, $this->Auth->user('organization.id'));
        }else {
            $subsidiaryTable = TableRegistry::get('OrganizationSubsidiaries');
            $subsidiary = $subsidiaryTable->getUserSubsidiary(
                $this->Auth->user('id')
            ) ; 
        }
        $this->loadModel('FinalReports');
        $reportEnglishTemplate = $this->FinalReports->getFinalReport(base64_decode($report_id),$lang);
        $report = $this->Reports->find()
            ->where([
                'Reports.id' => base64_decode($report_id) ,
                'Reports.modified_recently' => 1
            ])
            ->first();
        if(!empty($reportEnglishTemplate) && empty($report)) {
            $reportEnglishTemplate = $reportEnglishTemplate['body'];
        } else {
            $report = $this->Reports->getReport($report_id);// pr($report);die;
            $reportEnglishTemplate = $this->ReportPlaceholder->populateTemplate($report, $reportTemplate, $subsidiary, $lang);
        }
        
        $this->set(compact(
            'reportEnglishTemplate', 'reportFrenchTemplate', 
            'reportGermanTemplate','reportItalianTemplate', 'lang'
        ));
    }

    public function reportPdf() {
        if($this->request->is('post')) {
            $this->viewBuilder()->layout(false);           
            $data['body']   = $this->request->data['mail_body'];
            $data['user_id']    = $this->Auth->user('id');
            $data['report_id']  = base64_decode($this->request->data['report_id']);
            $data['lang']  = $this->request->data['lang'];
            $reportTable = TableRegistry::get('FinalReports');
            if($reportTable->addFinalReport($data)) {

            }
            if(!empty($this->request->data['pdf'])) {
                $this->viewBuilder()->options([
                    'pdfConfig' => [
                        'orientation' => 'portrait',
                        'filename' => 'lsereport.pdf'
                    ],
                ]);
                $data =  $this->request->data['mail_body'];
                $this->set(compact(['data']));
            }else {
                $newReportTable = TableRegistry::get('Reports');
                $report = $newReportTable->get($data['report_id']);
                $report->modified_recently = 0;
                try{
                    $newReportTable->save($report);
                }catch(NotFoundException $e){
                    
                }
                $this->Flash->success(__('Report has been saved successfully.'));
                return $this->redirect(['action' => 'reportListing']);
            }
        }
    }

    public function getReport($report_id) {
        $this->viewBuilder()->layout(false);
        $reportTable = TableRegistry::get('FinalReports');
        $finalReport = $reportTable->find()
            ->where(['FinalReports.report_id' => base64_decode($report_id)]) 
            ->order(['created' => 'DESC'])
            ->first();
        $report = $this->Reports->find()
            ->where([
                'Reports.id' => base64_decode($report_id) ,
                'Reports.modified_recently' => 1
            ])
            ->first();
        if(!empty($finalReport) && empty($report)) {            
            $this->viewBuilder()->options([
                'pdfConfig' => [
                    'orientation' => 'portrait',
                    'filename' => 'lsereport.pdf'
                ],
            ]);
            $data =  $finalReport->body;
            $this->set(compact(['data']));
            $this->render('/Reports/pdf/report_pdf');
        }else {
            // $this->Flash->success(__('Report i.'));
            return $this->redirect(['action' => 'report-editor',$report_id]);
        }
    }

    private function makeSearchCondition() {
        $whereCondition = [
            'Reports.user_id' => $this->Auth->user('id')
        ];
        if (!empty($this->request->query['search']) ) {
            $search = [
                'or' => [
                    'OrganizationCompanies.company_name LIKE ' => '%'. $this->request->query['search'] .'%',   
                ]
            ];
            $whereCondition = array_merge($search, $whereCondition);
        }
        if (!empty($this->request->query['date']) ) {
            $data = date('Y-m-d',strtotime(str_replace('/', '-', $this->request->query['date'])));
            $search = [
                'AND' => [
                    'Reports.report_date' => $data ,   
                ]
            ];
            $whereCondition = array_merge($search, $whereCondition);
        }
        return $whereCondition;
    }

    /**
     * Delete method
     *
     * @param string|null $id Specialize id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $specialize = $this->Reports->get(base64_decode($id));
        if ($this->Reports->delete($specialize)) {
            $this->Flash->success(__('The report has been deleted.'));
        } else {
            $this->Flash->error(__('The report could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'report-listing']);
    }

    public function editReport($id) {
        
        try {
            if ($id != null) {                
               $report = $this->Reports->get(base64_decode($id));
            }       
        } catch (NotFoundException $e) {
           $this->Flash->success('opinion not found please try agian');
           return $this->redirect($this->referer());
       }
        if($this->request->is('post')) {
            $this->request->data['modified_recently'] = 1;
            if(!empty($this->request->data['report_date'])) {   
            $this->request->data['report_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $this->request->data['report_date'])));
            }
            if(!empty($this->request->data['financial_year_from'])) {
            $this->request->data['financial_year_from'] = date('Y-m-d',strtotime(str_replace('/', '-', $this->request->data['financial_year_from'])));
            }
            if(!empty($this->request->data['financial_year_to'])) {
            $this->request->data['financial_year_to'] = date('Y-m-d',strtotime(str_replace('/', '-', $this->request->data['financial_year_to'])));
            }
            if(!empty($this->request->data['date_prior_audit_report'])) {
            $this->request->data['date_prior_audit_report'] = date('Y-m-d',strtotime(str_replace('/', '-', $this->request->data['date_prior_audit_report'])));
            }
            if(!empty($this->request->data['audit_terminated_date'])) {     
                $this->request->data['audit_terminated_date'] = date('Y-m-d',strtotime(str_replace('/', '-', $this->request->data['audit_terminated_date'])));
            }
            if(!empty($this->request->data['emphasis_of_matter_ids']) && is_array($this->request->data['emphasis_of_matter_ids'])) {
                $this->request->data['emphasis_of_matter_ids'] = implode(',', $this->request->data['emphasis_of_matter_ids']);
            }
             if(!empty($this->request->data['reference_ids']) && is_array($this->request->data['reference_ids'])) {
                $this->request->data['reference_ids'] = implode(',', $this->request->data['reference_ids']);
            }
            $report = $this->Reports->patchEntity($report, $this->request->data);
            if ($this->Reports->save($report)) {
                 $this->Flash->success(__('Report has been Added'));
                 $this->redirect(array('action' => 'report-listing'));
            } else {
                $error = true;
                $errors = $this->_setValidationError($report->errors());
                $this->Flash->error(__('Your account could not be created due to the below errors. Please, try again.' . $errors));
            }
            $this->redirect(array('action' => 'report-listing'));
        }
    }
}