<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * NewReportPlaceholders Controller
 *
 * @property \App\Model\Table\NewReportPlaceholdersTable $NewReportPlaceholders
 *
 * @method \App\Model\Entity\NewReportPlaceholder[] paginate($object = null, array $settings = [])
 */
class NewReportPlaceholdersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->layout('dashboard');
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['NewReports']
        ];
        $newReportPlaceholders = $this->paginate($this->NewReportPlaceholders);

        $this->set(compact('newReportPlaceholders'));
        $this->set('_serialize', ['newReportPlaceholders']);
    }

    /**
     * View method
     *
     * @param string|null $id New Report Placeholder id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {   
        $this->loadModel('NewPlaceholders');
        $newReportPlaceholder = $this->NewReportPlaceholders->get($id, [
            'contain' => ['NewReports']
        ]);
        $placeholder = $this->NewPlaceholders->newEntity();

        $this->loadModel('NewPlaceholderTypes');
        $types = $this->NewPlaceholderTypes->find('list');
        $this->loadModel('NewQuestionTypes');
        $questionTypes = $this->NewQuestionTypes->find('list');

        $this->set(compact('newReportPlaceholder', 'placeholder', 'types', 'questionTypes'));
        $this->set('_serialize', ['newReportPlaceholder', 'placeholder', 'types', 'questionTypes']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $newReportPlaceholder = $this->NewReportPlaceholders->newEntity();
        if ($this->request->is('post')) {
            $newReportPlaceholder = $this->NewReportPlaceholders->patchEntity($newReportPlaceholder, $this->request->getData());
            if ($this->NewReportPlaceholders->save($newReportPlaceholder)) {
                $this->Flash->success(__('The new report placeholder has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The new report placeholder could not be saved. Please, try again.'));
        }
        $newReports = $this->NewReportPlaceholders->NewReports->find('list', ['limit' => 200]);
        $this->set(compact('newReportPlaceholder', 'newReports'));
        $this->set('_serialize', ['newReportPlaceholder']);
    }

    /**
     * Edit method
     *
     * @param string|null $id New Report Placeholder id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $newReportPlaceholder = $this->NewReportPlaceholders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $newReportPlaceholder = $this->NewReportPlaceholders->patchEntity($newReportPlaceholder, $this->request->getData());
            if ($this->NewReportPlaceholders->save($newReportPlaceholder)) {
                $this->Flash->success(__('The new report placeholder has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The new report placeholder could not be saved. Please, try again.'));
        }
        $newReports = $this->NewReportPlaceholders->NewReports->find('list', ['limit' => 200]);
        $this->set(compact('newReportPlaceholder', 'newReports'));
        $this->set('_serialize', ['newReportPlaceholder']);
    }

    /**
     * Delete method
     *
     * @param string|null $id New Report Placeholder id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $newReportPlaceholder = $this->NewReportPlaceholders->get($id);
        if ($this->NewReportPlaceholders->delete($newReportPlaceholder)) {
            $this->Flash->success(__('The new report placeholder has been deleted.'));
        } else {
            $this->Flash->error(__('The new report placeholder could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
