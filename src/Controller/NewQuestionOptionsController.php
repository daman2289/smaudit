<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * NewQuestionOptions Controller
 *
 * @property \App\Model\Table\NewQuestionOptionsTable $NewQuestionOptions
 *
 * @method \App\Model\Entity\NewQuestionOption[] paginate($object = null, array $settings = [])
 */
class NewQuestionOptionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['NewQuestions']
        ];
        $newQuestionOptions = $this->paginate($this->NewQuestionOptions);

        $this->set(compact('newQuestionOptions'));
        $this->set('_serialize', ['newQuestionOptions']);
    }

    /**
     * View method
     *
     * @param string|null $id New Question Option id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $newQuestionOption = $this->NewQuestionOptions->get($id, [
            'contain' => ['NewQuestions']
        ]);

        $this->set('newQuestionOption', $newQuestionOption);
        $this->set('_serialize', ['newQuestionOption']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $newQuestionOption = $this->NewQuestionOptions->newEntity();
        if ($this->request->is('post')) {
            $newQuestionOption = $this->NewQuestionOptions->patchEntity($newQuestionOption, $this->request->getData());
            if ($this->NewQuestionOptions->save($newQuestionOption)) {
                $this->Flash->success(__('The new question option has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The new question option could not be saved. Please, try again.'));
        }
        $newQuestions = $this->NewQuestionOptions->NewQuestions->find('list', ['limit' => 200]);
        $this->set(compact('newQuestionOption', 'newQuestions'));
        $this->set('_serialize', ['newQuestionOption']);
    }

    /**
     * Edit method
     *
     * @param string|null $id New Question Option id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $newQuestionOption = $this->NewQuestionOptions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $newQuestionOption = $this->NewQuestionOptions->patchEntity($newQuestionOption, $this->request->getData());
            if ($this->NewQuestionOptions->save($newQuestionOption)) {
                $this->Flash->success(__('The new question option has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The new question option could not be saved. Please, try again.'));
        }
        $newQuestions = $this->NewQuestionOptions->NewQuestions->find('list', ['limit' => 200]);
        $this->set(compact('newQuestionOption', 'newQuestions'));
        $this->set('_serialize', ['newQuestionOption']);
    }

    /**
     * Delete method
     *
     * @param string|null $id New Question Option id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $newQuestionOption = $this->NewQuestionOptions->get($id);
        if ($this->NewQuestionOptions->delete($newQuestionOption)) {
            $this->Flash->success(__('The new question option has been deleted.'));
        } else {
            $this->Flash->error(__('The new question option could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
