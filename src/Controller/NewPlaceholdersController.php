<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Datasource\Exception\NotFoundException;
/**
 * NewPlaceholders Controller
 *
 * @property \App\Model\Table\NewPlaceholdersTable $NewPlaceholders
 *
 * @method \App\Model\Entity\NewPlaceholder[] paginate($object = null, array $settings = [])
 */
class NewPlaceholdersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->layout('dashboard');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['NewReports', 'NewPlaceholderTypes']
        ];
        $newPlaceholders = $this->paginate($this->NewPlaceholders);

        $this->set(compact('newPlaceholders'));
        $this->set('_serialize', ['newPlaceholders']);
    }

    /**
     * View method
     *
     * @param string|null $id New Placeholder id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {   
        $placeholder = $this->NewPlaceholders->get($id, [
            'contain' => ['NewQuestions']
        ]);
        $this->loadModel('NewPlaceholderTypes');
        $types = $this->NewPlaceholderTypes->find('list');
        $this->loadModel('NewQuestionTypes');
        $questionTypes = $this->NewQuestionTypes->find('list');

        $this->set(compact('placeholder', 'types', 'questionTypes'));
        $this->set('_serialize', ['newPlaceholder']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $newPlaceholder = $this->NewPlaceholders->newEntity();
        if ($this->request->is('post')) {
            $newPlaceholder = $this->NewPlaceholders->patchEntity($newPlaceholder, $this->request->getData());
            if ($this->NewPlaceholders->save($newPlaceholder)) {
                $this->Flash->success(__('The new placeholder has been saved.'));
                die;
                return $this->redirect(['controller'=> 'new-reports', 'action' => 'index']);
            }
            $this->Flash->error(__('The new placeholder could not be saved. Please, try again.'));
        }
        $newReports = $this->NewPlaceholders->NewReports->find('list', ['limit' => 200]);
        $newPlaceholderTypes = $this->NewPlaceholders->NewPlaceholderTypes->find('list', ['limit' => 200]);
        $this->set(compact('newPlaceholder', 'newReports', 'newPlaceholderTypes'));
        $this->set('_serialize', ['newPlaceholder']);
    }

    /**
     * Edit method
     *
     * @param string|null $id New Placeholder id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $newPlaceholder = $this->NewPlaceholders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $newPlaceholder = $this->NewPlaceholders->patchEntity($newPlaceholder, $this->request->getData(),[
                'associated' => ['NewQuestions' => ['associated' => 'NewQuestionOptions']]
            ]);
            if ($this->NewPlaceholders->save($newPlaceholder)) {
                $this->Flash->success(__('The new placeholder has been saved.'));
                return $this->redirect(['controller' => 'new-reports', 'action' => 'index']);
            }
            $this->Flash->error(__('The new placeholder could not be saved. Please, try again.'));
        }
        $newReports = $this->NewPlaceholders->NewReports->find('list', ['limit' => 200]);
        $newPlaceholderTypes = $this->NewPlaceholders->NewPlaceholderTypes->find('list', ['limit' => 200]);
        $this->set(compact('newPlaceholder', 'newReports', 'newPlaceholderTypes'));
        $this->set('_serialize', ['newPlaceholder']);
        return $this->redirect($this->referer());
    }

    /**
     * Delete method
     *
     * @param string|null $id New Placeholder id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $newPlaceholder = $this->NewPlaceholders->get($id);
        if ($this->NewPlaceholders->delete($newPlaceholder)) {
            $this->Flash->success(__('The new placeholder has been deleted.'));
        } else {
            $this->Flash->error(__('The new placeholder could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Edit method
     *
     * @param string|null $id New Placeholder id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function editPlaceholder($id = null)
    {
        $placeholder = $this->NewPlaceholders->get($id, [
            'contain' => [
                'NewQuestions' => ['NewQuestionOptions']
            ]
        ]);
        // pr($placeholder->toArray());die;
        if ($this->request->is(['patch', 'post', 'put'])) {
            // pr($this->request->data);die;
            $newPlaceholder = $this->NewPlaceholders->patchEntity($placeholder, $this->request->getData(),[
                'associated' => ['NewQuestions' => ['associated' => 'NewQuestionOptions']]
            ]);
            // $newPlaceholder  = $this->__deleteQuestion($placeholder, $newPlaceholder);
            if ($this->NewPlaceholders->save($newPlaceholder)) {
                $this->Flash->success(__('The new placeholder has been saved.'));
                return $this->redirect(['controller' => 'new-reports', 'action' => 'index']);
            }
            $this->Flash->error(__('The new placeholder could not be saved. Please, try again.'));
        }

        $this->loadModel('NewPlaceholderTypes');
        $types = $this->NewPlaceholderTypes->find('list');
        $this->loadModel('NewQuestionTypes');
        $questionTypes = $this->NewQuestionTypes->find('list');

        $this->set(compact('placeholder', 'types', 'questionTypes'));
        $this->set('_serialize', ['newPlaceholder']);
    }

    protected function __deleteQuestion($placeholder, $newentity) {
        //changing placeholder form question to system
        // remove placeholder question and options
        $this->loadModel('NewQuestions');
        $this->loadModel('NewQuestionOptions');
        if($placeholder->new_placeholder_type_id == 3 && $newentity->new_placeholder_type_id == 1) {
            try {
                $question = $this->NewQuestions->find()
                    ->where(['NewQuestions.new_placeholder_id' => $placeholder->id])->first();
                if($question) {
                    $this->NewQuestions->delete($question);
                }
            }   catch (RecordNotFoundException $e) {
            }   catch (NotFoundException $e) {
            }
        } else if($placeholder->new_placeholder_type_id == 3 && $newentity->new_placeholder_type_id == 3) {
            $question = $this->NewQuestions->find()
                    ->where(['NewQuestions.new_placeholder_id' => $placeholder->id])->first();
                if($question) {
                    $this->NewQuestionOptions->deleteAll(['new_question_id' => $question->id ]);
                }
        }else { // changing from system placehoder to qustion empty the content en field
            $newentity->content_en = '';            
        }

        return $newentity;
    }

    public function viewPlaceholder($id) {
        $placeholder = $this->NewPlaceholders->get($id, [
            'contain' => [
                'NewQuestions' => ['NewQuestionOptions'],
                'NewPlaceholderTypes'
            ]
        ]);
        
        $this->loadModel('NewPlaceholderTypes');
        $types = $this->NewPlaceholderTypes->find('list');
        $this->loadModel('NewQuestionTypes');
        $questionTypes = $this->NewQuestionTypes->find('list');

        $this->set(compact('placeholder', 'types', 'questionTypes'));
        $this->set('_serialize', ['newPlaceholder']);
    }

    

}
