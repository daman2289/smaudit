<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Utility\Text;
/**
 * ReportPlaceholder component
 */
class ReportPlaceholderComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    public  $replaceble = [];
    public $dateFormatt = 'j. F Y';
    public $germanMonth = [
        'January'   =>     'Januar',
        'February'  =>    'Februar',
        'March' =>   'März',
        'April' =>   'April',
        'May'   =>     'Mai',
        'June'  =>    'Juni',
        'July'  =>    'Juli',
        'August'    =>  'August',
        'September' =>   'September',
        'October'   =>     'Oktober',
        'November'  =>    'November',
        'December'  =>    'Dezember'
    ];

    function translateMonth($date,$lang) {
        if($lang == 'template_german') {
            foreach ($this->germanMonth as $key => $value) {
                if (strstr($date,  $key))  {               
                    $str = str_replace(
                        array($key),
                        array(
                            $value
                        ),
                        $date
                    );
                    return $str;
                }
            }
        }
        return $date;
    }
    public function companylogo ($table, $field, $dir, $prefix, $image) {
        $filePath = 'logo' . DS . $table . DS . $field . DS . $dir . DS . $prefix . $image;
        if (file_exists ( WWW_ROOT.$filePath )) {
            return DS . $filePath;
        } else {
            return DS . 'img' . DS . 'logo.png';
        }
    } 

    private function getLseReportReplacement($lang) {
        if ($lang != 'template_german') {
            $this->dateFormatt = 'F j, Y';
        }
        $accountingFrameworkTable = TableRegistry::get('accountingFrameworkValues');
        $accountingFramework = $accountingFrameworkTable->find('list', [
            'keyField' => 'placeholder_number',
            'valueField' => ($lang == 'template_german') ? 
                'ger_value' :'eng_value'
        ]);
        $legalFormTable = TableRegistry::get('LegalFormValues');
        $legalForm = $legalFormTable->find('list', [
            'keyField' => 'placeholder_number',
            'valueField' => ($lang == 'template_german') ? 
                'ger_value' :'eng_value'
        ]);
        $lseOthersTable = TableRegistry::get('LseOthers');
        $lseOthers = $lseOthersTable->find('list', [
            'keyField' => 'placeholder_number',
            'valueField' => ($lang == 'template_german') ? 
                'ger_value' :'eng_value'
        ]);
        $reportReferencesTable = TableRegistry::get('ReportReferences');
        $reportReferences = $reportReferencesTable->find('list', [
            'keyField' => 'place_holder_number',
            'valueField' => ($lang == 'template_german') ? 
                'content_eng' :'content_eng'
        ]);
        $this->replaceble = $accountingFramework->toArray()+ 
            $legalForm->toArray()+
            $lseOthers->toArray()+
            $reportReferences->toArray();
        // pr($this->replaceble);die;
    }

    public function populateTemplate($report,$reportTemplate,$subsidiary,$lang) {
        // pr($report->toArray());die;
        $this->getLseReportReplacement($lang);
        $opting_out = $this->getOptingStatement($report, $lang);
        $emphasis = explode(',', $report->emphasis_of_matter_ids);
        $tempString = $reportTemplate[$lang];
        $response = str_replace(
            array(
                'https://smaudit.ch/img/logo.png',
                '[V.MyAccount(Sub)]',
                '[W.MyAccount(Sub)]',
                '[M.MyAccount]',
                '[T.MyAccount]', 
                '[X.MyAccount(Sub)]', 
                '[Y.MyAccount(Sub)]',
                '[Z.MyAccount(Sub)]', 
                '[AA.MyAccount(Sub)]',
                '[A.CompanyInfo]', 
                '[1.Companies]',
                '[F.CompanyInfo]',
                '[J.MyAccount]', 
                '[80.Companies]', 
                '[A.CompanyInfo]', 
                '[53.7a.AddReportLSE]',// short /long 7, 7a is replaced available in array
                '[8.AddReportLSE]', // Audit teminated later than report 55 is with in 55 available in array
                '[9.AddReportLSE]', // opting out date prior report 54(date prior audit report) inside
                '[54.AddReportLSE]',// coverd in upper 9.Addreport 
                '[78.Companies]', 
                '[56.Companies]', 
                '[K.MyAccount]', 
                '[L.MyAccount]', 
                '[J.MyAccount]',
                '[12.AddReportLSE]', //assistanc in booking availabe in array
                // '[L.MyAccount]', 
                // '[J.MyAccount]',
                '[77.AddReportLSE]',// basis for modification 74 75 81 is selceted
                // '[74a.AddReportLSE]',  
                '[74.AddReportLSE]',//modified opnion 
                '[75.AddReportLSE]',// |
                '[76.AddReportLSE]',// |
                '[81.AddReportLSE]',///these all comes under 74            
                '[58.AddReportLSE]', //emphasis of matters 57, 56, 62 with in
                '[59.AddReportLSE]',//emphasis of matters  58 59 61 64 84 85, 56, 62 with in
                '[61.AddReportLSE]',// |
                '[64.AddReportLSE]',// |
                '[84.AddReportLSE]',// |
                '[85.AddReportLSE]',// replaced by empty as 59 provide all
                '[65.AddReportLSE]',// Reference
                '[66.AddReportLSE]',// Referene 63
                '[86.AddReportLSE]',// Reference amount of sharehoder loan
                '[87.AddReportLSE]',//
                '[AA.MyAccountSub]',//subsidiery data
                '[50.AddReportLSE]',//Report Date
                // '[T.MyAccount]',
                '[67.AddReportLSE]',  //first signature     
                '[69.AddReportLSE]',// second signature
                '[68.AddReportLSE]',// auditor in charge
                '[70.AddReportLSE]',// auditor in charge
                '[71.AddReportLSE]',// licence  
                '[72.AddReportLSE]',// licence,
                '[13.AddReportLSE]',// licence,
                '[B.CompanyInfo]',
                '[C.CompanyInfo]',
                '[E.CompanyInfo]',
                '[D.CompanyInfo]',
                '[73.AddReportLSE]',
                '[53.7.AddReportLSE]'
            ),
            array(
                $this->getOrganizatonLogo($report),
                $this->getSubsidiaryStatement($report, 'V', $subsidiary),
                $this->getSubsidiaryStatement($report, 'W', $subsidiary),
                $this->getMyAccount($report, 'M', null, $lang),
                $this->getMyAccount($report, 'T', null, $lang),
                $this->getSubsidiaryStatement($report, 'X', $subsidiary),
                $this->getSubsidiaryStatement($report, 'Y', $subsidiary),
                $this->getSubsidiaryStatement($report, 'Z', $subsidiary),
                $this->getSubsidiaryStatement($report, 'AA',$subsidiary),
                $this->getCompanyInfo($report,'A'),
                $this->getCompanyStatement($report,1, $lang),
                $this->getCompanyInfo($report,'F'),
                $this->getMyAccount($report, 'J', $emphasis, $lang),
                $this->getCompanyStatement($report,80, $lang),

                $this->getCompanyInfo($report,'A'),
                $this->getShortLong($report, '7a', $lang),
                $this->getAuditTerminatedLaterStatement($report,'55', $lang),
                $this->getOptingStatement($report, $lang),
                '',// replaced by space as already coved in above statement
                $this->getCompanyStatement($report,78, $lang),
                $this->getCompanyStatement($report,56, $lang),
                $this->getMyAccount($report, 'K', $emphasis, $lang),
                $this->getMyAccount($report, 'L', $emphasis, $lang),
                $this->getMyAccount($report, 'J', $emphasis, $lang),                
                ($report->assistance_book_keeping) ? $this->replaceble['12'] :'',
                $this->getBasisForModificationStatement($report),//bases for modification
                $this->getModifiedOpenionStatement($report, $lang),
                '',
                '',
                '',//coverd in above
                $this->getEmphasisStatement($report,$lang),
                '',
                '',
                '',
                '',
                '',// coverd in above
                $this->getReferencesStatement($report,$lang),
                '',
                '',
                '',//Reference need clearification for now
                $this->getSubsidiaryStatement($report, 'AA',$subsidiary),
                $this->translateMonth(date($this->dateFormatt, strtotime($report->report_date)),$lang),
                $this->getSignatureStatement($report, $report->first_signature_user_id),
                $this->getSignatureStatement($report, $report->second_signature_user_id),
                ($report->auditor_incharge == 0)?$this->replaceble[68]:
                '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp',
                ($report->auditor_incharge == 1)?$this->replaceble[70]:
                '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp',
                $this->getSignatureLicenceStatement($report, $report->first_signature_user_id,$lang),
                $this->getSignatureLicenceStatement($report, $report->second_signature_user_id,$lang),
                $this->replace13($report, $lang),
                $this->getCompanyInfo($report,'B'),
                $this->getCompanyInfo($report,'C'),
                $this->getCompanyInfo($report,'E'),
                $this->getCompanyInfo($report,'D'),
                $report->financial_year,
                $this->getShortLong($report, '7', $lang),

            ), 
            $tempString
        );
        return $response;
    }

    public function getCompanyStatement($report, $placeholder, $lang) {
        TableRegistry::get('AccountingFrameworkValues');
        TableRegistry::get('LegalFormValues');
        $companyStatementValue = []; 
        $legalFormTable = TableRegistry::get('LegalFormValues');
        $legalForm = $legalFormTable->find('list', [
            'keyField' => 'placeholder_number',
            'valueField' => ($lang == 'template_german') ? 
                'ger_value' :'eng_value'
        ])
        ->where([
            'LegalFormValues.legal_form_type_id' => $report->organization_company->legal_form_type->id
        ]);
        if(!empty($report->accounting_framework_type_id)) {
            $accountingFrameworkTable = TableRegistry::get('AccountingFrameworkValues');
            $accountingFramework = $accountingFrameworkTable->find('list', [
                'keyField' => 'placeholder_number',
                'valueField' => ($lang == 'template_german') ? 
                    'ger_value' :'eng_value'
            ])
            ->where([
                'AccountingFrameworkValues.accounting_framework_type_id' => $report->accounting_framework_type_id
            ]);
        }

        $companyStatementValue = $companyStatementValue + $legalForm->toArray();
        $companyStatementValue = $companyStatementValue + $accountingFramework->toArray();

        if(!empty($companyStatementValue[$placeholder])) {
            return $companyStatementValue[$placeholder];
        }
        return '';
    }

    public function getShortLong($report, $placeholder, $lang) {
        $lseOthersTable = TableRegistry::get('LseOthers');
        $lseOthers = $lseOthersTable->find('list',[
                'keyField' => 'value',
                'valueField' => ($lang == 'template_german') ? 
                'ger_value' :'eng_value'
            ])
            ->where(['LseOthers.placeholder_number' => $placeholder])
            ->toArray();
        if(!empty($report) && $report->short_long) {
            $string =  str_replace(
                array('[51.AddReportLSE]','[52.AddReportLSE]'),
                array(
                    $this->translateMonth(date($this->dateFormatt, strtotime($report->financial_year_from)), $lang),
                    $this->translateMonth(date($this->dateFormatt, strtotime($report->financial_year_to)), $lang),                    
                ),
                $lseOthers[1]
            );
            return $string;
        }else {
            $string =  str_replace(
                array('[52.AddReportLSE]'),
                array(                    
                    $this->translateMonth(date($this->dateFormatt, strtotime($report->financial_year_to)), $lang), 
                ),
                $lseOthers[0]
            );
            return $string;
        }
    }
    
    public function getSignatureStatement($report,$user_id) {
        $organizationTable = TableRegistry::get('OrganizationUsers');
        if(!empty($user_id)) {
            $user = $organizationTable->getOrganizaionUser($user_id);  
            if(!empty($user) && !empty($user->toArray())) {
                return ucwords($user->user->first_name. ' '. $user->user->last_name);
            }else {
                return '';
            }
        }else {
            return '';
        }
    }

    public function getBasisForModificationStatement($report) {
        //Show if Option 74,75,81 is selected) Modified Opinion i.e not equal to clean opinion
        // pr($report->basis_for_modification);die;
        if(!empty($report->modified_opinion) && $report->modified_opinion['id'] != 3) {
            return  '<br/>'.$report->basis_for_modification;
        }
        return '';
    }

    public function getSignatureLicenceStatement($report,$user_id, $lang) {
        $organizationTable = TableRegistry::get('OrganizationUsers');
        if(!empty($user_id)) {
            $user = $organizationTable->getOrganizaionUser($user_id);  
            if(!empty($user) && !empty($user->toArray()) && !empty($user->license)) {
                return ($lang == 'template_german') ? 
                $user->license->name_ger :$user->license->name;
            }
            return '';
        }else {
            return '';
        }
    }

    public function getAuditTerminatedLaterStatement($report,$placeholder, $lang) {
        if($report->audit_terminated_later_than_report) {            
            $string =  str_replace(
                array('[55.AddReportLSE]'),
                array(
                    $this->translateMonth(date($this->dateFormatt, strtotime($report->audit_terminated_date)), $lang), 
                ),
                $this->replaceble[$placeholder]
            );
            return $string;
        }
        return '';
    }

    public function replace13($report, $lang) {
        $lseOthersTable = TableRegistry::get('LseOthers');
        $lseOthers = $lseOthersTable->find('list',[
                'keyField' => 'placeholder_number',
                'valueField' => ($lang == 'template_german') ? 
                'ger_value' :'eng_value'
            ])
            ->where([
                    'LseOthers.placeholder_number' => '13',
                    'LseOthers.value' => ($report->appropriation_available_earning) ? 1 :0,
                ]
            )
            ->toArray();
        if(!empty($lseOthers)) {
            return $lseOthers['13'];
        }
        return '';
    }

    private function getOptingStatement($report, $lang) {
        $field = 'content_eng';
        if($lang == 'template_german') {
            $field = 'content_ger';
        }
        if($report->first_year_audit) {            
            if(!$report->opting_out) {
                $string =  str_replace(
                    array('[54.AddReportLSE]'),
                    array(                        
                        $this->translateMonth(date($this->dateFormatt, strtotime($report->date_prior_audit_report)), $lang)
                    ),
                    $report->opinion_type[$field]
                );
                return $string;
            }else {
                // pr( $report->opinion_type[$field]);die;
                return (!empty($report->opinion_type)) ? 
                    $report->opinion_type[$field]:
                    '';//what if opinion type is select but by date is not given
            }
        }
        return '';
    }

    private function getModifiedOpenionStatement($report, $lang) { 
        $field = 'content_eng';
        if($lang == 'template_german') {
            $field = 'content_ger';
        }       
        $lseOthersTable = TableRegistry::get('LseOthers');

        $lseOthers = $lseOthersTable->find('list',[
                'keyField' => 'placeholder_number',
                'valueField' => ($lang == 'template_german') ? 
                'ger_value' :'eng_value'
            ])
            ->where([
                    'LseOthers.placeholder_number' => '13a',
                    'LseOthers.value' => ($report->appropriation_available_earning) ? 1 :0,
                ]
            )
            ->toArray();
  
        if(!empty($report->modified_opinion)) {
            if($field == 'content_ger' ) {                
                return $string =  str_replace(
                    array(
                        '[13.AddReportLSE]',
                        '[82.Companies]',
                        '[83.Companies]',
                        '[13a.AddReportLSE]',
                    ),
                    array(
                        ($report->appropriation_available_earning) ? $this->replaceble['13'] :'',
                        $this->getCompanyStatement($report,'82', $lang),
                        $this->getCompanyStatement($report,'83', $lang),
                        $lseOthers['13a']
                    ),
                    $report->modified_opinion[$field]
                );
            } else {
                return $string =  str_replace(
                    array(
                        '[13.AddReportLSE]',
                        '[82.Companies]',
                        '[83.Companies]'
                    ),
                    array(
                        $this->replaceble['13'],
                        $this->getCompanyStatement($report,'82', $lang),
                        $this->getCompanyStatement($report,'83', $lang)
                    ),
                    $report->modified_opinion[$field]
                );
            }
        }
        return '';
    } 

    public function getCompanyInfo($report, $placeholder) {
        if($placeholder == 'A') {
            return $report->organization_company->company_name;
        }
        if($placeholder == 'B') {
            return $report->organization_company->street;
        }
        if($placeholder == 'C') {
            return $report->organization_company->street_number;
        }
        if($placeholder == 'D') {
            return $report->organization_company->place;
        }
        if($placeholder == 'E') {
            return $report->organization_company->zip;
        }
        if($placeholder == 'F') {
            return $report->organization_company->place_of_incorporation;
        }
    }

    public function getMyAccount($report,  $placeholder, $emphasis = null, $lang) {
        
        $lseOthersTable = TableRegistry::get('LseOthers');

        if($placeholder == 'M') {
            return $report->organization_company->organization->website;
        }if($placeholder == 'N') {
            return $report->organization_company->organization->phone;
        }if($placeholder == 'O') {
            return $report->organization_company->organization->fax;
        }if($placeholder == 'P') {
            return $report->organization_company->organization->street;
        }if($placeholder == 'Q') {
            return $report->organization_company->organization->street_number;
        }if($placeholder == 'R') {
            return $report->organization_company->organization->zip;
        }if($placeholder == 'S') {
            return $report->organization_company->organization->Place;
        }if($placeholder == 'T') {
            return $report->organization_company->organization->name;
        }
        $lseOthers = $lseOthersTable->find('list',[
            'keyField' => 'value',
            'valueField' => ($lang == 'template_german') ? 
            'ger_value' :'eng_value'
        ])
        ->where(['LseOthers.placeholder_number' => $placeholder])
        ->toArray();
        if(count($emphasis) > 0) {
            return (!empty($lseOthers)) ? $lseOthers[1] : '';
        }
        return (!empty($lseOthers))? $lseOthers[0] : '';
    }

    public function getSubsidiaryStatement($report, $placeholder, $subsidiary) {
        if($placeholder == 'U') {
            return $subsidiary->name;
        }if($placeholder == 'V') {
            return $subsidiary->phone;
        }if($placeholder == 'W') {
            return $subsidiary->fax;
        }if($placeholder == 'X') {
            return $subsidiary->street;
        }if($placeholder == 'Y') {
            return $subsidiary->street_number;
        }if($placeholder == 'Z') {
            return $subsidiary->zip;
        }if($placeholder == 'AA') {
            return $subsidiary->place;
        }
    }

    private function getEmphasisStatement($report,$lang) {
        $field = 'content_eng';
        if($lang == 'template_german') {
            $field = 'content_ger';
        }  
        $reportEmphasis = explode(',', $report->emphasis_of_matter_ids);
        $emphasisTable  = TableRegistry::get('EmphasisOfMatters');
        $emphasisList   = $emphasisTable->find()
            ->where(['EmphasisOfMatters.id IN' => $reportEmphasis]);
            
        if(!empty($emphasisList) && !empty($emphasisList->toArray())) {
            $string = "";
            foreach ($emphasisList as $key => $value) {
                if($value['place_holder_number'] == 58) {
                    $string =  $string .'<br/>'.  str_replace(
                        array(
                            '[A.CompanyInfo]',
                            '[A.CompanyInfo]',
                            '[57]',
                            '[56.Companies]'
                        ),
                        array(
                            $this->getCompanyInfo($report,'A'),
                            $this->getCompanyInfo($report,'A'),
                            $report->subordination_amount,
                            $this->getCompanyStatement($report, 56, $lang)
                        ),
                        $value[$field]
                    );
                }elseif($value['place_holder_number'] == 59) {
                    $string =  $string .'<br/>'.  str_replace(
                        array(
                            '[62.AddReportLSE]',
                            '[A.CompanyInfo]',
                        ),
                        array(
                            $report->going_concern_note_no,
                            $this->getCompanyInfo($report,'A'),
                        ),
                        $value[$field]
                    );
                }else {
                    $string = $string .'<br/>'.  str_replace(
                        array(
                            '[A.CompanyInfo]',
                        ),
                        array(
                            $this->getCompanyInfo($report,'A'),
                            $report->going_concern_note_no,
                        ),
                        $value[$field]
                    );
                }
            }
            return nl2br($string);
        }
        return chr(8);
    }  

    public function getReferencesStatement($report, $lang) {
        $field = 'content_eng';
        if($lang == 'template_german') {
            $field = 'content_ger';
        }  
        $reportReference = explode(',', $report->reference_ids);
        $referenceTable  = TableRegistry::get('ReportReferences');
        $referenceList   = $referenceTable->find()
            ->where(['ReportReferences.id IN' => $reportReference]);
        // pr($referenceList->toArray());die;
        if(!empty($referenceList) && !empty($referenceList->toArray())) {
            $string = '';
            foreach ($referenceList as $key => $value) {
                if($value['place_holder_number'] == 65) {
                    $string =  $string .'<br/>'.  str_replace(
                        array(
                            '[A.CompanyInfo]',
                        ),
                        array(
                            $this->getCompanyInfo($report,'A')
                        ),
                        $value[$field]
                    );
                }elseif($value['place_holder_number'] == 66) {
                    $string =  $string .'<br/>'.  str_replace(
                        array(
                            '[63.AddReportLSE]',
                        ),
                        array(
                            $report->amount_shareholder_loan,
                        ),
                        $value[$field]
                    );
                }elseif($value['place_holder_number'] == 86){
                    $string =  $string .'<br/>'.  str_replace(
                        array(
                            '[56.Companies]',
                        ),
                        array(
                            $this->getCompanyStatement($report, 56, $lang),
                        ),
                        $value[$field]
                    );
                }
                else {
                    $string = $string .'<br/>'.  str_replace(
                        array(
                            '[56.Companies]',
                            '[88.AddReportLSE]',
                            '[A.CompanyInfo]',
                            '[89.Companies]'
                        ),
                        array(
                            $this->getCompanyStatement($report,56, $lang),
                            $report->amount_dividend,
                            $this->getCompanyInfo($report,'A'),
                            $this->getCompanyStatement($report,89, $lang)
                        ),
                        $value[$field]
                    );
                }
            }
            return nl2br($string);
        }
        return '';
    }

    public function getOrganizatonLogo(){
        $image = $this->companylogo(
            'organizations', 
            'logo', 
            $report->organization_company->organization->logo_dir, 
            'square_', 
            $report->organization_company->organization->logo
        );
        $image = Router::url($image,true);
        $imageTag = $image;
        return $imageTag;
    }

}
