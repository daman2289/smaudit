old.php<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * ReportPlaceholder component
 */
class ReportPlaceholderComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    private function getLseReportReplacement() {
        $accountingFrameworkTable = TableRegistry::get('accountingFrameworkValues');
        $accountingFramework = $accountingFrameworkTable->find('list', [
            'keyField' => 'placeholder_number',
            'valueField' => 'eng_value'
        ]);
        $legalFormTable = TableRegistry::get('LegalFormValues');
        $legalForm = $legalFormTable->find('list', [
            'keyField' => 'placeholder_number',
            'valueField' => 'eng_value'
        ]);
        $lseOthersTable = TableRegistry::get('LseOthers');
        $lseOthers = $lseOthersTable->find('list', [
            'keyField' => 'placeholder_number',
            'valueField' => 'eng_value'
        ]);
        $reportReferencesTable = TableRegistry::get('ReportReferences');
        $reportReferences = $reportReferencesTable->find('list', [
            'keyField' => 'place_holder_number',
            'valueField' => 'content_eng'
        ]);
        $data 
        pr($accountingFramework->toArray());
        pr($legalForm->toArray());
        pr($lseOthers->toArray());
        pr($reportReferences->toArray());
        die;
    }

    public function populateTemplate($report,$reportTemplate,$subsidiary,$lang) {
        // pr($reportTemplate[$lang]);die;
        $this->getLseReportReplacement();
        $opting_out = $this->getOptingStatement($report);
        $emphasis = explode(',', $report->emphasis_of_matter_ids);
        $tempString = $reportTemplate[$lang];
        $response = str_replace(
            array(
                '[V.MyAccount(Sub)]',
                '[W.MyAccount(Sub)]',
                '[M.MyAccount]',
                '[T.MyAccount]', 
                '[X.MyAccount(Sub)]', 
                '[Y.MyAccount(Sub)]',
                '[Z.MyAccount(Sub)]', 
                '[AA.MyAccount(Sub)]',
                '[1.Companies]', 
                '[A.CompanyInfo]', 
                '[F.CompanyInfo]',
                '[J.MyAccount]', 
                '[80.Companies]', 
                '[A.CompanyInfo]', 
                '[53.7a.AddReportLSE]',
                '[8.AddReportLSE]', 
                '[9.AddReportLSE]', 
                // '[54.AddReportLSE]',

                '[56.Companies]', 
                '[K.MyAccount]', 
                '[L.MyAccount]', 
                '[J.MyAccount]',
                '[12.AddReportLSE]', 
                // '[L.MyAccount]', 
                // '[J.MyAccount]',
                '[77.AddReportLSE]',
                '[74.AddReportLSE]',
                '[75.AddReportLSE]',
                '[76.AddReportLSE]',
                '[81.AddReportLSE]',
                '[58.AddReportLSE]',
                '[59.AddReportLSE]',
                '[61.AddReportLSE]',
                '[64.AddReportLSE]',
                '[84.AddReportLSE]',
                '[85.AddReportLSE]',
                '[65.AddReportLSE]',
                '[66.AddReportLSE]',
                '[86.AddReportLSE]',
                '[87.AddReportLSE]',
                '[AA.MyAccountSub]',
                '[50.AddReportLSE]',
                '[T.MyAccount]',
                '[67.AddReportLSE]',       
                '[69.AddReportLSE]',
                '[68.AddReportLSE]',
                '[70.AddReportLSE]',
                '[71.AddReportLSE]',
                '[72.AddReportLSE]'
            ),
            array(
                $this->getSubsidiaryStatement($report, 'V', $subsidiary),
                $this->getSubsidiaryStatement($report, 'W', $subsidiary),
                $this->getMyAccount($report, 'M'),
                $this->getMyAccount($report, 'T'),
                $this->getSubsidiaryStatement($report, 'X', $subsidiary),
                $this->getSubsidiaryStatement($report, 'Y', $subsidiary),
                $this->getSubsidiaryStatement($report, 'Z', $subsidiary),
                $this->getSubsidiaryStatement($report, 'AA',$subsidiary),
                $this->getCompanyStatement($report,1),
                $this->getCompanyInfo($report,'A'),
                $this->getCompanyInfo($report,'F'),
                $this->getMyAccount($report, 'J', $emphasis),
                '',
                $this->getCompanyInfo($report,'A'),
                
                'for the period from '. date('d/m/Y', strtotime($report->financial_year_from)) .' to '. date('d/m/Y', strtotime($report->financial_year_to)),

                ($report->audit_terminated_later_than_report) ?
                'The statutory examination procedures were completed on ' .date('d/m/Y',strtotime($report->audit_terminated_date)) :
                '',

                $opting_out,
                // '',
                $this->getCompanyStatement($report,56),
                $this->getMyAccount($report, 'K', $emphasis),
                $this->getMyAccount($report, 'L', $emphasis),
                $this->getMyAccount($report, 'J', $emphasis),
                ($report->assistance_book_keeping) ? 'An employee of our company assisted in the bookkeeping during the reporting year. This individual was not involved in the limited statutory examination.' :'',
                // $this->getMyAccount($report, 'L', $emphasis),
                // $this->getMyAccount($report, 'J', $emphasis),
                'Modified Opinion',
                ($report->modified_opinion_id = 1) ? $this->getOpinionStatement(1, $report):'',
                ($report->modified_opinion_id = 2) ? $this->getOpinionStatement(2, $report):'',
                ($report->modified_opinion_id = 3) ? $this->getOpinionStatement(3, $report):'',
                ($report->modified_opinion_id = 4) ? $this->getOpinionStatement(4, $report):'',
                (in_array(1, $emphasis)) ? $this->getEmphasisStatement(1, $report):'',
                (in_array(2, $emphasis)) ? $this->getEmphasisStatement(2, $report):'',
                (in_array(3, $emphasis)) ? $this->getEmphasisStatement(3, $report):'',
                (in_array(4, $emphasis)) ? $this->getEmphasisStatement(4, $report):'',
                (in_array(5, $emphasis)) ? $this->getEmphasisStatement(5, $report):'',
                (in_array(6, $emphasis)) ? $this->getEmphasisStatement(6, $report):'',
                (in_array(7, $emphasis)) ? $this->getEmphasisStatement(7, $report):'',
                $report->organization_company->organization->place,
                $report->report_date
            ), 
            $tempString
        );
		
        return $response;
    }

     private function getOptingStatement($report) {
        if($report->first_year_audit) {
            $response = array(
                'The corresponding figures in the financial statements were not examined.',
                'The limited statutory examination of the prior period financial statements was performed by another auditor who expressed an unmodified examination conclusion on those financial statements on [54.AddReportLSE].',
                'The limited statutory examination of the prior year financial statements was performed by another auditor who expressed an unmodified examination conclusion on those financial statements on [54.AddReportLSE].',
                'The limited statutory examination of the prior period financial statements was performed by another auditor who expressed an modified examination conclusion on those financial statements on [54.AddReportLSE]. (PLEASE INSERT REASON FOR MODIFICATION)',
                'The limited statutory examination of the prior year financial statements was performed by another auditor who expressed an modified examination conclusion on those financial statements on [54.AddReportLSE]. (PLEASE INSERT REASON FOR MODIFICATION)',
            );
            if(!$report->opting_out) {
                $string =  str_replace(
                    array('[54.AddReportLSE]'),
                    array(date('d/m/y',strtotime($report->date_prior_audit_report))),
                    $response[$report->opinion_type_id]
                );
                return $string;
            }else {
                return $response[0];
            }
        }
        return '';
    }

    private function getOpinionStatement($key, $report) {
        $response = array(
            1 => 'Due to the possible effect of the matter described in the preceding paragraph, we are not in a position to express an examination conclusion.',
            2 => 'Based on our limited statutory examination, except for the qualification described in the preceding paragraph, nothing has come to our attention that causes us to believe that the financial statements [13.AddReportLSE] do not comply with the law and the company’s articles of incorporation.',
            3 => 'Based on our limited statutory examination, nothing has come to our attention that causes us to believe that the financial statements [13.AddReportLSE] do not comply with Swiss law and the company’s articles of incorporation.',
            4 => 'The limited statutory examination of the prior period financial statements was performed by another auditor who expressed an modified examination conclusion on those financial statements on [54.AddReportLSE]. (PLEASE INSERT REASON FOR MODIFICATION)',
        );
        if($key == 4) {
            return $string =  str_replace(
                array('[54.AddReportLSE]'),
                array(date('d/m/y',strtotime($report->date_prior_audit_report))),
                $response[$key]
            );
        }
        return $string =  str_replace(
            array('[13.AddReportLSE]'),
            array('and proposed appropriation of available earnings'),
            $response[$key]
        );
    }

    private function getEmphasisStatement($key, $report) {
       $response = array (
            1 => 'We draw attention to the fact that [A.CompanyInfo] is over-indebted as per art. 725 para. 2 CO. Due to the fact that creditors of [A.CompanyInfo] subordinated their claims amounting to [57] , [56.Companies] has refrained from notifying the court.',
            2 => 'Without qualifying our examination conclusion, we draw attention to note [62.AddReportLSE] in the notes to the financial statements describing a material uncertainty that may cast significant doubt about the ability of [A.CompanyInfo] to continue as a going concern. Should the company be unable to continue as a going concern, the financial statements would have to be prepared on the basis of liquidation values.',
            3 => 'In this case a serious concern of over-indebtedness as per art. 725 para. 2 CO would exist and the relevant provisions would have to be complied with.',
            4 => 'We draw attention to the fact, that half of the share capital and legal reserves is no longer covered (art. 725 para. 1 CO).',
            5 => 'We draw attention to the fact that the proposed dividend has a substantial impact on the liquidity of [A.CompanyInfo]. It can only be considered appropriate under the condition that the liquidity, which is considered necessary to the operating activity, will still be available after the dividend distribution. Should this not be the case, the proposed appropriation of available earnings would be in breach of the duty of care and loyalty (art. 717 para. 1 CO).',
            6 => 'We draw attention to the fact that the financial statements of [A.CompanyInfo] disclose an over-indebtedness.Furthermore, we point out that the Board of Directors has failed to prepare an interim balance sheet in accordance with art. 725 para. 2 CO. If this balance sheet were to show an over-indebtedness on the basis of going concern and liquidation values, the provisions of art. 725 para. 2 CO would have to be observed. ',
            7 => 'We point out that, in the absence of unrestricted reserves, the loan to shareholders of [63.AddReportLSE] constitutes a repayment of capital prohibited by art. 680 para. 2 CO.');
        if($key == 1) {
            return $string =  str_replace(
                array('[57]' , '[56.Companies]', '[A.CompanyInfo]'),
                array(
                    $report->subordination_amount, '', ''),
                $response[$key]
            );        
        }
        if($key == 2) {
            return $string =  str_replace(
                array('[62.AddReportLSE]', '[A.CompanyInfo]'),
                array($report->going_concern_note_no, ''),
                $response[$key]
            );        
        }
        if($key == 5 || $key == 6) {
            return $string =  str_replace(
                array('[A.CompanyInfo]'),
                array(''),
                $response[$key]
            );        
        }
        if($key == 7) {
            return $string =  str_replace(
                array('[63.AddReportLSE]'),
                array($report->amount_shareholder_loan),
                $response[$key]
            );        
        }
    }

    public function getCompanyStatement($report, $placeholder) {
    	$company = array(
    		'1' => array(
    			1 => 'shareholders’ meeting',
    			2 => 'general meeting of participants',
    			3 => 'general meeting of participants',
    			4 => 'general meeting of participants',
    			5 => "associations' meeting",
    			5 => "trusts' meeting",
    			6 => ''
    		),
    		'56' => array(
    			1 => 'the Board of Directors',
    			2 => 'the Participants',
    			3 => 'the Participants',
    			4 => 'the Participants',
    			5 => "the managing-committee",
    			5 => "the managing-committee",
    			6 => ''
    		),
    		'83' => array(
    			1 => 'company’s articles of incorporation',
    			2 => 'company’s articles of incorporation',
    			3 => 'company’s articles of incorporation',
    			4 => 'company’s articles of incorporation',
    			5 => "articles of association and regulations",
    			5 => "articles of foundation, foundation deed and regulations",
    			6 => 'articles of foundation, foundation deed and regulations'
    		),
    	);
    	return $company[$placeholder][$report->organization_company->legal_form_type->id];
    }

    public function getCompanyInfo($report, $placeholder) {
    	if($placeholder == 'A') {
    		return $report->organization_company->company_name;
    	}
    	if($placeholder == 'B') {
    		return $report->organization_company->street;
    	}
    	if($placeholder == 'C') {
    		return $report->organization_company->street_number;
    	}
    	if($placeholder == 'D') {
    		return $report->organization_company->place;
    	}
    	if($placeholder == 'E') {
    		return $report->organization_company->zip;
    	}
    	if($placeholder == 'F') {
    		return $report->organization_company->place_of_incorporation;
    	}
    }

    public function getMyAccount($report,  $placeholder, $emphasis = null) {
    	$account = array(
    		'0' => array(
    			'J' => 'I',
    			'K' => 'My',
    			'L' => 'I'
    		),
    		'1' => array(
    			'J' => 'We',
    			'K' => 'Our',
    			'L' => 'We'
    		),
    	);
    	if($placeholder == 'M') {
    		return $report->organization_company->organization->website;
    	}if($placeholder == 'N') {
    		return $report->organization_company->organization->phone;
    	}if($placeholder == 'O') {
    		return $report->organization_company->organization->fax;
    	}if($placeholder == 'P') {
    		return $report->organization_company->organization->street;
    	}if($placeholder == 'Q') {
    		return $report->organization_company->organization->street_number;
    	}if($placeholder == 'R') {
    		return $report->organization_company->organization->zip;
    	}if($placeholder == 'S') {
    		return $report->organization_company->organization->Place;
    	}if($placeholder == 'T') {
    		return $report->organization_company->organization->name;
    	}
    	if(count($emphasis) > 1) {
    		return $account[1][$placeholder];
    	}
    	return $account[0][$placeholder];
    }

    public function getSubsidiaryStatement($report, $placeholder, $subsidiary) {
    	if($placeholder == 'U') {
    		return $subsidiary->name;
    	}if($placeholder == 'V') {
    		return $subsidiary->phone;
    	}if($placeholder == 'W') {
    		return $subsidiary->fax;
    	}if($placeholder == 'X') {
    		return $subsidiary->street;
    	}if($placeholder == 'Y') {
    		return $subsidiary->street_number;
    	}if($placeholder == 'Z') {
    		return $subsidiary->zip;
    	}if($placeholder == 'AA') {
    		return $subsidiary->place;
    	}
    }

}
