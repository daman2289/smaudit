<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Utility\Text;
/**
 * ReportPlaceholder component
 */
class ReportComponent extends Component
{
    public $language = 'content_en';

    public $dateFormatt = 'j F Y';
    public $germanMonth = [
        'January'   =>     'Januar',
        'February'  =>    'Februar',
        'March' =>   'März',
        'April' =>   'April',
        'May'   =>     'Mai',
        'June'  =>    'Juni',
        'July'  =>    'Juli',
        'August'    =>  'August',
        'September' =>   'September',
        'October'   =>     'Oktober',
        'November'  =>    'November',
        'December'  =>    'Dezember'
    ];
    public $frenchMonth = [
        'January'   =>     'Janvier',
        'February'  =>    'Février',
        'March' =>   'Mars',
        'April' =>   'Avril',
        'May'   =>     'Mai',
        'June'  =>    'Juin',
        'July'  =>    'Juillet',
        'August'    =>  'Août',
        'September' =>   'Septembre',
        'October'   =>     'Octobre',
        'November'  =>    'Novembre',
        'December'  =>    'Décembre'
    ];

    function translateMonth($date,$lang) {
        if($lang == 'content_er') {
            foreach ($this->germanMonth as $key => $value) {
                if (strstr($date,  $key))  {               
                    $str = str_replace(
                        array($key),
                        array(
                            $value
                        ),
                        $date
                    );
                    return $str;
                }
            }
        }elseif($lang == 'content_fr') {
            foreach ($this->frenchMonth as $key => $value) {
                if (strstr($date,  $key))  {               
                    $str = str_replace(
                        array($key),
                        array(
                            $value
                        ),
                        $date
                    );
                    return $str;
                }
            }
        }
        return $date;
    }

    public function report($id) {
        $this->NewReports = TableRegistry::get('NewReports');
        $newReport = $this->NewReports->get($id);

        $placeholders = [];
        // getting the last answer to get the report uuid
        $this->Answers= TableRegistry::get('Answers');
        $lastAnswer = $this->Answers->find()
            ->where([
                'Answers.organization_user_id' => $this->request->Session()->read('Auth.User.id')
            ])
            ->order(['id' => 'DESC'])
            ->first();
        // get system placeholder
        $systemPlaceholders = $this->__getValueOfSystemPlacholders($id);
        $placeholders = array_merge($placeholders, $systemPlaceholders);
        // getting normal placeholder
        $normalPlaceholder = $this->__getValueOfNormalPlacehoders($id);
        $placeholders = array_merge($placeholders, $normalPlaceholder);

        // getting text only placeholder
        $textOnlyPlaceholder = $this->__getValueOfTextOnlyPlacehoders($id);
        $placeholders = array_merge($placeholders, $textOnlyPlaceholder);
        
        // getting licence only placeholder
        $licencePlaceholder = $this->__getValueOfLicencePlacehoders($id, $lastAnswer->report_uuid);
        $placeholders = array_merge($placeholders, $licencePlaceholder);

        // getting Legal form type placeholder
        $LegaFormPlaceholder = $this->__getLegaFormTypePlacehoders($id, $lastAnswer->report_uuid);
        $placeholders = array_merge($placeholders, $LegaFormPlaceholder);
        // getting Legal form type placeholder
        $signatureGenderDependentPlaceholder = $this->__getSignatureDependentPlacehoders($id, $lastAnswer->report_uuid);
        $placeholders = array_merge($placeholders, $signatureGenderDependentPlaceholder);
            
        $answers    = $this->Answers->find()
            ->where([
                'Answers.organization_user_id' =>$this->request->Session()->read('Auth.User.id'),
                'Answers.report_uuid'   => $lastAnswer->report_uuid
            ])
            ->innerJoinWith('NewPlaceholders', function($q) use ($id) {
                return $q->where(['NewPlaceholders.new_report_id' => $id]);
            })
            ->innerJoinWith('NewQuestions', function($q) use ($id) {
                return $q->where(['NewQuestions.new_question_type_id <> ' => 3]);
            })
            ->contain([
                'NewPlaceholders', 
                'NewQuestions',
                'NewQuestionOptions'
            ])
            ->order(['Answers.created' => 'ASC']);

        $multiselectanswers    = $this->Answers->find()
            ->where([
                'Answers.organization_user_id' =>$this->request->Session()->read('Auth.User.id'),
                'Answers.report_uuid'   => $lastAnswer->report_uuid
            ])
            ->innerJoinWith('NewPlaceholders', function($q) use ($id) {
                return $q->where(['NewPlaceholders.new_report_id' => $id]);
            })
            ->innerJoinWith('NewQuestions', function($q) {
                return $q->where(['NewQuestions.new_question_type_id' => 3]);
            })
            ->contain([
                'NewPlaceholders', 
                'NewQuestions',
                'NewQuestionOptions'
            ])
            ->order(['Answers.created' => 'ASC']);

        $multiselectanswers = $this->getValueOfMultiseletAnswer($multiselectanswers);
        $placeholders   = array_merge($placeholders, $multiselectanswers);
        // getting placeholders of questions
        $answers    = $this->__getValueOfPlaceholders($answers);
        $placeholders   = array_merge($placeholders, $answers);
        // getting cc placeholder
        $ccPlaceholder = $this->__getValueOfCcPlaceholder($id);
        $placeholders = array_merge($placeholders, $ccPlaceholder);
        
        $keys   = array_keys($placeholders);
        $values     = array_values($placeholders);
        // getting body with replaced placeholder value
        $report     = $str  = str_replace(
            $keys,                
            $values,
            $newReport[$this->language]
        );
        $report     = $this->__recursivelyReplacePlaceholders($report, $placeholders);
        $report = str_replace(['{{LOGO}}'], [$this->getOrganizatonLogo()], $report);
        // Getting header with replaced placeholder value
        $header     = str_replace(
            $keys,                
            $values,
            $newReport['header']
        );
        $header     = $this->__recursivelyReplacePlaceholders($header, $placeholders);
        $header     = str_replace(['{{LOGO}}'], [$this->getOrganizatonLogo()], $header);
        // gettinf footer with replace value
        $footer     = str_replace(
            $keys,                
            $values,
            $newReport['footer']
        );
        $footer     = $this->__recursivelyReplacePlaceholders($footer, $placeholders);
        $footer = str_replace(['{{LOGO}}'], [$this->getOrganizatonLogo()], $footer);

        return [
            'header'    => $header,
            'footer'    => $footer,
            'report'    => $report ,
            'company'   => (!empty($ccPlaceholder) && $ccPlaceholder['[CCName]'])
                ? $ccPlaceholder['[CCName]']
                : ''
        ];
    }

    protected function __getIncharegeReplacement ($data) {
        $this->OrganizationUsers= TableRegistry::get('OrganizationUsers');
        $user = $this->OrganizationUsers->getOrganizationUsersForGender($this->request->Session()->read('Auth.User.organization.id'));
        $user  = $user->toArray();
        $placeholder = $this->NewPlaceholders->find('list', [
                'keyField' => 'id',
                'valueField'    => 'id'
            ])
            ->where([
                'new_report_id' => $data->new_placeholder->new_report_id,
                'name IN' => ['[FirstSignature]', '[SecondSignature]']
            ])
            ->contain([
                'NewQuestions'
            ]);
        if(!empty($placeholder->toArray()) ) {
            // getting the answer from the paceholder
            $answer = $this->Answers->find()
                ->where([
                    'new_placeholder_id IN'    => $placeholder->toArray(),
                    'organization_user_id'  => $this->request->Session()->read('Auth.User.id'),
                    'report_uuid' => $data->report_uuid
                ])
                ->contain(['NewPlaceholders']);
            $signatures = [];
            if(!empty($answer->toArray())) {
                $this->OrganizationUsers = TableRegistry::get('OrganizationUsers');
                foreach ($answer as $key => $value) {                   
                    $signatures[$value->new_placeholder->name] = $user[$value->answer];
                }
            }
        }
        // pr($signatures);die;
        // return empty answer that is incharge is not checked return empty
        if(in_array($data->new_placeholder->name, ['[FirstInCharge]', '[SecondInCharge]']) && empty($data->answer)) {
            $replementText = '';
        // if langauge is german check the gender of corresponding Signature([[FirstInCharge] => '[FirstSignature]'])
        }else if($this->language== 'content_en') {
            $replementText = 'Auditor in Charge';
        }else if($this->language== 'content_fr') {
            $replementText = 'Réviseur responsable';                
        } else if($this->language== 'content_er') {
            if($data->new_placeholder->name == '[FirstInCharge]' && !empty($signatures['[FirstSignature]'])) {
                $replementText = ($signatures['[FirstSignature]'] == 1) ? 'Leitender Revisor' : 'Leitende Revisorin' ;
            } elseif ($data->new_placeholder->name == '[SecondInCharge]' && !empty($signatures['[SecondSignature]'])) {
                $replementText = ($signatures['[SecondSignature]'] == 1) ? 'Leitender Revisor' : 'Leitende Revisorin' ;
            }
        }
        return $replementText;
    }

    protected function __getValueOfPlaceholders($data) {
        $placeholders = [];
        // pr($data->toArray());die;
        foreach ($data as $key => $value) {
            if($value->has('new_question_option')) {
                if(!empty($value->new_placeholder)) {
                    $placeholders[$value->new_placeholder->name] = $value->new_question_option[$this->language];
                }
            }
            if(empty($value->new_question_option_id) && $value->new_question->new_question_type_id == 4) {
                // pr($value);
                $placeholders[$value->new_placeholder->name] = $this->__getIncharegeReplacement($value);
            }elseif(empty($value->new_question_option_id) && $value->new_question->new_question_type_id == 8) {
                // pr($value);
                if($this->language == 'content_er') {
                    $this->dateFormatt = 'j. F Y';
                }else {
                    $this->dateFormatt = 'j F Y';
                }
                
                $placeholders[$value->new_placeholder->name] = $this->translateMonth(date($this->dateFormatt, strtotime(str_replace('/', '-', $value->answer))), $this->language);
            }else if(!empty($value->answer)) { 
                if(!empty($value->new_placeholder)) {
                    $placeholders[$value->new_placeholder->name] = $value->answer;
                }              
            }        
        }
        // pr($placeholders);die;
        return $placeholders;
    }

    function getValueOfMultiseletAnswer($data) {
        $placeholders = [];
        foreach ($data as $key => $value) {
            if(!empty($value->answer) && !empty($value->new_placeholder)) { 
                $options = explode(',', $value->answer);
                $this->NewQuestionOptions = TableRegistry::get('NewQuestionOptions');
                $options = $this->NewQuestionOptions->find('list', [
                    'valueField' => $this->language
                ])
                ->where(['id IN' => $options])
                ->toArray();

                $options = array_filter($options);
                if(!empty($options)) {
                    $placeholders[$value->new_placeholder->name] = implode('<br/><br/>', $options);
                }
            }        
        }
        return $placeholders;
    }

    protected function __getValueOfCcPlaceholder($id) {
        $this->Answers = TableRegistry::get('Answers');
        $this->NewPlaceholders = TableRegistry::get('NewPlaceholders');
        $placeholders = [];
        $placeholder = $this->NewPlaceholders->find()
            ->where([
                'new_report_id' => $id,
                'name' => '[CCName]'
            ])
            ->contain([
                'NewQuestions'
            ])->first();

        if(!empty($placeholder) && !empty($placeholder->new_questions)) {
            $answer = $this->Answers->find()
                ->where([
                    'new_question_id'       => $placeholder->new_questions[0]->id,
                    'new_placeholder_id'    => $placeholder->id,
                    'organization_user_id'  => $this->request->Session()->read('Auth.User.id')
                ])
                ->order(['created' => 'DESC'])->first();
            if(!empty($answer)) {
                $placeholders = $this->__getCcValues($answer->answer);
            }
        }
        return $placeholders;
    }

    protected function __getCcValues($company_id) {
        $this->OrganizationCompanies = TableRegistry::get('OrganizationCompanies');
        $company = $this->OrganizationCompanies->find()
            ->where([
                'OrganizationCompanies.id'  => $company_id
            ])
            ->innerJoinWith('LegalFormTypes')
            ->leftJoinWith('AccountingFrameworkTypes')
            ->contain(['LegalFormTypes', 'AccountingFrameworkTypes'])
            ->first();
        
        if(!empty($company)) {
            return [
                '[CCName]' => $company->company_name,
                '[CCAdress]'    => $company->street,
                '[CCAdressNo]'    => $company->street_number,
                '[CCPlace]'    => $company->place,
                '[CCZip]'    => $company->zip,
                '[CCPlaceIncorp]'    => $company->place_of_incorporation,
                '[CCLegalForm]'    => (!empty($company->legal_form_type))
                    ?   $company->legal_form_type->name
                    :   '',
                '[CCAccFrame]'    => (!empty($company->accounting_framework_type))
                    ?   $company->accounting_framework_type->name
                    :   '',
            ];
        }
        return [];
    }

    protected function __getValueOfSystemPlacholders($id) {
        $this->NewPlaceholders = TableRegistry::get('NewPlaceholders');
        $this->OrganizationUsers = TableRegistry::get('OrganizationUsers');
        $this->OrganizationSubsidiaries = TableRegistry::get('OrganizationSubsidiaries');
        $placeholders = [];
        // Setting organization detail for replacement
        $data['organization'] = $this->request->Session()->read('Auth.User.organization');
        // Setting Subsidiary details
        $data['subsidiary'] = $this->__getSubsidiaryDetail();
        // get list of system placeholders
        $data['company'] = $this->__getCompanyDetail();
        // get list of system placeholders
        $systemPlaceholder = $this->NewPlaceholders->find('list', [
                'keyField' => 'name',
                'valueField' => 'content_en'
            ])
            ->where([
                'NewPlaceholders.new_placeholder_type_id' => 1,
                'NewPlaceholders.new_report_id' => $id
            ]);

        foreach ($systemPlaceholder as $key => $value) {
            // getting model and field part from _ seprated string
            $match = explode('_', $value);
            if(in_array($match[0], ['company', 'organization', 'subsidiary'])) {
                $model = $match[0];
            } else {
                $model = 'organization';
            }
            unset($match[0]);
            $field = implode('_', $match);
            // setting placeholder replacement corresponding to their type company, orgni.., subsidi..
            if(!empty($data[$model]) && !empty($data[$model][$field])) {
                $placeholders[$key] = $data[$model][$field];
            } else {
                // if not found set default to organization
                $placeholders[$key] = $this->request->Session()->read('Auth.User.organization.'.$field);
            }
        }
        return $placeholders;
    }

    protected function __getValueOfNormalPlacehoders($id) {
        $this->NewPlaceholders = TableRegistry::get('NewPlaceholders');
        $placeholders = [];
        $normalPlaceholder = $this->NewPlaceholders->find('list', [
            'keyField' => 'name',
            'valueField' => $this->language
        ])
        ->where([
            'NewPlaceholders.new_placeholder_type_id' => 2,
            'NewPlaceholders.new_report_id' => $id
        ]);
        foreach ($normalPlaceholder as $key => $value) {
            $placeholders[$key] = $value;
        }
        return $placeholders;
    }

    protected function __getValueOfTextOnlyPlacehoders($id) {
        $this->NewPlaceholders = TableRegistry::get('NewPlaceholders');
        $placeholders = [];
        $textOnlyPlaceholder = $this->NewPlaceholders->find('list', [
            'keyField' => 'name',
            'valueField' => $this->language
        ])
        ->where([
            'NewPlaceholders.new_placeholder_type_id' => 5,
            'NewPlaceholders.new_report_id' => $id
        ]);
        foreach ($textOnlyPlaceholder as $key => $value) {
            $placeholders[$key] = $value;
        }
        return $placeholders;
    }

    protected function __getLegaFormTypePlacehoders($id, $report_uuid) {
        $this->NewPlaceholders = TableRegistry::get('NewPlaceholders');
        $placeholders = [];
        // getting company paceholder for report
        $placeholder = $this->NewPlaceholders->find()
            ->where([
                'new_report_id' => $id,
                'name' => '[CCName]'
            ])
            ->contain([
                'NewQuestions'
            ])->first();

        if(!empty($placeholder) ) {
            // getting the company from the Ccname paceholder
            $answer = $this->Answers->find()
                ->where([
                    'new_question_id'       => $placeholder->new_questions[0]->id,
                    'new_placeholder_id'    => $placeholder->id,
                    'organization_user_id'  =>  $this->request->Session()->read('Auth.User.id'),
                    'report_uuid' => $report_uuid
                ])
                ->order(['created' => 'DESC'])->first();

            if(!empty($answer)) {
                $company = $this->OrganizationCompanies->find()
                ->where([
                    'OrganizationCompanies.id'  => $answer->answer
                ])
                ->innerJoinWith('LegalFormTypes')
                ->leftJoinWith('AccountingFrameworkTypes')
                ->contain(['LegalFormTypes', 'AccountingFrameworkTypes'])
                ->first();
            }
        }
        if(!empty($company)) {
            $formType = $company->legal_form_type_id;
        }else {
           $formType = 1; 
        }
        // getting all the Legalform dependent placeholder of report.
        $legalFormPlaceholders = $this->NewPlaceholders->find('list', [
            'keyField' => 'name',
            'valueField' => 'id'
        ])
        ->where([
            'NewPlaceholders.new_placeholder_type_id' => 6,
            'NewPlaceholders.new_report_id' => $id
        ]);
        // getting replement of placeholders for a report
        $this->LegalFormReplacements = TableRegistry::get('LegalFormReplacements');
        // pr($legalFormPlaceholders->toArray());die;
        foreach ($legalFormPlaceholders as $key => $value) {
            $formPlaceholder = $this->LegalFormReplacements->find()
                ->where([
                    'legal_form_type_id'   => $formType,
                    'new_placeholder_id' => $value,
                    'new_report_id' => $id
                ])
                ->first();
            $placeholders[$key] = $formPlaceholder[$this->language];
        }
        // pr($placeholders);die;
        return $placeholders;
    }

    protected function __getSignatureDependentPlacehoders($id, $report_uuid) {
        $this->NewPlaceholders = TableRegistry::get('NewPlaceholders');
        $placeholders = [];
        // getting signature paceholder for report
        $placeholder = $this->NewPlaceholders->find('list', [
                'keyField' => 'id',
                'valueField'    => 'id'
            ])
            ->where([
                'new_report_id' => $id,
                'name IN' => ['[FirstSignature]', '[SecondSignature]']
            ])
            ->contain([
                'NewQuestions'
            ]);

        if(!empty($placeholder->toArray()) ) {
            // getting the answer from the paceholder
            $answer = $this->Answers->find()
                ->where([
                    'new_placeholder_id IN'    => $placeholder->toArray(),
                    'organization_user_id'  =>  $this->request->Session()->read('Auth.User.id'),
                    'report_uuid' => $report_uuid
                ])
                ->contain(['NewPlaceholders']);
            $signatures = [];
            if(!empty($answer->toArray())) {
                $this->OrganizationUsers = TableRegistry::get('OrganizationUsers');
                foreach ($answer as $key => $value) {
                    $user = $this->OrganizationUsers->getOrganizationUsersForGender( $this->request->Session()->read('Auth.User.organization.id'));
                    $user  = $user->toArray();
                    $signatures[$value->new_placeholder->name] = $user[$value->answer];
                }
            }
        }
        // getting all the Legalform dependent placeholder of report.
        $genderDependendPlaceholders = $this->NewPlaceholders->find('list', [
            'keyField' => 'name',
            'valueField' => 'id'
        ])
        ->where([
            'NewPlaceholders.new_placeholder_type_id' => 7,
            'NewPlaceholders.new_report_id' => $id
        ]);
        // getting replement of placeholders for a report
        $this->SignatureGenderReplacements = TableRegistry::get('SignatureGenderReplacements');
        // pr($legalFormPlaceholders->toArray());die;
        $formPlaceholder = $this->SignatureGenderReplacements->find('list', [
            'keyField'      => 'new_placeholder_id',
            'valueField'    => $this->language
        ]);
        $is_first = true;
        if(empty($signatures)) {
            return [];
        }
        foreach ($signatures as $key => $value) {
            if($is_first) {
                $formPlaceholder = $formPlaceholder->where([
                    'new_report_id' => $id,
                    'signature'    => $key,
                    'gender'    => $value
                ]);

                $is_first   = false;
            }else {                
                $formPlaceholder = $formPlaceholder->orwhere([
                    'new_report_id' => $id,
                    'signature'    => $key,
                    'gender'    => $value
                ]);
            }
        }

        $formPlaceholder = $formPlaceholder->toArray();
        foreach ($genderDependendPlaceholders as $key => $value) {
            if(!empty($formPlaceholder[$value])) {

                $placeholders[$key] = $formPlaceholder[$value];
            }else{

                $placeholders[$key] = '';
            }
        }
        // pr($placeholders);die;
        return $placeholders;
    }

    protected function __getValueOfLicencePlacehoders($id, $report_uuid) {
        $this->NewPlaceholders= TableRegistry::get('NewPlaceholders');
        $placeholders = [];
        // getting signature paceholder for report
        $placeholder = $this->NewPlaceholders->find('list', [
                'keyField' => 'id',
                'valueField'    => 'id'
            ])
            ->where([
                'new_report_id' => $id,
                'name IN' => ['[FirstSignature]', '[SecondSignature]']
            ])
            ->contain([
                'NewQuestions'
            ]);
        if(!empty($placeholder->toArray()) ) {
            // getting the answer from the paceholder
            $answer = $this->Answers->find()
                ->where([
                    'new_placeholder_id IN'    => $placeholder->toArray(),
                    'organization_user_id'  => $this->request->Session()->read('Auth.User.id'),
                    'report_uuid' => $report_uuid
                ])
                ->contain(['NewPlaceholders']);
            $signatures = [];
            if(!empty($answer->toArray())) {
                $this->OrganizationUsers = TableRegistry::get('OrganizationUsers');
                foreach ($answer as $key => $value) {
                    // echo $this->language;die;
                    $user = $this->OrganizationUsers->getOrganizationUsersLicence($this->request->Session()->read('Auth.User.organization.id'), $this->language);
                    $user  = $user->toArray();
                    $signatures[$value->new_placeholder->name] = $user[$value->answer];
                }
            }
        }

        // getting all the Legalform dependent placeholder of report.
        $licensePlaceholders = $this->NewPlaceholders->find('list', [
            'keyField' => 'name',
            'valueField' => 'id'
        ])
        ->where([
            'NewPlaceholders.new_placeholder_type_id' => 8,
            'NewPlaceholders.new_report_id' => $id
        ]);
        foreach ($licensePlaceholders as $key => $value) {
            if($key == '[FirstLicence]') {
                $placeholders[$key] = (!empty($signatures['[FirstSignature]'])) 
                    ? $signatures['[FirstSignature]']
                    : '';
            }elseif ($key == '[SecondLicence]') {
                $placeholders[$key] = (!empty($signatures['[SecondSignature]'])) 
                    ? $signatures['[SecondSignature]']
                    : '';
            }
        }
        return $placeholders;
    }
    protected function __getSubsidiaryDetail() {
        if(!empty($this->request->Session()->read('Auth.User.organization_user'))) {
            $userSubsidiary = $this->OrganizationSubsidiaries->get(
                $this->request->Session()->read('Auth.User.organization_user.organization_subsidiary_id')); 
            if(!empty($userSubsidiary)){
                return $userSubsidiary->toArray();
            }
            return [];

        } else {
            $organizationDefaultSubsidiary = $this->OrganizationSubsidiaries->find()
                ->where([
                    'OrganizationSubsidiaries.organization_id' => $this->request->Session()->read('Auth.User.organization.id'),
                    'OrganizationSubsidiaries.is_primary' => 1
                ])
                ->first(); 
            if(!empty($organizationDefaultSubsidiary)) {
                return $organizationDefaultSubsidiary->toArray();
            }
            return [];
        }
    }

    protected function __getCompanyDetail() {
        $this->OrganizationCompanies = TableRegistry::get('OrganizationCompanies');
        $organizationCompany = $this->OrganizationCompanies->find()
            ->where([
                'OrganizationCompanies.organization_id' => $this->request->Session()->read('Auth.User.organization.id'),
                'OrganizationCompanies.status' => 1
            ])
            ->first(); 
        if(!empty($organizationCompany)) {
            return $organizationCompany->toArray();
        }
        return [];
    }

    protected function __recursivelyReplacePlaceholders($template, $allPlaceholders) {
        $placeholders = $this->__getTemplatePlacholder($template);
        if(!empty($placeholders)) {
            $template   =   $this->__replacePlaceholders($template, $placeholders, $allPlaceholders);
            $template   =   $this->__recursivelyReplacePlaceholders($template, $allPlaceholders);
            return $template;
        }
        return $template;
    }

    protected function __getTemplatePlacholder($temp) {
        preg_match_all('#\[(.*?)\]#', $temp, $match);
        return $match[0];
    }

    protected function __replacePlaceholders($template, $placeholders, $allPlaceholders) {
        foreach ($placeholders as $key => $value) {
            $template = str_replace(
                $value,                
               ( !empty($allPlaceholders[$value])) ? $allPlaceholders[$value] : '',
                $template
            );
        }
        return $template;         
    }

    public function getOrganizatonLogo() {

        $image = $this->__companylogo(
            'organizations', 
            'logo', 
            $this->request->Session()->read('Auth.User.organization.logo_dir'), 
            'square_', 
            $this->request->Session()->read('Auth.User.organization.logo')
        );
        $image = Router::url($image,true);
        //$imageTag = '<img src="'.$image.'" class="logo" />';
        $imageTag = '<img src="'.$image.'" class="logo" style="height:50px; width:200px;" />';
        return $imageTag;
    }

    protected function __companylogo ($table, $field, $dir, $prefix, $image) {
        $filePath = 'logo' . DS . $table . DS . $field . DS . $dir . DS . $prefix . $image;
        if (file_exists ( WWW_ROOT.$filePath )) {
            return DS . $filePath;
        } else {
            return DS . 'img' . DS . 'logo.png';
        }
    } 

}
