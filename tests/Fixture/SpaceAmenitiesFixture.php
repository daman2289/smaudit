<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SpaceAmenitiesFixture
 *
 */
class SpaceAmenitiesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'space_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'amenity_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'space_id' => ['type' => 'index', 'columns' => ['space_id'], 'length' => []],
            'amenity_id' => ['type' => 'index', 'columns' => ['amenity_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'space_amenities_ibfk_1' => ['type' => 'foreign', 'columns' => ['space_id'], 'references' => ['spaces', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'space_amenities_ibfk_2' => ['type' => 'foreign', 'columns' => ['amenity_id'], 'references' => ['amenities', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'space_id' => 1,
            'amenity_id' => 1,
            'created' => '2017-03-07 06:00:11',
            'modified' => '2017-03-07 06:00:11'
        ],
    ];
}
