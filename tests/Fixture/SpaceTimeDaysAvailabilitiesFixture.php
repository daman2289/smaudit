<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SpaceTimeDaysAvailabilitiesFixture
 *
 */
class SpaceTimeDaysAvailabilitiesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'space_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'monday' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '1', 'comment' => '0 => close, 1 => open', 'precision' => null],
        'tuesday' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '1', 'comment' => '0 => close, 1 => open', 'precision' => null],
        'wednesday' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '1', 'comment' => '0 => close, 1 => open', 'precision' => null],
        'thursday' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '1', 'comment' => '0 => close, 1 => open', 'precision' => null],
        'friday' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '1', 'comment' => '0 => close, 1 => open', 'precision' => null],
        'saturday' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '1', 'comment' => '0 => close, 1 => open', 'precision' => null],
        'sunday' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '1', 'comment' => '0 => close, 1 => open', 'precision' => null],
        'is_24_hours_open' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '1', 'comment' => '0 => no, 1 => yes', 'precision' => null],
        'open_time' => ['type' => 'time', 'length' => null, 'null' => true, 'default' => null, 'comment' => 'if 24 hours not open, then this is start time of space available', 'precision' => null],
        'close_time' => ['type' => 'time', 'length' => null, 'null' => true, 'default' => null, 'comment' => 'if 24 hours not open, then this is close time of space available', 'precision' => null],
        'is_parking_closed_during_certain_hours' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '0 => no, 1 => yes', 'precision' => null],
        'parking_closed_start_time' => ['type' => 'time', 'length' => null, 'null' => true, 'default' => null, 'comment' => 'space closed start time', 'precision' => null],
        'parking_closed_close_time' => ['type' => 'time', 'length' => null, 'null' => true, 'default' => null, 'comment' => 'space closed end time', 'precision' => null],
        'is_available_during_certain_date_range' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '0 => always, 1 => available in a cetain date range', 'precision' => null],
        'start_date' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'end_date' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'space_id' => ['type' => 'index', 'columns' => ['space_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'space_time_days_availabilities_ibfk_1' => ['type' => 'foreign', 'columns' => ['space_id'], 'references' => ['spaces', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'space_id' => 1,
            'monday' => 1,
            'tuesday' => 1,
            'wednesday' => 1,
            'thursday' => 1,
            'friday' => 1,
            'saturday' => 1,
            'sunday' => 1,
            'is_24_hours_open' => 1,
            'open_time' => '06:03:19',
            'close_time' => '06:03:19',
            'is_parking_closed_during_certain_hours' => 1,
            'parking_closed_start_time' => '06:03:19',
            'parking_closed_close_time' => '06:03:19',
            'is_available_during_certain_date_range' => 1,
            'start_date' => '2017-03-07',
            'end_date' => '2017-03-07'
        ],
    ];
}
