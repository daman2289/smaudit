<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ReportsFixture
 *
 */
class ReportsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'organization_company_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => 'user who created- i.e created by', 'precision' => null, 'autoIncrement' => null],
        'report_type_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => '1', 'comment' => 'relation with report_types table', 'precision' => null, 'autoIncrement' => null],
        'report_date' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'financial_year_from' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'financial_year_to' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'financial_year' => ['type' => 'string', 'length' => null, 'null' => false, 'default' => null, 'collate' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'short_long' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '1->yes, 0->no', 'precision' => null],
        'interim_statement' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '1->yes, 0->no', 'precision' => null],
        'first_year_audit' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '1->yes, 0->no', 'precision' => null],
        'opting_out' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => null, 'comment' => '1->yes, 0->no', 'precision' => null],
        'date_prior_audit_report' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'opinion_type_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => '1', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'qualified_opinion' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => null, 'comment' => '1->yes, 0->no', 'precision' => null],
        'qualification' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_bin', 'comment' => '', 'precision' => null, 'fixed' => null],
        'assistance_book_keeping' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '1->yes, 0->no', 'precision' => null],
        'appropriation_available_earning' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '1->yes, 0->no', 'precision' => null],
        'audit_terminated_later_than_report' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '1->yes, 0->no', 'precision' => null],
        'audit_terminated_date' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'emphasis_of_matter_ids' => ['type' => 'string', 'length' => 50, 'null' => false, 'default' => null, 'collate' => 'utf8_bin', 'comment' => 'comma separated for multiple selection', 'precision' => null, 'fixed' => null],
        'subordination_amount' => ['type' => 'decimal', 'length' => 10, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'going_concern_note_no' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'amount_shareholder_loan' => ['type' => 'decimal', 'length' => 10, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'modified_opinion_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => 'relation with modified_opinions table', 'precision' => null, 'autoIncrement' => null],
        'basis_for_modification' => ['type' => 'text', 'length' => null, 'null' => false, 'default' => null, 'collate' => 'utf8_bin', 'comment' => '', 'precision' => null],
        'first_signature_user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'second_signature_user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'organization_company_id' => ['type' => 'index', 'columns' => ['organization_company_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'MyISAM',
            'collation' => 'utf8_bin'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'organization_company_id' => 1,
            'user_id' => 1,
            'report_type_id' => 1,
            'report_date' => '2017-03-22 09:52:16',
            'financial_year_from' => '2017-03-22',
            'financial_year_to' => '2017-03-22',
            'financial_year' => 'Lorem ipsum dolor sit amet',
            'short_long' => 1,
            'interim_statement' => 1,
            'first_year_audit' => 1,
            'opting_out' => 1,
            'date_prior_audit_report' => '2017-03-22',
            'opinion_type_id' => 1,
            'qualified_opinion' => 1,
            'qualification' => 'Lorem ipsum dolor sit amet',
            'assistance_book_keeping' => 1,
            'appropriation_available_earning' => 1,
            'audit_terminated_later_than_report' => 1,
            'audit_terminated_date' => '2017-03-22',
            'emphasis_of_matter_ids' => 'Lorem ipsum dolor sit amet',
            'subordination_amount' => 1.5,
            'going_concern_note_no' => 1,
            'amount_shareholder_loan' => 1.5,
            'modified_opinion_id' => 1,
            'basis_for_modification' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'first_signature_user_id' => 1,
            'second_signature_user_id' => 1,
            'created' => '2017-03-22 09:52:16',
            'modified' => '2017-03-22 09:52:16'
        ],
    ];
}
