<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\ReportPlaceholderComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\ReportPlaceholderComponent Test Case
 */
class ReportPlaceholderComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Controller\Component\ReportPlaceholderComponent
     */
    public $ReportPlaceholder;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->ReportPlaceholder = new ReportPlaceholderComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ReportPlaceholder);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
