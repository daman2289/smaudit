<?php
namespace App\Test\TestCase\Controller;

use App\Controller\OrganizationAdditionalUserSubscriptionPlansController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\OrganizationAdditionalUserSubscriptionPlansController Test Case
 */
class OrganizationAdditionalUserSubscriptionPlansControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.organization_additional_user_subscription_plans',
        'app.organizations',
        'app.users',
        'app.roles',
        'app.organization_users',
        'app.licenses',
        'app.organization_subsidiaries',
        'app.social_profiles',
        'app.organization_companies',
        'app.legal_form_types',
        'app.accounting_framework_types',
        'app.place_holders',
        'app.reports',
        'app.report_types',
        'app.opinion_types',
        'app.modified_opinions',
        'app.additional_user_subscription_plans'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
