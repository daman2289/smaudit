<?php
namespace App\Test\TestCase\Controller;

use App\Controller\NewReportsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\NewReportsController Test Case
 */
class NewReportsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.new_reports',
        'app.new_placeholders',
        'app.new_placeholder_types',
        'app.new_questions',
        'app.new_question_types',
        'app.new_question_options',
        'app.new_report_placeholders'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
