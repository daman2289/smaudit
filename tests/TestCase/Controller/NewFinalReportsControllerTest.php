<?php
namespace App\Test\TestCase\Controller;

use App\Controller\NewFinalReportsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\NewFinalReportsController Test Case
 */
class NewFinalReportsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.new_final_reports',
        'app.new_reports',
        'app.new_placeholders',
        'app.new_placeholder_types',
        'app.new_questions',
        'app.new_question_types',
        'app.new_question_options',
        'app.new_report_placeholders',
        'app.users',
        'app.roles',
        'app.organizations',
        'app.organization_companies',
        'app.legal_form_types',
        'app.accounting_framework_types',
        'app.place_holders',
        'app.reports',
        'app.report_types',
        'app.opinion_types',
        'app.modified_opinions',
        'app.organization_subsidiaries',
        'app.organization_users',
        'app.licenses',
        'app.social_profiles'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
