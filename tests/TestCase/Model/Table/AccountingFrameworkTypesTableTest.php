<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AccountingFrameworkTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AccountingFrameworkTypesTable Test Case
 */
class AccountingFrameworkTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AccountingFrameworkTypesTable
     */
    public $AccountingFrameworkTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.accounting_framework_types',
        'app.organization_companies',
        'app.organizations',
        'app.users',
        'app.roles',
        'app.organization_users',
        'app.licenses',
        'app.organization_subsidiaries',
        'app.social_profiles',
        'app.legal_form_types',
        'app.reports',
        'app.report_types',
        'app.opinion_types',
        'app.modified_opinions',
        'app.place_holders'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AccountingFrameworkTypes') ? [] : ['className' => 'App\Model\Table\AccountingFrameworkTypesTable'];
        $this->AccountingFrameworkTypes = TableRegistry::get('AccountingFrameworkTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AccountingFrameworkTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
