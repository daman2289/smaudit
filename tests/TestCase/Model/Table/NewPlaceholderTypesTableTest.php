<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NewPlaceholderTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NewPlaceholderTypesTable Test Case
 */
class NewPlaceholderTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NewPlaceholderTypesTable
     */
    public $NewPlaceholderTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.new_placeholder_types',
        'app.new_placeholders'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('NewPlaceholderTypes') ? [] : ['className' => 'App\Model\Table\NewPlaceholderTypesTable'];
        $this->NewPlaceholderTypes = TableRegistry::get('NewPlaceholderTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NewPlaceholderTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
