<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NewReportPlaceholdersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NewReportPlaceholdersTable Test Case
 */
class NewReportPlaceholdersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NewReportPlaceholdersTable
     */
    public $NewReportPlaceholders;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.new_report_placeholders',
        'app.new_reports'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('NewReportPlaceholders') ? [] : ['className' => 'App\Model\Table\NewReportPlaceholdersTable'];
        $this->NewReportPlaceholders = TableRegistry::get('NewReportPlaceholders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NewReportPlaceholders);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
