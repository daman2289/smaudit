<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NewFinalReportsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NewFinalReportsTable Test Case
 */
class NewFinalReportsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NewFinalReportsTable
     */
    public $NewFinalReports;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.new_final_reports',
        'app.new_reports',
        'app.new_placeholders',
        'app.new_placeholder_types',
        'app.new_questions',
        'app.new_question_types',
        'app.new_question_options',
        'app.new_report_placeholders',
        'app.users',
        'app.roles',
        'app.organizations',
        'app.organization_companies',
        'app.legal_form_types',
        'app.accounting_framework_types',
        'app.place_holders',
        'app.reports',
        'app.report_types',
        'app.opinion_types',
        'app.modified_opinions',
        'app.organization_subsidiaries',
        'app.organization_users',
        'app.licenses',
        'app.social_profiles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('NewFinalReports') ? [] : ['className' => 'App\Model\Table\NewFinalReportsTable'];
        $this->NewFinalReports = TableRegistry::get('NewFinalReports', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NewFinalReports);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
