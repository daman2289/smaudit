<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AdditionalUserSubscriptionPlansTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AdditionalUserSubscriptionPlansTable Test Case
 */
class AdditionalUserSubscriptionPlansTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AdditionalUserSubscriptionPlansTable
     */
    public $AdditionalUserSubscriptionPlans;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.additional_user_subscription_plans',
        'app.stripe_plans',
        'app.organization_additional_user_subscription_plans'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AdditionalUserSubscriptionPlans') ? [] : ['className' => 'App\Model\Table\AdditionalUserSubscriptionPlansTable'];
        $this->AdditionalUserSubscriptionPlans = TableRegistry::get('AdditionalUserSubscriptionPlans', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AdditionalUserSubscriptionPlans);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
