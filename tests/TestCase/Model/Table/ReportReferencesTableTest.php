<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReportReferencesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReportReferencesTable Test Case
 */
class ReportReferencesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ReportReferencesTable
     */
    public $ReportReferences;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.report_references',
        'app.report_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ReportReferences') ? [] : ['className' => 'App\Model\Table\ReportReferencesTable'];
        $this->ReportReferences = TableRegistry::get('ReportReferences', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ReportReferences);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
