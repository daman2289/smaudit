<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LseOthersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LseOthersTable Test Case
 */
class LseOthersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LseOthersTable
     */
    public $LseOthers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.lse_others'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('LseOthers') ? [] : ['className' => 'App\Model\Table\LseOthersTable'];
        $this->LseOthers = TableRegistry::get('LseOthers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LseOthers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
