<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NewReportsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NewReportsTable Test Case
 */
class NewReportsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NewReportsTable
     */
    public $NewReports;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.new_reports',
        'app.new_placeholders',
        'app.new_placeholder_types',
        'app.new_questions',
        'app.new_question_types',
        'app.new_question_options',
        'app.new_report_placeholders'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('NewReports') ? [] : ['className' => 'App\Model\Table\NewReportsTable'];
        $this->NewReports = TableRegistry::get('NewReports', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NewReports);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
