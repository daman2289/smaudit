<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SpaceAmenitiesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SpaceAmenitiesTable Test Case
 */
class SpaceAmenitiesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SpaceAmenitiesTable
     */
    public $SpaceAmenities;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.space_amenities',
        'app.spaces',
        'app.countries',
        'app.states',
        'app.cities',
        'app.space_contact_infos',
        'app.space_galleries',
        'app.space_pricings',
        'app.space_spot_letters',
        'app.space_stash_houses',
        'app.space_time_days_availabilities',
        'app.amenities'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SpaceAmenities') ? [] : ['className' => 'App\Model\Table\SpaceAmenitiesTable'];
        $this->SpaceAmenities = TableRegistry::get('SpaceAmenities', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SpaceAmenities);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
