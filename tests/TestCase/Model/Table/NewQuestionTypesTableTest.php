<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NewQuestionTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NewQuestionTypesTable Test Case
 */
class NewQuestionTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NewQuestionTypesTable
     */
    public $NewQuestionTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.new_question_types',
        'app.new_questions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('NewQuestionTypes') ? [] : ['className' => 'App\Model\Table\NewQuestionTypesTable'];
        $this->NewQuestionTypes = TableRegistry::get('NewQuestionTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NewQuestionTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
