<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReportHeadersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReportHeadersTable Test Case
 */
class ReportHeadersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ReportHeadersTable
     */
    public $ReportHeaders;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.report_headers',
        'app.organizations',
        'app.users',
        'app.roles',
        'app.organization_users',
        'app.licenses',
        'app.organization_subsidiaries',
        'app.social_profiles',
        'app.organization_companies',
        'app.legal_form_types',
        'app.accounting_framework_types',
        'app.place_holders',
        'app.reports',
        'app.report_types',
        'app.opinion_types',
        'app.modified_opinions',
        'app.info_fields'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ReportHeaders') ? [] : ['className' => 'App\Model\Table\ReportHeadersTable'];
        $this->ReportHeaders = TableRegistry::get('ReportHeaders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ReportHeaders);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
