<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SpacePricingsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SpacePricingsTable Test Case
 */
class SpacePricingsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SpacePricingsTable
     */
    public $SpacePricings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.space_pricings',
        'app.spaces',
        'app.countries',
        'app.states',
        'app.cities',
        'app.space_amenities',
        'app.amenities',
        'app.space_contact_infos',
        'app.space_galleries',
        'app.space_spot_letters',
        'app.space_stash_houses',
        'app.space_time_days_availabilities'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SpacePricings') ? [] : ['className' => 'App\Model\Table\SpacePricingsTable'];
        $this->SpacePricings = TableRegistry::get('SpacePricings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SpacePricings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
