<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WorkInProgressesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WorkInProgressesTable Test Case
 */
class WorkInProgressesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\WorkInProgressesTable
     */
    public $WorkInProgresses;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.work_in_progresses',
        'app.work_in_progress_features',
        'app.work_in_progress'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('WorkInProgresses') ? [] : ['className' => 'App\Model\Table\WorkInProgressesTable'];
        $this->WorkInProgresses = TableRegistry::get('WorkInProgresses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WorkInProgresses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
