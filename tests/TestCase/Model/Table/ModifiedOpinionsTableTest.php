<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ModifiedOpinionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ModifiedOpinionsTable Test Case
 */
class ModifiedOpinionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ModifiedOpinionsTable
     */
    public $ModifiedOpinions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.modified_opinions',
        'app.report_types',
        'app.reports',
        'app.organization_companies',
        'app.organizations',
        'app.users',
        'app.roles',
        'app.organization_users',
        'app.licenses',
        'app.organization_subsidiaries',
        'app.social_profiles',
        'app.legal_form_types',
        'app.accounting_framework_types',
        'app.place_holders',
        'app.opinion_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ModifiedOpinions') ? [] : ['className' => 'App\Model\Table\ModifiedOpinionsTable'];
        $this->ModifiedOpinions = TableRegistry::get('ModifiedOpinions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ModifiedOpinions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
