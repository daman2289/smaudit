<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SpaceTimeDaysAvailabilitiesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SpaceTimeDaysAvailabilitiesTable Test Case
 */
class SpaceTimeDaysAvailabilitiesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SpaceTimeDaysAvailabilitiesTable
     */
    public $SpaceTimeDaysAvailabilities;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.space_time_days_availabilities',
        'app.spaces',
        'app.countries',
        'app.states',
        'app.cities',
        'app.space_amenities',
        'app.amenities',
        'app.space_contact_infos',
        'app.space_galleries',
        'app.space_pricings',
        'app.space_spot_letters',
        'app.space_stash_houses'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SpaceTimeDaysAvailabilities') ? [] : ['className' => 'App\Model\Table\SpaceTimeDaysAvailabilitiesTable'];
        $this->SpaceTimeDaysAvailabilities = TableRegistry::get('SpaceTimeDaysAvailabilities', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SpaceTimeDaysAvailabilities);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
