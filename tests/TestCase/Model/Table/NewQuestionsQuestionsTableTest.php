<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NewQuestionsQuestionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NewQuestionsQuestionsTable Test Case
 */
class NewQuestionsQuestionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NewQuestionsQuestionsTable
     */
    public $NewQuestionsQuestions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.new_questions_questions',
        'app.parent_questions',
        'app.child_questions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('NewQuestionsQuestions') ? [] : ['className' => 'App\Model\Table\NewQuestionsQuestionsTable'];
        $this->NewQuestionsQuestions = TableRegistry::get('NewQuestionsQuestions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NewQuestionsQuestions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
