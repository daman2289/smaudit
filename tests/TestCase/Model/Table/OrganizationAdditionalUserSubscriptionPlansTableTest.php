<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrganizationAdditionalUserSubscriptionPlansTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrganizationAdditionalUserSubscriptionPlansTable Test Case
 */
class OrganizationAdditionalUserSubscriptionPlansTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OrganizationAdditionalUserSubscriptionPlansTable
     */
    public $OrganizationAdditionalUserSubscriptionPlans;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.organization_additional_user_subscription_plans',
        'app.organizations',
        'app.users',
        'app.roles',
        'app.organization_users',
        'app.licenses',
        'app.organization_subsidiaries',
        'app.social_profiles',
        'app.organization_companies',
        'app.legal_form_types',
        'app.accounting_framework_types',
        'app.place_holders',
        'app.reports',
        'app.report_types',
        'app.opinion_types',
        'app.modified_opinions',
        'app.additional_user_subscription_plans',
        'app.stripe_plans',
        'app.stripe_customers',
        'app.stripe_subscriptions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OrganizationAdditionalUserSubscriptionPlans') ? [] : ['className' => 'App\Model\Table\OrganizationAdditionalUserSubscriptionPlansTable'];
        $this->OrganizationAdditionalUserSubscriptionPlans = TableRegistry::get('OrganizationAdditionalUserSubscriptionPlans', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrganizationAdditionalUserSubscriptionPlans);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
