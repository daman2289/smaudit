<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CommissionTaxesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CommissionTaxesTable Test Case
 */
class CommissionTaxesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CommissionTaxesTable
     */
    public $CommissionTaxes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.commission_taxes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CommissionTaxes') ? [] : ['className' => 'App\Model\Table\CommissionTaxesTable'];
        $this->CommissionTaxes = TableRegistry::get('CommissionTaxes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CommissionTaxes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
