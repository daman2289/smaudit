<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrganizationUsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrganizationUsersTable Test Case
 */
class OrganizationUsersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OrganizationUsersTable
     */
    public $OrganizationUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.organization_users',
        'app.users',
        'app.roles',
        'app.organizations',
        'app.organization_companies',
        'app.organization_subsidiaries',
        'app.social_profiles',
        'app.diplomas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OrganizationUsers') ? [] : ['className' => 'App\Model\Table\OrganizationUsersTable'];
        $this->OrganizationUsers = TableRegistry::get('OrganizationUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrganizationUsers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
