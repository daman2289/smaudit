<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FinalReportsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FinalReportsTable Test Case
 */
class FinalReportsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FinalReportsTable
     */
    public $FinalReports;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.final_reports',
        'app.reports',
        'app.organization_companies',
        'app.organizations',
        'app.users',
        'app.roles',
        'app.organization_users',
        'app.licenses',
        'app.organization_subsidiaries',
        'app.social_profiles',
        'app.legal_form_types',
        'app.accounting_framework_types',
        'app.place_holders',
        'app.report_types',
        'app.opinion_types',
        'app.modified_opinions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FinalReports') ? [] : ['className' => 'App\Model\Table\FinalReportsTable'];
        $this->FinalReports = TableRegistry::get('FinalReports', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FinalReports);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
