<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SpaceStashHousesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SpaceStashHousesTable Test Case
 */
class SpaceStashHousesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SpaceStashHousesTable
     */
    public $SpaceStashHouses;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.space_stash_houses',
        'app.spaces',
        'app.countries',
        'app.states',
        'app.cities',
        'app.space_amenities',
        'app.space_contact_infos',
        'app.space_galleries',
        'app.space_pricings',
        'app.space_spot_letters',
        'app.space_time_days_availabilities'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SpaceStashHouses') ? [] : ['className' => 'App\Model\Table\SpaceStashHousesTable'];
        $this->SpaceStashHouses = TableRegistry::get('SpaceStashHouses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SpaceStashHouses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
