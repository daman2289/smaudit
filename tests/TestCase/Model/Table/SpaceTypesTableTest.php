<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SpaceTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SpaceTypesTable Test Case
 */
class SpaceTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SpaceTypesTable
     */
    public $SpaceTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.space_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SpaceTypes') ? [] : ['className' => 'App\Model\Table\SpaceTypesTable'];
        $this->SpaceTypes = TableRegistry::get('SpaceTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SpaceTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
