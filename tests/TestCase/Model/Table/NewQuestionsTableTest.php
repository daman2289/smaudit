<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NewQuestionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NewQuestionsTable Test Case
 */
class NewQuestionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NewQuestionsTable
     */
    public $NewQuestions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.new_questions',
        'app.new_placeholders',
        'app.new_reports',
        'app.new_placeholder_types',
        'app.new_question_types',
        'app.new_question_options'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('NewQuestions') ? [] : ['className' => 'App\Model\Table\NewQuestionsTable'];
        $this->NewQuestions = TableRegistry::get('NewQuestions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NewQuestions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
