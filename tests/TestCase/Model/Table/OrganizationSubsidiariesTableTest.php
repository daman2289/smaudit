<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrganizationSubsidiariesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrganizationSubsidiariesTable Test Case
 */
class OrganizationSubsidiariesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OrganizationSubsidiariesTable
     */
    public $OrganizationSubsidiaries;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.organization_subsidiaries',
        'app.organizations',
        'app.users',
        'app.roles',
        'app.social_profiles',
        'app.organization_companies',
        'app.organization_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OrganizationSubsidiaries') ? [] : ['className' => 'App\Model\Table\OrganizationSubsidiariesTable'];
        $this->OrganizationSubsidiaries = TableRegistry::get('OrganizationSubsidiaries', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrganizationSubsidiaries);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
