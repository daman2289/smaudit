<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReportFootersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReportFootersTable Test Case
 */
class ReportFootersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ReportFootersTable
     */
    public $ReportFooters;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.report_footers',
        'app.organizations',
        'app.users',
        'app.roles',
        'app.organization_users',
        'app.licenses',
        'app.organization_subsidiaries',
        'app.social_profiles',
        'app.organization_companies',
        'app.legal_form_types',
        'app.accounting_framework_types',
        'app.place_holders',
        'app.reports',
        'app.report_types',
        'app.opinion_types',
        'app.modified_opinions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ReportFooters') ? [] : ['className' => 'App\Model\Table\ReportFootersTable'];
        $this->ReportFooters = TableRegistry::get('ReportFooters', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ReportFooters);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
