<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrganizationCompaniesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrganizationCompaniesTable Test Case
 */
class OrganizationCompaniesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OrganizationCompaniesTable
     */
    public $OrganizationCompanies;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.organization_companies',
        'app.organizations',
        'app.users',
        'app.roles',
        'app.organization_users',
        'app.licenses',
        'app.organization_subsidiaries',
        'app.social_profiles',
        'app.legal_form_types',
        'app.accounting_framework_types',
        'app.reports'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OrganizationCompanies') ? [] : ['className' => 'App\Model\Table\OrganizationCompaniesTable'];
        $this->OrganizationCompanies = TableRegistry::get('OrganizationCompanies', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrganizationCompanies);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
