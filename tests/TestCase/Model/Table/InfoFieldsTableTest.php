<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InfoFieldsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InfoFieldsTable Test Case
 */
class InfoFieldsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InfoFieldsTable
     */
    public $InfoFields;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.info_fields',
        'app.report_headers',
        'app.organizations',
        'app.users',
        'app.roles',
        'app.organization_users',
        'app.licenses',
        'app.organization_subsidiaries',
        'app.social_profiles',
        'app.organization_companies',
        'app.legal_form_types',
        'app.accounting_framework_types',
        'app.place_holders',
        'app.reports',
        'app.report_types',
        'app.opinion_types',
        'app.modified_opinions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InfoFields') ? [] : ['className' => 'App\Model\Table\InfoFieldsTable'];
        $this->InfoFields = TableRegistry::get('InfoFields', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InfoFields);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
