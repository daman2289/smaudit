<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EmphasisOfMattersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EmphasisOfMattersTable Test Case
 */
class EmphasisOfMattersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\EmphasisOfMattersTable
     */
    public $EmphasisOfMatters;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.emphasis_of_matters',
        'app.report_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('EmphasisOfMatters') ? [] : ['className' => 'App\Model\Table\EmphasisOfMattersTable'];
        $this->EmphasisOfMatters = TableRegistry::get('EmphasisOfMatters', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->EmphasisOfMatters);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
