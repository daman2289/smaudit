<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OpinionTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OpinionTypesTable Test Case
 */
class OpinionTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OpinionTypesTable
     */
    public $OpinionTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.opinion_types',
        'app.report_types',
        'app.reports',
        'app.organization_companies',
        'app.organizations',
        'app.users',
        'app.roles',
        'app.organization_users',
        'app.licenses',
        'app.organization_subsidiaries',
        'app.social_profiles',
        'app.legal_form_types',
        'app.accounting_framework_types',
        'app.modified_opinions',
        'app.first_signature_users',
        'app.second_signature_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OpinionTypes') ? [] : ['className' => 'App\Model\Table\OpinionTypesTable'];
        $this->OpinionTypes = TableRegistry::get('OpinionTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OpinionTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
