<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PermissionAreasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PermissionAreasTable Test Case
 */
class PermissionAreasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PermissionAreasTable
     */
    public $PermissionAreas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.permission_areas',
        'app.organization_user_permissions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PermissionAreas') ? [] : ['className' => 'App\Model\Table\PermissionAreasTable'];
        $this->PermissionAreas = TableRegistry::get('PermissionAreas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PermissionAreas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
