<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SpecializesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SpecializesTable Test Case
 */
class SpecializesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SpecializesTable
     */
    public $Specializes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.specializes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Specializes') ? [] : ['className' => 'App\Model\Table\SpecializesTable'];
        $this->Specializes = TableRegistry::get('Specializes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Specializes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
