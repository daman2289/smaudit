<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LegalFormValuesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LegalFormValuesTable Test Case
 */
class LegalFormValuesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LegalFormValuesTable
     */
    public $LegalFormValues;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.legal_form_values',
        'app.legal_form_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('LegalFormValues') ? [] : ['className' => 'App\Model\Table\LegalFormValuesTable'];
        $this->LegalFormValues = TableRegistry::get('LegalFormValues', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LegalFormValues);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
