<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NewPlaceholdersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NewPlaceholdersTable Test Case
 */
class NewPlaceholdersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NewPlaceholdersTable
     */
    public $NewPlaceholders;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.new_placeholders',
        'app.new_reports',
        'app.new_placeholder_types',
        'app.new_questions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('NewPlaceholders') ? [] : ['className' => 'App\Model\Table\NewPlaceholdersTable'];
        $this->NewPlaceholders = TableRegistry::get('NewPlaceholders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NewPlaceholders);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
