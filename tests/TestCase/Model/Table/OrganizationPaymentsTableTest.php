<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrganizationPaymentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrganizationPaymentsTable Test Case
 */
class OrganizationPaymentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OrganizationPaymentsTable
     */
    public $OrganizationPayments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.organization_payments',
        'app.organization_subscription_plans',
        'app.organizations',
        'app.users',
        'app.roles',
        'app.organization_users',
        'app.licenses',
        'app.organization_subsidiaries',
        'app.social_profiles',
        'app.organization_companies',
        'app.legal_form_types',
        'app.accounting_framework_types',
        'app.place_holders',
        'app.reports',
        'app.report_types',
        'app.opinion_types',
        'app.modified_opinions',
        'app.subscription_plans',
        'app.plan_features',
        'app.subscription_plans_plan_features',
        'app.stripe_subscriptions',
        'app.organization_additional_user_subscription_plans',
        'app.additional_user_subscription_plans'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OrganizationPayments') ? [] : ['className' => 'App\Model\Table\OrganizationPaymentsTable'];
        $this->OrganizationPayments = TableRegistry::get('OrganizationPayments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrganizationPayments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
