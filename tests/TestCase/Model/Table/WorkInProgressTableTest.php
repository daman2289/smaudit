<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WorkInProgressTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WorkInProgressTable Test Case
 */
class WorkInProgressTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\WorkInProgressTable
     */
    public $WorkInProgress;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.work_in_progress'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('WorkInProgress') ? [] : ['className' => 'App\Model\Table\WorkInProgressTable'];
        $this->WorkInProgress = TableRegistry::get('WorkInProgress', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WorkInProgress);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
