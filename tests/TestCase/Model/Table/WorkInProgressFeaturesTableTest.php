<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WorkInProgressFeaturesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WorkInProgressFeaturesTable Test Case
 */
class WorkInProgressFeaturesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\WorkInProgressFeaturesTable
     */
    public $WorkInProgressFeatures;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.work_in_progress_features',
        'app.work_in_progresses'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('WorkInProgressFeatures') ? [] : ['className' => 'App\Model\Table\WorkInProgressFeaturesTable'];
        $this->WorkInProgressFeatures = TableRegistry::get('WorkInProgressFeatures', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WorkInProgressFeatures);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
